/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProcessReports;

import java.io.*;
import java.util.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.Vector;
import ProcessReports.util.*;
import ProcessReports.jdbc.*;
import ProcessReports.rndi.*;

/**
 *
 * @author Administrator
 */
public class OrderWiseExcessShortageNew extends HttpServlet {

    Common              common;
    HttpSession         session;
    Vector              VDeliveryDate, VCount, VParty, VOrderWt, VMixingWt, VMixingDate, VOrderNo, VOrderDate, VShortage, VShortOrderNo, VShortOrderNo1, VCorrectionMix, VRMixDate;
    Vector              VDOrderNo2, VPacking2, VDespatch2;
    Vector              VDOrderNo, VPacking, VDespatch;
    Vector              VGodownOrderNo, VGodownDespatch;

    Vector              VExWt, VExPer, VDueDepo, VDueMill, VDDye, VDLMix, VDRMix, VPTotal, VRSimple, VRBobbin, VRSpining, VRCone;
    Vector              VCSimplex, VCSpinning, VConeStartDt, VConeCompDt;
    Vector              VOrderNo1, VOrderDate1, VCount1, VParty1, VOrderWt1, VMixingWt1, VPacking1, VDespatch1, VExWt1, VExPer1, VDueDepo1, VDueMill1, VGodownDespatch1;
    Vector              VDDye1, VDLMix1, VDRMix1, VPTotal1, VRSimple1, VRBobbin1, VRSpining1, VRCone1, VExRemark;
    Vector              VElementAt;

    String              SStDate, SEnDate, SSelectType = "";
    String              bgColor, bgHead, bgUom, bgBody, fgHead, fgUom, fgBody;
    String              SUnit = "";
    String              SParty = "";
    String              SOrderNo = "";
    String              SCount = "";
    String              SPitch;
    FileWriter          FW;
    int                 iSNo = 0;
    int                 iStDate = 0;
    int                 iEnDate = 0;
    String              End = "";
    String              End1 = "";
    double              dOrdWt = 0;
    double              dPackWt = 0;
    double              dDespWt = 0;
    ResultSet           theResult;
    ResultSetMetaData   rsmd;
    int                 iPage = 1;
    int                 TLine = 0;
    double              dXMillWt = 0;
    double              dXMillPer = 0;
    double              dXOrderWt = 0;
    double              dXPartyWt = 0;
    double              dXPartyPer = 0;
    Vector              VRealisation, VRealisationOrder, VGroupOrder;
    ArrayList           AMultiOrderNo, AMultiMixNo, AMultiRealisation;
    String              Note1 = "";
    String              Note2 = "";
    String              Note3 = "";
    String              Note4 = "";
    String              SProcessTypeCode, SProcessTypeName;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        common  = new Common();

        bgColor = common.bgColor;
        bgHead  = common.bgHead;
        bgUom   = common.bgUom;
        bgBody  = common.bgBody;
        fgHead  = common.fgHead;
        fgUom   = common.fgUom;
        fgBody  = common.fgBody;

        try {
                        response. setContentType("text/html;charset=UTF-8");
            PrintWriter out     = response.getWriter();

            iEnDate  =common.toInt(common.pureDate((String)request.getParameter("TEnDate")));
            iStDate  =common.toInt(common.pureDate((String)request.getParameter("TStDate")));
            SSelectType     = common.parseNull((String)request.getParameter("Select"));

            SUnit    =request.getParameter("Unit");
            SProcessTypeCode    = null;
            SProcessTypeName    = null;

            StringTokenizer ST  = new StringTokenizer(request.getParameter("processType"), "#");
            while(ST.hasMoreTokens())   {
                SProcessTypeCode    = ST.nextToken();
                SProcessTypeName    = ST.nextToken();
            }

            System.out.println("1");
            initValues();
            System.out.println("2");
            MixingDetails(iEnDate);
            System.out.println("3");
            DespatchDetails(iEnDate);
            System.out.println("4");
            GodownDespatchDetails(iEnDate);
            System.out.println("5");
            getCorrectionMix();
            System.out.println("6");
            setDateVectors();
            System.out.println("7");
            setRealisation();
            System.out.println("8");
            setMultiMixRealisation();
            System.out.println("9");
            ShortageData();
            System.out.println("10");
            setHtml(out);
            System.out.println("11");
            FileWriter();
            System.out.println("12");
            out     . close();
        } catch (Exception ex) {    
            ex.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    private void  initValues()  {
        TLine       = 0;
        iPage       = 1;
        dXMillWt    = 0;
        dXMillPer   = 0;
        dXPartyWt   = 0;
        dXPartyPer  = 0;
        dOrdWt      = 0;
        dPackWt     = 0;
        dDespWt     = 0;
        iSNo        = 0;
    }

    private void setHtml(PrintWriter out) throws  ServletException  {

        SStDate            = common.parseDate(String.valueOf(iStDate));
        SEnDate            = common.parseDate(String.valueOf(iEnDate));

        out.println("<html>");
        out.println("<head>");
        out.println("<title>InvPro</title>");
        out.println("</head>");
        out.println("<body bgcolor='"+bgColor+"' text = '#0000FF'>");
        out.println("<p align='center'><font size='5'><b><u>Order Excess and  Shortage Report</u> ");
        out.println(" As On  :"+SEnDate+"</b></font></p>");
        out.println("<p><b>Process Type : "+SProcessTypeName+"</b></p>");
        out.println("<div align='center'>");
        out.println("  <table border='0' bordercolor='"+bgColor+"'>");
        out.println("    <tr>");
        out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>SL.No</b></font></td>");
        out.println("      <td  rowspan='2'align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>OrderNo</b></font></td>");
        out.println("      <td  rowspan='2'align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>OrderDate</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Count</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>NameoftheBuyer</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>OrderWeight</b></font></td>");
        out.println("      <td  colspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>MixingQuantity</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Packed Weight</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Despatched Weight</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>GodownDespatch</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>BalanceToBe DespatchFrom TUP Godown</b></font></td>");
        out.println("      <td  colspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Mill Excess Shortage</b></font></td>");
        out.println("      <td  colspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Party Excess Shortage</b></font></td>");
        out.println("      <td  colspan='3' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Yarn Realisation</b></font></td>");
        out.println("      <td  colspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>DueDate DeleayDays</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Dyeing Delay Days</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>LMix Delay Days</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>RMix Delay Days</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Total Process Days</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Days For Simplex Runout</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Bobin Rack Exhaus Days(Mill)</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Days for Spinning Runout</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>CWG Runout Delay days</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Remarks</b></font></td>");
        out.println("    </tr>");
        out.println("    <tr>");
        out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>As for Table</b></font></td>");
        out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>MixingWt</b></font></td>");
        out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b> Weight(Kgs)</b></font></td>");
        out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Percen</b></font></td>");
        out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b> Weight(Kgs)</b></font></td>");
        out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Percen</b></font></td>");
        out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Actual</b></font></td>");
        out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Mix-1</b></font></td>");
        out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Mix-2</b></font></td>");
        out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>As Per Depot</b></font></td>");
        out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>As Per Mill</b></font></td>");
        out.println("    </tr>");

        for(int i=0; i < VOrderNo.size();i++)   {
            String OrdNo           = (String)VOrderNo.elementAt(i);
            double dOrderWt        = common.toDouble((String)VOrderWt.elementAt(i));
            double dGodownWt       = getGodownDespatchWt(OrdNo);
            int    iShortageType   = common.toInt((String)VShortage.elementAt(i));
            int    iCMix           = common.toInt((String)VCorrectionMix.elementAt(i));

            double dPacking        = getPackingWt(OrdNo);
            double dBalance        = dPacking-dGodownWt;

            if(iShortageType==0 && iCMix != 1)  {
                        iSNo        = iSNo+1;
                        dOrdWt      = dOrdWt+dOrderWt;
                        dPackWt     = dPackWt+dPacking;
                        dDespWt     = dDespWt+getDespatchWt(OrdNo);

                double  dMixWt      = common.toDouble((String)VMixingWt.elementAt(i));
                double  dMix1Real   = common.toDouble(getRealisationPer(OrdNo, 1));
                double  dAsPerTable = dOrderWt*100/dMix1Real;
                double  dActual     = dPacking*100/dMixWt;
                double  dMix2Real   = common.toDouble(getRealisationPer(OrdNo, 2));

                        out.println("    <tr>");
                        out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+String.valueOf(iSNo)+"</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VOrderNo.elementAt(i)+"</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.parseDate((String)VOrderDate.elementAt(i))+"</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VCount.elementAt(i)+"</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VParty.elementAt(i)+"</font></td>");
                        out.println("      <td   align='Right' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.getRound(common.toDouble((String)VOrderWt.elementAt(i)),3)+"</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.getRound(dAsPerTable,3)+"</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.getRound(dMixWt,3)+"</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.getRound(dPacking,3)+"</font></td>");
                        out.println("      <td   align='Right' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.getRound(getDespatchWt(OrdNo),3)+"</font></td>");
                        out.println("      <td   align='Right' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.getRound(getGodownDespatchWt(OrdNo),3)+"</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.getRound(dBalance,3)+"</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VExWt.elementAt(i)+"</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VExPer.elementAt(i)+"%</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VExWt.elementAt(i)+"</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VExPer.elementAt(i)+"%</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.getRound(dActual,2)+"%</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.getRound(dMix1Real,2)+"%</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.getRound(dMix2Real,2)+"%</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VDueDepo.elementAt(i)+"</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VDueMill.elementAt(i)+"</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VDDye.elementAt(i)+"</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VDLMix.elementAt(i)+"</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VDRMix.elementAt(i)+"</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VPTotal.elementAt(i)+"</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VRSimple.elementAt(i)+"</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VRBobbin.elementAt(i)+"</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VRSpining.elementAt(i)+"</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VRCone.elementAt(i)+"</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VExRemark.elementAt(i)+"</font></td>");
                        out.println("    </tr>");
            }

            double dMWt= common.toDouble((String)VExWt.elementAt(i));
            double dMPer= common.toDouble((String)VExPer.elementAt(i));
            int  iRow =getShortageRow(OrdNo,dMWt,dMPer,dOrderWt);

            if(iRow!=-1 && iCMix !=1 )  {

                String SOrder1   = (String)VOrderNo1.elementAt(iRow);
                double dMixWt1   = common.toDouble((String)VMixingWt1.elementAt(iRow));
                double dPacking1 = common.toDouble((String)VPacking1.elementAt(iRow));
                double dGodownWt1 =getGodownDespatchWt(SOrder1);


                double dMix1Real =common.toDouble(getRealisationPer(SOrder1, 1));
                double dAsPerTable1  =dOrderWt*100/dMix1Real;
                double dActual1      = dPacking1*100/dMixWt1;
                double dBal1         = dPacking1-dGodownWt1;

                double dMix2Real =common.toDouble(getRealisationPer(SOrder1, 2));

                out.println("    <tr>");
                out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>&nbsp</font></td>");
                out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+SOrder1+"</font></td>");
                out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.parseDate((String)VOrderDate1.elementAt(iRow))+"</font></td>");
                out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VCount1.elementAt(iRow)+"</font></td>");
                out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VParty1.elementAt(iRow)+"</font></td>");
                out.println("      <td   align='Right' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.getRound(common.toDouble((String)VOrderWt1.elementAt(iRow)),3)+"</font></td>");
                out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.getRound(dAsPerTable1,3)+"</font></td>");
                out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.getRound(dMixWt1,3)+"</font></td>");
                out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.getRound(dPacking1,3)+"</font></td>");
                out.println("      <td   align='Right' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.getRound(common.toDouble((String)VDespatch1.elementAt(iRow)),3)+"</font></td>");
                out.println("      <td   align='Right' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.getRound(common.toDouble((String)VGodownDespatch1.elementAt(iRow)),3)+"</font></td>");
                out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.getRound(dBal1,3)+"</font></td>");
                out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VExWt1.elementAt(iRow)+"</font></td>");
                out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VExPer1.elementAt(iRow)+"%</font></td>");
                out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VExWt1.elementAt(iRow)+"</font></td>");
                out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VExPer1.elementAt(iRow)+"%</font></td>");
                out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.getRound(dActual1,2)+"%</font></td>");
                out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.getRound(dMix1Real,2)+"</font></td>");
                out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.getRound(dMix2Real,2)+"</font></td>");
                out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'></font>"+(String)VDueDepo1.elementAt(iRow)+"</td>");
                out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VDueMill1.elementAt(iRow)+"</font></td>");

                out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VDDye1.elementAt(iRow)+"</font></td>");
                out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VDLMix1.elementAt(iRow)+"</font></td>");
                out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VDRMix1.elementAt(iRow)+"</font></td>");
                out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VPTotal1.elementAt(iRow)+"</font></td>");
                out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VRSimple1.elementAt(iRow)+"</font></td>");
                out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VRBobbin1.elementAt(iRow)+"</font></td>");
                out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VRSpining1.elementAt(iRow)+"</font></td>");
                out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VRCone1.elementAt(iRow)+"</font></td>");
                out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>&nbsp;</font></td>");
                out.println("    </tr>");

                out.println("    <tr>");
                out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>Closing</b></font></td>");
                out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp;</font></td>");
                out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</td>");
                out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                out.println("      <td   align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>"+common.getRound(dXMillWt,3)+"</b></font></td>");
                out.println("      <td   align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>"+common.getRound(dXMillPer,2)+"</b></font></td>");
                out.println("      <td   align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>"+common.getRound(dXPartyWt,3)+"</b></font></td>");
                out.println("      <td   align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>"+common.getRound(dXPartyPer,2)+"</b></font></td>");
                out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp;</font></td>");
                out.println("    </tr>");
            }
        }
        out.println("    <tr>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>Total</b></font></td>");
        out.println("      <td   align='Right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>"+common.getRound(dOrdWt,3)+"</b></font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp;</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</td>");
        out.println("      <td   align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>"+common.getRound(dPackWt,3)+"</b></font></td>");
        out.println("      <td   align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>"+common.getRound(dDespWt,3)+"</b></font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp;</font></td>");
        out.println("    </tr>");
    }

    private void Head() {

        String  Head1   = "MCompany  :AMARJOTHI SPINNING MILL \n";
        String  Head2   = "Document :OrderWise Excess Shortage Report-As onDate  "+common.parseDate(SEnDate)+"\n";
        String  Head3   = "Unit : "+SUnit+", Process Type : "+SProcessTypeName+"\n";
        String  Head4   = "Page :"+iPage+" M";

        String  Str1    = "|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n";
        String  Str2    = "|     |           |          |     |                           |       | Mixing Qurtity |       |       |       |Bal. To| Mill  Exce/Short |Party Exce/Short  | Yarn Realisation | Due Date| Dyeg|LMix |RMix |Total| Days|Bobin| Days| CWG |                             |\n";
        String  Str3    = "|Sl.No| OrderNo   | OrderDate|Count|    Name Of the Buyer      |Order  |----------------| Packed|Desp.  | Godown|Be Desp|------------------|-------------------------------------|  As Per |Delay|Delay|Delay|Procs| For |Rack | For | R/O |      Remarks                |\n";
        String  Str4    = "|     |           |          |     |                           |Weight | As Per | Actual| Weight|Weight |Desp.  | From  |  Weight  |Percen |  Weight  |Percen | Actual  | Mixing |---------| Days|Days |Days |Days | Simp|Exhau| Spin|Delay|                             |\n";
        String  Str5    = "|     |           |          |     |                           | Kgs   | Table  |Mix.Wgt|   Kgs | Kgs   | Kgs   |TUP God|    Kgs   | +-2%  |    Kgs   | +-2%  |         |        |Depo|Mill|     |     |     |     | R/O |Days | R/O | Days|                             |\n";
        String  Str6    = "|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------";
                End     = "|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|";
                End1    = "|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n";

                Note1   = "\nFoot Note : Due Date Delay Days= Report Date - Due Date , Last Mixing Delay Days= Last Mix Dt - Order Date , Remixing Delay Days=Remixing Mix Dt-Ord ,Prcess Days = Winding Dt - Last MixingDate,";
                Note2   = "            Dyes For Simplex RunOut = Simplex RunOut - Last Mix Dt, Bobbin Rack Exhaust Days = Bobbin Rack Exhaust - Simplex RunOut Date, Days For Spinning Runout = Spinning RunOut - Bobbin Rack Exhaust Date";
                Note3   = "            CWG Runout Delay Days = CWG Date - Spinning RunOut Date";
                Note4   = "Target Days : For Simplex RunOut ----> 7 Days      For Spinning RunOut ---->  6 Days    For ConeWinding RunOut  ---->  1 Day ";

        prnWriter(Head1+Head2+Head3+Head4);
        prnWriter(Str1+Str2+Str3+Str4+Str5+Str6);
    }                                      

    private void Body() {

        for(int i=0; i < VOrderNo.size();i++)   {

            double  dOrderWt        = common.toDouble((String)VOrderWt.elementAt(i));
            String  OrdNo2          = (String)VOrderNo.elementAt(i);
            int     iShortageType   = common.toInt((String)VShortage.elementAt(i));
            int     iCMix           = common.toInt((String)VCorrectionMix.elementAt(i));
            double dGodownWt        = getGodownDespatchWt(OrdNo2);

            if(iShortageType==0 && iCMix !=1 )  {

                double  dMixWt      = common.toDouble((String)VMixingWt.elementAt(i));
                double  dPacking    = getPackingWt(OrdNo2);
                double  dBal        = dPacking-dGodownWt;
                        iSNo        = iSNo+1;

                double  dMix1Real   = common.toDouble(getRealisationPer(OrdNo2, 1));
                double  dAsPerTable = dOrderWt*100/dMix1Real;
                double  dActual     = dPacking*100/dMixWt;

                String  SNo     = "|"+common.Cad(String.valueOf(iSNo),5)+"|";
                String  OrdNo   = common.Pad((String)VOrderNo.elementAt(i),11)+"|";
                String  OrdDate = common.Pad(common.parseDate((String)VOrderDate.elementAt(i)),10)+"|";
                String  Count   = common.Cad((String)VCount.elementAt(i),5)+"|";
                String  Party   = common.Pad((String)VParty.elementAt(i),27)+"|";
                String  OrdWt   = common.Rad(common.getRound(dOrderWt,0),7)+"|";
                String  AsPer   = common.Pad(common.getRound(dAsPerTable,2),8)+"|";
                String  MixWt   = common.Rad(common.getRound(dMixWt,0),7)+"|";
                String  Pack    = common.Rad(common.getRound(dPacking,0),7)+"|";
                String  Desp    = common.Rad(common.getRound(getDespatchWt(OrdNo2),0),7)+"|";
                String  GDesp   = common.Rad(common.getRound(getGodownDespatchWt(OrdNo2),0),7)+"|";
                String  Bal1    = common.Rad(common.getRound(dBal,0),7)+"|";
                String  ExWt    = common.Rad((String)VExWt.elementAt(i),10)+"|";
                String  ExPer   = common.Rad(common.getRound((String)VExPer.elementAt(i),2)+"%",7)+"|";
                String  DueDepo = common.Cad((String)VDueDepo.elementAt(i),4)+"|";
                String  DueMill = common.Cad((String)VDueMill.elementAt(i),4)+"|";
                String  DDye    = common.Pad((String)VDDye.elementAt(i),5)+"|";
                String  DLMix   = common.Pad((String)VDLMix.elementAt(i),5)+"|";
                String  DRmix   = common.Pad((String)VDRMix.elementAt(i),5)+"|";
                String  PTotal  = common.Pad((String)VPTotal.elementAt(i),5)+"|";
                String  RSim    = common.Pad((String)VRSimple.elementAt(i),5)+"|";
                String  RBob    = common.Pad((String)VRBobbin.elementAt(i),5)+"|";
                String  RSpin   = common.Pad((String)VRSpining.elementAt(i),5)+"|";
                String  RCone   = common.Pad((String)VRCone.elementAt(i),5)+"|";
                String  Rem     = common.Pad((String)VExRemark.elementAt(i),29)+"|";
                String  RActual = common.Pad(common.getRound(dActual,3)+"%",9)+"|";
                String  RMixing = common.Pad(common.getRound(dMix1Real,2)+"%",8)+"|"; 

                prnWriter(SNo+OrdNo+OrdDate+Count+Party+OrdWt+AsPer+MixWt+Pack+Desp+GDesp+Bal1+ExWt+ExPer+ExWt+ExPer+RActual+RMixing+DueDepo+DueMill+DDye+DLMix+DRmix+PTotal+RSim+RBob+RSpin+RCone+Rem);
            }
            String SEmptyRow      ="|"+common.Space(5) +"|"+
                                    common.Space(11)+"|"+
                                    common.Space(10)+"|"+
                                    common.Space(5) +"|"+
                                    common.Space(27)+"|"+
                                    common.Space(7)+"|"+
                                    common.Space(8) +"|"+
                                    common.Space(7)+"|"+
                                    common.Space(7)+"|"+
                                    common.Space(7)+"|"+
                                    common.Space(7)+"|"+
                                    common.Space(7)+"|"+
                                    common.Space(10)+"|"+
                                    common.Space(7) +"|"+
                                    common.Space(10)+"|"+
                                    common.Space(7) +"|"+
                                    common.Space(9) +"|"+
                                    common.Space(8) +"|"+
                                    common.Space(4) +"|"+
                                    common.Space(4) +"|"+
                                    common.Space(5) +"|"+
                                    common.Space(5) +"|"+
                                    common.Space(5) +"|"+
                                    common.Space(5) +"|"+
                                    common.Space(5) +"|"+
                                    common.Space(5) +"|"+
                                    common.Space(5) +"|"+
                                    common.Space(5) +"|"+
                                    common.Space(29)+"|";

                double dMWt= common.toDouble((String)VExWt.elementAt(i));
                double dMPer= common.toDouble((String)VExPer.elementAt(i));
                int iRow =getShortageRow(OrdNo2,dMWt,dMPer,dOrderWt);

                if(iRow!=-1 && iCMix !=1 )  {

                    double dMixWt1     = common.toDouble((String)VMixingWt1.elementAt(iRow));
                    double dPacking1   = common.toDouble((String)VPacking1.elementAt(iRow));

                    String SOrder1     = (String)VOrderNo1.elementAt(iRow);
                    double dMix1Real   = common.toDouble(getRealisationPer(SOrder1, 1));
                    double dAsPerTable1= dOrderWt*100/dMix1Real;
                    double dActual1    = dPacking1*100/dMixWt1;

                    String SNo1         = "|"+common.Cad("",5)+"|";
                    String OrdNo1       = common.Pad(SOrder1,11)+"|";
                    String OrdDate1     = common.Pad(common.parseDate((String)VOrderDate1.elementAt(iRow)),10)+"|";
                    String Count1       = common.Cad((String)VCount1.elementAt(iRow),5)+"|";
                    String Party1       = common.Pad((String)VParty1.elementAt(iRow),27)+"|";
                    String OrdWt1       = common.Rad(common.getRound(common.toDouble((String)VOrderWt1.elementAt(iRow)),0),7)+"|";
                    String AsPer1       = common.Pad(common.getRound(dAsPerTable1,2),8)+"|";
                    String MixWt1       = common.Rad(common.getRound(dMixWt1,0),7)+"|";
                    String Pack1        = common.Rad(common.getRound(dPacking1,0),7)+"|";
                    String Desp1        = common.Rad(common.getRound(common.toDouble((String)VDespatch1.elementAt(iRow)),0),7)+"|";
                    String ExWt1        = common.Rad((String)VExWt1.elementAt(iRow),10)+"|";
                    String ExPer1       = common.Rad(common.getRound((String)VExPer1.elementAt(iRow),2)+"%",7)+"|";
                    String DueDepo1     = common.Cad((String)VDueDepo1.elementAt(iRow),4)+"|";
                    String DueMill1     = common.Cad((String)VDueMill1.elementAt(iRow),4)+"|";
                    String DDye1        = common.Pad((String)VDDye1.elementAt(iRow),5)+"|";
                    String DLMix1       = common.Pad((String)VDLMix1.elementAt(iRow),5)+"|";
                    String DRmix1       = common.Pad((String)VDRMix1.elementAt(iRow),5)+"|";
                    String PTotal1      = common.Pad((String)VPTotal1.elementAt(iRow),5)+"|";
                    String RSim1        = common.Pad((String)VRSimple1.elementAt(iRow),5)+"|";
                    String RBob1        = common.Pad((String)VRBobbin1.elementAt(iRow),5)+"|";
                    String RSpin1       = common.Pad((String)VRSpining1.elementAt(iRow),5)+"|";
                    String RCone1       = common.Pad((String)VRCone1.elementAt(iRow),5)+"|";
                    String Rem1         = common.Pad("",29)+"|";
                    String RActual1     = common.Pad(common.getRound(dActual1,2)+"%",9)+"|";
                    String RMixing1     = common.Pad(common.getRound(dMix1Real,2)+"%",8)+"|"; 

                    String STShortage   = "|"+common.Space(5) +"|"+
                                              common.Space(11)+"|"+
                                              common.Space(10)+"|"+
                                              common.Space(5) +"|"+
                                              common.Cad("Closing",27)+"|"+
                                              common.Space(7)+"|"+
                                              common.Space(8) +"|"+
                                              common.Space(7)+"|"+
                                              common.Space(7)+"|"+
                                              common.Space(7)+"|"+
                                              common.Space(7)+"|"+
                                              common.Space(7)+"|"+
                                              common.Rad(common.getRound(dXMillWt,3),10)+"|"+
                                              common.Rad(common.getRound(dXMillPer,2),7)+"|"+
                                              common.Rad(common.getRound(dXPartyWt,3),10)+"|"+
                                              common.Rad(common.getRound(dXPartyPer,2),7)+"|"+
                                              common.Space(9) +"|"+
                                              common.Space(8) +"|"+
                                              common.Space(4) +"|"+
                                              common.Space(4) +"|"+
                                              common.Space(5) +"|"+
                                              common.Space(5) +"|"+
                                              common.Space(5) +"|"+
                                              common.Space(5) +"|"+
                                              common.Space(5) +"|"+
                                              common.Space(5) +"|"+
                                              common.Space(5) +"|"+
                                              common.Space(5) +"|"+
                                              common.Space(29)+"|";

                prnWriter(SNo1+OrdNo1+OrdDate1+Count1+Party1+OrdWt1+AsPer1+MixWt1+Pack1+Desp1+common.Space(7)+"|"+common.Space(7)+"|"+ExWt1+ExPer1+ExWt1+ExPer1+RActual1+RMixing1+DueDepo1+DueMill1+DDye1+DLMix1+DRmix1+PTotal1+RSim1+RBob1+RSpin1+RCone1+Rem1);
                prnWriter(STShortage);
            }
            prnWriter(SEmptyRow);
        }
        prnWriter(End);
        prnWriter("|"+common.Cad("Total ==>",62)+"|"+common.Rad(common.getRound(dOrdWt,0),7)+"|"+common.Pad("",8)+"|"+common.Pad("",7)+"|"+common.Rad(common.getRound(dPackWt,0),7)+"|"+common.Rad(common.getRound(dDespWt,0),7)+"|"+common.Pad("",10));
        prnWriter(End1);
        prnWriter(Note1);
        prnWriter(Note2);
        prnWriter(Note3);
        prnWriter(Note4);
        prnWriter("");
    }

    public void MixingDetails(int OnDate)   {
        VOrderNo       = new Vector();
        VOrderDate     = new Vector();
        VDeliveryDate  = new Vector();
        VCount         = new Vector();
        VParty         = new Vector();
        VOrderWt       = new Vector();
        VMixingWt      = new Vector();
        VMixingDate    = new Vector();
        VShortage      = new Vector();
        VShortOrderNo  = new Vector();
        VCSimplex      = new Vector();
        VCSpinning     = new Vector();
        VCorrectionMix = new Vector();
        VConeStartDt   = new Vector();
        VConeCompDt    = new Vector();
        VRMixDate      = new Vector();
        VExRemark      = new Vector();

        try {
            String  QS  =   " Select PackingStatus.OrderNo,OrderDate,DeliveryDate,YarnCount.CountName,"+
                            " PartyMaster.PartyName,RegularOrder.Weight,Rmixir.MixingWeight,Rmixir.MixingDate,RegularOrder.Shortage,RegularOrder.RefOrderNo,"+
                            " SimplexStatus.CompDate,SpinningStatus.CompDate,Rmixir.CorrectionMixing,ConeWindingStatus.StartDate,ConeWindingStatus.CompDate,RegularOrder.ProcessRemark From PackingStatus"+
                            " Inner Join RegularOrder On RegularOrder.ROrderNo = PackingStatus.OrderNo"+
                            " Inner Join Unit On Unit.UnitCode  = RegularOrder.UnitCode"+
                            " Inner Join YarnCount On  YarnCount.CountCode = RegularOrder.CountCode"+
                            " Inner Join PartyMaster On  Partymaster.partyCode = RegularOrder.partyCode"+
                            " Inner Join ProcessingType on ProcessingType.ProcessCode=RegularOrder.ProcessTypeCode "+
                            " Left  Join RMixir On  Rmixir.OrdNo = PackingStatus.OrderNo"+
                            " Left Join ConeWindingStatus On ConeWindingStatus.OrderNo = PackingStatus.OrderNo"+
                            " Left Join   SimplexStatus On SimplexStatus.OrderNo = ConeWindingStatus.OrderNo "+
                            " Left Join   SpinningStatus On SpinningStatus.OrderNo = ConeWindingStatus.OrderNo ";

            if(SUnit.equals("A_Unit") || SUnit.equals("B_Unit")){
                QS = QS + " Where  Packingstatus.Status=1 and PackingStatus.OrderStatus=0 and  PackingStatus.compDate < ="+iEnDate +" ";
            }   else    {
                QS = QS + " Where  Packingstatus.Status=1 and PackingStatus.OrderStatus=0 and  PackingStatus.compDate < ="+iEnDate +" and SimplexStatus.curStatus=1 ";
            }

            if(!SUnit.equals("All"))    {
                QS = QS + " and Unit.UnitName='"+SUnit+"'";
            }
            if(!SProcessTypeCode.equals("All")) {
                if(SProcessTypeCode.equals("BR-C Orders"))  {
                    QS += " and ProcessingType.OEStatus = 1 ";
                }   else if(SProcessTypeCode.equals("Other than BR-C Orders"))  {
                    QS += " and ProcessingType.OEStatus = 0 ";
                }   else    {
                    QS += " and RegularOrder.ProcessTypeCode = "+SProcessTypeCode;
                }
            }
            if(SSelectType.equalsIgnoreCase("Depo"))    {
                QS=QS+" and regularorder.rorderno like 'DM%'";
            }
            if(SSelectType.equalsIgnoreCase("Party"))   {
                QS=QS+" and regularorder.rorderno not like 'DM%'";
            }
            QS = QS + " Order By OrderDate,PackingStatus.OrderNo";

            JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
            Connection     conn = jdbc.getConnection();

            Statement stat      = conn.createStatement();
            ResultSet res       = stat.executeQuery(QS);

            while(res.next()) {
                VOrderNo        . addElement(res.getString(1));
                VOrderDate      . addElement(res.getString(2));
                VDeliveryDate   . addElement(res.getString(3));
                VCount          . addElement(res.getString(4));
                VParty          . addElement(res.getString(5));
                VOrderWt        . addElement(res.getString(6));
                VMixingWt       . addElement(res.getString(7));
                VMixingDate     . addElement(res.getString(8));
                VShortage       . addElement(res.getString(9));
                VShortOrderNo   . addElement(res.getString(10));
                VCSimplex       . addElement(res.getString(11));
                VCSpinning      . addElement(res.getString(12));
                VCorrectionMix  . addElement(res.getString(13));
                VConeStartDt    . addElement(res.getString(14));
                VConeCompDt     . addElement(res.getString(15));
                VExRemark       . addElement(res.getString(16));
                VRMixDate       . addElement(String.valueOf(0));
            }
            res.close();
            stat.close();
        }   catch(Exception e)  {
            System.out.println(e);
        }
    }

    public void DespatchDetails(int OnDate) {

        VDOrderNo       = new Vector();
        VPacking        = new Vector();
        VDespatch       = new Vector();

        String QS=  " Select OrderNo,Sum(Packing),Sum(Despatch) From"+
                    " ("+
                    " Select OrderNo,Sum(Balepacking.NetWeight) as Packing , 0 As Despatch "+
                    " From PackingStatus"+
                    " Inner Join RegularOrder On RegularOrder.ROrderNo = PackingStatus.OrderNo"+
                    " Inner Join Unit On Unit.UnitCode  = RegularOrder.UnitCode"+
                    " Inner Join ProcessingType on ProcessingType.ProcessCode=RegularOrder.ProcessTypeCode "+
                    " Left Join Balepacking On Balepacking.Orderid = RegularOrder.Orderid"+
                    " Where PackingStatus.Status=1  and PackingStatus.OrderStatus=0 and BalePacking.Flag<>1 ";
        if(!SUnit.equals("All"))    {
            QS = QS + " and Unit.UnitName='"+SUnit+"'";
        }
        if(!SProcessTypeCode.equals("All")) {
            if(SProcessTypeCode.equals("BR-C Orders")){
                QS += " and ProcessingType.OEStatus = 1 ";
            }else if(SProcessTypeCode.equals("Other than BR-C Orders")){
                QS += " and ProcessingType.OEStatus = 0 ";
            }   else{
                QS += " and RegularOrder.ProcessTypeCode = "+SProcessTypeCode;
            }
        }
        QS  = QS +  " group by OrderNo"+
                    " Union All"+
                    " select OrderNo, 0 as Packing, Sum(Balepacking.NetWeight)  As Despatch from PackingStatus "+
                    " Inner Join RegularOrder On RegularOrder.ROrderNo = PackingStatus.OrderNo"+
                    " Inner Join Unit On Unit.UnitCode  = RegularOrder.UnitCode"+
                    " Inner Join ProcessingType on ProcessingType.ProcessCode=RegularOrder.ProcessTypeCode "+
                    " Left Join Balepacking On Balepacking.Orderid = RegularOrder.Orderid"+
                    " Where BalePacking.DespStatus=1 And Packingstatus.Status=1 and BalePAcking.Flag<>1"  ;
        if(!SUnit.equals("All"))    {
            QS = QS + " and Unit.UnitName='"+SUnit+"'";
        }
        if(!SProcessTypeCode.equals("All")) {
            if(SProcessTypeCode.equals("BR-C Orders"))  {
                QS += " and ProcessingType.OEStatus = 1 ";
            }   else if(SProcessTypeCode.equals("Other than BR-C Orders"))  {
                QS += " and ProcessingType.OEStatus = 0 ";
            }   else    {
                QS += " and RegularOrder.ProcessTypeCode = "+SProcessTypeCode;
            }
        }
        QS = QS + " group by OrderNo"+
        " )"+
        " Group By OrderNo"+
        " Order By OrderNo";
        try {
            JDBCConnection  jdbc    = JDBCConnection.getJDBCConnection();
            Connection      conn    = jdbc.getConnection();
            Statement       stat    = conn.createStatement();
            ResultSet       res     = stat.executeQuery(QS);

            while(res.next())   {
                VDOrderNo   . addElement(res.getString(1));
                VPacking    . addElement(res.getString(2));
                VDespatch   . addElement(res.getString(3));
            }
            res . close();
            stat. close();
        }catch(Exception e) {
            System.out.println(e);
        }
    }
     
    public void GodownDespatchDetails(int OnDate)   {

        VGodownOrderNo       = new Vector();
        VGodownDespatch      = new Vector();

        String QS=  " Select OrderNo,Sum(Weight) from ( "+
                    " Select Invoice.OrderNo,Sum(DespatchWeight) as Weight From Invoice "+
                    " Inner Join PackingStatus On PackingStatus.OrderNo=Invoice.OrderNo "+
                    " Where  PackingStatus.Status=1 "+
                    " Group By Invoice.OrderNo "+
                    " Union All "+
                    " Select ROlexIssue.OrderNo,Sum(Weight) From ROlexIssue "+
                    " Inner Join PackingStatus On PackingStatus.OrderNo=RolexIssue.OrderNo "+
                    " Where PackingStatus.Status=1 "+
                    " Group By RolexIssue.OrderNo )"+
                    " Group By OrderNo ";

        try {
            JDBCConnection  jdbc = JDBCConnection.getJDBCConnection();
            Connection      conn = jdbc.getConnection();
            Statement       stat = conn.createStatement();
            ResultSet       res  = stat.executeQuery(QS);

            while(res.next()) {
                VGodownOrderNo.addElement(res.getString(1));
                VGodownDespatch.addElement(res.getString(2));
            }
            res.close();
            stat.close();
        }   catch(Exception e)  {
            System.out.println(e);
        }
    }

    private void setDateVectors()   {
        VExWt     = new Vector();
        VExPer    = new Vector();
        VDueDepo  = new Vector();
        VDueMill  = new Vector();
        VDDye     = new Vector();
        VDLMix    = new Vector();
        VDRMix    = new Vector();
        VPTotal   = new Vector();
        VRSimple  = new Vector();
        VRBobbin  = new Vector();
        VRSpining = new Vector();
        VRCone    = new Vector();

        for(int i=0;i<VOrderNo.size();i++)  {
            try {
                double  TOrderWt    = 0;
                double  TPack       = 0;
                double  TDesp       = 0;

                String  OrdNo       = (String)VOrderNo.elementAt(i);
                double  dOrderWt    = common.toDouble((String)VOrderWt.elementAt(i));
                double  dPackWt     = getPackingWt(OrdNo);
                int     iConeDt     = common.toInt((String)VConeStartDt.elementAt(i));
                int     iOrderDate  = common.toInt((String)VOrderDate.elementAt(i));
                int     iDueDate    = common.toInt((String)VDeliveryDate.elementAt(i));
                int     iMixDate    = common.toInt((String)VMixingDate.elementAt(i));
                int     iCSimplex   = common.toInt((String)VCSimplex.elementAt(i));
                int     iCSpinning  = common.toInt((String)VCSpinning.elementAt(i));
                int     iConeCompDt = common.toInt((String)VConeCompDt.elementAt(i));

                TOrderWt=TOrderWt+dOrderWt;

                int     iRepDate    = common.toInt(common.getServerDate());
                double  dExWt       = dOrderWt-dPackWt;
                double  dExPer      = (dExWt*100)/dOrderWt;
                int     iDyeDt      = 0;
                int     iRMixDt     = 0;
                int     iBobinDt    = 0;
                int     iSpinning   = 0;
                int     iDueDep     = common.getDateDiff(iRepDate,iDueDate);;
                int     iDueMill    = common.getDateDiff(iRepDate,iOrderDate);
                int     iDLMix      = common.getDateDiff(iMixDate,iOrderDate);
                int     iProDate    = common.getDateDiff(iConeCompDt,iMixDate);
                int     iCSimplex1  = common.getDateDiff(iCSimplex,iMixDate);
                int     iCSpinning1 = common.getDateDiff(iCSpinning,iMixDate);
                int     iCWG        = common.getDateDiff(iConeCompDt,iCSpinning);

                VExWt       . addElement(common.getRound(dExWt,3));
                VExPer      . addElement(common.getRound(dExPer,2));
                VDueDepo    . addElement(Integer.toString(iDueDep));
                VDueMill    . addElement(Integer.toString(iDueMill));
                VDDye       . addElement(Integer.toString(iDyeDt));
                VDLMix      . addElement(Integer.toString(iDLMix));
                VDRMix      . addElement(Integer.toString(iRMixDt));
                VPTotal     . addElement(Integer.toString(iProDate));
                VRSimple    . addElement(Integer.toString(iCSimplex1));
                VRBobbin    . addElement(Integer.toString(iBobinDt));
                VRSpining   . addElement(Integer.toString(iSpinning));
                VRCone      . addElement(Integer.toString(iCWG));
            }
            catch(Exception e)  {
              System.out.println(e);
            }
        }
    }

    private void FileWriter()   {
        try {
            iSNo=0;

            //  FW = new FileWriter("//"+SCRIPTHOST+"/d/"+SCRIPTFOLDER+"/src/Temp/ExcessShortageAsOn.prn");
//            FW = new FileWriter("d:/ExcessShortageAsOn.prn");
                  FW = new FileWriter("/software/MixPrint/ExcessShortageAsOn.prn");
            Head();
            Body();
            FW.close();
        }   catch(Exception e)  {
            System.out.println(e);
        }
    }
    private void prnWriter(String Str)  {
        try {
            TLine = TLine+1;
            if(TLine<53)    {
                    FW.write(Str+"\n");
            }else   {
                iPage=iPage+1;
                TLine=0;
                prnWriter(Str);
                FW.write(End);
                prnWriter(Note1);
                prnWriter(Note2);
                prnWriter(Note3);
                prnWriter(Note4);
                Head();
            }
        }
        catch(Exception e)  {
            System.out.println(e);
        }
    }
    
    private void ShortageData() {
        
        VOrderNo1           = new Vector();
        VOrderDate1         = new Vector();
        VCount1             = new Vector();
        VParty1             = new Vector();
        VOrderWt1           = new Vector();
        VMixingWt1          = new Vector();
        VPacking1           = new Vector();
        VDespatch1          = new Vector();
        VGodownDespatch1    = new Vector();
        VExWt1              = new Vector();
        VExPer1             = new Vector();
        VDueDepo1           = new Vector();
        VDueMill1           = new Vector();
        VDDye1              = new Vector();
        VDLMix1             = new Vector();
        VDRMix1             = new Vector();
        VPTotal1            = new Vector();
        VRSimple1           = new Vector();
        VRBobbin1           = new Vector();
        VRSpining1          = new Vector();
        VRCone1             = new Vector();
        VShortOrderNo1      = new Vector();
        VElementAt          =  new Vector();

        for(int i= 0; i<VOrderNo.size();i++)    {
            int iShortage          = common.toInt((String)VShortage.elementAt(i));
            if(iShortage!=0)    {

                String  OrdNo           = (String)VOrderNo.elementAt(i);
                        VOrderNo1       . addElement(OrdNo);
                        VOrderDate1     . addElement((String)VOrderDate.elementAt(i));
                        VCount1         . addElement((String)VCount.elementAt(i));
                        VParty1         . addElement((String)VParty.elementAt(i));
                        VOrderWt1       . addElement((String)VOrderWt.elementAt(i));
                        VMixingWt1      . addElement((String)VMixingWt.elementAt(i));
                        VPacking1       . addElement(common.getRound(getPackingWt(OrdNo),3));
                        VDespatch1      . addElement(common.getRound(getDespatchWt(OrdNo),3));
                        VGodownDespatch1. addElement(common.getRound(getGodownDespatchWt(OrdNo),3)); 
                        VExWt1          . addElement((String)VExWt.elementAt(i));
                        VExPer1         . addElement((String)VExPer.elementAt(i));
                        VDueDepo1       . addElement((String)VDueDepo.elementAt(i));
                        VDueMill1       . addElement((String)VDueMill.elementAt(i));
                        VDDye1          . addElement((String)VDDye.elementAt(i));
                        VDLMix1         . addElement((String)VDLMix.elementAt(i));
                        VDRMix1         . addElement((String)VDRMix.elementAt(i));
                        VPTotal1        . addElement((String)VPTotal.elementAt(i));
                        VRSimple1       . addElement((String)VRSimple.elementAt(i));
                        VRBobbin1       . addElement((String)VRBobbin.elementAt(i));
                        VRSpining1      . addElement((String)VRSpining.elementAt(i));
                        VRCone1         . addElement((String)VRCone.elementAt(i));
                        VShortOrderNo1  . addElement((String)VShortOrderNo.elementAt(i));
            }
        }
    }
     
    private int getShortageRow(String OrdNo,double MillExWt,double MillPer,double dOrderWt)    {
        int iRow=-1;
        for(int i =0 ; i<VOrderNo1.size();i++){
            String RefNo =(String)VShortOrderNo1.elementAt(i);

            if(OrdNo.equals(RefNo)){
                double dxmillwt = common.toDouble((String)VExWt1.elementAt(i));
                double dxmillper= common.toDouble((String)VExPer1.elementAt(i));
                double dxorderwt= common.toDouble((String)VOrderWt1.elementAt(i));

                dXMillWt=MillExWt+dxmillwt;
                dXOrderWt =dOrderWt+dxorderwt;
                dXMillPer=dXMillWt*100/dXOrderWt;

                dXPartyWt  = dxmillwt;
                dXPartyPer = dxmillwt*100/dOrderWt;
                iRow=i;
            }
        }
        return iRow;
    }
     
    private void getCorrectionMix() {
      
        int     iCorrectionMix  = 0;
        double  dCMixWt         = 0;
        String  CDate           = "";
        String  COrderNo        = "";

        try {
            for(int i=0; i<VOrderNo.size();i++) {
                iCorrectionMix = common.toInt((String)VCorrectionMix.elementAt(i));
                if(iCorrectionMix==1)   {
                    COrderNo = (String)VOrderNo.elementAt(i);
                    dCMixWt  = common.toDouble((String)VMixingWt.elementAt(i));
                    CDate    = (String)VMixingDate.elementAt(i);

                    for(int j=0;j<VOrderNo.size();j++)  {
                        SOrderNo = (String)VOrderNo.elementAt(j);
                        int iStatus = common.toInt((String)VCorrectionMix.elementAt(j));
                        if(SOrderNo.equals(COrderNo) && iStatus!=1) {
                            double  dWt         = common.toDouble((String)VMixingWt.elementAt(j))+dCMixWt;
                                    VRMixDate   . set(j,CDate);
                                    VMixingWt   . set(j,common.getRound(dWt,3));
                        }
                    }
                }
            }
        }   catch(Exception e)  {
            System.out.println(e);
        }
    }
  
    private double getPackingWt(String OrdNo)   {
        for(int i=0;i<VDOrderNo.size();i++) {
            String SOrdNo =(String)VDOrderNo.elementAt(i);
            if(SOrdNo.equals(OrdNo))    {
                return common.toDouble((String)VPacking.elementAt(i));
            }
        }
        return 0;
    }
  
    private double getDespatchWt(String OrdNo)  {
        for(int i=0;i<VDOrderNo.size();i++) {
            String SOrdNo =(String)VDOrderNo.elementAt(i);
            if(SOrdNo.equals(OrdNo))    {
                return common.toDouble((String)VDespatch.elementAt(i));
            }
        }
        return 0;
    }
    private double getGodownDespatchWt(String OrdNo)    {
        for(int i=0;i<VGodownOrderNo.size();i++)    {
            String SOrdNo =(String)VGodownOrderNo.elementAt(i);
            if(SOrdNo.equals(OrdNo))    {
                return common.toDouble((String)VGodownDespatch.elementAt(i));
            }
        }
        return 0;
    }

    private void setRealisation()   {

        VRealisation     = new Vector();
        VRealisationOrder= new Vector();
        VGroupOrder      = new Vector();

        String  QS =    " Select  PackingStatus.OrderNo,Realisation_Per,OrderGroupMaster.OrderNo from PackingStatus"+
                        " Inner Join RegularOrder On RegularOrder.ROrderNo = packingStatus.OrderNo"+
                        " Left Join SampleToRegular On SampleToRegular.RegularOrderNo = RegularOrder.ROrderNo"+
                        " Left Join OrderGroupDetails On OrderGroupDetails.OrderNo    = RegularOrder.ROrderNo"+
                        " Left Join OrderGroupMaster On OrderGroupMaster.id           = OrderGroupDetails.Groupid"+
                        " Inner Join Unit On Unit.UnitCode = RegularOrder.Unitcode "+
                        " Where PackingStatus.Status=1 and PackingStatus.OrderStatus=0" ;

        if(!SUnit.equals("All")){
            QS = QS+"and Unit.UnitName ='"+SUnit+"'";
        }

        try
        {

            JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
            Connection     conn = jdbc.getConnection();
            Statement stat      = conn.createStatement();
            ResultSet res       = stat.executeQuery(QS);

            while(res.next())   {
                VRealisationOrder.addElement(res.getString(1));
                VRealisation.addElement(res.getString(2));
                VGroupOrder.addElement(res.getString(3));
            }
            res.close();
            stat.close();
        }   catch(Exception e)  {
            System.out.println(e);
        }
    }

    private String getRealisationPer(String ROrdNo, int iMultiMixNo)    {

        if(AMultiOrderNo.indexOf(ROrdNo) != -1) {
            for(int i=0; i<AMultiOrderNo.size(); i++)   {
                if(ROrdNo.equals(common.parseNull((String)AMultiOrderNo.get(i))) && iMultiMixNo == common.toInt((String)AMultiMixNo.get(i)))    {
                    return common.parseNull((String)AMultiRealisation.get(i));
                }
            }
        }else{
            if(iMultiMixNo == 2)    {
                return "";
            }
            for(int i=0;i<VRealisationOrder.size();i++) {
                String SPer    = "";
                String OrdNo   = (String)VRealisationOrder.elementAt(i);

                if(OrdNo.equals(ROrdNo))    {
                    SPer      = common.parseNull((String)VRealisation.elementAt(i));
                    if(!SPer.equals(""))    {
                        return SPer;
                    }else{
                        return getRealisationPer((String)VGroupOrder.elementAt(i), iMultiMixNo);
                    }
                }
            }
        }
        return "";
    }

    private void setMultiMixRealisation()   {
        AMultiOrderNo       = null;
        AMultiMixNo         = null;
        AMultiRealisation   = null;

        AMultiOrderNo       = new ArrayList();
        AMultiMixNo         = new ArrayList();
        AMultiRealisation   = new ArrayList();

        AMultiOrderNo       . clear();
        AMultiMixNo         . clear();
        AMultiRealisation   . clear();

        try {
            StringBuffer    SB  = null;
                            SB  = new StringBuffer();

                            SB  . append(" Select OrderMultiMixRealisation.OrderNo, OrderMultiMixRealisation.MultiMixNo, ");
                            SB  . append(" OrderMultiMixRealisation.Realisation ");
                            SB  . append(" from OrderMultiMixRealisation ");
                            SB  . append(" Inner Join PackingStatus on PackingStatus.OrderNo = OrderMultiMixRealisation.OrderNo ");
                            SB  . append(" where nvl(PackingStatus.Status, 0) = 1 ");
                            SB  . append(" and nvl(PackingStatus.OrderStatus, 0) = 0 ");

            JDBCConnection      jdbc    = JDBCConnection.getJDBCConnection();
            Connection          theConn = jdbc.getConnection();
            PreparedStatement   thePS   = theConn.prepareStatement(SB.toString());
            ResultSet           rs      = thePS.executeQuery();

            while(rs.next())    {
                AMultiOrderNo       . add(common.parseNull(rs.getString(1)));
                AMultiMixNo         . add(common.parseNull(rs.getString(2)));
                AMultiRealisation   . add(common.parseNull(rs.getString(3)));
            }
            rs                       . close();
            thePS                    . close();
        }   catch(Exception ex) {
            ex.printStackTrace();
        }
    }
}
