/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProcessReports.regularorder;

/**
 *
 * @author Administrator
 */

import java.util.Vector;

public interface RMSMRetriveInf {

    public Vector  getMainVector();
    public Vector  getSamplesVector();
    public Vector  getDyeingVector();
    public Vector  getGIDetails();
    public Vector  getPartyOrderVector();
    public Vector  getReceiptDetails();
    public Vector  getNewGIDetails();

    public String  getUptoDateValues();
}
