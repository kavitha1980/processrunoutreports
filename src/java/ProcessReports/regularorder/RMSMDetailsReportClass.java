/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProcessReports.regularorder;

/**
 *
 * @author Administrator
 */
public class RMSMDetailsReportClass {
    String    SRegularOrderNo,SSampleOrderNo,SOrderDate,SApprovedDate;
    String    SPartyRefNo,SMillRefNo,SApprovedBy,SPartyName,SDelayReason,SCountName,SReMixingDate,SDyeingOrderNo,SFormName;
    double    dWeight;

    String    SOrderNo,SFibreCode;

    String    STSOrderNo;
    int       iNoOfSamples;

    String    SGIDate;

    public RMSMDetailsReportClass() {

    }

     //For Dyeing Vector

    public RMSMDetailsReportClass(String SOrderNo,String SFibreCode){
        this . SOrderNo          = SOrderNo;
        this . SFibreCode        = SFibreCode;
    }

     //For Sample Vector

    public RMSMDetailsReportClass(String STSOrderNo,int iNoOfSamples)   {
        this . STSOrderNo        = STSOrderNo;
        this . iNoOfSamples      = iNoOfSamples;
    }

     //For Main Vector

    public void setRegularOrderNo(String SRegularOrderNo)   {this.SRegularOrderNo    = SRegularOrderNo;}
    public void setSampleOrderNo(String SSampleOrderNo)     {this.SSampleOrderNo     = SSampleOrderNo;}
    public void setOrderDate(String SOrderDate)             {this.SOrderDate         = SOrderDate;}
    public void setApprovedDate(String SApprovedDate)       {this.SApprovedDate      = SApprovedDate;}
    public void setPartyRefNo(String SPartyRefNo)           {this.SPartyRefNo        = SPartyRefNo;}
    public void setMillRefNo(String SMillRefNo)             {this.SMillRefNo         = SMillRefNo;}
    public void setApprovedBy(String SApprovedBy)           {this.SApprovedBy        = SApprovedBy;}
    public void setPartyName(String SPartyName)             {this.SPartyName         = SPartyName;}
    public void setDelayReason(String SDelayReason)         {this.SDelayReason       = SDelayReason;}
    public void setCountName(String SCountName)             {this.SCountName         = SCountName;}
    public void setWeight(double dWeight)                   {this.dWeight            = dWeight;}
    public void setReMixingDate(String SReMixingDate)       {this.SReMixingDate      = SReMixingDate;}
    public void setDyeingOrderNo(String SDyeingOrderNo)     {this.SDyeingOrderNo     = SDyeingOrderNo;}
    public void setFormName(String SFormName)               {this.SFormName     = SFormName;}

    public String getRegularOrderNo()                       {return this.SRegularOrderNo;}
    public String getSampleOrderNo()                        {return this.SSampleOrderNo;}
    public String getOrderDate()                            {return this.SOrderDate;}
    public String getApprovedDate()                         {return this.SApprovedDate;}
    public String getPartyRefNo()                           {return this.SPartyRefNo;}
    public String getMillRefNo()                            {return this.SMillRefNo;}
    public String getApprovedBy()                           {return this.SApprovedBy;}
    public String getPartyName()                            {return this.SPartyName;}
    public String getDelayReason()                          {return this.SDelayReason;}
    public String getCountName()                            {return this.SCountName;}
    public double getWeight()                               {return this.dWeight;}
    public String getReMixingDate()                         {return this.SReMixingDate;}
    public String getDyeingOrderNo()                        {return this.SDyeingOrderNo;}
    public String getFormName()                             {return this.SFormName;}

     //For GI Vector

    public void setGIDate(String SGIDate)                   {this.SGIDate   = SGIDate;}
    public void setOrderNo(String SOrderNo)                 {this.SOrderNo  = SOrderNo;}

    public String getGIDate()                               {return this.SGIDate;}
    public String getOrderNo()                              {return this.SOrderNo;}
}
