/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProcessReports.regularorder;

/**
 *
 * @author Administrator
 */

import java.util.*;
import java.sql.*;
import ProcessReports.jdbc.*;
import ProcessReports.util.*;
import ProcessReports.rndi.*;

public class RMSMRetriveClass implements RMSMRetriveInf{

    String    SFromDate,SToDate,SUnitCode,SDayType,SOrderType,SProcessTypeCode;

     Common    common;

     Vector    theGIVector,theReceiptVector;

     Vector    theNewGIVector = new Vector();
	 
	 int iOrderQty =0;

     public RMSMRetriveClass(String SFromDate,String SToDate,String SUnitCode,String SDayType, String SOrderType,String SProcessTypeCode,int iOrderQty)
     {
          this . SFromDate              = SFromDate;
          this . SToDate                = SToDate;
          this . SUnitCode              = SUnitCode;
          this . SDayType               = SDayType;
          this . SOrderType             = SOrderType;
          this . SProcessTypeCode       = SProcessTypeCode;
		  this . iOrderQty				= iOrderQty;

          common                        = new Common();

          theNewGIVector                = getNewGIDetails();
     }

     public Vector getMainVector()
     {
          Vector theVector              = new Vector();

          try
          {
               JDBCConnection1 jdbc     = JDBCConnection1.getJDBCConnection1();
               Connection theConnection = jdbc.getConnection();
               Statement theStatement   = theConnection.createStatement();

               ResultSet rs             = theStatement.executeQuery(getQuery("Days"));
               while(rs.next())
               {
                    RMSMDetailsReportClass theClass = null;

                    theClass            = new RMSMDetailsReportClass();

                    theClass            . setRegularOrderNo(common.parseNull(rs.getString(1)));
                    theClass            . setSampleOrderNo(common.parseNull(rs.getString(2)));
                    theClass            . setOrderDate(common.parseNull(rs.getString(3)));
                    theClass            . setApprovedDate(common.parseNull(rs.getString(4)));
                    theClass            . setPartyRefNo(common.parseNull(rs.getString(5)));
                    theClass            . setMillRefNo(common.parseNull(rs.getString(6))+".");
                    theClass            . setApprovedBy(common.parseNull(rs.getString(7))+".");
                    theClass            . setPartyName(common.parseNull(rs.getString(8)));
                    theClass            . setDelayReason(common.parseNull(rs.getString(9)));
                    theClass            . setCountName(common.parseNull(rs.getString(10)));
                    theClass            . setWeight(common.toDouble(rs.getString(11)));
                    theClass            . setReMixingDate(common.parseNull(rs.getString(13)));
                    theClass            . setDyeingOrderNo(common.parseNull(rs.getString(14)));
					theClass            . setFormName(common.parseNull(rs.getString(15)));

                    theVector           . addElement(theClass);
               }

               rs                       . close();
               theStatement             . close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }

          return theVector;
     }

     private String getQuery(String SGetType)
     {
          StringBuffer SB = new StringBuffer();

          SB.append(" Select RegularOrderNo,SampleOrderNo,OrderDate,ApprovedDate, ");
          SB.append(" PartyRefNo,MillRefNo,ApprovedByName, ");
          SB.append(" PartyName,DelayReason,CountName,Weight,DelayDays,ReMixingDate,DyeingOrderNo,FormName from ");
          SB.append(" ( ");
          SB.append(" Select SampleToRegularAuth.RegularOrderNo,SampleToRegularAuth.SampleOrderNo, ");
          SB.append(" RegularOrder.OrderDate,to_char(SampleToRegularAuth.LastModified,'yyyymmdd') as ApprovedDate, ");
          SB.append(" RegularOrder.RefOrderNo as PartyRefNo,SampleToRegularAuth.RefOrderNo as MillRefNo,MixingApprovedBy.ApprovedByName, ");
          SB.append(" PartyMaster.PartyName,SampleToRegularAuth.Remarks as DelayReason,YarnCount.CountName,RegularOrder.Weight, ");
          SB.append(" (to_Date(SampleToRegularAuth.LastModified) - to_Date(RegularOrder.OrderDate,'yyyymmdd')) as DelayDays, ");
          SB.append(" SampleToRegularAuth.ReMixingDate, Max(DyeingOrderDetails.DyeingOrderNo) as DyeingOrderNo,FibreForm.FormName as formname  ");
          SB.append(" from RegularOrder ");
          SB.append(" Inner   Join YarnCount           on YarnCount.CountCode = RegularOrder.CountCode ");
          SB.append(" Inner   Join PartyMaster         on PartyMaster.PartyCode = RegularOrder.PartyCode ");
          SB.append(" Inner   Join SampleToRegularAuth on SampleToRegularAuth.RegularOrderNo = RegularOrder.ROrderNo ");
          SB.append(" Left    Join MixingApprovedBy    on MixingApprovedBy.ApprovedByCode = SampleToRegularAuth.ApprovedByCode ");
          SB.append(" Left    Join DyeingOrderDetails  on DyeingOrderDetails.OrderNo = RegularOrder.ROrderNo ");
          SB.append(" Inner   Join ProcessingType      on ProcessingType.ProcessCode = regularorder.ProcessTypeCode ");
		  SB.append(" Inner  join FibreForm on FibreForm.FormCode = regularorder.FIBREFORMCODE ");

          SB.append(" where SampleToRegularAuth.SampleOrderNo Not like 'COR%' ");

          if(SGetType.equals("Month"))
          {
               SB.append(" and to_Char(SampleToRegularAuth.LastModified,'YYYYMMDD') >= "+SToDate.substring(0,6)+"01 and to_Char(SampleToRegularAuth.LastModified,'YYYYMMDD') <= "+SToDate+" ");
          }
          else
          {
               SB.append(" and to_Char(SampleToRegularAuth.LastModified,'YYYYMMDD') >= "+SFromDate+" and to_Char(SampleToRegularAuth.LastModified,'YYYYMMDD') <= "+SToDate+" ");
          }

          if(!SUnitCode.equals("All"))
          {
               SB.append(" and RegularOrder.UnitCode = "+SUnitCode);
          }

          if(SOrderType.equals("Depo Order"))
          {
               SB.append(" and SampleToRegularAuth.RegularOrderNo like 'DM%' ");
          }
          if(SOrderType.equals("Party Order"))
          {
               SB.append(" and SampleToRegularAuth.RegularOrderNo not like 'DM%' ");
          }

          if(!SProcessTypeCode.equals("All"))
          {
               if(SProcessTypeCode.equals("BR-C Orders"))
               {
                    SB.append(" and ProcessingType.OEStatus = 1 ");
               }
               else if(SProcessTypeCode.equals("Other than BR-C Orders"))
               {
                    SB.append(" and ProcessingType.OEStatus = 0 ");
               }
               else
               {
                    SB.append(" and RegularOrder.ProcessTypeCode = "+SProcessTypeCode);
               }
          }
		  
		  if(iOrderQty==0)
		  {
			  SB.append(" and RegularOrder.Weight<=50");
		  }
		  if(iOrderQty==1)
		  {
			  SB.append(" and RegularOrder.Weight>50");
		  }

          SB.append(" Group by ");
          SB.append(" SampleToRegularAuth.RegularOrderNo,SampleToRegularAuth.SampleOrderNo, ");
          SB.append(" RegularOrder.OrderDate,SampleToRegularAuth.LastModified, ");
          SB.append(" RegularOrder.RefOrderNo,SampleToRegularAuth.RefOrderNo,MixingApprovedBy.ApprovedByName, ");
          SB.append(" PartyMaster.PartyName,SampleToRegularAuth.Remarks,YarnCount.CountName,RegularOrder.Weight, ");
          SB.append(" RegularOrder.OrderDate,SampleToRegularAuth.ReMixingDate,FibreForm.FormName");
          SB.append(" ) ");

          SB.append(" order by ApprovedDate,SampleOrderNo,RegularOrderNo ");

          return SB.toString();
     }

     public Vector getSamplesVector()
     {
          Vector theVector              = new Vector();

          try
          {
               JDBCConnection1 jdbc     = JDBCConnection1.getJDBCConnection1();
               Connection theConnection = jdbc.getConnection();
               Statement theStatement   = theConnection.createStatement();
               ResultSet rs             = theStatement.executeQuery(getSamplesQS());

               while(rs.next())
               {
                    RMSMDetailsReportClass theClass = null;

                    String STSOrderNo   = common.parseNull(rs.getString(1));
                    int iNoOfSamples    = rs.getInt(2);

                    theClass            = new RMSMDetailsReportClass(STSOrderNo,iNoOfSamples);

                    theVector           . addElement(theClass);
               }
               rs                       . close();

               theStatement             . close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }

          return theVector;
     }

     private String getSamplesQS()
     {
          StringBuffer SB     = new StringBuffer();

          SB.append(" Select Substr(OrdNo,0,Length(OrdNo)-1) as OrderNo,Count(*) from SMIXIR ");
          SB.append(" where Substr(MixingDate,0,6) >= "+common.getPreviousMonth(common.toInt(SFromDate.substring(0,6)))+" ");
          SB.append(" Group by SubStr(OrdNo,0,Length(OrdNo)-1) ");

          return SB.toString();
     }

     public Vector getDyeingVector()
     {
          Vector theVector              = new Vector();

          try
          {
               Class                    . forName("oracle.jdbc.OracleDriver");
               Connection theConnection = DriverManager.getConnection("jdbc:oracle:thin:@192.168.252.39:1521:amardye2","dyeraw","dyeraw");
               Statement theStatement   = theConnection.createStatement();
               ResultSet rs             = theStatement.executeQuery("Select OrderNo,FibreCode from DyeingOrderDetails Order by 1");

               while(rs.next())
               {
                    RMSMDetailsReportClass theClass = null;

                    String SOrderNo     = common.parseNull(rs.getString(1));
                    String SFibreCode   = common.parseNull(rs.getString(2));

                    theClass            = new RMSMDetailsReportClass(SOrderNo,SFibreCode);

                    theVector           . addElement(theClass);
               }

               rs                       . close();
               theStatement             . close();
               theConnection            . close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }

          return theVector;
     }

     public Vector getGIDetailsVector()
     {
          Vector theVector              = new Vector();

          try
          {
               JDBCConnection1 jdbc     = JDBCConnection1.getJDBCConnection1();
               Connection theConnection = jdbc.getConnection();
               Statement theStatement   = theConnection.createStatement();
               ResultSet rs             = theStatement.executeQuery(getGIDetailsQS());

               while(rs.next())
               {
                    RMSMDetailsReportClass theClass = null;

                    theClass            = new RMSMDetailsReportClass();

                    theClass            . setGIDate(common.parseNull(rs.getString(1)));
                    theClass            . setOrderNo(common.parseNull(rs.getString(2)));

                    theVector           . addElement(theClass);
               }

               rs                       . close();
               theStatement             . close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }

          return theVector;
     }

     private String getGIDetailsQS()
     {
          StringBuffer SB = new StringBuffer();

          SB.append(" Select Max(Agn.GIDate) as GIDate,t.OrderNo,t.GINo from ");
          SB.append(" ( ");
          SB.append(" Select GIFibreDetails.RegularOrderNo as OrderNo,Max(GIFibreDetails.GINo) as GINo from GIFibreDetails "); // ChangesMade
          SB.append(" Inner Join Agn on Agn.GiNo = GIFibreDetails.GINo ");
          SB.append(" where SubStr(Agn.GIDate,0,6) >= "+common.getPreviousMonth(common.toInt(SFromDate.substring(0,6))));
          SB.append(" Group by GIFibreDetails.RegularOrderNo ");
          SB.append(" )t ");
          SB.append(" Inner Join Agn on Agn.GiNo = t.GINo ");
          SB.append(" Group by t.OrderNo,t.GINo ");
          SB.append(" Order by 1 ");

/*
          SB.append(" Select Max(Agn.GIDate) as GIDate,DyeingOrderDetails.OrderNo from DyeingOrderDetails ");
          SB.append(" left Join  AgnDetails on DyeingOrderDetails.DyeingOrderNo = AgnDetails.Orderno ");
          SB.append(" Left Join Agn  on AgnDetails.AgnNo = Agn.AgnNo ");
          SB.append(" Group by DyeingOrderDetails.OrderNo ");
          SB.append(" Order by 1 Desc ");
*/

          return SB.toString();
     }

     public String getUptoDateValues()
     {
          int iUpToDateOrders           = 0;
          int iUpToDateBelow3Days       = 0;
          int iUpToDateAbove3Days       = 0;
          int iUpToDateAppPending       = 0;

          theGIVector                   = new Vector();
          theReceiptVector              = new Vector();

          try
          {
               theGIVector              = getGIDetailsVector();
               theReceiptVector         = getReceiptDetailsVector();

               JDBCConnection1 jdbc     = JDBCConnection1.getJDBCConnection1();
               Connection theConnection = jdbc.getConnection();
               Statement theStatement   = theConnection.createStatement();

               ResultSet rs             = theStatement.executeQuery(getQuery("Month"));
               while(rs.next())
               {
                    if(!common.parseNull(rs.getString(1)).startsWith("DM"))
                    {
                         iUpToDateOrders++;
     
                         int iDelayDays           = common.getDateDiff(common.toInt(common.parseNull(rs.getString(4))),common.toInt(common.parseNull(rs.getString(3))))+1;
                         int iFDDays              = getGIDate(common.parseNull(rs.getString(14)),common.parseNull(rs.getString(2)),common.parseNull(rs.getString(1)),common.parseNull(rs.getString(3)));
                         int iSamplingDays        = iDelayDays - iFDDays;

                         String SReMixingDate     = common.parseNull(rs.getString(13));

                         if(iSamplingDays <= 3)
                         {
                              iUpToDateBelow3Days += 1;
                         }
                         else
                         {
                              iUpToDateAbove3Days += 1;
                         }

                         System.out.println("Order No --: "+rs.getString(1)+" Days---: "+ iSamplingDays);
                    }
               }
               rs                       . close();

               rs                       = theStatement.executeQuery(getRemixingPendingQuery());
               while(rs.next())
               {
                    iUpToDateAppPending++;
               }
               rs                       . close();

               theStatement             . close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }

          double dUpToDateBelowPer   = 0;

          try
          {
               dUpToDateBelowPer     = (common.toDouble(String.valueOf(iUpToDateBelow3Days)) / common.toDouble(String.valueOf(iUpToDateOrders))) * 100;
          }
          catch(Exception ex)
          {
               dUpToDateBelowPer     = 0;
          }

          double dUpToDateAbovePer   = 0;

          try
          {
               dUpToDateAbovePer     = (common.toDouble(String.valueOf(iUpToDateAbove3Days)) / common.toDouble(String.valueOf(iUpToDateOrders))) * 100;
          }
          catch(Exception ex)
          {
               dUpToDateAbovePer     = 0;
          }

          return String.valueOf(iUpToDateOrders)+"&"+
                 String.valueOf(iUpToDateBelow3Days)+"&"+
                 String.valueOf(iUpToDateAbove3Days)+"&"+
                 common.getRound(dUpToDateBelowPer,2)+"&"+
                 common.getRound(dUpToDateAbovePer,2)+"&"+
                 String.valueOf(iUpToDateAppPending);
     }

     private String getRemixingPendingQuery()
     {
          String QS = " Select SampleToRegularAuth.ReMixingDate,SampleToRegularAuth.RegularOrderNo,SampleToRegularAuth.SampleOrderNo,SampleToRegularAuth.Weight,YarnCount.CountName,"+
                      " to_char(SampleToRegularAuth.LastModified,'DD/MM/YYYY HH24:MI') as AppDate,RawUser.UserName as AppBy,Unit.UnitName,"+
                      " to_char(SampleToRegularAuth.RecdTime,'DD/MM/YYYY HH24:MI') as RecdDate,t.UserName as RecdBy,"+
                      " decode(SampleToRegularAuth.CorrectionMixing,0,'','CorrectionMixing') as Type,nvl(YarnM.YShnm,'') as ShadeName, "+
                      " decode(RegularOrder.OrderType,10,'Depo Order','Party Order') as OrderType  "+
                      " from SampleToRegularAuth "+
                      " Inner join RegularOrder On SampleToRegularAuth.RegularOrderNo=RegularOrder.ROrderNo "+
                      " Inner join YarnCount on YarnCount.CountCode = RegularOrder.CountCode "+
                      " Inner join Unit on RegularOrder.UnitCode = Unit.UnitCode "+
                      " Left join Yarnm on SampleToRegularAuth.ShadeCode=Yarnm.YShcd "+
                      " Inner join RawUser on SampleToRegularAuth.UserCode=RawUser.UserCode "+
                      " Inner join (Select UserCode,UserName from RawUser) t On SampleToRegularAuth.RecdUserCode=t.UserCode "+
                      " Where SampleToRegularAuth.Status=1 and SampleToRegularAuth.RecdStatus=1 "+
                      " and to_char(SampleToRegularAuth.LastModified,'YYYYMMDD') >= 20100301 "+
                      " and SampleToRegularAuth.Weight>0 and RegularOrder.OrderType<>10 "+
                      " and SampleToRegularAuth.ReMixingDate is null ";

                      if(!SUnitCode.equals("All"))
                      {
                           QS = QS + " and RegularOrder.UnitCode="+SUnitCode;
                      }

                      QS = QS + " Order by to_char(SampleToRegularAuth.RecdTime,'YYYYMMDD HH24:MI'),Unit.UnitName,SampleToRegularAuth.RegularOrderNo ";

          return QS;
     }

     private int getGIDate(String SDyeingOrderNo,String SSampleOrderNo,String SRegularOrderNo, String SOrderDate)
     {
          try
          {
               if(SDyeingOrderNo.length() > 0)
               {
                    for(int i=0;i<theNewGIVector.size();i++)
                    {
                         RMSMDetailsReportClass theClass = (RMSMDetailsReportClass)theNewGIVector.elementAt(i);
          
                         if(SRegularOrderNo.equals(theClass.getOrderNo()))
                         {
                              int iFDDays    = common.getDateDiff(common.toInt(theClass.getGIDate()),common.toInt(SOrderDate));

                              return iFDDays;
                         }
                    }
     
                    int iFDDays    = common.getDateDiff(common.toInt(common.getServerDate()),common.toInt(SOrderDate));
     
                    return iFDDays;
               }
          }catch(Exception ex){}
          
          return 0;
     }

     public Vector getPartyOrderVector()
     {
          Vector theVector              = new Vector();

          int iNoOfOrders               = 0, iFDOrders = 0;

          try
          {
               JDBCConnection1 jdbc     = JDBCConnection1.getJDBCConnection1();
               Connection theConnection = jdbc.getConnection();
               Statement theStatement   = theConnection.createStatement();
               ResultSet rs             = theStatement.executeQuery(getPartyOrderQS());

               while(rs.next())
               {
                    iNoOfOrders++;

                    String SStatus      = common.parseNull(rs.getString(13));

                    if(SStatus.equals("FD"))
                    {
                         iFDOrders++;
                    }
               }

               rs                       . close();
               theStatement             . close();

               theVector                . addElement(String.valueOf(iFDOrders));
               theVector                . addElement(String.valueOf(iNoOfOrders));
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }

          return theVector;
     }

     public String getPartyOrderQS()
     {
          StringBuffer SB = new StringBuffer();

          SB.append(" Select  T.OrderDate,T.ROrderNo,PartyName,t.Weight,CountName,CotPer,VisPer, ");
          SB.append(" PolyPer,T.RefOrderNo,Depth,subStr(SOrderNo,0,length(SOrderNo)-1),Color.ColorName, ");
          SB.append(" Generalstatus.Status,T.OrderType,T.AuthDate,t.SourceName,t.Instruction From  ");
          SB.append(" ( ");
          SB.append(" Select RegularOrder.AuthDate,RegularOrder.OrderDate,RegularOrder.ROrderNo, ");
          SB.append(" PartyMaster.PartyName,RegularOrder.Weight,YarnCount.CountName,CotPer, ");
          SB.append(" VisPer,PolyPer,RegularOrder.RefOrderNo,RMixir.Depth,Max(RegularOrderStatus.id) as id, ");
          SB.append(" RegularOrder.ColorCode,max(SampleOrder.Orderid) as Orderid ,RegularOrder.UnitCode, ");
          SB.append(" RegularOrder.OrderType,LightSource.SourceName,RegularOrder.Instruction ");
          SB.append(" from RegularORder ");
          SB.append(" left join SampleToRegularAuth on SampleToRegularAuth.RegularORderNo=RegularORder.ROrderNO ");
          SB.append(" Left  Join SampleOrder On SampleOrder.ForOrder = RegularOrder.ROrderNo ");
          SB.append(" Inner Join PartyMaster On PartyMaster.PartyCode = RegularOrder.PartyCode ");
          SB.append(" Inner Join YarnCount On YarnCount.CountCode = RegularOrder.CountCode ");
          SB.append(" Inner Join Blend On Blend.BlendCode = RegularOrder.BlendCode ");
          SB.append(" Left Join RMixir On RmixIr.OrdNo = RegularOrder.RefOrderNo and Rmixir.CorrectionMixing = 0 ");
          SB.append(" Left Join RegularOrderStatus On RegularOrderStatus.Orderid = RegularOrder.Orderid ");
          SB.append(" Left join LightSource on LightSource.SourceCode= Regularorder.LightCode ");
          SB.append(" Where RegularOrder.Status = 1 and RegularOrder.OrderDate >= 20061001 and ");
          SB.append(" RegularOrder.OrderDate <= "+common.getNextDate(common.toInt(SToDate))+" and RegularOrder.Shortage <> 2 And ");
          SB.append(" RegularOrder.RorderNo not Like 'T%'and RegularOrder.RorderNo not Like 'PR%' ");
          SB.append(" and SampleToRegularAuth.status is Null Or SampleToRegularAuth.Status=0 ");
          SB.append(" group by ");
          SB.append(" RegularOrder.AuthDate, RegularOrder.OrderDate, RegularOrder.ROrderNo, ");
          SB.append(" PartyMaster.PartyName,RegularOrder.Weight,YarnCount.CountName,CotPer, ");
          SB.append(" VisPer,PolyPer,RegularOrder.RefOrderNo,RMixir.Depth,RegularOrder.ColorCode, ");
          SB.append(" RegularOrder.Unitcode,RegularOrder.OrderType,LightSource.SourceName,RegularOrder.Instruction ");
          SB.append(" )t ");
          SB.append(" Left join  OrderGroupDetails on OrderGroupDetails.ORderno=t.ROrderNo ");
          SB.append(" Left  Join SampleOrder On SampleOrder.OrderId = T.OrderId ");
          SB.append(" Left Join Color On Color.ColorCode = T.ColorCode ");
          SB.append(" Left Join  RegularOrderStatus On RegularOrderStatus.id = T.id ");
          SB.append(" Left  Join GeneralStatus On GeneralStatus.Statuscode = RegularorderStatus.StatusCode ");
          SB.append(" Inner Join Unit On Unit.UnitCode = T.UnitCode ");

          if(!SUnitCode.equals("All"))
          {
               SB.append(" and Unit.UnitCode = "+SUnitCode);
          }

          SB.append(" Where T.RorderNo not in('DM02312') and OrderGroupDetails.OrderNo is Null ");
          SB.append(" and T.ROrderNo Not like 'DM%' and PartyName Not like 'AMARJOTHI%' ");
          SB.append(" Group By ");
          SB.append(" T.OrderDate,T.ROrderNo,PartyName,t.Weight,CountName, ");
          SB.append(" CotPer,VisPer,PolyPer,T.RefOrderNo,Depth,T.id, subStr(SOrderNo,0,length(SOrderNo)-1),GeneralStatus.Status, ");
          SB.append(" Color.ColorName,T.OrderType,T.AuthDate,t.SourceName,t.Instruction ");
          SB.append(" Order By  T.OrderDate ");

          return SB.toString();
     }

     public Vector getReceiptDetailsVector()
     {
          Vector theVector    = new Vector();

          try
          {
               JDBCConnection1 jdbc     = JDBCConnection1.getJDBCConnection1();
               Connection theConnection = jdbc.getConnection();
               Statement theStatement   = theConnection.createStatement();
               ResultSet rs             = theStatement.executeQuery(getReceiptDetailsQS());

               while(rs.next())
               {
                    RMSMDetailsReportClass theClass = null;

                    theClass            = new RMSMDetailsReportClass();

                    theClass            . setGIDate(common.parseNull(rs.getString(1)));
                    theClass            . setOrderNo(common.parseNull(rs.getString(2)));

                    theVector           . addElement(theClass);
               }

               rs                       . close();
               theStatement             . close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }

          return theVector;
     }

     private String getReceiptDetailsQS()
     {
          StringBuffer SB = new StringBuffer();

          SB.append(" Select Max(GIDate) as GIDate, OrdNo from ");
          SB.append(" ( ");
          SB.append(" Select Max(GIDate) as GIDate,GINO,AgnDetails.AgnNo,BaseCode,DyerWeight,SMixIr.OrdNo from AgnDetails ");
          SB.append(" Inner join Agn on Agn.AgnNo=AgnDetails.AgnNo ");
          SB.append(" Inner Join Fibre on Fibre.FibreCode = AgnDetails.BaseCode ");
          SB.append(" Inner Join SBlenr on SBlenr.FibCd = Fibre.FibreCode ");
          SB.append(" Inner Join SMixir on SMixir.MixNo = SBlenr.MixNo ");
          SB.append(" where substr(SMixIr.MixingDate,0,6) >= "+common.getPreviousMonth(common.toInt(SFromDate.substring(0,6))));
          SB.append(" Group by GINo,AgnDetails.AgnNo,BaseCode,DyerWeight,SMixIr.OrdNo ");
          SB.append(" Union All ");
          SB.append(" Select Max(AgnDate) as GIDate,GINO,PurchaseAgn.AgnNo,Fibre.FibreCode,0 as DyerWeight,SMixIr.OrdNo From PurchaseAgn ");
          SB.append(" Inner join Fibre on Fibre.FibreCode=PurchaseAgn.FibreCode ");
          SB.append(" Inner Join SBlenr on SBlenr.FibCd = Fibre.FibreCode ");
          SB.append(" Inner Join SMixir on SMixir.MixNo = SBlenr.MixNo ");
          SB.append(" where substr(SMixIr.MixingDate,0,6) >= "+common.getPreviousMonth(common.toInt(SFromDate.substring(0,6))));
          SB.append(" Group by AgnDate,GINO,PurchaseAgn.AgnNo,Fibre.FibreCode,SMixIr.OrdNo ");
          SB.append(" ) ");
          SB.append(" Group by OrdNo ");
          SB.append(" order by 1 Desc ");

          return SB.toString();
     }

     public Vector getNewGIDetails()
     {
          Vector theVector              = new Vector();

          try
          {
               JDBCConnection1 jdbc     = JDBCConnection1.getJDBCConnection1();
               Connection theConnection = jdbc.getConnection();
               Statement theStatement   = theConnection.createStatement();
               ResultSet rs             = theStatement.executeQuery(getNewGIQS());

               while(rs.next())
               {
                    RMSMDetailsReportClass theClass = null;

                    theClass            = new RMSMDetailsReportClass();

                    theClass            . setOrderNo(common.parseNull(rs.getString(1)));
                    theClass            . setGIDate(common.parseNull(rs.getString(2)));

                    theVector           . addElement(theClass);
               }

               rs                       . close();
               theStatement             . close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }

          return theVector;
     }

     private String getNewGIQS()
     {
          StringBuffer SB = new StringBuffer();

          SB.append(" Select DyeingOrderDetails.OrderNo, Max(GIFibreDetailsChild.GIDate) as GIDate from GIFibreDetailsChild ");
          SB.append(" Inner Join DyeingOrderDetails on DyeingOrderDetails.OrderNo = GIFibreDetailsChild.RegularOrderNo ");
          SB.append(" where DyeingOrderDetails.GICompleteStatus = 1 ");
          SB.append(" Group by DyeingOrderDetails.OrderNo ");

          return SB.toString();
     }

     public Vector getGIDetails()
     {
          return theGIVector;
     }

     public Vector getReceiptDetails()
     {
          return theReceiptVector;
     }
}
