/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProcessReports.orderdetails;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import ProcessReports.util.*;
import ProcessReports.rndi.*;
import ProcessReports.jdbc.*;

/**
 *
 * @author Administrator
 */
public class OrderAllocationStockCrit extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    Connection  theConnection = null;
    Common common = new Common();

    Vector VUnitCode,VUnitName;
    String SToday = "";
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            PrintWriter out = response.getWriter();
            setDataintoVector();
                      out.println("<html>");
          out.println("<SCRIPT LANGUAGE='JavaScript'>");

          out.println("function func1(xyz,a1)");
          out.println("{");
          out.println(" typedstring = a1.value;");
          out.println(" for(ctr=0;ctr<xyz.options.length;ctr++)");
          out.println(" {");
          out.println(" str=xyz.options[ctr].value;");
          out.println(" if(typedstring==str.substring(0,typedstring.length))");
          out.println(" break;");
          out.println(" }");
          out.println(" if (ctr < xyz.options.length)");
          out.println(" xyz.options[ctr].selected=true;");
          out.println(" return;");
          out.println("}");
          out.println("</Script>");

          out.println("</head>");
          out.println("<body bgcolor='#9AA8D6' onload='dynAnimation()' language='Javascript1.2'>");
          out.println("<base target='topmain'>");

          out.println("<form method='GET' action='OrderAllocationStock'>");
          out.println("  <font color='#FFFFFF' size='5'><b>Order Details</b><br><br></font>");
          out.println("  <center>");
          out.println("<div align='center'>");
          out.println("  <table border='1' width='600' height='14'>");


          out.println("    <tr>");
          out.println("      <td><font color='#FFFF66'><b>As on Date: </b></font></td>");
          out.println("      <td><input type='text' size='10' name='asonDate' value='"+SToday+"'>");
          out.println("    </tr>");            


          out.println("    <tr>");
          out.println("      <td><font color='#FFFF66'><b>Unit: </b></font></td>");
          out.println("      <td>");
          out.println("      <select size=1 name=Unit>");
		  out.println("      <option value='9999'>ALL</option>");
          out.println("      <option value='ALL'>ALL(Only A,B)</option>");
		  
          for(int i=0;i<VUnitCode.size();i++)
          {
               out.println("   <option value='"+VUnitName.elementAt(i)+"'>"+VUnitName.elementAt(i)+"</option>");
          }
          out.println("      </select>");
          out.println("    </tr>");            

          out.println("      <tr><td colspan='2' align='center'><input type='submit' value='Submit' name='T1'></input></td></tr>");
          out.println("  </table>");
          out.println("</div>");

          out.println("  <h1 dynamicanimation='fpAnimelasticBottomFP1' id='fpAnimelasticBottomFP1' style='position: relative !important; visibility: hidden' language='Javascript1.2'><font color='#800000' face='Arial'><input type='submit' value='Submit' name='B1' style='font-family: Arial'></font><input type='reset' value='Reset' name='B2'></h1>");
          out.println("  </center>");
          out.println("</form>");
          out.println("</body>");
          out.println("</html>");
          out.close();

        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
     public void setDataintoVector()
     {
          VUnitCode       = new Vector();
          VUnitName       = new Vector();

          try
          {	
          	 if(theConnection==null){
          	    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                theConnection       = jdbc . getConnection();
             }
             java.sql.Statement st = theConnection.createStatement();
             java.sql.ResultSet rs = st.executeQuery(" Select unitcode,unitname from Unit where unitcode in (1,2) order by 1");
               
             while(rs.next())
             {
                    VUnitCode.addElement(rs.getString(1));
                    VUnitName.addElement(rs.getString(2));
             }
             rs.close();
             st.close();

          }
          catch(Exception ex){}
     }
}
