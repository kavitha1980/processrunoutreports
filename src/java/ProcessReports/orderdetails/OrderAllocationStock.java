/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProcessReports.orderdetails;

import ProcessReports.jdbc.*;
import ProcessReports.util.*;

import java.io.*;
import java.sql.*;           
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

/**
 *
 * @author Administrator
 */
public class OrderAllocationStock extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    Common         common;
    HttpSession    session;
    
    Connection 	   theConnection  = null;    

    
    Vector          VOrderNo, VOrderdate, VPartyName, VOrdWeight, VCountName, VMixingDate, VUnit, VRefOrderNo, VUnitCode, VStock,VYarnType,VOBAStatus,VDepth;
    java.util.List  thePlacedOrdersList = null;

    String          SStDate,SEnDate;
    String          bgColor,bgHead,bgUom,bgBody,fgHead,fgUom,fgBody;
    String          SUnit="";
    int             TLine=0;
    int             iPage=1;
    int             iEnDate = 0;
    int             iSlNo=0;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            /* TODO output your page here. You may use following sample code. */
            PrintWriter out = response.getWriter();
            common = new Common();
            bgColor = common.bgColor;
            bgHead  = common.bgHead;
            bgUom   = common.bgUom;
            bgBody  = common.bgBody;
            fgHead  = common.fgHead;
            fgUom   = common.fgUom;
            fgBody  = common.fgBody;

            iEnDate        = common.toInt(common.pureDate((String)request.getParameter("asonDate")));
            SUnit    	   = request.getParameter("Unit");
            System.out.println("SUnit--->"+SUnit);
            valueInt();
            System.out.println("1");
            SetData();
            System.out.println("2");
            setHtml(out);
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void setHtml(PrintWriter out) throws  ServletException
    {

    try{
        SEnDate            = common.parseDate(String.valueOf(iEnDate));
        
        out.println("<html>");
        out.println("<head>");
        out.println("<title>InvPro</title>");
        out.println("</head>");
        out.println("<body bgcolor='"+bgColor+"' text = '#0000FF'>");
        out.println("<p align='center'><font size='5'><b><u>"+SUnit+" - Order Allocation Stock Details</u>  ");
        out.println(" As On Date: "+SEnDate+"</b></font></p>");
        out.println("<div align='center'>");
        
        out.println("<p align='center'><font size='5'><b><u> Order Allocation Standards</u>  ");
        out.println("  <table border='0' bordercolor='"+bgColor+"'>");
        out.println("    <tr>");
        out.println("      <td  align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Priority</b></font></td>");
        out.println("      <td  align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Description</b></font></td>");
        out.println("      <td  align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>A-Unit</b></font></td>");
        out.println("      <td  align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>B-Unit</b></font></td>");
        out.println("    </tr>");
        

        out.println("    <tr>");
        out.println("      <td  align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>1</b></font></td>");
        out.println("      <td  align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>&nbsp;</b></font></td>");
        out.println("      <td  align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>Special YarnTypes (Injection, Vario, Grindle etc..)<br/>Count 20s-40s VCGREY, VCLGREY<br/>Count 34s-40s PCSGREY, PCSLGREY</b></font></td>");
        //out.println("      <td  align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>Compact Orders, OBA Orders, Pallete Packing Orders</b></font></td>");
        out.println("      <td  align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>Compact Orders, OBA Orders</b></font></td>");
		out.println("    </tr>");
        
        out.println("    <tr>");
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>2</b></font></td>");
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>Stock(Mixing+SampleUnit) Kgs</b></font></td>");
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>&nbsp;</b></font></td>");
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>&nbsp;Stock<60000</b></font></td>");
        out.println("    </tr>");

        out.println("    <tr>");
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>3</b></font></td>");
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>A, B Unit Stock 60%:40%</b></font></td>");
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>&nbsp;</b></font></td>");
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>&nbsp;Stock<60%</b></font></td>");
        out.println("    </tr>");

        out.println("    <tr>");
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>4</b></font></td>");
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>A, B Unit Stock 60%:40%</b></font></td>");
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>Count 24s-40s, Depth 0-30, Weight>1000 and YarnType (None, Fancy yarn, Ball Neps, DrawRoom) and Stock<40% </b></font></td>");
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>&nbsp;</b></font></td>");
        out.println("    </tr>");
        out.println("    </table>");
        
        
        out.println("<p align='center'><font size='5'><b><u> Placed Orders </u> As On Date: "+SEnDate+"  </b></font></p>");
        
        out.println("  <table border='0' bordercolor='"+bgColor+"'>");
        out.println("    <tr>");
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>SL.No</b></font></td>");
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>OrderNo</b></font></td>");
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Weight</b></font></td>");
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Count Name</b></font></td>");
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Yarn Type</b></font></td>");
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Packing Type</b></font></td>");
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Depth</b></font></td>");
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Unit</b></font></td>");
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>OBA Status</b></font></td>");
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Transfered Unit</b></font></td>");
        out.println("    </tr>");
        
        
        for(int i=0;i<thePlacedOrdersList.size();i++) {
        	java.util.HashMap theMap = (java.util.HashMap)thePlacedOrdersList.get(i);
        	
        out.println("    <tr>");
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>"+String.valueOf(i+1)+"</b></font></td>");
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>"+common.parseNull((String)theMap.get("RORDERNO"))+"</b></font></td>");
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>"+common.parseNull((String)theMap.get("WEIGHT"))+"</b></font></td>");
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>"+common.parseNull((String)theMap.get("COUNTNAME"))+"</b></font></td>");
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>"+common.parseNull((String)theMap.get("FORMNAME"))+"</b></font></td>");
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>"+common.parseNull((String)theMap.get("PACKNAME"))+"</b></font></td>");
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>"+common.parseNull((String)theMap.get("DEPTH"))+"</b></font></td>");
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>"+common.parseNull((String)theMap.get("UNITNAME"))+"</b></font></td>");
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>"+common.parseNull((String)theMap.get("OBASTATUS"))+"</b></font></td>");
        
        if(common.toInt(common.parseNull((String)theMap.get("TOUNIT")))==1) {
        out.println("     <td  rowspan='1' align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>A_Unit</b></font></td>");	
        }else if(common.toInt(common.parseNull((String)theMap.get("TOUNIT")))==2) {
        out.println("     <td  rowspan='1' align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>B_Unit</b></font></td>");
        }else{
        out.println("     <td  rowspan='1' align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>&nbsp;</b></font></td>");
        //out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>"+common.parseNull((String)theMap("TOUNIT"))+"</b></font></td>");
        }
        
        out.println("    </tr>");
        
		}        
        out.println("  </table>");
        
        out.println("  </br>");
        
        out.println("<p align='center'><font size='5'><b><u> Stock Details</u>  ");
        out.println(" As On Date: "+SEnDate+"</b></font></p>");
        
        
        out.println("  <table border='0' bordercolor='"+bgColor+"'>");
        out.println("    <tr>");
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>SL.No</b></font></td>");
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>OrderNo</b></font></td>");
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>OrderDate</b></font></td>");
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Party Name</b></font></td>");
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Count Name</b></font></td>");
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Order Weight</b></font></td>");
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Ref. OrderNo</b></font></td>");
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Unit</b></font></td>");
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Stock At</b></font></td>");
		out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Mixing Date</b></font></td>");
		out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Yarn Type</b></font></td>");
		out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>OBA Status</b></font></td>");
		out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Depth</b></font></td>");
        out.println("    </tr>");

        String SUnitCode   = "";
        
        String StockStatus = "";
        
        int iSlNo = 1;
        
        double dSubTotWeight = 0,dTotWeight = 0, dGTotWeight = 0;
        
		System.out.println("VOrderNo.size()-->"+VOrderNo.size());
		
        for(int i=0;i<VOrderNo.size();i++)
        {
            if(!common.parseNull((String)VStock.elementAt(i)).equals(StockStatus) && i!=0)
            {
                    out.println("    <tr>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"' colspan='5'><font color='"+fgUom+"'>"+StockStatus+" Stock</font></td>");
                    out.println("      <td   align='Right'  bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>"+common.getRound(dSubTotWeight,3)+"</b></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
					out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
					out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("    </tr>");
                    
                    dSubTotWeight  = 0;
                    
                    iSlNo    = 1;
            }
        
            if(!common.parseNull((String)VUnitCode.elementAt(i)).equals(SUnitCode) && i!=0)
            {
                    out.println("    <tr>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("      <td   align='Right'  bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>"+common.getRound(dTotWeight,3)+"</b></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
					out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
					out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("    </tr>");
                    
                    dTotWeight  = 0;
                    
                    iSlNo    = 1;
            }
            
            dSubTotWeight+= common.toDouble((String)VOrdWeight.elementAt(i));
            dTotWeight  += common.toDouble((String)VOrdWeight.elementAt(i));
            dGTotWeight += common.toDouble((String)VOrdWeight.elementAt(i));
			
			String SMixingDate = common.parseDate((String)VMixingDate.elementAt(i));

            out.println("    <tr>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>"+String.valueOf(iSlNo)+"</font></td>");
            out.println("      <td width='40' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VOrderNo.elementAt(i)+"</font></td>");
            out.println("      <td width='180' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+common.parseDate((String)VOrderdate.elementAt(i))+"</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VPartyName.elementAt(i)+"</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VCountName.elementAt(i)+"</font></td>");
            out.println("      <td width='40' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>"+(String)VOrdWeight.elementAt(i)+"</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VRefOrderNo.elementAt(i)+"</font></td>");
            out.println("      <td width='40' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VUnit.elementAt(i)+"</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VStock.elementAt(i)+"</font></td>");
			
			if(SMixingDate.equals("-1"))
			{
				out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'></font></td>");
			}
			else
			{
				out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+SMixingDate+"</font></td>");
			}
			
			out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VYarnType.elementAt(i)+"</font></td>");
			out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VOBAStatus.elementAt(i)+"</font></td>");
            
			out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+common.parseNull((String)VDepth.elementAt(i))+"</font></td>");
			out.println("    </tr>");
            
            SUnitCode   = common.parseNull((String)VUnitCode.elementAt(i));
            StockStatus = common.parseNull((String)VStock.elementAt(i));
            
            iSlNo++;
        }

        out.println("    <tr>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"' colspan='5'><font color='"+fgUom+"'>"+StockStatus+" Stock</font></td>");
        out.println("      <td   align='Right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>"+common.getRound(dSubTotWeight,3)+"</b></font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
		out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
		out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
		out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("    </tr>");

        out.println("    <tr>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='Right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>"+common.getRound(dTotWeight,3)+"</b></font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
		out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
		out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
		out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("    </tr>");
        
        out.println("    <tr>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>GRAND TOTAL</font></td>");
        out.println("      <td   align='Right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>"+common.getRound(dGTotWeight,3)+"</b></font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
		out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
		out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
		out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("    </tr>");
        
        dTotWeight  = 0; 
        dGTotWeight = 0;
        
        out.println("  </table>");
        out.println("  </center>");
        out.println("</div>");
        out.println("");
        out.println("</body>");
        out.println("");
        out.println("</html>");
        out.println("");
        out.close();
        
     }catch(Exception ex){
	    ex.printStackTrace();
     }

    }
    
    private void SetData() {
		thePlacedOrdersList = new java.util.ArrayList();
		
	    VOrderNo          = new Vector();
	    VOrderdate        = new Vector();
	    VPartyName        = new Vector();
	    VOrdWeight        = new Vector();
	    VCountName        = new Vector();
	    VMixingDate       = new Vector();
	    VUnit             = new Vector();
	    VRefOrderNo       = new Vector();
	    VUnitCode         = new Vector();
	    VStock            = new Vector();
		VYarnType		  = new Vector();
		VOBAStatus		  = new Vector();
		VDepth			  = new Vector();

		String SUnitCode = "";
		
		if(SUnit.equals("ALL")) {
			SUnitCode = "1,2";
		}else if(!SUnit.equals("ALL")) {
			if(SUnit.equals("A_Unit")) {
				SUnitCode = "1";
			}
			if(SUnit.equals("B_Unit")) {
			    SUnitCode = "2";
			}
		}
		

     String QS = " ";
     
    QS = " Select ROrderNo, OrderDate, PartyName, OrderWeight, CountName, MixingDate, UnitName, RefOrderNo, unitCode,"+
		 " Stock,YarnType,obastatus,Referenceno,Shadecardreforderno ,Shadecardrefno from ( "+

		 " Select ROrderNo, OrderDate, PartyName, OrderWeight, CountName, -1 as MixingDate, UnitName, RefOrderNo, unitCode,"+
		 " 'Sample Unit' as Stock,YarnType,obastatus,Referenceno ,Shadecardreforderno ,Shadecardrefno  from ( "+

		 " Select  T.OrderDate,T.ROrderNo,PartyName,t.Weight as OrderWeight,CountName, CotPer,VisPer,PolyPer, T.RefOrderNo,Depth, "+
		 " decode(subStr(SOrderNo,length(SOrderNo),length(SOrderNo)-1),'1', subStr(SOrderNo,0,length(SOrderNo)-2), subStr(SOrderNo,0,length(SOrderNo)-1)), "+
		 " Color.ColorName,Generalstatus.Status,T.OrderType,T.AuthDate,t.SourceName,   "+
		 " Replace(Replace(t.Instruction, chr(10), ' '), chr(9),'') as Instruction,  T.SampleOrderNo, unitcode,T.BlendChanged, T.YarnType,  T.CountVariation, "+
		 " T.Spirality, T.PerfectShadeMatching, T.AvoidNeps, T.DyeingMethod, T.FastnessStandard,  T.DischargePrintOrder,  "+
		 " T.OrderRequirementLevel,   T.ShortageQtyAccepted, T.MatchingOrderNo, T.BinName, T.ProcessType, T.PartyCode,T.QtyGrade,  "+
		 " T.ShortageType, T.Name,T.OrderGroupNo,T.ClothEnClosed, Unit.UnitName,obastatus,T.referenceno,T.Shadecardreforderno, T.Shadecardrefno From  (  "+

		 " Select RegularOrder.AuthDate,RegularOrder.OrderDate, RegularOrder.ROrderNo,PartyMaster.PartyName, RegularOrder.Weight,YarnCount.CountName, "+  
		 " CotPer,VisPer,PolyPer,RegularOrder.RefOrderNo,RMixir.Depth,Max(RegularOrderStatus.id) as id,RegularOrder.ColorCode,max(SampleOrder.Orderid) as Orderid, "+  
		 " RegularOrder.UnitCode,RegularOrder.OrderType,LightSource.SourceName,RegularOrder.Instruction,  "+ 
		 " SampleToRegularAuth.SampleOrderNo,RegularOrder.MixingChanged as BlendChanged, FibreForm.FormName as YarnType,  CountVariation.Name as CountVariation, "+  
		 " Spirality.Name as Spirality, PerfectShadeMatching.Name as PerfectShadeMatching,  AvoidNeps.Name as AvoidNeps, DyeingMethod.Name as DyeingMethod, "+ 
		 " FastnessStandard.Name as FastnessStandard,  DischargePrintOrder.Name as DischargePrintOrder, OrderRequirementLevel.Name as OrderRequirementLevel, "+   
		 " ShortageQtyAccepted.Name as ShortageQtyAccepted, RegularOrder.MatchingOrderNo, SampleBin.Name as BinName,   "+
		 " ProcessingType.ProcessType, PartyMaster.PartyCode,PartyMaster.QtyGrade, RegularOrder.Shortage as ShortageType,  "+
		 " SpinningProcessType.Name,RegularOrder.OrderGroupNo, decode(RegularOrder.ClothEnclosed,null,'NO','YES')  as ClothEnclosed,Oba.name as obastatus "+
		 " , regularorder.referenceno,ShadeCardNo.reforderno as Shadecardreforderno, ShadeCardNo.refno as Shadecardrefno"+
		 " from RegularORder "+

		 " left join SampleToRegularAuth on SampleToRegularAuth.RegularORderNo = RegularORder.ROrderNO "+
		 " Left  Join SampleOrder On SampleOrder.ForOrder = RegularOrder.ROrderNo   "+
		 " Inner Join PartyMaster On PartyMaster.PartyCode = RegularOrder.PartyCode  ";
		 if(!SUnit.equals("9999"))
		 {
			 QS = QS + "  and RegularOrder.unitcode in ("+SUnitCode+") ";
		 }
		
QS = QS +" Inner Join YarnCount On YarnCount.CountCode = RegularOrder.CountCode   "+
		 " Inner Join Blend On Blend.BlendCode = RegularOrder.BlendCode   "+
		 " Left  Join RMixir On RmixIr.OrdNo = RegularOrder.RefOrderNo and Rmixir.CorrectionMixing=0   "+
		 " Left Join RegularOrderStatus On  RegularOrderStatus.Orderid = RegularOrder.Orderid   "+
		 " Left join LightSource on LightSource.SourceCode= Regularorder.LightCode    "+
		 " Left Join FibreForm on FibreForm.FormCode = RegularOrder.FibreFormCode    "+
		 " Left Join CountVariation on CountVariation.Code = RegularOrder.CountVariationCode    "+
		 " Left Join Spirality on Spirality.Code = RegularOrder.SpiralityCode    "+
		 " Left Join PerfectShadeMatching on PerfectShadeMatching.Code = RegularOrder.PerfectShadeMatchingCode "+   
		 " Left Join AvoidNeps on AvoidNeps.Code =  RegularOrder.AvoidNepsCode  "+
		 " Left Join DyeingMethod on DyeingMethod.Code = RegularOrder.DyeingMethodCode    "+
		 " Left Join FastnessStandard on FastnessStandard.Code = RegularOrder.FastnessStandardCode    "+
		 " Left Join DischargePrintOrder on DischargePrintOrder.Code = RegularOrder.DischargePrintOrderCode    "+
		 " Left Join OrderRequirementLevel on OrderRequirementLevel.Code = RegularOrder.OrderRequirementLevelCode    "+
		 " Left Join ShortageQtyAccepted on ShortageQtyAccepted.Code = RegularOrder.ShortageQtyAcceptedCode    "+
		 " Left Join SampleUnitOrderBin on RegularOrder.ROrderNo = SampleUnitOrderBin.OrderNo and SampleUnitOrderBin.Status=0 "+   
		 " Left Join SampleBin on SampleUnitOrderBin.BinCode = SampleBin.Code    "+
		 " Inner join ProcessingType on ProcessingType.ProcessCode = RegularOrder.ProcessTypeCode    "+
		 " Inner Join SpinningProcessType on SpinningProcessType.CODE = RegularOrder.SpinningProcessTypeCode and ProcessingType.OEStatus = 0 "+
		 "  inner join Oba on Oba.code = regularorder.obacode "+
		 "  left join ShadeCardNo on ShadeCardNo.OrderNo = regularorder.reforderno"+
		 " Where  to_char(to_date(RegularORder.entrydate,'dd.mm.yyyy hh24:mi:ss'),'yyyymmdd')<=to_char(to_date("+iEnDate+",'yyyymmdd')-1,'yyyymmdd')  and  RegularOrder.Status=1 and RegularOrder.OrderDate >= 20061001 and RegularOrder.Shortage<>2 And RegularOrder.RorderNo not Like 'T%'   "+
		 
		 " and RegularOrder.RorderNo not Like 'PR%'  and nvl(SampleToRegularAuth.status, 0) = 0  and RegularOrder.ROrderNo Not like 'DM%'    "+
		 " group by  RegularOrder.AuthDate, RegularOrder.OrderDate, RegularOrder.ROrderNo,PartyMaster.PartyName, RegularOrder.Weight,YarnCount.CountName,   "+
		 " CotPer,VisPer,PolyPer,RegularOrder.RefOrderNo,RMixir.Depth,RegularOrder.ColorCode,RegularOrder.Unitcode,RegularOrder.OrderType,  "+
		 " LightSource.SourceName,RegularOrder.Instruction,SampleToRegularAuth.SampleOrderNo,RegularOrder.MixingChanged, FibreForm.FormName,    "+
		 " CountVariation.Name, Spirality.Name, PerfectShadeMatching.Name,  AvoidNeps.Name, DyeingMethod.Name, FastnessStandard.Name,    "+
		 " DischargePrintOrder.Name, OrderRequirementLevel.Name,   ShortageQtyAccepted.Name, RegularOrder.MatchingOrderNo, SampleBin.Name,   "+
		 " ProcessingType.ProcessType, PartyMaster.PartyCode,PartyMaster.QtyGrade ,RegularOrder.Shortage,SpinningProcessType.Name,RegularOrder.OrderGroupNo,   "+
		 " decode(RegularOrder.ClothEnclosed,null,'NO','YES'),Oba.name, regularorder.referenceno,ShadeCardNo.reforderno, ShadeCardNo.refno     "+
		 " )t   "+
		 " Left join  OrderGroupDetails on OrderGroupDetails.ORderno=t.ROrderNo   "+
		 " Left  Join SampleOrder On SampleOrder.OrderId = T.OrderId   "+
		 " Left Join Color On Color.ColorCode = T.ColorCode   "+
		 " Left Join  RegularOrderStatus On RegularOrderStatus.id = T.id   "+
		 " Left  Join GeneralStatus On GeneralStatus.Statuscode = RegularorderStatus.StatusCode "+  
		 " Inner Join Unit On Unit.UnitCode = T.UnitCode   "+
		 " Where T.RorderNo not in('DM02312', 'LM28636') and OrderGroupDetails.OrderNo is Null   "+
		 " Group By T.OrderDate,T.ROrderNo,PartyName,t.Weight,CountName,   "+
		 " CotPer,VisPer,PolyPer,T.RefOrderNo,Depth,T.id ,  "+
		 " decode(subStr(SOrderNo,length(SOrderNo),length(SOrderNo)-1),'1', subStr(SOrderNo,0,length(SOrderNo)-2),   "+
		 " subStr(SOrderNo,0,length(SOrderNo)-1)),GeneralStatus.Status,Color.ColorName,T.OrderType,T.AuthDate,  "+
		 " t.SourceName,t.Instruction,T.SampleOrderNo,unitcode,T.BlendChanged,T.YarnType,  T.CountVariation,   "+
		 " T.Spirality, T.PerfectShadeMatching,  T.AvoidNeps, T.DyeingMethod, T.FastnessStandard,  T.DischargePrintOrder,   "+
		 " T.OrderRequirementLevel,   T.ShortageQtyAccepted, T.MatchingOrderNo, T.BinName, T.ProcessType,   "+
		 " T.PartyCode,T.QtyGrade,T.ShortageType,T.Name,T.OrderGroupNo,T.ClothEnclosed, Unit.UnitName,obastatus       "+
		 " ,T.referenceno,T.Shadecardreforderno, T.Shadecardrefno  "+
		 " Order By  T.ShortageType desc, T.QtyGrade,T.OrderGroupNo,T.YarnType "+
		 " )  "+

		 " union all "+

		 " Select ROrderNo, OrderDate, PartyName, OrderWeight, CountName, MixingDate, UnitName, RefOrderNo, UnitCode,"+
		 " 'Mixing' as Stock,YarnType,obastatus,Referenceno,Shadecardreforderno,Shadecardrefno from "+
		 " (    "+
		 " Select ROrderNo, OrderDate, PartyName, OrderWeight, CountName, MixingDate, UnitName, RefOrderNo, UnitCode, "+
		 " Process.GETBLOWROOMENTRYSTATUS_ASON(ROrderNo, UnitCode,"+iEnDate+") as BlowRoomCount,YarnType,obastatus"+
		 " ,Referenceno,Shadecardreforderno,Shadecardrefno from ( "+

		 " Select RegularOrder.ROrderNo, RegularOrder.OrderDate, PartyMaster.PartyName, YarnCount.CountName, OrderShade.ShadeCode, RegularOrder.Weight as "+
		 " OrderWeight,  Max(RMIXIR.MixingDate) as MixingDate, RMIXIR.MixingWeight, Max(SimplexStatus.CompDate) as SimplexCompDate,  "+  
		 " SpinningStatus.StartDate as SpinningStartDate, SpinningStatus.CompDate as SpinningCompDate,  ConeWindingStatus.CompDate as ConeWindingCompDate,  "+ 
		 " PackingStatus.CompDate as PackingCompDate,  Regularorder.DeliveryDate, nvl(Sum(BalePacking.NetWeight), 0) as PackingWeight, Unit.UnitName,  "+  
		 " RegularOrder.RefOrderNo, RegularOrder.PartyOrderNo, RegularOrder.UnitCode,FibreForm.FormName as YarnType,Oba.name as obastatus  "+  
		 " ,RegularOrder.Referenceno,ShadeCardNo.reforderno as Shadecardreforderno, ShadeCardNo.refno as Shadecardrefno"+
		 " from Regularorder  "+
		 " Left join partymaster on partymaster.partycode = regularorder.partycode  "+  
		 " inner join yarncount on yarncount.countcode = regularorder.countcode  "+  
		 " inner join unit on unit.unitcode =  RegularOrder.UnitCode  ";
		 if(!SUnit.equals("9999"))
		 {
			 QS = QS + "  and RegularOrder.unitcode in ("+SUnitCode+") ";
		 }
		
QS = QS +" Inner Join ProcessingType on ProcessingType.ProcessCode = RegularOrder.ProcessTypeCode  and ProcessingType.OeStatus = 0  "+ 
		 " Left Join Rmixir On Rmixir.OrdNo = RegularORder.ROrderNo  "+  
		 " Left join ordershade on ordershade.id = regularorder.shadeid  "+  
		 " Left Join Representative On Representative.Code = RegularOrder.RepresentativeCode  "+  
		 " Left Join SimplexStatus On SimplexStatus.OrderNo = RegularORder.ROrderNo   "+ 
		 " Left Join SpinningStatus On SpinningStatus.OrderNo = RegularORder.ROrderNo  "+ 
		 " Left Join ConeWindingStatus On ConewindingStatus.OrderNo = RegularORder.ROrderNo  "+  
		 " Left Join PackingStatus On PackingStatus.OrderNo = RegularORder.ROrderNo   "+ 
		 " Left Join Balepacking On Balepacking.Orderid = RegularORder.Orderid and Balepacking.Flag <> 1  "+  
		 " Left Join SampleToRegularAuth on SampleToRegularAuth.RegularOrderNo = RegularOrder.ROrderNo and nvl(SampleToRegularAuth.CorrectionMixing, 0) = 0  "+
		 " Left Join FibreForm On FibreForm.FormCode = RegularORder.FibreFormCode    "+
		 " Left Join FibreFormGroupBooking On FibreForm.FIBREFORMGROUPBOOKINGCODE = FibreFormGroupBooking.GroupCode    "+
		 " inner join Oba on Oba.code = regularorder.obacode "+
		 " left join ShadeCardNo on ShadeCardNo.OrderNo = regularorder.reforderno"+
		 " where RegularOrder.ORderDAte >= 20070123 "+
		 " and RegularOrder.OrderDate <= 99999999   "+
		 " and Nvl(PackingStatus.Status, 0) = 0    "+
		 " and Nvl(RMIXIR.CorrectionMixing,0) = 0    "+
		 " and Nvl(RMIXIR.MixingDate, to_Char(SysDate, 'YYYYMMDD')) >= to_char(to_date("+iEnDate+",'yyyymmdd'),'yyyymmdd')  "+  
		 " and ((RegularOrder.ORderType in (1, 2, 3, 9, 11, 12, 13, 14)) or  (RegularOrder.AuthUserCode = 3912))   "+ 
		 " and nvl(SampleToRegularAuth.Status, 0) = 1  "+
		 " group By RegularOrder.ROrderNo, RegularOrder.OrderDate, PartyMaster.Partyname, YarnCount.Countname, OrderShade.shadecode, RegularOrder.Weight,  "+  
		 " SpinningStatus.StartDate, SpinningStatus.CompDate, ConeWindingStatus.CompDate,  SpinningStatus.StartDate, PackingStatus.CompDate,   "+
		 " regularorder.deliverydate,  RMIXIR.MixingWeight, Unit.unitname, regularorder.RefOrderNo, RegularOrder.PartyOrderNo, RegularOrder.UnitCode,FibreForm.FormName,Oba.name       "+
		 " ,RegularOrder.Referenceno ,ShadeCardNo.reforderno,ShadeCardNo.refno"+
		 " Order by PartyMaster.PartyName, RegularOrder.orderdate, RegularOrder.rorderno    "+
		 " )  "+

		 " )  "+
		 " where nvl(BlowRoomCount, 0) = 0   "+

		 " ) "+
		 " Order by UnitCode, Stock, OrderDate, ROrderNo ";
		 
		 System.out.println("QS-->"+QS);


        try{
	    	if(theConnection==null) {
            	JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
            	theConnection       = jdbc.getConnection();
            }
            Statement stat      = theConnection.createStatement();
            ResultSet res       = stat.executeQuery(QS);
            
            while(res.next())
            {
		        VOrderNo          . addElement(common.parseNull(res.getString(1)));
		        VOrderdate        . addElement(common.parseNull(res.getString(2)));
		        VPartyName        . addElement(common.parseNull(res.getString(3)));
		        VOrdWeight        . addElement(common.parseNull(res.getString(4)));
		        VCountName        . addElement(common.parseNull(res.getString(5)));
		        VMixingDate       . addElement(common.parseNull(res.getString(6)));
		        VUnit 		      . addElement(common.parseNull(res.getString(7)));
		        VRefOrderNo       . addElement(common.parseNull(res.getString(8)));
		        VUnitCode         . addElement(common.parseNull(res.getString(9)));
		        VStock            . addElement(common.parseNull(res.getString(10)));
				VYarnType		  . addElement(common.parseNull(res.getString(11)));
				VOBAStatus		  . addElement(common.parseNull(res.getString(12)));
				if(res.getInt(13)==3 && !common.parseNull(res.getString(8)).equals("DIRECT"))
				{
					
					VDepth		  	  . addElement(common.parseNull(String.valueOf(getDepth(res.getString(14), res.getInt(15)))));
					
				}
				else if(res.getInt(13)==1 || res.getInt(13)==2)
				{
					
					VDepth		  	  . addElement(common.parseNull(String.valueOf(getDepth(res.getString(8), res.getInt(13)))));
				}
				else
				{
					VDepth		  	  . addElement("");
				}
				
            }
				
            res.close();
            //stat.close();
            
        
  		StringBuffer sb = new StringBuffer();
                     sb . append(" select regularorder.rorderno, regularorder.weight, ");
                     sb . append(" regularorder.countcode, yarncount.countname, regularorder.shadeid, yarnm.YSHNM, ");
                     sb . append(" regularorder.fibreformcode, fibreform.formname, regularorder.packcode, packingmaterial.packname, ");
                     sb . append(" regularorder.unitcode, regularorder.reforderno, regularorder.referenceno, ShadeCardNo.reforderno, ShadeCardNo.refno, yarncount.count, regularorder.obacode, Oba.name as obastatus, nvl(unit_changeslog.unit_from,0) as FromUnit, nvl(unit_changeslog.unit_to,0) as ToUnit, unit.unitname from regularorder ");
                     sb . append(" inner join yarncount on yarncount.countcode = regularorder.countcode ");
                     sb . append(" inner join Oba on Oba.code = regularorder.obacode ");
                     sb . append(" inner join unit on unit.unitcode = regularorder.unitcode ");
                     sb . append(" left join unit_changeslog on unit_changeslog.rorderno = regularorder.rorderno and to_char(changes_date,'yyyymmdd')=to_char(to_date("+iEnDate+",'yyyymmdd'),'yyyymmdd') ");
                     sb . append(" left join ShadeCardNo on ShadeCardNo.OrderNo = regularorder.reforderno ");
                     sb . append(" left join yarnm on yarnm.YSHCD = regularorder.shadeid ");
                     sb . append(" inner join fibreform on fibreform.formcode = regularorder.fibreformcode ");
                     sb . append(" inner join packingmaterial on packingmaterial.packcode = regularorder.packcode ");
                     sb . append(" inner join scm.processingtype on processingtype.processcode = regularorder.processtypecode ");
                     sb . append(" where to_char(to_date(entrydate,'dd.mm.yyyy hh24:mi:ss'),'yyyymmdd') = to_char(to_date("+iEnDate+",'yyyymmdd'),'yyyymmdd') ");
                     sb . append(" and processingtype.OESTATUS = 0 and regularorder.weight>50  and regularorder.rorderno not like  'DM%'  ");
                     sb . append(" order by regularorder.obacode desc,  regularorder.weight ");
        
        	
        	res   = stat.executeQuery(sb.toString());
            
            java.sql.ResultSetMetaData rsm = res.getMetaData();
            
            java.util.HashMap theMap = new java.util.HashMap();
            
            while(res.next()) {
            	theMap = new java.util.HashMap();
            	
            	for(int i=0;i<rsm.getColumnCount();i++) {
            		theMap.put(rsm.getColumnName(i+1),res.getString(i+1));
            	}

                if(res.getInt(13)==3 && !common.parseNull(res.getString(12)).equals("DIRECT")){
                    theMap .put("DEPTH", String.valueOf(getDepth(res.getString(14), res.getInt(15))));
                }else if(res.getInt(13)==1 || res.getInt(13)==2){
					
                    theMap .put("DEPTH", String.valueOf(getDepth(res.getString(12), res.getInt(13))));
                }
                
                thePlacedOrdersList.add(theMap);
            }
            res.close();
            stat.close();
        
        }catch(Exception ex){
            ex.printStackTrace();
        }        

    }
     
     
	 private double getDepth(String SRefOrderNo, int SRefNo){
        double dDepth = 0;
        String  SQs = "";
            if(SRefNo==1){
                SQs = " SELECT DEPTH FROM RMIXIR WHERE ORDNO = '"+SRefOrderNo+"' and  Correctionmixing = 0 "; 
            }
            if(SRefNo==2){
                SQs = " SELECT DEPTH FROM SMIXIR WHERE ORDNO = '"+SRefOrderNo+"' "; 
            }
		
				System.out.println("SQs-->"+SQs);
			
				
            
        try{
            java.sql.Statement st = theConnection.createStatement();
            java.sql.ResultSet rs = st . executeQuery(SQs);
            
            while(rs.next()){
                dDepth = rs.getDouble(1);
            }
            rs.close();
            st.close();
            
        }catch(Exception ex){
            ex.printStackTrace();
        }
        
        return dDepth;
    }     
    private void valueInt()
    {
        TLine    =0;
        iPage    =1;
        
        
    }

}
