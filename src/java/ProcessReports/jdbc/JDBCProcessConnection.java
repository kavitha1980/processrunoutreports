/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProcessReports.jdbc;

import java.sql.*;

public class JDBCProcessConnection  {
     
    static  JDBCProcessConnection   connect         = null;
            Connection              theConnection   = null;

            String                  SDriver         = "oracle.jdbc.OracleDriver";
            String                  SDSN            = "jdbc:oracle:thin:@172.16.2.28:1521:arun";

            String                  SUser           = "process";
            String                  SPassword       = "process";

    private JDBCProcessConnection() {

        try {
               Class.forName(SDriver);
               theConnection = DriverManager.getConnection(SDSN,SUser,SPassword);
		}
		catch(Exception e)
		{
               System.out.println("JDBCProcessConnection : "+e);
		}
	}

    public static JDBCProcessConnection getJDBCProcessConnection()
	{
		if (connect == null)
		{
               connect = new JDBCProcessConnection();
		}
		return connect;
	}

	public Connection getConnection()
	{
		return theConnection;
	}

	public void finalize()
	{
		if (theConnection != null)
		{
			try
			{
				theConnection.close();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}

}
