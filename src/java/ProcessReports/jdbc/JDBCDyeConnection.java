/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/*
     A Singleton Class for getting the Connection
     object of a Database.

     Change this when you port in a different RDBMS
     environment

*/

package ProcessReoprts.jdbc;

import java.sql.*;


public class JDBCDyeConnection
{
     
     static JDBCDyeConnection connect = null;

	Connection theConnection = null;

     String SDriver   = "oracle.jdbc.OracleDriver";
     String SDSN      = "jdbc:oracle:thin:@192.168.252.39:1521:amardye2";

     String SUser     = "DyeRaw";
     String SPassword = "DyeRaw";

     private JDBCDyeConnection()
	{
		try
		{
               Class.forName(SDriver);
               theConnection = DriverManager.getConnection(SDSN,SUser,SPassword);
		}
		catch(Exception e)
		{
               System.out.println("JDBCDyeConnection : "+e);
		}
	}

     public static JDBCDyeConnection getJDBCDyeConnection()
	{
		if (connect == null)
		{
               connect = new JDBCDyeConnection();
		}
		return connect;
	}

	public Connection getConnection()
	{
		return theConnection;
	}

	public void finalize()
	{
		if (theConnection != null)
		{
			try
			{
				theConnection.close();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}

}
