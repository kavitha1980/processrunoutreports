/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProcessReports.util;

/**
 *
 * @author Administrator
 */

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.*;

import ProcessReports.jdbc.*;

public class Common {
     public String bgColor,bgHead,bgUom,bgBody,fgHead,fgUom,fgBody;
     HashMap theMap;

     String S0To9[]   = {"Zero ","One ","Two ","Three ","Four ","Five ","Six ","Seven ","Eight ","Nine "};
     String S10To19[] = {"Ten ","Eleven ","Twelve ","Thirteen ","Fourteen ","Fifteen ","Sixteen ","Seventeen ","Eightteen ","Nineteen "};
     String TensArr[] = {"Twenty ","Thirty ","Forty ","Fifty ","Sixty ","Seventy ","Eighty ","Ninety "};

	 java.util.List theShadeList = null;
	 
     public Common()
     {
          bgColor = "#FEFCD8";
          bgHead  = "#DFD9F2";     // "#000080";
          bgUom   = "#FFB0B0";     // "#CCCCFF";
          bgBody  = "#9DECFF";
          fgHead  = "#000080";     // "#00FFFF";
          fgUom   = "#000080";
          fgBody  = "#000080";

          setMonthNames();
     }
     public String getRound(double dAmount,int iScale)
     {
          try
          {
               java.math.BigDecimal bd1 = new java.math.BigDecimal(String.valueOf(dAmount));
               return (bd1.setScale(iScale,4)).toString();
          }
          catch(Exception ex){}
          return "0";
     }



     public String getUpperRound(double dAmount,int iScale)
     {
          try
          {
               java.math.BigDecimal bd1 = new java.math.BigDecimal(String.valueOf(dAmount));
               return (bd1.setScale(iScale,3)).toString();
          }
          catch(Exception ex){}
          return "0";
     }

     public String getRound(String SAmount,int iScale)
     {
          try
          {
                  SAmount = SAmount.trim();
                  java.math.BigDecimal bd1 = new java.math.BigDecimal(SAmount);
                  return (bd1.setScale(iScale,4)).toString();
          }
          catch(Exception ex){}
          return "0";
     }

     public String getRoundCeiling(double dAmount,int iScale)
     {
          try
          {
               java.math.BigDecimal bd1 = new java.math.BigDecimal(String.valueOf(dAmount));
               return (bd1.setScale(iScale,bd1.ROUND_CEILING)).toString();
          }
          catch(Exception ex){}
          return "0";
     }

     public String Pad(String Str,int Len)
     {
            Str = ""+Str;
            if(Str.startsWith("null"))
               return Space(Len);

            if(Str.length() > Len)
                  return Str.substring(0,Len);
            else
                  return Str+Space(Len-Str.length());
     }

     public String Rad(String Str,int Len)
     {
          Str = ""+Str;
          if(Str.startsWith("null"))
               return Space(Len);

          if(Str.length() > Len)
               return Str.substring(0,Len);
          else
               return Space(Len-Str.length())+Str;
     }

     public String Space(int Len)
     {
            String RetStr="";
            for(int index=0;index<Len;index++)
                  RetStr=RetStr+" ";
            return RetStr;
     }

     public String Replicate(String Str,int Len)
     {
            String RetStr="";
            for(int index=0;index<Len;index++)
                  RetStr = RetStr+Str;
            return RetStr;
     }

     public String parseNull(String str)
     {
          String RetStr=str;
          str = ""+str;
          if(str.startsWith("null"))
               return "";
          return RetStr;
     }

     public String Cad(String str,int Len)
     {
          if(Len < str.length())
               return Pad(str,Len);

          String RetStr="";
          int FirstLen = (Len-str.length())/2;
          int SecondLen = Len-FirstLen;
          return Pad(Space(FirstLen)+str,Len);
     }

     public String parseDate(String str) 
     {
          str=parseNull(str);
          if(str.length()!=8)
               return str;
          try
          {
               return str.substring(6,8)+"."+str.substring(4,6)+"."+str.substring(0,4);
          }
          catch(Exception ex){}
          return "";
     }              
	public int getNextMonth(int iMonth)
	{
		iMonth = toInt(String.valueOf(iMonth).substring(0,6));
		String SMonth = String.valueOf(iMonth);
		String STmp = "";
		int iYear = toInt(SMonth.substring(0,4));
		int iNextMonth = iMonth+1;
		if(String.valueOf(iNextMonth).substring(4,6).equals("13"))
			STmp=String.valueOf(iYear+1)+"01";
		else
			STmp=String.valueOf(iNextMonth);
		return toInt(STmp);
	}

     public int[] getNextMonths(int iMonth,int iMany)  
     {
          int iMonthArr[] = new int[iMany];
          iMonth = toInt(String.valueOf(iMonth).substring(0,6));
          iMonthArr[0] = iMonth;
          for(int i=1;i<iMonthArr.length;i++)
          {
               iMonthArr[i]=getNextMonth(iMonth);
               iMonth=iMonthArr[i];
          }
          return iMonthArr;
     }
     public String pureDate(String str)
     {
          str=parseNull(str);

          str=str.trim();

          if(str.length()!=10)
               return "0";
          try
          {
               return str.substring(6,10)+str.substring(3,5)+str.substring(0,2);
          }
          catch(Exception ex){}
          return "";
     }

     public double toDouble(String str)
     {
          str = parseNull(str);
          try
          {
               return Double.parseDouble(str.trim());       
          }
          catch(Exception ex)
          {
               return 0;
          }
     }

     public double toDouble(int iNo)
     {
          String str = String.valueOf(iNo);

          try
          {
               return Double.parseDouble(str.trim());       
          }
          catch(Exception ex)
          {
               return 0;
          }
     }

     public int toInt(String str)
     {
          str = parseNull(str);
          try
          {
               return Integer.parseInt(str);       
          }
          catch(Exception ex)
          {
               return 0;
          }
     }

     public float toFloat(String str)
     {
          str = parseNull(str);
          try
          {
               return Float.parseFloat(str);       
          }
          catch(Exception ex)
          {
               return 0;
          }
     }

     public String getDateDiff(String str1,String str2)
     {
            int iYear1,iMonth1,iDay1;
            int iYear2,iMonth2,iDay2;

            try
            {
                  iYear1  = toInt(str1.substring(6,10))-1900;
                  iMonth1 = toInt(str1.substring(3,5))-1;
                  iDay1   = toInt(str1.substring(0,2));
            }
            catch(Exception ex)
            {
                  return "";
            }
            try
            {
                  iYear2  = toInt(str2.substring(6,10))-1900;
                  iMonth2 = toInt(str2.substring(3,5))-1;
                  iDay2   = toInt(str2.substring(0,2));
            }
            catch(Exception ex)
            {
                  return "";
            }

            java.util.Date dt1 = new java.util.Date(iYear1,iMonth1,iDay1);
            java.util.Date dt2 = new java.util.Date(iYear2,iMonth2,iDay2);
            return String.valueOf((dt1.getTime()-dt2.getTime())/(24*60*60*1000));
     }

     public int getDateDiff(int int1,int int2)
     {
            String str1 = "";
            String str2 = "";

            str1 = parseDate(String.valueOf(int1));
            str2 = parseDate(String.valueOf(int2));

            int iYear1,iMonth1,iDay1;
            int iYear2,iMonth2,iDay2;

            try
            {
                  iYear1  = toInt(str1.substring(6,10))-1900;
                  iMonth1 = toInt(str1.substring(3,5))-1;
                  iDay1   = toInt(str1.substring(0,2));
            }
            catch(Exception ex)
            {
                  return 0;
            }
            try
            {
                  iYear2  = toInt(str2.substring(6,10))-1900;
                  iMonth2 = toInt(str2.substring(3,5))-1;
                  iDay2   = toInt(str2.substring(0,2));
            }
            catch(Exception ex)
            {
                  return 0;
            }

            java.util.Date dt1 = new java.util.Date(iYear1,iMonth1,iDay1);
            java.util.Date dt2 = new java.util.Date(iYear2,iMonth2,iDay2);
            return toInt(String.valueOf((dt1.getTime()-dt2.getTime())/(24*60*60*1000)));
     }


     public String getDate(String str1,long days,int Sig)
     {
            String SDay="",SMonth="",SYear="";
            int iYear1=0,iMonth1=0,iDay1=0;
            try
            {
               iYear1  = toInt(str1.substring(6,10))-1900;
               iMonth1 = toInt(str1.substring(3,5))-1;
               iDay1   = toInt(str1.substring(0,2));
            }
            catch(Exception ex)
            {
               return "0";
            }
            java.util.Date dt1 = new java.util.Date(iYear1,iMonth1,iDay1);

            long num1 = dt1.getTime();
            long num2 = (days*24*60*60*1000);
            if (Sig == -1)
                  dt1.setTime(num1-num2);
            else
                  dt1.setTime(num1+num2);

            int iDay   = dt1.getDate();
            int iMonth = dt1.getMonth()+1;
            int iYear  = dt1.getYear()+1900;

            SDay   = (iDay<10 ?"0"+iDay:""+iDay);
            SMonth = (iMonth<10 ?"0"+iMonth:""+iMonth);
            SYear  = ""+iYear;
            return SDay+"."+SMonth+"."+SYear;
     }




     public int getNextDate(int int1)
     {
            String str1 = parseDate(String.valueOf(int1));
            String SDay="",SMonth="",SYear="";
            int iYear1=0,iMonth1=0,iDay1=0;
            try
            {
               iYear1  = toInt(str1.substring(6,10))-1900;
               iMonth1 = toInt(str1.substring(3,5))-1;
               iDay1   = toInt(str1.substring(0,2));
            }
            catch(Exception ex)
            {
               return 0;
            }
            java.util.Date dt1 = new java.util.Date(iYear1,iMonth1,iDay1);

            long num1 = dt1.getTime();
            long num2 = (24*60*60*1000);
             dt1.setTime(num1+num2);

            int iDay   = dt1.getDate();
            int iMonth = dt1.getMonth()+1;
            int iYear  = dt1.getYear()+1900;
			
			
            SDay   = (iDay<10 ?"0"+iDay:""+iDay);
            SMonth = (iMonth<10 ?"0"+iMonth:""+iMonth);
            SYear  = ""+iYear;
            return toInt(SYear+SMonth+SDay);
     }

	 
     public int getNextDate1(int int1)
     {
            String str1 = parseDate(String.valueOf(int1));
            String SDay="",SMonth="",SYear="";
            int iYear1=0,iMonth1=0,iDay1=0;
            try
            {
               iYear1  = toInt(str1.substring(6,10))-1900;
               iMonth1 = toInt(str1.substring(3,5))-1;
               iDay1   = toInt(str1.substring(0,2));
			   
			   System.out.println("substr:"+iYear1);
			   System.out.println("substr:"+iMonth1);
			   System.out.println("substr:"+iDay1);
            }
            catch(Exception ex)
            {
               return 0;
            }
            java.util.Date dt1 = new java.util.Date(iYear1,iMonth1,iDay1);

            long num1 = dt1.getTime();
            long num2 = (24*60*60*1000);
             dt1.setTime(num1+num2);

            int iDay   = dt1.getDate();
            int iMonth = dt1.getMonth()+1;
            int iYear  = dt1.getYear()+1900;
			
			System.out.println("dt1:"+iDay);
			System.out.println("dt1:"+iMonth);
			System.out.println("dt1:"+iYear);
			
			if(iDay==iDay1) {
				iDay= iDay+1;
				System.out.println("dt1 euals:"+iDay);
			}

            SDay   = (iDay<10 ?"0"+iDay:""+iDay);
            SMonth = (iMonth<10 ?"0"+iMonth:""+iMonth);
            SYear  = ""+iYear;
            return toInt(SYear+SMonth+SDay);
     }
	 
	 

     public String getCurrentDate()
     {
            String SDay,SMonth,SYear;

            java.util.Date dt    = new java.util.Date();
            int iDay   = dt.getDate();
            int iMonth = dt.getMonth()+1;
            int iYear  = dt.getYear()+1900;

            if(iDay < 10)
               SDay = "0"+iDay;
            else
               SDay = String.valueOf(iDay);
     
            if(iMonth < 10)
               SMonth = "0"+iMonth;
            else
               SMonth = String.valueOf(iMonth);

            SYear = String.valueOf(iYear);

            return(SYear+SMonth+SDay);
     }

     public int getIntCurrentDate()
     {

           return(toInt(getCurrentDate()));
     }

     public int indexOf(Vector vect,String str)
     {
           int iindex=-1;
           str = str.trim();
           for(int i=0;i<vect.size();i++)
           {
                  String str1 = (String)vect.elementAt(i);
                  str1 = str1.trim();
                  if(str1.startsWith(str))
                        return i;
           }
           return iindex;
     }

     public int exactIndexOf(Vector vect,String str)
     {
           int iindex=-1;
           str = str.trim();

           for(int i=0;i<vect.size();i++)
           {
                  String str1 = (String)vect.elementAt(i);
                  str1 = str1.trim();
                  if(str1.equals(str))
                        return i;
           }
           return iindex;
     }

     // SOp - operation : "S" - SUM; "A" - Average
     public String getSum(Vector Vx,String SOp)
     {
          
          double dAmount=0.0;   
          for(int i=0;i<Vx.size();i++)
          {
               dAmount = dAmount+toDouble((String)Vx.elementAt(i));
          }
          if(SOp.equals("A"))
          {
               dAmount = dAmount/Vx.size();
          }
          return getRound(dAmount,3);
     }


     public String  getDate()
     {
          java.util.Date date = new java.util.Date();

          int iDay   = date.getDate();
          int iMonth = date.getMonth()+1;
          String sYear  = String.valueOf(date.getYear()+1900);

          String sDay    = (iDay>=10?""+iDay:"0"+iDay);
          String sMonth  = (iMonth>=10?""+iMonth:"0"+iMonth);

          String sDate  = sYear+sMonth+sDay;

          return sDate;
     }
     public String getTime()
     {
          java.util.Date dt        = new java.util.Date();

          int iHrs       = dt.getHours();
          int iMin       = dt.getMinutes();

          String sHrs    =(iHrs<10?"0"+iHrs:""+iHrs);
          String sMin    =(iMin<10?"0"+iMin:""+iMin);

          return sHrs+":"+sMin;
     }
     public String getUser()
     {
          return "";
     }
     public String getFinancePeriod()
     {
          return "";
     }

     public String stuff(String str,int iLen)
     {

          for(;str.length()<iLen;)
          {
               str="0"+str;
          }
          return str;
     }
     public String getMonthName(int iMonth)
     {
                String SArr[] = {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};
				String SMonth = null;
                try
                {
                    SMonth = String.valueOf(iMonth);
                    iMonth = toInt(SMonth.substring(4,6))-1;
                    int iYear  = toInt(SMonth.substring(0,4));  
                    SMonth = SArr[iMonth]+" "+iYear;
                }
                catch(Exception ex)
                {
                    return "";
                }
                return SMonth;
      }

     private void setMonthNames()
     {
          theMap = new HashMap();
          theMap.put("01","JAN");
          theMap.put("02","FEB");
          theMap.put("03","MAR");
          theMap.put("04","APR");
          theMap.put("05","MAY");
          theMap.put("06","JUN");
          theMap.put("07","JUL");
          theMap.put("08","AUG");
          theMap.put("09","SEP");
          theMap.put("10","OCT");
          theMap.put("11","NOV");
          theMap.put("12","DEC");
     }

     public String getMonthYear(int iYearMonth)
     {
          String str = "";
          String SYearMonth=String.valueOf(iYearMonth);
          str = theMap.get(SYearMonth.substring(4,6))+"_"+SYearMonth.substring(0,4);
          return str;
     }


        public int getPreviousMonth(int iMonth)
        {
		String SMonth = null;
		SMonth = String.valueOf(iMonth);
		int iYear = toInt(SMonth.substring(0,4));
		String SPreMonth = String.valueOf(iMonth-1);
		if(SPreMonth.substring(4,6).equals("00"))
			SPreMonth=String.valueOf(iYear-1)+"12";
		return toInt(SPreMonth);
        }

	public int getPreviousMonth(int iMonth,int iMany)
	{
		int iTemp = iMonth;
                for(int i = 0; i < iMany; i++)
                {
                        iTemp = getPreviousMonth(iTemp);
                }
                return iTemp;
	}

        public int[] getPreviousMonths(int iMonth,int iMany)  
        {
                int iMonthArr[] = new int[iMany];
                for(int i=0;i<iMonthArr.length;i++)
                        iMonthArr[i]=0;
                for(int i=0;i<iMonthArr.length;i++)
                {
                        iMonthArr[i]=getPreviousMonth(iMonth);
                        iMonth=iMonthArr[i];
                }
                return iMonthArr;
        }

        public String[] getMonths(int iMonthArr[])
        {
                String SMonthArr[] = new String[iMonthArr.length];
                for(int i=0;i<SMonthArr.length;i++)
                        SMonthArr[i]=getMonthName(iMonthArr[i]);
                return SMonthArr;
        }
        //Returns a date less by iMonths from iDate (in pure form)
        public int getPreviousDate(int iDate,int iStMonth)
        {
                int iRetDate = iDate;
                if(iStMonth==0)
                        return iRetDate;
                if(iStMonth<0)
                        return 0;                         

                String SDay   = (String.valueOf(iDate)).substring(6,8);
                String SMonth = (String.valueOf(iDate)).substring(4,6);
                String SYear  = (String.valueOf(iDate)).substring(0,4);

                if((toInt(SMonth)-iStMonth)<=0)
                {
                        SYear    = String.valueOf(toInt(SYear)-1);
                        int iMon = 12-(iStMonth-toInt(SMonth));
                        SMonth   = iMon<=9?"0"+String.valueOf(iMon):String.valueOf(iMon);
                        iRetDate = toInt(SYear+SMonth+SDay);
                }
                else
                {
                        int iMon = toInt(SMonth)-iStMonth;
                        SMonth   = iMon<=9?"0"+String.valueOf(iMon):String.valueOf(iMon);
                        iRetDate = toInt(SYear+SMonth+SDay);                        
                }
                return iRetDate;
        }

	public int getNextMonth(int iMonth,int iNoOfMonth)
	{
		int iTMonth = iMonth;
		for (int i = 1; i <= iNoOfMonth; i++)
			iTMonth = getNextMonth(iTMonth);
		return iTMonth;
	}


	public String getDayOfDate(int iDate)
        {
                if(String.valueOf(iDate).length()<8)
                    return "000";
                String[] retString = {"Sun","Mon","Tue","Wed","Thu","Fri","Sat"};
                String SYear  = String.valueOf(toInt((String.valueOf(iDate)).substring(0,4))-1900);
                String SMonth = String.valueOf(toInt((String.valueOf(iDate)).substring(4,6))-1);
                String SDate  = (String.valueOf(iDate)).substring(6,8);

                java.util.Date DT = new java.util.Date(toInt(SYear),toInt(SMonth),toInt(SDate));
                return retString[DT.getDay()]; 
        }



     public int getNextMonthYear(int iYearMonth)
     {
          int nextMonth = 0;
          String SYearMonth=String.valueOf(iYearMonth);
          String SYear  = SYearMonth.substring(0,4);
          String SMonth = SYearMonth.substring(4,6);
          
          if(toInt(SMonth)>0 && toInt(SMonth)<9)
               nextMonth=toInt( SYear +"0"+String.valueOf( toInt(SMonth)+1 ) );
          else if(toInt(SMonth)>8 && toInt(SMonth)<12)
               nextMonth=toInt( SYear + String.valueOf( toInt(SMonth)+1 ) );
          else if(toInt(SMonth)==12)
               nextMonth=toInt( String.valueOf(toInt(SYear)+1) + "01");

          return nextMonth;
     }


     public int getQuaterMonthYear(int iYearMonth)
     {
          int nextMonth = 0;
          String SYearMonth=String.valueOf(iYearMonth);
          String SYear  = SYearMonth.substring(0,4);
          String SMonth = SYearMonth.substring(4,6);
          
          if(toInt(SMonth)>0 && toInt(SMonth)<9)
               nextMonth=toInt( SYear +"0"+String.valueOf( toInt(SMonth)+3 ) );
          else if(toInt(SMonth)>8 && toInt(SMonth)<12)
               nextMonth=toInt( SYear + String.valueOf( toInt(SMonth)+3 ) );
          else if(toInt(SMonth)==12)
               nextMonth=toInt( String.valueOf(toInt(SYear)+1) + "01");

          return nextMonth;
     }


     public String getRupee(String str)
     {
          str = getRound(str,2);
          int  lRupee=0;
          int  iPaise=0;
          java.util.StringTokenizer ST = new java.util.StringTokenizer(str,".");
          while(ST.hasMoreTokens())
          {
               lRupee=toInt(ST.nextToken());
               iPaise=toInt(ST.nextToken());
          }
          String SRs = (lRupee > 0 ? getCrores(lRupee):"Zero");
          String SPs = (iPaise > 0 ? " and paise "+getTens(iPaise):"");
          return "Rupees "+SRs+SPs+"Only";
     }

     private String getCrores(int i)
     {
          if(i<10000000)
               return getLacs(i);
          String str="";
          int j=0,k=0;
          if(i<=99999999)
          {
               str   = String.valueOf(i);
               j        = toInt(str.substring(0,1));
               k        = toInt(str.substring(1,8));
               return getSingle(j)+"Crore "+getLacs(k);
          }
          str   = String.valueOf(i);
          j        = toInt(str.substring(0,2));
          k        = toInt(str.substring(2,9));
          return getTens(j)+"Crore "+getLacs(k);
     }

     private String getLacs(int i)
     {
          if(i<100000)
               return getThousands(i);

          String str="";
          int j=0,k=0;
          if(i<=999999)
          {
               str   = String.valueOf(i);
               j        = toInt(str.substring(0,1));
               k        = toInt(str.substring(1,6));
               return getSingle(j)+"Lac "+getThousands(k);
          }
          str   = String.valueOf(i);
          j        = toInt(str.substring(0,2));
          k        = toInt(str.substring(2,7));
          return getTens(j)+"Lac "+getThousands(k);
     }

     private String getThousands(int i)
     {
          if(i<1000)
               return getHundreds(i);

          String str="";
          int j=0,k=0;
          if(i<=9999)
          {
               str   = String.valueOf(i);
               j        = toInt(str.substring(0,1));
               k        = toInt(str.substring(1,4));
               return getSingle(j)+"Thousand "+getHundreds(k);
          }
          str   = String.valueOf(i);
          j        = toInt(str.substring(0,2));
          k        = toInt(str.substring(2,5));
          return getTens(j)+"Thousand "+getHundreds(k);
     }

     private String getHundreds(int i)
     {
          if(i<100)
               return getTens(i);

          String str="";
          int j=0,k=0;
          str   = String.valueOf(i);
          j        = toInt(str.substring(0,1));
          k        = toInt(str.substring(1,3));
          return getSingle(j)+"Hundred "+getTens(k);
     }

     private String getTens(int i)
     {
          if(i==0)
               return "";
          if(i >= 0 && i <= 9)
               return getSingle(i);
          if(i >= 10 && i <= 19)
               return S10To19[i-10];

          String str   = String.valueOf(i);
          int j        = toInt(str.substring(0,1))-2;
          int k        = toInt(str.substring(1,2));
          return TensArr[j]+(k>0?getSingle(k):"");
     }

     private String getSingle(int i)
     {
          return S0To9[i];          
     }

     public static void main(String[] args)
     {
          Common common = new Common();

          try
          {

          System.out.println(common.getIntCurrentDate());
               
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     public String getServerDate() throws Exception
     {

          Connection theConnection = null;
          String SServerDate       = "";

          if(theConnection == null)
          {
               JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
               theConnection        = jdbc.getConnection();
          }

          Statement stat   = theConnection.createStatement();
          ResultSet result = stat.executeQuery(getDateQS());
          if(result.next())
               SServerDate = result.getString(1);
          result.close();
          stat.close();

          return SServerDate;
     }
     public String getServerTime() throws Exception
     {

          Connection theConnection = null;
          String SServerTime       = "";

          if(theConnection == null)
          {
               JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
               theConnection        = jdbc.getConnection();
          }

          Statement stat   = theConnection.createStatement();
          ResultSet result = stat.executeQuery("select to_char(sysDate,'hh24:mi:ss')  from dual");
          if(result.next())
               SServerTime = result.getString(1);
          result.close();
	  stat.close();

          return SServerTime;
     }


     public Vector formDateVector(String SStDate,String SEnDate)
     {
          Vector retVect = new Vector();
          if(toInt(SEnDate)<toInt(SStDate))
               return retVect;

          String STemp   = SStDate;
          retVect.addElement(SStDate);
          while(toInt(SEnDate)>toInt(STemp))
          {
               STemp  = pureDate(getDate(parseDate(STemp),1,1));
               retVect.addElement(STemp);
          }
          return retVect;
     }


     public String getSerDateTimeQS()
     {
          Connection theConnection = null;

          String SDate = "";
          try
          {
               String QS = "Select to_Char(Sysdate,'YYYYMMDD HH24:MI:SS') From Dual";
               if(theConnection == null)
               {
                    JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement stat   = theConnection.createStatement();

               ResultSet res    = stat.executeQuery(QS);
               while(res.next())
               {
                    SDate = res.getString(1);
               }
		res.close();
		stat.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return SDate;
     }

	public String getSerDateTime()
     {
          Connection theConnection = null;

          String SDate = "";
          try
          {
               String QS = "Select to_Char(Sysdate,'DD.MM.YYYY HH24:MI:SS') From Dual";
               if(theConnection == null)
               {
                    JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement stat   = theConnection.createStatement();

               ResultSet res    = stat.executeQuery(QS);
               while(res.next())
               {
                    SDate = res.getString(1);
               }
		res.close();
		stat.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return SDate;
     }



     private String getDateQS()
     {
          String QS = "Select to_Char(sysdate,'yyyymmdd') from Dual";
          return QS;
     }

     public String setDueDays(String SDate)
     {
          java.util.Date dt    = new java.util.Date();

          int iDay   = dt.getDate()+toInt(SDate);
          int iMonth = dt.getMonth()+1;
          int iYear  = dt.getYear()+1900;
          
          String SDay,SMonth,SYear;
          
          if(iDay < 10)
               SDay = "0"+iDay;
          else
               SDay = String.valueOf(iDay);
          
          if(iMonth < 10)
               SMonth = "0"+iMonth;
          else
               SMonth = String.valueOf(iMonth);

          SYear = String.valueOf(iYear);

          java.util.Date dt1 = new java.util.Date(toInt(SYear)-1900,toInt(SMonth)-1,toInt(SDay));
          
          int iDay1  = dt1.getDate();
          int iMonth1= dt1.getMonth()+1;
          int iYear1 = dt1.getYear()+1900;
          
          String SDay1,SMonth1,SYear1;
          
          SDay1  = (iDay1<10 ?"0"+iDay1:""+iDay1);
          SMonth1= (iMonth1<10 ?"0"+iMonth1:""+iMonth1);
          SYear1 = ""+iYear1;
          
          String SDate1 = SYear1+SMonth1+SDay1;

          return SDate1;
     }

     public String getDate1(String str1,long days,int Sig)
     {
          String SDay="",SMonth="",SYear="";
          int iYear1=0,iMonth1=0,iDay1=0;
          try
          {
               iYear1  = toInt(str1.substring(0,4))-1900;
               iMonth1 = toInt(str1.substring(4,6))-1;
               iDay1   = toInt(str1.substring(6,8));
          }
          catch(Exception ex)
          {
               return "0";
          }

          java.util.Date dt1 = new java.util.Date(iYear1,iMonth1,iDay1);

          long num1 = dt1.getTime();
          long num2 = (days*24*60*60*1000);
          if (Sig == -1)
               dt1.setTime(num1-num2);
          else
               dt1.setTime(num1+num2);

          int iDay   = dt1.getDate();
          int iMonth = dt1.getMonth()+1;
          int iYear  = dt1.getYear()+1900;

          SDay   = (iDay<10 ?"0"+iDay:""+iDay);
          SMonth = (iMonth<10 ?"0"+iMonth:""+iMonth);
          SYear  = ""+iYear;
          return SDay+"."+SMonth+"."+SYear;
     }

     public String getPrevDate(String SDate)
     {
          return pureDate(getDate(parseDate(SDate),1,-1));
     }
    public int getMonths(int iStartMonth,int iEndMonth)
    {
        int iCntr=0;

        while(iStartMonth<=iEndMonth)
        {
            iCntr++;
            int iMonth = getNextMonth(iStartMonth);
            iStartMonth=iMonth;
        }
        return iCntr;
     }

     public boolean isLeap(int iYear)
     {
          double dYear = iYear;
          if(Math.IEEEremainder(dYear,100.0)==0)
               return true;
          else if(Math.IEEEremainder(dYear,4.0)==0)
               return true;
          else
               return false;
 
     }

     public String getPreviousDate(String SDate)
     {
          return pureDate(getDate(parseDate(SDate),1,-1));
     }
	 public String getPreviousDate(String SDate,int iNoOfDays)
     {
          return pureDate(getDate(parseDate(SDate),1,-iNoOfDays));
     }


public String getTimeDiffNew(String SStartDate, String SStartTime, String SEndDate, String SEndTime)
     {
          double dTimeDiff                   = 0;
          String SH                          = "",SM = "";

          StringBuffer SB                    = null;
          SB                                 = new StringBuffer();

          SB.append(" Select (to_Date('"+SEndDate+" "+SEndTime+"','YYYYMMDD HH24:MI') - ");
          SB.append(" to_Date('"+SStartDate+" "+SStartTime+"','YYYYMMDD HH24:MI')) * 24 * 60 from Dual ");
          
          System.out.println("Qry-->"+SB.toString());

          try
          {
               JDBCConnection theConnect     = JDBCConnection.getJDBCConnection();

               Connection theConnection      = theConnect.getConnection();
               Statement theStatement        = theConnection.createStatement();
               ResultSet theResult           = theStatement.executeQuery(SB.toString());

               if(theResult.next())
               {
                     dTimeDiff               = toDouble(theResult.getString(1));
               }

               theResult                     . close();
               theStatement                  . close();
               //theConnection .close();
               theConnection=null;
          }
          catch(Exception ex)
          {
               return "";
          }

          double dHours  = dTimeDiff / 60.0;
          double dMins   = dTimeDiff % 60.0;

          double dActHrs = Math.floor(dHours);

          if(dHours <= 9.0)
          {
               SH        = "0"+getRound(dActHrs,0);
          }
          else
          {
               SH        = getRound(dActHrs,0);
          }

          if(dMins <= 9.0)
          {
               SM        = "0"+getRound(dMins,0);
          }
          else
          {
               SM        = getRound(dMins,0);
          }

          return  SH+":"+SM;
     }
 //Newly Added
      public String getMonthName(String SMonth1)
        {
                       
                String SArr[] = {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};
                String SMonth = null;
                int iMonth;
                try
                {
                     iMonth = Integer.parseInt(SMonth1);
           
                     SMonth = String.valueOf(iMonth);
                    iMonth = toInt(SMonth.substring(4,6))-1;
                    int iYear  = toInt(SMonth.substring(0,4));  
                    SMonth = SArr[iMonth]+" "+iYear;
                }
                catch(Exception ex)
                {
                    if(SMonth.length()==4)
                         return "EL'"+SMonth;

                    return "";
                }
                return SMonth;
        }

    //gate

       public String getTimeDiff1(int iOutDate,int iInDate,String SOutTime,String SInTime)
     {
          String STimeDiff="";
          String SH="",SM="",SS="";

          String QS="     SELECT  "+
                    "     TO_CHAR(INT,'YYYYMMDD HH:MI:SS AM') AS IT,"+
                    "     TO_CHAR(OT,'YYYYMMDD HH:MI:SS AM') AS O,"+
                    "     OT-INT AS DIFF, Round(((OT-INT)* 24*60*60),0) AS DIFFSECONDS FROM "+
                    "     (SELECT   "+
                    "     TO_DATE(CONCAT("+iInDate+"||' ','"+SInTime+"'),'YYYYMMDD HH:MI:SS AM') AS INT, "+
                    "     TO_DATE(CONCAT("+iOutDate+"||' ','"+SOutTime+"'),'YYYYMMDD HH:MI:SS AM') AS OT "+
                    "     FROM DUAL ) ";

          try
          {
               Class.forName("oracle.jdbc.OracleDriver");
               Connection   theConnection     = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","gate","gatepass");
			Statement theStatement = theConnection.createStatement();
               ResultSet theResult = theStatement.executeQuery(QS);
               if(theResult.next())
               {
                     STimeDiff = (String) theResult.getString(4);
               }


               theStatement.close();
          }
          catch(Exception ex)
          {
               return "";
          }
          int iTimeDiff  = Integer.parseInt(STimeDiff);

          if(iTimeDiff>0)
          {
               SH = String.valueOf(iTimeDiff/3600);

               iTimeDiff = (iTimeDiff%3600);

               if(iTimeDiff>60)
               {
                    SM = String.valueOf(iTimeDiff/60);
                    iTimeDiff = (iTimeDiff%60);

                    SS = String.valueOf(iTimeDiff);
               }
               else 
               {
                    SM = String.valueOf(iTimeDiff);
                    SS = String.valueOf(0);
               }
          }
          else
          {
                    SH   = String.valueOf(iTimeDiff);
                    SM   = String.valueOf(0);
                    SS   = String.valueOf(0);
          }

          if(SH.length()<2)
               SH   = "0"+SH;

          if(SM.length()<2)
               SM   = "0"+SM;

          if(SS.length()<2)
               SS   = "0"+SS;

          return  SH+":"+SM+":"+SS;
     }

    public int getPreviousDate(int iDate)
     {
          return toInt(pureDate(getDate(parseDate(String.valueOf(iDate)),1,-1)));
     }


    public java.util.List getShadeLines(String Shade,int iLength){
        theShadeList = new java.util.ArrayList();
        int iLen  = 0;
        int iLen1 = 0;
        int iLen2 = 0;

        java.math.BigDecimal  bd1,bd2;
                    bd1 = new java.math.BigDecimal(Shade.length());
                    bd2 = new java.math.BigDecimal(iLength);
            iLen =  bd1 . divide(bd2,java.math.RoundingMode.UP).intValue();
            
            iLen1 = 0;
            iLen2 = iLength;

            for(int i=0;i<iLen;i++){
                if(i==(iLen-1)){
                    theShadeList.add(Shade.substring(iLen1));
                }else{
                    theShadeList.add(Shade.substring(iLen1, iLen2));
                    iLen1 = iLen2;
                    iLen2 += iLength; 
                }
            }

        return theShadeList;
    }

    //Newly Added

	public String getMonthEndDate(String SDate)
     {
            String retStr = "";
            int    iMonth = toInt(SDate.substring(4,6));
            int    iYear  = toInt(SDate.substring(0,4));
            String SDay;
            switch (iMonth)
            {
                  case 1:
                        SDay="31";
                        break;   
                  case 2:
                       // if(isLeap(iYear)==1)
						 if(isLeap(iYear))
                           SDay="29";
                        else
                           SDay="28";
                        break;   
                  case 3:
                        SDay="31";
                        break;   
                  case 4:
                        SDay="30";
                        break;   
                  case 5:
                        SDay="31";
                        break;   
                  case 6:
                        SDay="30";
                        break;   
                  case 7:
                        SDay="31";
                        break;   
                  case 8:
                        SDay="31";
                        break;   
                  case 9:
                        SDay="30";
                        break;   
                  case 10:
                        SDay="31";
                        break;   
                  case 11:
                        SDay="30";
                        break;   
                  case 12:
                        SDay="31";
                        break;
                  default:
                        SDay="30";
                        break;
            }
            return (SDate.substring(0,6)+SDay);
     } 
	
	public int getNoOfDaysInMonth(String SDate)
     {
            String retStr = "";
            int    iMonth = toInt(SDate.substring(4,6));
            int    iYear  = toInt(SDate.substring(0,4));
            int iDay;
            switch (iMonth)
            {
                  case 1:
                        iDay=31;
                        break;   
                  case 2:
                       // if(isLeap(iYear)==1)
						 if(isLeap(iYear))
                           iDay=29;
                        else
                           iDay=28;
                        break;   
                  case 3:
                        iDay=31;
                        break;   
                  case 4:
                        iDay=30;
                        break;   
                  case 5:
                        iDay=31;
                        break;   
                  case 6:
                        iDay=30;
                        break;   
                  case 7:
                        iDay=31;
                        break;   
                  case 8:
                        iDay=31;
                        break;   
                  case 9:
                        iDay=30;
                        break;   
                  case 10:
                        iDay=31;
                        break;   
                  case 11:
                        iDay=30;
                        break;   
                  case 12:
                        iDay=31;
                        break;
                  default:
                        iDay=30;
                        break;
            }
            return iDay;
     } 

}
