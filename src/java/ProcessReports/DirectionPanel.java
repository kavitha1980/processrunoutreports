/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProcessReports;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Administrator
 */
public class DirectionPanel extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try  {
            PrintWriter out = response.getWriter();
            /* TODO output your page here. You may use following sample code. */
          out.println("<html>");
          out.println("<head>");
          out.println("<base target='main'>");
          out.println("</head>");

          out.println("<body bgcolor='#C0C0C0' link='#003366' vlink='#005500' alink='#002346'>");
          out.println("<div align='left'>");
          out.println("  <table border='0' width='270' bgcolor='#C0C0C0' cellspacing='0' cellpadding='4'>");

          out.println("    <tr>");
          out.println("      <td width='228' height='29'><font color='#990033' size='4'><b>Process Reports</b></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#003366'><a href='OrderAllocationStockCrit'><b>Order Allocation Stock Details </b></a></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#003366'><a href='RegularOrderSampleStatusCritNew'><b>Regular Order Sample Status Report(New)</b></a></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#003366'><a href='YarnTypeWise_CountCrit'><b>Yarn Type Wise Count Qty</b></a></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#003366'><a href='RegMixingPendingCritNew'><b>Regular Order Mixing Pending Unitwise(New)</b></a></font></td>");
          out.println("    </tr>");


          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#003366'><a href='ProcessSimplexRunoutNewCrit'><b>Simplex RunOut And FollowUp(A & B-Unit-Prep. Setting)</b></a></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#003366'><a href='ProcessSimplexRunoutReportCrit'><b>Simplex RunOut And FollowUp(A & B-Unit Process)</b></a></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#003366'><a href='ProcessSpinningRunoutCrit'><b>Spinning RunOut </b></a></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#003366'><a href='ProcessConeWindingRunOutCritNew'><b>Cone Winding RunOut </b></a></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#003366'><a href='OrderWiseExcessShortageCritNew'><b>Order Excess Shortage As On <font color ='FF0066'>**</b></a></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#003366'><a href='DrawingClothCheckDetailsCrit'><b>Drawing Cloth Check Details<font color ='FF0066'>**</b></a></font></td>");
          out.println("    </tr>");
          out.println("  </table>");
          out.println("</div>");
          out.println("</body>");
          out.println("</html>");

          out.close();
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
