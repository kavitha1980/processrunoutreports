/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProcessReports;

import ProcessReports.jdbc.*;
import ProcessReports.util.*;
import java.io.*;
import java.sql.*;           
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

/**
 *
 * @author Administrator
 */
public class YarnTypeWise_CountCrit extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */

    HttpSession    session;
    Common         common = new Common();
    Vector         VUnitCode,VUnitName;
    JDBCConnection theConnect;
    Connection     connect;
    String         SToday  = "";

    Vector         VOrderType,VOrderTypeCode;

    ArrayList      AProcessTypeCode, AProcessTypeName;
    java.util.List AShortageTypeCode, AShortageTypeName;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        SToday= common.parseDate(common.getCurrentDate());
        try {
                        response. setContentType("text/html");
            PrintWriter out     = response.getWriter();
                        session = request.getSession(false);
            String      SServer = (String)session.getValue("Server");
            setDataintoVector();

            out.println("<html>");
            out.println("<body bgcolor='#9AA8D6'>");
            out.println("");
            out.println("<base target='topmain'>");
            out.println("<form method='GET' action='YarnTypeWise_CountQty'>");

            out.println("<table>");

            out.println("</tr>");
            out.println("<tr>");
            out.println("<td><font color='#FFFF66'><b>As On Date: </b></font></td>");
            out.println("     <td><input type='text' name='TEnDate'value="+SToday+" size='12'></td>");

            out.println("       <td><font color='#FFFF66'><b>Order Type</b></font></td>");
            out.println("       <td><select name='shortageType'>");
            out.println("            <option value='All#All'>All</option>");
            for(int i=0;i<AShortageTypeCode.size();i++) {
                out.println("       <option value='"+AShortageTypeCode.get(i)+"#"+AShortageTypeName.get(i)+"'>"+AShortageTypeName.get(i)+"</option>");
            }
            out.println("  </select></td>");

            out.println("       <td><select name='orderType'>");
            out.println("            <option value='All'>All</option>");
            out.println("            <option value='Depo Orders'>Depo Orders</option>");
            out.println("            <option value='Party Orders'>Party Orders</option>");
            out.println("  </select></td>");
            out.println("</tr>");
            out.println("<tr>");
            out.println("<td><font color='#FFFF66'><b>Unit: </b></font></td>");

            out.println("      <td><select size=1 name=Unit>");
            out.println("      <option value='All'>All</option>");
            for(int i=0;i<VUnitCode.size();i++) {
                out.println("      <option value='"+VUnitName.elementAt(i)+"'>"+VUnitName.elementAt(i)+"</option>");
            }
            out.println("      </select></td>");

            out.println("<td><font color='#FFFF66'><b>Process Type : </b></font></td>");

            out.println("      <td><select size=1 name='processType'>");
            out.println("      <option value='All#All'>All</option>");
            out.println("      <option value='BR-C Orders#BR-C Orders'>BR-C Orders</option>");
            out.println("      <option value='Other than BR-C Orders#Other than BR-C Orders'>Other than BR-C Orders</option>");
            for(int i=0;i<AProcessTypeCode.size();i++)  {
                out.println("      <option value='"+AProcessTypeCode.get(i)+"#"+AProcessTypeName.get(i)+"'>"+AProcessTypeName.get(i)+"</option>");
            }
            out.println("      </select></td>");
            out.println("<td><font color='#FFFF66'><b>Fibre Dyeing : </b></font></td>");

            out.println("      <td><select size=1 name='Fibredyeing'>");
            out.println("      <option value='All'>All</option>");
            out.println("      <option value='Fibre Dyeing'>Fibre Dyeing</option>");
            out.println("      </select></td>");
            out.println("      </tr>");

            out.println("     <tr> ");
            out.println("      <td><font color='#FFFF66'><b>OrderType: </b></font></td><td>");
            out.println("      <select size=1 name='ordertype'>");
            out.println("      <option value='9999'>All</option>");
            out.println("      <option value='1234'>A1234</option>");
            out.println("      <option value='1000'>Local and OutStation</option>");
            out.println("      <option value='1001' selected='true'>Local and OutStation And Export</option>");
            for(int i=0;i<VOrderType.size();i++)    {
                out.println("      <option value='"+VOrderTypeCode.elementAt(i)+"'>"+VOrderType.elementAt(i)+"</option>");
            }
            out.println("  </td>    </select>");

            out.println("<td><font color='#FFFF66'><b>From Weight: </b></font></td>");
            out.println("<td><input type='text' name='frmweight' value='' size='12'></td>");
            out.println("<td><font color='#FFFF66'><b>To Weight: </b></font></td>");
            out.println("<td><input type='text' name='toweight' value='' size='12'></td>");

            out.println("</tr>");
            out.println("     <td><input type='submit' value='Submit' name='B1' style='color: #003300; font-size: 10pt; font-weight: bold'></td>");
            out.println("     <td><input type='reset' value='Reset' name='B2' style='color: #800000; font-size: 10pt; font-weight: bold'></td>");
            out.println("     </tr> ");

            out.println("</table>");
            out.println("</form>");
            out.println("</body>");
            out.println("</html>");

            out.close();

        } catch(Exception ex){
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    public void setDataintoVector() {

        VUnitCode    =new Vector();
        VUnitName    =new Vector();

        AProcessTypeCode     = new ArrayList();
        AProcessTypeName     = new ArrayList();

        AShortageTypeCode   = new ArrayList();
        AShortageTypeName   = new ArrayList();

        VOrderType     =new Vector();
        VOrderTypeCode =new Vector();

        try {
            theConnect  = JDBCConnection.getJDBCConnection();
            connect     = theConnect.getConnection();

            PreparedStatement   thePS   = connect.prepareStatement("Select UnitCode, UnitName from Unit Order by 2");
            ResultSet           rs      = thePS.executeQuery();
            while(rs.next())    {
                VUnitCode   . addElement(rs.getString(1));
                VUnitName   . addElement(rs.getString(2));
            }
            rs      . close();
            thePS   . close();

            thePS                    = connect.prepareStatement("Select ShortageTypeCode, ShortageTypeName from ShortageType Order by 2");
            rs                       = thePS.executeQuery();
            while(rs.next())    {
                AShortageTypeCode   . add(rs.getString(1));
                AShortageTypeName   . add(rs.getString(2));
            }
            rs      . close();
            thePS   . close();

            thePS   = connect.prepareStatement(" Select ProcessCode, ProcessType from ProcessingType Order by 2 ");
            rs      = thePS.executeQuery();
            while(rs.next())    {
                AProcessTypeCode.add(common.parseNull(rs.getString(1)));
                AProcessTypeName.add(common.parseNull(rs.getString(2)));
            }
            rs      . close();
            thePS   . close();

            thePS   = connect.prepareStatement(" select name,code from ordertype order by 2 ");
            rs      = thePS.executeQuery();
            while(rs.next())    {
                VOrderType.addElement(rs.getString(1));
                VOrderTypeCode.addElement(rs.getString(2));
            }
            rs      . close();
            thePS   . close();
        }
        catch(Exception e)  {
            System.out.println(e);
        }
    }
}
