/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProcessReports.PartyPriority;

import java.io.*;
import java.util.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.Vector;
import ProcessReports.util.*;
import ProcessReports.jdbc.*;

import ProcessReports.rndi.*;
import ProcessReports.jreporter.*;

/**
 *
 * @author Administrator
 */
public class ProcessSpinningRunOutReport extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    Common          common;
    HttpSession     session;

    String          SStDate, SEnDate;
    String          bgColor, bgHead, bgUom, bgBody, fgHead, fgUom, fgBody;
    String          SOrder, SUnit, SParty, SCount;
    String          SProcessTypeCode, SProcessTypeName;
    Vector          VARNo;
    FileWriter      FW;

    String          End = "", SNote = "", SNote1 = "";
    String          End1 = "";
    String          SSelectType     = "";

    double          dTotOrderWt     = 0;
    double          dTotMixWt       = 0;
    double          dTotBalance     = 0;
    double          dTotPackWt      = 0;

    double          dGrantOrdTot    = 0;
    double          dGrantMixTot    = 0;
    double          dGrantBalance   = 0;
    double          dGrantPackTot   = 0;

    int             iSlNo           = 0;

    int             iPage           = 1;
    int             TLine           = 0;
    int             iStDate         = 0;
    int             iEnDate         = 0;
    String          SOrderNo1       = "All";

    Vector          VOrderNo, VOrderDate, VMillDueDate, VOrderWeight, VCount, VParty, VMixDate, VMixWt, VPackWt, VBColor, VRColor, VCColor, VCleerDate, VCorrectionMix, VRemarks, VProcessRemarks;
    Vector          VDueDepo, VDueMill, VDLMix, VDRMix, VPTotal, VDF, VRMixDate, VRunDelay, VCleerDelay, VBalance, VSimpRunOut, VSimpRunOutD;
    Vector          VInsOrderNo, VInsShadeName, VInsRefNo;
    Vector          VDOrderNo, VPacking, VPartyPriority;

    String          SRunOutTarget;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {

                        response. setContentType("text/html;charset=UTF-8");
            PrintWriter out     = response.getWriter();
            common = new Common();
            valueInt();

            bgColor     = common.bgColor;
            bgHead      = common.bgHead;
            bgUom       = common.bgUom;
            bgBody      = common.bgBody;
            fgHead      = common.fgHead;
            fgUom       = common.fgUom;
            fgBody      = common.fgBody;

            //iStDate = common.toInt((String) session.getValue("StartDate"));
            session             = request.getSession(false);
            iEnDate             = common.toInt(common.pureDate((String) request.getParameter("TEnDate")));
            SUnit               = request.getParameter("Unit");
            //SOrderNo1           = request.getParameter("ROrderNo");
            SSelectType         = common.parseNull((String) request.getParameter("Select"));
            SProcessTypeCode    = null;
            SProcessTypeName    = null;
            StringTokenizer ST = new StringTokenizer(request.getParameter("processType"), "#");
            while (ST.hasMoreTokens()) {
                SProcessTypeCode = ST.nextToken();
                SProcessTypeName = ST.nextToken();
            }

            // set Target Days..
            SRunOutTarget = "7 + 6 = 13";

            if (SProcessTypeCode.equals("BR-C Orders")) {
                SRunOutTarget = "5 + 5 = 10";
            }

            valueInt();
            DespatchDetails(iEnDate);
            setVector();
            setInstruction();
            getCorrectionMix();
            DataAlign();
            setHtml(out);
            FileWriter();
            out.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void valueInt() {
        iPage       = 1;
        TLine       = 0;

        dTotOrderWt = 0;
        dTotMixWt   = 0;
        dTotBalance = 0;
        dTotPackWt  = 0;
    }

    private void setHtml(PrintWriter out) throws ServletException {

        SStDate = common.parseDate(String.valueOf(iStDate));
        SEnDate = common.parseDate(String.valueOf(iEnDate));

        out.println("<html>");
        out.println("<head>");
        out.println("<title>InvPro</title>");
        out.println("</head>");
        out.println("<body bgcolor='" + bgColor + "' text = '#0000FF'>");
        out.println("<p align='center'><font size='5'><b><u>Spinning Runout Status - ");
        out.println(" From  " + common.parseDate(SStDate) + "  To " + common.parseDate(SEnDate) + "</b></font></p>");
        out.println("<p><b>Process Type : " + SProcessTypeName + ", Target Days : " + SRunOutTarget + "</b></p>");
        out.println("<div align='center'>");

        out.println("  <table border='0' bordercolor='" + bgColor + "'>");
        out.println("    <tr>");
        out.println("      <td  rowspan='2' align='center' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>SL.No</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>OrderNo</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>OrderDate</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>Duedate As Per Mill</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>Count</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>Nameofthe Buyer</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>Order Weight</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>UptoDate Mixing Weight</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>Bobin Color</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>BobRing Color</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>Cops Color</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>Packed Weight Kgs</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>Balance to Supply</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>Pro Inst Bobin Exhaust </b></font></td>");
        out.println("      <td  colspan='4' align='center' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>Delayed Days</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>Process days</b></font></td>");
        out.println("      <td  colspan='2' align='center' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>Spinning Runout Delay</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>Bobbin Exhaust Delay Days</b></font></td>");
        out.println("      <td  colspan='2' align='center' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>Spinning Runout Delay</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>Remarks</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>Process Remarks</b></font></td>");
        out.println("    </tr>");
        out.println("    <tr>");
        out.println("      <td   align='center' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>DueDate AsPer Depo</b></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>DueDate AsPer Mill</b></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>Last Mixing Days</b></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>Remixing Days</b></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>Spx Runout</b></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>Creeling date</b></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>Bobbin Rack Ex</b></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>Spx Runout</b></font></td>");

        out.println("    </tr>");

        String SPriorityStatus = "";

        int     iSlNo       = 0;
        double  dGrantTot   = 0;
        double  dGrantTotal = 0;

        for (int i = 0; i < VOrderNo.size(); i++) {
            if (!common.parseNull((String) VPartyPriority.elementAt(i)).equals(SPriorityStatus)) {
                if (i != 0) {

                    out.println("<tr>");
                    out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>Total</b></font></td>");
                    out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
                    out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
                    out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
                    out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
                    out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
                    out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>" + common.getRound(dTotOrderWt, 3) + "</b></font></td>");
                    out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>" + common.getRound(dTotMixWt, 3) + "</b></font></td>");
                    out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
                    out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
                    out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
                    out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>" + common.getRound(dTotPackWt, 3) + "</b></font></td>");
                    out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>" + common.getRound(dTotBalance, 3) + "</b></font></td>");
                    out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
                    out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
                    out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
                    out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
                    out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
                    out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
                    out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
                    out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
                    out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
                    out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
                    out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
                    out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
                    out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
                    out.println("</tr>");

                    dTotOrderWt = 0;
                    dTotMixWt = 0;
                    dTotBalance = 0;
                    dTotPackWt = 0;
                    iSlNo = 1;

                }
                out.println("    <tr>");
                out.println("      <td colspan=20 width='30' bgcolor='" + bgBody + "' align='Left'><font color='" + fgBody + "'>" + ((String) VPartyPriority.elementAt(i)) + "</font></td>");
                out.println("    </tr>");
            }

            try {
                double dOrdWt = common.toDouble((String) VOrderWeight.elementAt(i));
                double dMixWt = common.toDouble((String) VMixWt.elementAt(i));
                double dPackWt = common.toDouble((String) VPackWt.elementAt(i));
                double dBalance = common.toDouble((String) VBalance.elementAt(i));
                String SOrderNo = (String) VOrderNo.elementAt(i);
                String SCreelingDate = common.parseNull((String) VCleerDelay.elementAt(i));

                dTotOrderWt = dTotOrderWt + dOrdWt;
                dTotMixWt = dTotMixWt + dMixWt;
                dTotBalance = dTotBalance + dBalance;
                dTotPackWt = dTotPackWt + dPackWt;

                dGrantOrdTot += dOrdWt;
                dGrantMixTot += dMixWt;
                dGrantBalance += dBalance;
                dGrantPackTot += dPackWt;

                String SCopsColor = "";  //getCopsColor(SOrderNo).trim();		//Commented on 10.03.2018

                /*
                 if(SCopsColor.equals("Nil")) {

                    SCopsColor ="";
                 } 
                 */
                out.println("    <tr>");
                out.println("      <td width='30' bgcolor='" + bgBody + "' align='right'><font color='" + fgBody + "'>" + String.valueOf(i + 1) + "</font></td>");
                out.println("      <td width='30' bgcolor='" + bgBody + "' align='right'><font color='" + fgBody + "'>" + SOrderNo + "</font></td>");
                out.println("      <td width='30' bgcolor='" + bgBody + "' align='left'><font color='" + fgBody + "'>" + common.parseDate((String) VOrderDate.elementAt(i)) + "</font></td>");
                out.println("      <td width='30' bgcolor='" + bgBody + "' align='left'><font color='" + fgBody + "'>" + common.parseDate((String) VMillDueDate.elementAt(i)) + "</font></td>");
                out.println("      <td width='30' bgcolor='" + bgBody + "' align='left'><font color='" + fgBody + "'>" + VCount.elementAt(i) + "</font></td>");
                out.println("      <td width='30' bgcolor='" + bgBody + "' align='left'><font color='" + fgBody + "'>" + VParty.elementAt(i) + "</font></td>");
                out.println("      <td width='30' bgcolor='" + bgBody + "' align='right'><font color='" + fgBody + "'><p align='right'>" + common.getRound(dOrdWt, 3) + "</font></td>");
                out.println("      <td width='30' bgcolor='" + bgBody + "' align='left'><font color='" + fgBody + "'><p align='right'>" + common.getRound(dMixWt, 3) + "</font></td>");
                out.println("      <td width='30' bgcolor='" + bgBody + "' align='left'><font color='" + fgBody + "'>" + VBColor.elementAt(i) + "</font></td>");
                out.println("      <td width='30' bgcolor='" + bgBody + "' align='left'><font color='" + fgBody + "'>" + VRColor.elementAt(i) + "</font></td>");
//                 out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+VCColor.elementAt(i)+"</font></td>");
                out.println("      <td width='30' bgcolor='" + bgBody + "' align='left'><font color='" + fgBody + "'>" + SCopsColor + "</font></td>");

                out.println("      <td width='30' bgcolor='" + bgBody + "' align='right'><font color='" + fgBody + "'><p align='right'>" + common.getRound(dPackWt, 3) + "</font></td>");
                out.println("      <td width='30' bgcolor='" + bgBody + "' align='right'><font color='" + fgBody + "'><p align='right'>" + common.getRound(dBalance, 3) + "</font></td>");
                out.println("      <td width='30' bgcolor='" + bgBody + "' align='left'><font color='" + fgBody + "'>" + getInstrucion(SOrderNo) + "</font></td>");
                out.println("      <td width='30' bgcolor='" + bgBody + "' align='left'><font color='" + fgBody + "'>" + VDueDepo.elementAt(i) + "</font></td>");
                out.println("      <td width='30' bgcolor='" + bgBody + "' align='left'><font color='" + fgBody + "'>" + VDueMill.elementAt(i) + "</font></td>");
                out.println("      <td width='30' bgcolor='" + bgBody + "' align='left'><font color='" + fgBody + "'>" + VDLMix.elementAt(i) + "</font></td>");
                out.println("      <td width='30' bgcolor='" + bgBody + "' align='left'><font color='" + fgBody + "'>" + VDRMix.elementAt(i) + "</font></td>");
                out.println("      <td width='30' bgcolor='" + bgBody + "' align='left'><font color='" + fgBody + "'>" + VPTotal.elementAt(i) + "</font></td>");
                out.println("      <td width='30' bgcolor='" + bgBody + "' align='left'><font color='" + fgBody + "'>" + VRunDelay.elementAt(i) + "</font></td>");
                if (!SCreelingDate.equals("")) {
                    out.println("      <td width='30' bgcolor='" + bgBody + "' align='left'><font color='" + fgBody + "'>" + SCreelingDate + "</font></td>");
                } else {
                    out.println("      <td width='30' bgcolor='" + bgBody + "' align='left'><font color='" + fgBody + "'>0</font></td>");
                }
                //out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+VCleerDelay.elementAt(i)+"</font></td>");
                out.println("      <td width='30' bgcolor='" + bgBody + "' align='left'><font color='" + fgBody + "'>&nbsp</font></td>");
                out.println("      <td width='30' bgcolor='" + bgBody + "' align='left'><font color='" + fgBody + "'>&nbsp</font></td>");
                out.println("      <td width='30' bgcolor='" + bgBody + "' align='left'><font color='" + fgBody + "'>&nbsp</font>" + VRunDelay.elementAt(i) + "</td>");
                out.println("      <td width='30' bgcolor='" + bgBody + "' align='left'><font color='" + fgBody + "'>" + VRemarks.elementAt(i) + "</font></td>");
                out.println("      <td width='30' bgcolor='" + bgBody + "' align='left'><font color='" + fgBody + "'>" + VProcessRemarks.elementAt(i) + "</font></td>");
                out.println("    </tr>");

            } catch (Exception ex) {
                ex.printStackTrace();
            }

            SPriorityStatus = common.parseNull((String) VPartyPriority.elementAt(i));
            iSlNo++;

        }
        setTotal(out);
        dTotOrderWt = 0;
        dTotMixWt = 0;
        dTotBalance = 0;
        dTotPackWt = 0;

        out.println("  </table>");
        //out.println(getBUnitQs());
        out.println("  </center>");
        out.println("</div>");
        out.println("</body>");
        out.println("");
        out.println("</html>");
        out.println("");
        out.close();
    }

    private void setTotal(PrintWriter out) {
        out.println("<tr>");
        out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
        out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
        out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
        out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
        out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
        out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>Total</b></font></td>");
        out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>" + common.getRound(dTotOrderWt, 3) + "</b></font></td>");
        out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>" + common.getRound(dTotMixWt, 3) + "</b></font></td>");
        out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
        out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
        out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
        out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>" + common.getRound(dTotPackWt, 3) + "</b></font></td>");
        out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>" + common.getRound(dTotBalance, 3) + "</b></font></td>");
        out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
        out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
        out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
        out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
        out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
        out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
        out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
        out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
        out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
        out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
        out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
        out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
        out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
        out.println("</tr>");

        out.println("<tr>");
        out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
        out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
        out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
        out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
        out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
        out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>Grand Total</b></font></td>");
        out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>" + common.getRound(dGrantOrdTot, 3) + "</b></font></td>");
        out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>" + common.getRound(dGrantMixTot, 3) + "</b></font></td>");
        out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
        out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
        out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
        out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>" + common.getRound(dGrantBalance, 3) + "</b></font></td>");
        out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>" + common.getRound(dGrantPackTot, 3) + "</b></font></td>");
        out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
        out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
        out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
        out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
        out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
        out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
        out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
        out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
        out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
        out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
        out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
        out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
        out.println("      <td  align='right' bgcolor='" + bgUom + "' valign='top'><font color='#000099'><b>&nbsp</b></font></td>");
        out.println("</tr>");

        dGrantOrdTot    = 0;
        dGrantMixTot    = 0;
        dGrantBalance   = 0;
        dGrantPackTot   = 0;

    }

    public void setVector() {

        VOrderNo        = new Vector();
        VOrderDate      = new Vector();
        VMillDueDate    = new Vector();
        VOrderWeight    = new Vector();
        VCount          = new Vector();
        VParty          = new Vector();
        VMixDate        = new Vector();
        VMixWt          = new Vector();
        VBColor         = new Vector();
        VRColor         = new Vector();
        VCColor         = new Vector();
        VCorrectionMix  = new Vector();
        VPackWt         = new Vector();
        VRMixDate       = new Vector();
        VCleerDate      = new Vector();
        VSimpRunOut     = new Vector();
        VRemarks        = new Vector();
        VPartyPriority  = new Vector();
        VProcessRemarks = new Vector();
        StringBuffer sb = new StringBuffer();
        if (SUnit.trim().equals("A_Unit")) {
            sb.append(getAUnitQs());
        }
        if (SUnit.trim().equals("B_Unit")) {
            sb.append(getBUnitQs());
        }

        System.out.println("Qry-->" + sb.toString());

        try {
            Connection conn = null;
            if (SUnit.trim().equals("A_Unit")) {
                JDBCProcessaConnection  jdbc = JDBCProcessaConnection.getJDBCProcessaConnection();
                                        conn = jdbc.getConnection();
            }
            if (SUnit.trim().equals("B_Unit")) {
                JDBCProcessConnection   jdbc = JDBCProcessConnection.getJDBCProcessConnection();
                                        conn = jdbc.getConnection();
            }
            Statement stat = conn.createStatement();
            ResultSet res = stat.executeQuery(sb.toString());

            while (res.next()) {
                String SOrderNo     = res.getString(1);
                String SFibreForm   = "," + common.parseNull(res.getString(18));

                if (SFibreForm.indexOf("NONE") != -1) {
                    SFibreForm = "";
                }

                VOrderNo        . addElement(SOrderNo);
                VOrderDate      . addElement(res.getString(2));
                VMillDueDate    . addElement(res.getString(3));
                VOrderWeight    . addElement(res.getString(4));
                VCount          . addElement(res.getString(5));
                VParty          . addElement(res.getString(6));
                VMixDate        . addElement(res.getString(7));
                VMixWt          . addElement(res.getString(8));
                VBColor         . addElement(common.parseNull(res.getString(9)));
                VRColor         . addElement(common.parseNull(res.getString(10)));
                VCColor         . addElement(common.parseNull(res.getString(11)));
                VCorrectionMix  . addElement(res.getString(12));
                VPackWt         . add(getPackWt(SOrderNo));
                VCleerDate      . add(common.parseNull(res.getString(14)));
                VSimpRunOut     . add(res.getString(15));
                VRemarks        . addElement(common.parseNull(res.getString(16)) + SFibreForm);
                VRMixDate       . add("");
                VPartyPriority  . addElement(common.parseNull(res.getString(17)));
                VProcessRemarks . addElement(common.parseNull(res.getString(20)));
            }
            res.close();
            stat.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println(ex);
        }
    }

    private String getAUnitQs() {

        StringBuffer sb = new StringBuffer();

                    sb.append("  Select distinct ROrderNo, OrderDate, DeliveryDate, Weight, CountName,PartyName, MixingDate, MixingWeight, SimplexPumb,");
                    sb.append("  SimplexRing, CopsColor, CorrectionMixing, packwgt, creelingdate, simplexrunoutdate, remarks, PriorityName, FormName, PriorityCode, ProcessRemarks  From ");
                    sb.append("  (");
                    sb.append("  Select  RegularOrder.ROrderNo, RegularOrder.OrderDate, RegularOrder.DeliveryDate, RegularOrder.Weight, YarnCount.CountName,");
                    sb.append("  PartyMaster.PartyName, Rmixir.MixingDate, Rmixir.MixingWeight,'' as SimplexPumb,'' as SimplexRing, '' as CopsColor, Rmixir.CorrectionMixing, ");
                    sb.append("   '' as packwgt,   '' as creelingdate, '' as simplexrunoutdate,'' as remarks, ");

                    //               sb.append("  PartyPriority.Name as PriorityName, ");
                    sb.append("  deCode(RegularOrder.Shortage, 1, 'Shortage', PartyPriority.Name) as PriorityName, ");

                    sb.append("  FibreForm.FormName, ");
                    sb.append("   PartyPriority.Code as PriorityCode, PROCESSINGTYPE.OESTATUS, processingofindent.remarks as ProcessRemarks  From scm.RegularOrder ");
                    sb.append("  Inner Join scm.ProcessingType on ProcessingType.ProcessCode = Regularorder.ProcessTypeCode ");
                    sb.append("  inner join ( ");

                    //sb.append("  select distinct rorderno from (  ");
                    //Spinning Orders			
                    sb.append("  select rorderno  from ( ");
                    sb.append("  select distinct processingofindent.rorderno from processa.pro_drawing_Can_Details   ");
                    sb.append("  inner join processa.processingofindent on processingofindent.rorderno = pro_drawing_Can_Details.rorderno  ");
                    sb.append("  Inner Join scm.ProcessingType on ProcessingType.ProcessCode = processingofindent.ProcessCode and ProcessingType.oestatus = 0 ");
                    sb.append("  where  pro_drawing_Can_Details.orderfollowstatus=1  and processingofindent.simpcomp = 0  and  ");
                    sb.append("  processingofindent.rorderno not in ( Select Rorderno from processa.Pro_drawing_can_details where orderfollowstatus = 0 ) ");
                    //OE Orders			
                    sb.append("  union all ");
                    sb.append("  select rorderno  from ( ");
                    sb.append("  select rorderno, sum(cnt)  from ( ");
                    sb.append("  select distinct processingofindent.rorderno, 1 as cnt from processa.pro_drawing_Can_Details ");
                    sb.append("  inner join processa.processingofindent on processingofindent.rorderno = pro_drawing_Can_Details.rorderno  ");
                    sb.append("  Inner Join scm.ProcessingType on ProcessingType.ProcessCode = processingofindent.ProcessCode and ProcessingType.oestatus = 1 ");
                    sb.append("  where  processingofindent.simpcomp = 0  and processingofindent.oecomp = 0 ");
                    //All drawing can runout except finisher
                    sb.append(" and ");
                    sb.append("  ( pro_drawing_can_details.processlevel in  ");
                    sb.append("     (  ");
                    sb.append("  SELECT AUNIT_ORDERDRAWINGSETTINGDLS.PROCESSLEVEL FROM PROCESSA.AUNIT_ORDERDRAWINGSETTINGDLS ");
                    sb.append("  WHERE  AUNIT_ORDERDRAWINGSETTINGDLS.ID !=  ");
                    sb.append("          (  ");
                    sb.append("  SELECT MAX(AUNIT_ORDERDRAWINGSETTINGDLS.ID)  FROM  PROCESSA.AUNIT_ORDERDRAWINGSETTINGDLS ");
                    sb.append("  WHERE AUNIT_ORDERDRAWINGSETTINGDLS.RORDERNO = processingofindent.rorderno AND AUNIT_ORDERDRAWINGSETTINGDLS.PROCESSLEVEL!='none' AND AUNIT_ORDERDRAWINGSETTINGDLS.PROCESSLEVEL!='NONE' ");
                    sb.append("  		 )  ");
                    sb.append("  	) and pro_drawing_Can_Details.orderfollowstatus = 1  ");
                    sb.append("  ) 	");

                    sb.append("  union all ");
                    sb.append("  select distinct processingofindent.rorderno, 1 as cnt from processa.pro_drawing_Can_Details ");
                    sb.append("  inner join processa.processingofindent on processingofindent.rorderno = pro_drawing_Can_Details.rorderno  ");
                    sb.append("  Inner Join scm.ProcessingType on ProcessingType.ProcessCode = processingofindent.ProcessCode and ProcessingType.oestatus = 1 ");
                    sb.append("  where  processingofindent.simpcomp = 0  and processingofindent.oecomp = 0 ");
                    //Finisher can running			
                    sb.append(" and ");
                    sb.append("   ( pro_drawing_can_details.processlevel in ");
                    sb.append("   	  ( ");
                    sb.append("     	SELECT AUNIT_ORDERDRAWINGSETTINGDLS.PROCESSLEVEL FROM PROCESSA.AUNIT_ORDERDRAWINGSETTINGDLS ");
                    sb.append(" 		WHERE  AUNIT_ORDERDRAWINGSETTINGDLS.ID = ");
                    sb.append(" 	    ( ");
                    sb.append(" 		    SELECT MAX(AUNIT_ORDERDRAWINGSETTINGDLS.ID)  FROM  PROCESSA.AUNIT_ORDERDRAWINGSETTINGDLS ");
                    sb.append(" 		    WHERE AUNIT_ORDERDRAWINGSETTINGDLS.RORDERNO = processingofindent.rorderno AND AUNIT_ORDERDRAWINGSETTINGDLS.PROCESSLEVEL!='none' AND AUNIT_ORDERDRAWINGSETTINGDLS.PROCESSLEVEL!='NONE' ");
                    sb.append(" 		 ) ");
                    sb.append(" 	  ) and pro_drawing_Can_Details.orderfollowstatus=0 ");
                    sb.append("   ) ");

                    sb.append("   ) group by rorderno having sum(cnt)=2 ");
                    sb.append("   ) ");

                    sb.append("  )  ");

                    //commented on 12.06.2018 
                    /*
			sb.append("  select rorderno from ( ");
			sb.append("  select rorderno, sum(cnt)  from ( ");
			
                    //Spinning			
                    if(SProcessTypeCode.equals("Other than BR-C Orders")  || SProcessTypeCode.equals("All")){
		
			sb.append("  select distinct processingofindent.rorderno, 1 as cnt from processa.pro_drawing_Can_Details  ");
			sb.append("  inner join processa.processingofindent on processingofindent.rorderno = pro_drawing_Can_Details.rorderno ");
			sb.append("  Inner Join scm.ProcessingType on ProcessingType.ProcessCode = processingofindent.ProcessCode ");
			sb.append("  where  ");
			sb.append("  (");
			sb.append("    ( PROCESSINGTYPE.OESTATUS = 0  AND  ");
			sb.append("      (select count(*) from processa.pro_drawing_can_details where pro_drawing_can_details.rorderno = processingofindent.rorderno  and orderfollowstatus = 0) = 0 ");
			sb.append("      and processingofindent.simpcomp = 0 ) ");
			
                    //Spinning Order - All drawing can runout - except finisher
			sb.append("  OR ");
			sb.append("    ( PROCESSINGTYPE.OESTATUS = 1  AND pro_drawing_can_details.processlevel in ");
			sb.append("    ( ");
			sb.append("      SELECT AUNIT_ORDERDRAWINGSETTINGDLS.PROCESSLEVEL FROM PROCESSA.AUNIT_ORDERDRAWINGSETTINGDLS ");
			sb.append("      WHERE AUNIT_ORDERDRAWINGSETTINGDLS.RORDERNO = Processingofindent.Rorderno AND AUNIT_ORDERDRAWINGSETTINGDLS.ID = ");
			sb.append("      ( ");
			sb.append("      SELECT MAX(AUNIT_ORDERDRAWINGSETTINGDLS.ID)  FROM  PROCESSA.AUNIT_ORDERDRAWINGSETTINGDLS ");
			sb.append("      WHERE AUNIT_ORDERDRAWINGSETTINGDLS.RORDERNO = processingofindent.rorderno AND AUNIT_ORDERDRAWINGSETTINGDLS.PROCESSLEVEL!='none' AND AUNIT_ORDERDRAWINGSETTINGDLS.PROCESSLEVEL!='NONE' ");
			sb.append("      ) ");
			sb.append("      AND AUNIT_ORDERDRAWINGSETTINGDLS.PROCESSLEVEL!='none' AND AUNIT_ORDERDRAWINGSETTINGDLS.PROCESSLEVEL!='NONE' ");
			sb.append("      ) ");
			sb.append("      AND pro_drawing_can_details.orderfollowstatus = 0 and processingofindent.oecomp = 0 ");
			sb.append("    )");
			sb.append("  )");
		}
	
		if(SProcessTypeCode.equals("All")) {
			sb.append("  union all");
		}
	
		if(SProcessTypeCode.equals("BR-C Orders") || SProcessTypeCode.equals("All")) {

//OE
			sb.append("  select distinct processingofindent.rorderno, 1 as cnt  from processa.pro_drawing_Can_Details");
			sb.append("  inner join processa.processingofindent on processingofindent.rorderno = pro_drawing_Can_Details.rorderno and processingofindent.windcomp = 0");
			sb.append("  Inner Join scm.ProcessingType on ProcessingType.ProcessCode = processingofindent.ProcessCode ");
			sb.append("   where ");
			sb.append("  (");
			sb.append("    ( PROCESSINGTYPE.OESTATUS = 0  AND  ");
			sb.append("      (select count(*) from processa.pro_drawing_can_details where pro_drawing_can_details.rorderno = processingofindent.rorderno  and orderfollowstatus = 0) = 0 ");
			sb.append("      and processingofindent.simpcomp = 0 ) ");
			
//Oe Order - except finisher - drawing can runout
			sb.append("  OR");
			sb.append("    ( PROCESSINGTYPE.OESTATUS = 1  AND pro_drawing_can_details.processlevel in ");
			sb.append("      ( ");
			sb.append("      SELECT AUNIT_ORDERDRAWINGSETTINGDLS.PROCESSLEVEL FROM PROCESSA.AUNIT_ORDERDRAWINGSETTINGDLS ");
			sb.append("      WHERE AUNIT_ORDERDRAWINGSETTINGDLS.RORDERNO = processingofindent.rorderno  AND  AUNIT_ORDERDRAWINGSETTINGDLS.ID != ");
			sb.append("      ( ");
			sb.append("      SELECT MAX(AUNIT_ORDERDRAWINGSETTINGDLS.ID)  FROM  PROCESSA.AUNIT_ORDERDRAWINGSETTINGDLS ");
			sb.append("      WHERE AUNIT_ORDERDRAWINGSETTINGDLS.RORDERNO = processingofindent.rorderno AND AUNIT_ORDERDRAWINGSETTINGDLS.PROCESSLEVEL!='none' AND AUNIT_ORDERDRAWINGSETTINGDLS.PROCESSLEVEL!='NONE' ");
			sb.append("      ) ");
			sb.append("      AND AUNIT_ORDERDRAWINGSETTINGDLS.PROCESSLEVEL!='none' AND AUNIT_ORDERDRAWINGSETTINGDLS.PROCESSLEVEL!='NONE' ");
			sb.append("      ) ");
			sb.append("      AND pro_drawing_can_details.orderfollowstatus = 1  and processingofindent.oecomp = 0 ");
			sb.append("    )");
			sb.append("  )");
		}

			sb.append("  )");
			sb.append("  group by rorderno");
			sb.append("  having sum(cnt)=2");
			sb.append("  ) ) t on t.rorderno = RegularOrder.rorderno ");
         */
                    sb.append("  )  t on t.rorderno = RegularOrder.rorderno ");

                    sb.append("  Inner Join processa.pro_process_base on pro_process_base.rorderno = RegularOrder.rorderno  and pro_process_base.MIXSTKSTATUS = 1 ");
                    sb.append("  Inner Join processa.pro_carding_can_details on pro_carding_can_details.rorderno = RegularOrder.rorderno   and pro_carding_can_details.orderfollowstatus = 1 ");
                    sb.append("  Left join processa.pro_simplex_can_details on pro_simplex_can_details.rorderno = RegularOrder.Rorderno and pro_simplex_can_details.orderfollowstatus = 0 ");
                    sb.append("  Inner join processingofindent on processingofindent.rorderno =  RegularOrder.rorderno ");
                    sb.append("  Inner Join scm.YarnCount On  YarnCount.CountCode = RegularOrder.CountCode");
                    sb.append("  Inner Join scm.PartyMaster On  Partymaster.partyCode = RegularOrder.partyCode");
                    sb.append("  Inner Join scm.PartyPriority On PartyPriority.Code = nvl(PartyMaster.PriorityStatus,0) ");
                    sb.append("  Inner Join scm.Unit On  Unit.UnitCode = RegularOrder.UnitCode");
                    sb.append("  Inner Join scm.RMixir On  Rmixir.OrdNo = RegularOrder.RorderNo and RMixir.correctionmixing = 0");
                    sb.append("  Inner Join scm.FibreForm on FibreForm.FormCode = nvl(RegularOrder.FibreFormCode, 0) ");
                    sb.append("  Where regularorder.unitcode =  1  ");

        if (!SUnit.equals("All")) {
            sb.append("   and Unit.UnitName='" + SUnit + "' ");
        }
        if (SSelectType.equalsIgnoreCase("Depo")) {
            sb.append("   and regularorder.rorderno like 'DM%'");
        }
        if (SSelectType.equalsIgnoreCase("Party")) {
            sb.append("   and regularorder.rorderno not like 'DM%'");
        }
        if (!SOrderNo1.equals("All")) {
            sb.append("  and regularorder.rorderno='" + SOrderNo1 + "' ");
        }
        if (!SProcessTypeCode.equals("All")) {
            if (SProcessTypeCode.equals("BR-C Orders")) {
                sb.append("  and ProcessingType.OEStatus = 1 ");
            } else if (SProcessTypeCode.equals("Other than BR-C Orders")) {
                sb.append("  and ProcessingType.OEStatus = 0 ");
            } else {
                sb.append("  and RegularOrder.ProcessTypeCode = " + SProcessTypeCode);
            }
        }
        sb.append("  ) Order By  PriorityCode desc, OrderDate, ROrderNo ");
        System.out.println("AUnitQS -->" + sb.toString());

        return sb.toString();
    }

    private String getBUnitQs() {

        StringBuffer    sb  = new StringBuffer();

                        sb  . append(" Select distinct ROrderNo, OrderDate, DeliveryDate, Weight, CountName,PartyName, MixingDate, MixingWeight, SimplexPumb, ");
                        sb  . append(" SimplexRing, CopsColor, CorrectionMixing, packwgt, creelingdate, simplexrunoutdate,remarks, PriorityName, FormName, PriorityCode, '' as ProcessRemarks  From  ");
                        sb  . append(" ( ");

                        sb  . append(" Select distinct RegularOrder.ROrderNo, RegularOrder.OrderDate, RegularOrder.DeliveryDate, RegularOrder.Weight, YarnCount.CountName, ");
                        sb  . append(" PartyMaster.PartyName, Rmixir.MixingDate, Rmixir.MixingWeight,'' as SimplexPumb,'' as SimplexRing, '' as CopsColor, Rmixir.CorrectionMixing, ");
                        //   comment by ananthi 05.11.2020 for creeling Date
                        //  sb.append(" '' as packwgt,   substr(MIN(intime), 0, 10) as creelingdate, '' as simplexrunoutdate,'' as remarks, deCode(RegularOrder.Shortage, 1, 'Shortage', PartyPriority.Name) as PriorityName, FibreForm.FormName,  ");
                        sb  . append(" '' as packwgt,   MIN(indate) as creelingdate, '' as simplexrunoutdate,'' as remarks, deCode(RegularOrder.Shortage, 1, 'Shortage', PartyPriority.Name) as PriorityName, FibreForm.FormName,  ");
                        sb  . append(" PartyPriority.Code as PriorityCode, PROCESSINGTYPE.OESTATUS  From scm.RegularOrder ");
                        sb  . append(" Inner Join scm.ProcessingType on ProcessingType.ProcessCode = Regularorder.ProcessTypeCode ");
                        sb  . append(" inner join  ");
                        sb. append("   (  ");
                        sb. append("   select distinct rorderno from (  ");
                        sb. append("   select rorderno, sum(cnt)  from (   ");
                        sb. append("   select distinct processingofindent.rorderno, 1 as cnt from process.pro_drawing_Can_Details  ");
                        sb. append("   inner join process.processingofindent on processingofindent.rorderno = pro_drawing_Can_Details.rorderno ");
                        sb. append("   where  pro_drawing_Can_Details.orderfollowstatus=1  and processingofindent.simpcomp = 0  and ");
                        sb. append("   processingofindent.rorderno not in ( Select Rorderno from process.Pro_drawing_can_details where orderfollowstatus = 0 ) ");
                        //sb.append(" (select count(*) from process.pro_drawing_can_details where pro_drawing_can_details.rorderno = processingofindent.rorderno  and orderfollowstatus = 0) = 0  ");
                        sb. append("   ) group by rorderno  having sum(cnt)=1 ");
                        sb. append("   )  ");

                        sb. append("   ) t on t.rorderno = RegularOrder.rorderno ");
                        sb. append(" Left join  pro_spinning on pro_spinning.ROrderNo = RegularOrder.rorderno");
                        sb. append("   Inner Join process.pro_process_base on pro_process_base.rorderno = RegularOrder.rorderno  and pro_process_base.MIXSTKSTATUS = 1 ");
                        sb. append("   Inner Join process.pro_carding_can_details on pro_carding_can_details.rorderno = RegularOrder.rorderno   and pro_carding_can_details.orderfollowstatus = 1  ");
                        sb. append("   Left join process.pro_simplex_can_details on pro_simplex_can_details.rorderno = pro_process_base.Rorderno and pro_simplex_can_details.orderfollowstatus = 0  ");
                        sb. append("   Inner Join scm.YarnCount On  YarnCount.CountCode = RegularOrder.CountCode ");
                        sb. append("   Inner Join scm.PartyMaster On  Partymaster.partyCode = RegularOrder.partyCode ");
                        sb. append("   Inner Join scm.PartyPriority On PartyPriority.Code = nvl(PartyMaster.PriorityStatus,0)  ");
                        sb. append("   Inner Join scm.Unit On  Unit.UnitCode = RegularOrder.UnitCode ");
                        sb. append("   Inner Join scm.RMixir On  Rmixir.OrdNo = RegularOrder.RorderNo and RMixir.correctionmixing = 0 ");
                        sb. append("  Inner Join scm.FibreForm on FibreForm.FormCode = nvl(RegularOrder.FibreFormCode, 0)  ");
                        sb. append("  Where regularorder.unitcode =  2   "); 
        if (!SUnit.equals("All")) {
            sb.append("   and Unit.UnitName='" + SUnit + "' ");
        }
        if (SSelectType.equalsIgnoreCase("Depo")) {
            sb.append("   and regularorder.rorderno like 'DM%'");
        }
        if (SSelectType.equalsIgnoreCase("Party")) {
            sb.append("   and regularorder.rorderno not like 'DM%'");
        }
        if (!SOrderNo1.equals("All")) {
            sb.append("  and regularorder.rorderno='" + SOrderNo1 + "' ");
        }
        if (!SProcessTypeCode.equals("All")) {
            if (SProcessTypeCode.equals("BR-C Orders")) {
                sb.append("  and ProcessingType.OEStatus = 1 ");
            } else if (SProcessTypeCode.equals("Other than BR-C Orders")) {
                sb.append("  and ProcessingType.OEStatus = 0 ");
            } else {
                sb.append("  and RegularOrder.ProcessTypeCode = " + SProcessTypeCode);
            }
        }

        sb.append(" group by RegularOrder.ROrderNo, RegularOrder.OrderDate, RegularOrder.DeliveryDate, RegularOrder.Weight, YarnCount.CountName,");
        sb.append(" PartyMaster.PartyName, Rmixir.MixingDate, Rmixir.MixingWeight, Rmixir.CorrectionMixing,");
        sb.append("FibreForm.FormName,PartyPriority.Code , PROCESSINGTYPE.OESTATUS,deCode(RegularOrder.Shortage, 1, 'Shortage', PartyPriority.Name)");

        sb.append("  ) Order By  17 desc, OrderDate, ROrderNo ");
        System.out.println("B-UNIT--->"+sb.toString());
        return sb.toString();
    }

    private String getCopsColor(String SOrder) {
        String          SColor  = "";
        StringBuffer    sb      = new StringBuffer();
                        sb      . append("  Select wm_concat(pro_spinningpumb.name) as CopsColor from pro_spinning_pumb_details");
                        sb      . append("  inner join pro_spinningpumb on pro_spinningpumb.id = pro_spinning_pumb_details.spinningpumbid");
                        sb      . append("  Where rorderno ='" + SOrder + "'");

        try {
            Connection conn = null;
            if (SUnit.trim().equals("A_Unit")) {
                JDBCProcessaConnection  jdbc = JDBCProcessaConnection.getJDBCProcessaConnection();
                                        conn = jdbc.getConnection();
            }
            if (SUnit.trim().equals("B_Unit")) {
                JDBCProcessConnection   jdbc = JDBCProcessConnection.getJDBCProcessConnection();
                                        conn = jdbc.getConnection();
            }
            Statement stat = conn.createStatement();
            ResultSet res = stat.executeQuery(sb.toString());

            while (res.next()) {
                SColor = common.parseNull(res.getString(1));
            }
            res.close();
            stat.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return SColor;
    }

    private void DataAlign() {
        VDueDepo        = new Vector();
        VDueMill        = new Vector();
        VDLMix          = new Vector();
        VDF             = new Vector();
        VDRMix          = new Vector();
        VPTotal         = new Vector();
        VRunDelay       = new Vector();
        VCleerDelay     = new Vector();
        VBalance        = new Vector();
        VSimpRunOutD    = new Vector();

        System.out.println("VOrderNo.size()-->" + VOrderNo.size());
        for (int i = 0; i < VOrderNo.size(); i++) {
            try {
                int iOrderDate      = common.toInt((String) VOrderDate.elementAt(i));
                int iDueDate        = common.toInt((String) VMillDueDate.elementAt(i));
                int iMixDate        = common.toInt((String) VMixDate.elementAt(i));
                int iReMix          = common.toInt((String) VRMixDate.elementAt(i));
                int iRepDate        = common.toInt(common.getServerDate());
                int iCreer          = common.toInt((String) VCleerDate.elementAt(i));
                int iSimRunOut      = common.toInt((String) VSimpRunOut.elementAt(i));

                double dOrderWt     = common.toDouble((String) VOrderWeight.elementAt(i));
                double dPackingWt   = common.toDouble((String) VPackWt.elementAt(i));
                String SBalance     = common.getRound(dOrderWt - dPackingWt, 3);

                if (i == 0) {
                    System.out.println("iCreer-->" + iCreer);
                    System.out.println("iRepDate-->" + iRepDate);
                }

                int iDueDep         = common.getDateDiff(iRepDate, iDueDate);
                int iDueMill        = common.getDateDiff(iRepDate, iOrderDate);
                int iDLMix          = common.getDateDiff(iMixDate, iOrderDate);
                int iProDate        = common.getDateDiff(iRepDate, iMixDate);
                int iRMixDate       = common.getDateDiff(iReMix, iRepDate);
                int iCreerD         = common.getDateDiff(iRepDate, iCreer);
                int iSimRunOutD     = common.getDateDiff(iRepDate, iSimRunOut);
                if (i == 0) {
                    System.out.println("iCreerD-->" + iCreerD);
                }

                VDueDepo            . addElement(Integer.toString(iDueDep));
                VDueMill            . addElement(Integer.toString(iDueMill));
                VDLMix              . addElement(Integer.toString(iDLMix));
                VDRMix              . addElement(Integer.toString(iRMixDate));
                VDF                 . addElement(Integer.toString(iSimRunOutD));

                VPTotal             . addElement(Integer.toString(iProDate));
                VRunDelay           . addElement(Integer.toString(iSimRunOutD));
                VCleerDelay         . addElement(Integer.toString(iCreerD));
                VSimpRunOutD        . addElement(Integer.toString(iSimRunOutD));
                VBalance            . addElement(SBalance);
            } catch (Exception e) {
                System.out.println(e);
                e.printStackTrace();
            }
        }
    }

    private void getCorrectionMix() {
        int     iCorrectionMix  = 0;
        double  dCMixWt         = 0;
        String  CDate           = "";
        String  COrderNo        = "";

        for (int i = 0; i < VOrderNo.size(); i++) {
            iCorrectionMix = common.toInt((String) VCorrectionMix.elementAt(i));
            if (iCorrectionMix == 1) {
                COrderNo    = (String) VOrderNo.elementAt(i);
                dCMixWt     = common.toDouble((String) VMixWt.elementAt(i));
                CDate       = (String) VMixDate.elementAt(i);
                for (int j = 0; j < VOrderNo.size(); j++) {
                    String  SOrderNo    = (String) VOrderNo.elementAt(j);
                    int     iStatus     = common.toInt((String) VCorrectionMix.elementAt(j));
                    if (SOrderNo.equals(COrderNo) && iStatus != 1) {

                        double  dWt         = common.toDouble((String) VMixWt.elementAt(j)) + dCMixWt;
                                VRMixDate   . set(j, CDate);
                                VMixWt      . set(j, common.getRound(dWt, 3));
                    }
                }
                VOrderNo        . remove(i);
                VOrderDate      . remove(i);
                VMillDueDate    . remove(i);
                VOrderWeight    . remove(i);
                VCount          . remove(i);
                VParty          . remove(i);
                VMixDate        . remove(i);
                VMixWt          . remove(i);
                VBColor         . remove(i);
                VRColor         . remove(i);
                VCColor         . remove(i);
                VCorrectionMix  . remove(i);
                VPackWt         . remove(i);
                VRMixDate       . remove(i);
                VCleerDate      . remove(i);
                VSimpRunOut     . remove(i);
                VRemarks        . remove(i);
                VProcessRemarks . remove(i);
            }
        }
    }

    public void Head() {

        String  SPitch  = "M";
        String  Head1   = "Company  :AMARJOTHI SPINNING MILL \n";
        String  Head2   = "Document :Spinning RunOut Report-As onDate  " + common.parseDate(SEnDate) + "\n";
        String  Head3   = "Unit : " + SUnit + " ,Process Type :  " + SProcessTypeName + ", Target Days : " + SRunOutTarget + "\n";
        String  Head4   = "Page :" + iPage + "";
        String  Str1    = "|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n";
        String  Str2    = "|    |            |          |          |     |                             |        | Up to  |           |          |     Process        |   Delay Days Due Date |        | Spinning Runout     | Bobbin|   Spinning Runout   |               |                         |\n";
        String  Str3    = "|Sl. | OrderNo    | OrderDate|DueDate As|Count|     Name Of The Buyer       | Order  |  date  |   Packed  |Balance to|     Instr          |-----------------------|        |  Dly days from      |Exhaust|    Dly days From    |    Remarks    |   Process Remarks       |\n";
        String  Str4    = "|No. |            |          | Per Mill |     |                             | Weight | Mixing |    Kgs    | Supply   |     BobbinExh      | As  | As  | LMix| RMix|Process |---------------------| Delay |---------------------|               |                         |\n";
        String  Str5    = "|    |            |          |          |     |                             |        | Weight |           |          |     Spg Runout     |Depo | Mill| Days| Days| Days   |Spx RunOut|CreelingDt| Days  |BobbinRack|Spx Runout|               |                         |\n";
        String  Str6    = "|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|";
                End     = "|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|";
                End1    = "|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n";
                SParty  = "|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|";

                SNote   = "\n Foot Note : Due Date Dly Days=Rep Dt - Due Dt /*/ Last Mixing Delay Days = L.Mix - Ord Dt /*/ Remixing Delay Days = R.Mix Dt - Ord Dt /*/Process Days = Rep Dt- L.Mix /*/ SPG RO Dly Days From Spx RO = Report Dt - Simplex RO Dt";
                SNote1  = "             SPG RO Dly Days From Spg Creeling Dt = Report Dt - Spg Creeling Dt /*/ Bob Ex Dly Days = Bob ExDt - Simplex RO Dt /*/ SPG RO Dly Days From BRE Dt = Report Dt - Bobbin Rack Ex Dt - 7Days /*/ SPG RO Dly Days From Simplex Runout = Report Dt - Simplex Runout Dt ";

        prnWriter(Head1 + Head2 + Head3 + Head4 + SPitch);
        prnWriter(Str1 + Str2 + Str3 + Str4 + Str5 + Str6);
    }

    private void Body() {
        try {
            String  SPriorityStatus = "";
            int     SlNo            = 0;

            double  dTotOrderWt     = 0;
            double  dTotMixWt       = 0;
            double  dTotBalance     = 0;
            double  dTotPackWt      = 0;

            double  dGrantOrdTot    = 0;
            double  dGrantMixTot    = 0;
            double  dGrantBalance   = 0;
            double  dGrantPackTot   = 0;

            for (int i = 0; i < VOrderNo.size(); i++) {
                SlNo++;

                if (!common.parseNull((String) VPartyPriority.elementAt(i)).equals(SPriorityStatus)) {
                    if (i != 0) {

                        /*               prnWriter("|"+common.Cad("TOTAL  ",76)+
                         "|"+common.Rad(common.getRound(dTotOrderWt,3),12)+
                         "|"+common.Rad(common.getRound(dTotMixWt,3),13)+
                         "|"+common.Space(36)+
                         "|"+common.Rad(common.getRound(dTotPackWt,3),11)+
                         "|"+common.Rad(common.getRound(dTotBalance,3),10)+"|"+common.Space(130)+"|");  */
                        prnWriter(SParty);
                        prnWriter("|" + common.Space(4)
                                + "|" + common.Space(12)
                                + "|" + common.Space(10)
                                + "|" + common.Space(10)
                                + "|" + common.Space(5)
                                + "|" + common.Cad("TOTAL", 29)
                                + "|" + common.Rad(common.getRound(dTotOrderWt, 3), 8)
                                + "|" + common.Rad(common.getRound(dTotMixWt, 3), 8)
                                + "|" + common.Rad(common.getRound(dTotPackWt, 3), 11)
                                + "|" + common.Rad(common.getRound(dTotBalance, 3), 10)
                                + "|" + common.Space(20)
                                + "|" + common.Space(5)
                                + "|" + common.Space(5)
                                + "|" + common.Space(5)
                                + "|" + common.Space(5)
                                + "|" + common.Space(8)
                                + "|" + common.Space(10)
                                + "|" + common.Space(10)
                                + "|" + common.Space(7)
                                + "|" + common.Space(10)
                                + "|" + common.Space(10)
                                + "|" + common.Space(15)
                                + "|" + common.Space(25) + "|");

                        dTotOrderWt = 0;
                        dTotMixWt = 0;
                        dTotBalance = 0;
                        dTotPackWt = 0;
                        iSlNo = 0;
                    }

                    String SPartyStatus = "|" + common.Space(4)
                            + "|" + common.Space(12)
                            + "|" + common.Space(10)
                            + "|" + common.Space(10)
                            + "|" + common.Space(5)
                            + "|E" + common.Cad(common.parseNull((String) VPartyPriority.elementAt(i)), 29)
                            + "F|" + common.Space(8)
                            + "|" + common.Space(8)
                            + "|" + common.Space(11)
                            + "|" + common.Space(10)
                            + "|" + common.Space(20)
                            + "|" + common.Space(5)
                            + "|" + common.Space(5)
                            + "|" + common.Space(5)
                            + "|" + common.Space(5)
                            + "|" + common.Space(8)
                            + "|" + common.Space(10)
                            + "|" + common.Space(10)
                            + "|" + common.Space(7)
                            + "|" + common.Space(10)
                            + "|" + common.Space(10)
                            + "|" + common.Space(15)
                            + "|" + common.Space(25) + "|";

                    String S1 = "|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|";

                    prnWriter(S1);
                    prnWriter(SPartyStatus);
                    prnWriter(S1);

                }
                double dOrdWt = common.toDouble((String) VOrderWeight.elementAt(i));
                double dMixWt = common.toDouble((String) VMixWt.elementAt(i));
                double dPackWt = common.toDouble((String) VPackWt.elementAt(i));
                double dBalance = common.toDouble((String) VBalance.elementAt(i));

                dTotOrderWt     = dTotOrderWt + dOrdWt;
                dTotMixWt       = dTotMixWt + dMixWt;
                dTotBalance     = dTotBalance + dBalance;
                dTotPackWt      = dTotPackWt + dPackWt;

                dGrantOrdTot    += dOrdWt;
                dGrantMixTot    += dMixWt;
                dGrantBalance   += dBalance;
                dGrantPackTot   += dPackWt;

                String SNo              = "|" + common.Cad(String.valueOf(i + 1), 4) + "|";
                String OrdNo            = common.Pad((String) VOrderNo.elementAt(i), 12) + "|";
                String OrdDate          = common.Pad(common.parseDate((String) VOrderDate.elementAt(i)), 10) + "|";
                String Due              = common.Pad(common.parseDate((String) VMillDueDate.elementAt(i)), 10) + "|";
                String OrdWt            = common.Rad(common.getRound(dOrdWt, 0), 8) + "|";
                String Count            = common.Cad((String) VCount.elementAt(i), 5) + "|";
                String Party            = common.Pad((String) VParty.elementAt(i), 29) + "|";
                String MixDate          = common.Pad((String) VMixDate.elementAt(i), 10) + "|";
                String MixWt            = common.Rad(common.getRound(dMixWt, 0), 8) + "|";
                String BColor           = common.Cad(getNilRemark((String) VBColor.elementAt(i)), 13) + "|";
                String RColor           = common.Cad(getNilRemark((String) VRColor.elementAt(i)), 11) + "|";
                String CColor           = common.Cad(getNilRemark((String) VCColor.elementAt(i)), 10) + "|";
                String SPack            = common.Rad(common.getRound(dPackWt, 3), 11) + "|";

                String Process          = common.Cad(getInstrucion((String) VOrderNo.elementAt(i)), 20) + "|";

                String Emp1             = common.Pad("", 10) + "|";
                String Emp3             = common.Pad("", 18) + "|";
                String DueDepo          = common.Cad((String) VDueDepo.elementAt(i), 5) + "|";
                String DueMill          = common.Cad((String) VDueMill.elementAt(i), 5) + "|";
                String FDelay           = common.Cad((String) VDLMix.elementAt(i), 5) + "|";
                String RDelay           = common.Cad((String) VDF.elementAt(i), 5) + "|";
                String DDelay           = common.Cad((String) VDRMix.elementAt(i), 5) + "|";
                String PDelay           = common.Cad((String) VPTotal.elementAt(i), 8) + "|";
                String SpxRunOut        = common.Cad((String) VRunDelay.elementAt(i), 10) + "|";
                String SCreeling        = common.Cad((String) VCleerDelay.elementAt(i), 10) + "|";
                String SBalance         = common.Rad(common.getRound(dBalance, 3), 10) + "|";

                String Emp6             = common.Pad("", 7) + "|";
                String SRemarks         = common.Pad(getNilRemark((String) VRemarks.elementAt(i)), 15) + "|";
                String SProcessRemarks  = common.Pad(common.parseNull((String) VProcessRemarks.elementAt(i)), 25) + "|";

                String SStrSpace = "|" + common.Space(4)
                        + "|" + common.Space(12)
                        + "|" + common.Space(10)
                        + "|" + common.Space(10)
                        + "|" + common.Space(5)
                        + "|" + common.Space(29)
                        + "|" + common.Space(8)
                        + "|" + common.Space(8)
                        + "|" + common.Space(11)
                        + "|" + common.Space(10)
                        + "|" + common.Space(20)
                        + "|" + common.Space(5)
                        + "|" + common.Space(5)
                        + "|" + common.Space(5)
                        + "|" + common.Space(5)
                        + "|" + common.Space(8)
                        + "|" + common.Space(10)
                        + "|" + common.Space(10)
                        + "|" + common.Space(7)
                        + "|" + common.Space(10)
                        + "|" + common.Space(10)
                        + "|" + common.Space(15)
                        + "|" + common.Space(25) + "|";
                prnWriter(SNo + OrdNo + OrdDate + Due + Count + Party + OrdWt + MixWt + SPack + SBalance + Process + DueDepo + DueMill + FDelay + DDelay + PDelay + SpxRunOut + SCreeling + Emp6 + Emp1 + SpxRunOut + SRemarks + SProcessRemarks);
                //prnWriter(SNo+OrdNo+OrdDate+Due+Count+Party+OrdWt+MixWt+BColor+RColor+CColor+SPack+SBalance+Process+DueDepo+DueMill+FDelay+DDelay+PDelay+SpxRunOut+SCreeling+Emp6+Emp1+SpxRunOut+SRemarks+SProcessRemarks);
                prnWriter(SStrSpace);
                SPriorityStatus = common.parseNull((String) VPartyPriority.elementAt(i));
            }

            String S2 = "|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|";
//               prnWriter(End);
            prnWriter(S2);
            String S1 = "|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|";
            prnWriter("|" + common.Space(4)
                    + "|" + common.Space(12)
                    + "|" + common.Space(10)
                    + "|" + common.Space(10)
                    + "|" + common.Space(5)
                    + "|" + common.Cad("TOTAL", 29)
                    + "|" + common.Rad(common.getRound(dTotOrderWt, 3), 8)
                    + "|" + common.Rad(common.getRound(dTotMixWt, 3), 8)
                    + "|" + common.Rad(common.getRound(dTotPackWt, 3), 11)
                    + "|" + common.Rad(common.getRound(dTotBalance, 3), 10)
                    + "|" + common.Space(20)
                    + "|" + common.Space(5)
                    + "|" + common.Space(5)
                    + "|" + common.Space(5)
                    + "|" + common.Space(5)
                    + "|" + common.Space(8)
                    + "|" + common.Space(10)
                    + "|" + common.Space(10)
                    + "|" + common.Space(7)
                    + "|" + common.Space(10)
                    + "|" + common.Space(10)
                    + "|" + common.Space(15)
                    + "|" + common.Space(25) + "|");

            prnWriter(SParty);

            prnWriter("|" + common.Space(4)
                    + "|" + common.Space(12)
                    + "|" + common.Space(10)
                    + "|" + common.Space(10)
                    + "|" + common.Space(5)
                    + "|" + common.Cad("GRAND TOTAL", 29)
                    + "|" + common.Rad(common.getRound(dGrantOrdTot, 0), 8)
                    + "|" + common.Rad(common.getRound(dGrantMixTot, 0), 8)
                    + "|" + common.Rad(common.getRound(dGrantPackTot, 3), 11)
                    + "|" + common.Rad(common.getRound(dGrantBalance, 3), 10)
                    + "|" + common.Space(20)
                    + "|" + common.Space(5)
                    + "|" + common.Space(5)
                    + "|" + common.Space(5)
                    + "|" + common.Space(5)
                    + "|" + common.Space(8)
                    + "|" + common.Space(10)
                    + "|" + common.Space(10)
                    + "|" + common.Space(7)
                    + "|" + common.Space(10)
                    + "|" + common.Space(10)
                    + "|" + common.Space(15)
                    + "|" + common.Space(25) + "|");

            prnWriter(End);

            dTotOrderWt     = 0;
            dTotMixWt       = 0;
            dTotBalance     = 0;
            dTotPackWt      = 0;

            dGrantOrdTot    = 0;
            dGrantMixTot    = 0;
            dGrantBalance   = 0;
            dGrantPackTot   = 0;
//               prnWriter(End1);
            prnWriter(SNote);
            prnWriter(SNote1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void FileWriter() {
        try {

            //    FW = new FileWriter("//"+SCRIPTHOST+"/d/"+SCRIPTFOLDER+"/src/Temp/SpinningRunoutNew.prn");
//            FW = new FileWriter("d:/SpinningRunout.prn");
            FW = new FileWriter("/software/MixPrint/SpinningRunoutNew.prn");
            //FW = new FileWriter("d://SpinningRunoutNew.prn");

            Head();
            Body();
            FW.close();
        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
        }
    }

    private void prnWriter(String Str) {
        try {
            TLine = TLine + 1;
            if (TLine < 53) {
                FW.write(Str + "\n");
            } else {

                TLine = 0;
                FW.write(Str + "\n");
                FW.write(End + "\n");
                FW.write(SNote + "\n");
                FW.write(SNote1 + "\n");
                iPage = iPage + 1;

                Head();
            }
        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
        }
    }

    /*
          private String getNilRemark(String Str)
          {
              Str = common.parseNull(Str).trim();
              if(Str.equals("Nil")|| Str.equals("NIL"))
              return "";
     
              if(Str.length()==3) 
                    Str ="";
     
              return Str;
          }
     */
    private String getNilRemark(String Str) {
        return "" + Str;
    }

    public void setInstruction() {

        VInsOrderNo     = new Vector();
        VInsShadeName   = new Vector();
        VInsRefNo       = new Vector();
        try {

            StringBuffer    sb  = new StringBuffer();
                            sb  . append("  Select  RegularOrder.RorderNo, ShadeGroupName , RefOrderNo, Shortage  from RegularOrder ");
            if (SUnit.trim().equals("A_Unit")) {
                sb.append("  Inner Join processa.processingofindent on processingofindent.rorderno = regularorder.rorderno and processingofindent.packcomp = 0");
            }
            if (SUnit.trim().equals("B_Unit")) {
                sb.append("  Inner Join process.processingofindent on processingofindent.rorderno = regularorder.rorderno and processingofindent.packcomp = 0");
            }
            sb.append("  Left  Join OrderShadeGroup On OrderShadeGroup.id =RegularOrder.Shadeid");
            sb.append("  inner join unit on unit.unitcode= RegularOrder.UnitCode  ");
            if (!SUnit.equals("All")) {
                sb.append("  Where Unit.UnitName='" + SUnit + "' ");
            }

            JDBCConnection  jdbc = JDBCConnection.getJDBCConnection();
            Connection      conn = jdbc.getConnection();

            Statement       stat = conn.createStatement();
            ResultSet       res = stat.executeQuery(sb.toString());

            while (res.next()) {
                VInsOrderNo     . addElement(res.getString(1));
                VInsShadeName   . addElement(res.getString(2));
                VInsRefNo       . addElement(res.getString(3));
            }
            res.close();
            stat.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    private String getInstrucion(String SOrderNo) {

        for (int i = 0; i < VInsOrderNo.size(); i++) {
            String OrdNo = (String) VInsOrderNo.elementAt(i);
            if (SOrderNo.equals(OrdNo)) {
                if (SOrderNo.startsWith("DM")) {
                    return common.parseNull((String) VInsShadeName.elementAt(i));
                } else {
                    String RefNo = (String) VInsRefNo.elementAt(i);
                    if (RefNo.startsWith("PREM")) {
                        return "AS PER SHADE CARD";
                    }
                    if (RefNo.startsWith("SM")) {
                        return "AS PER SAMPLES";
                    }
                    if (RefNo.startsWith("DM") || RefNo.startsWith("LM")) {
                        return "AS PER REGULAR";
                    }
                    if (RefNo.startsWith("DIRECT")) {
                        return "DIRECT MATCHING";
                    }
                    int iNo = common.toInt(RefNo.substring(0, 1));
                    if (iNo > 0) {
                        return "AS PER SAMPLES";
                    }
                }
            }
        }
        return "";
    }

    public void DespatchDetails(int OnDate) {

        VDOrderNo   = new Vector();
        VPacking    = new Vector();

        try {
            StringBuffer    sb  = new StringBuffer();
                            sb  . append("  Select RegularOrder.RorderNo,nvl(Sum(Balepacking.NetWeight),0) as Packing From RegularOrder");
                            sb  . append("  Left  Join Balepacking On Balepacking.Orderid = RegularOrder.Orderid and Balepacking.Flag<>1");
                            sb  . append("  inner join unit on unit.unitcode = RegularOrder.UnitCode ");
                            sb  . append("  Inner Join ProcessingType on ProcessingType.ProcessCode = RegularOrder.ProcessTypeCode ");
            if (SUnit.trim().equals("A_Unit")) {
                sb.append("  Inner Join processa.processingofindent on processingofindent.rorderno = regularorder.rorderno and processingofindent.packcomp=0");
            }
            if (SUnit.trim().equals("B_Unit")) {
                sb.append("  Inner Join process.processingofindent on processingofindent.rorderno = regularorder.rorderno and processingofindent.packcomp=0");
            }
            sb  . append("  Where ");
            if (!SUnit.equals("All")) {
                sb.append("  Unit.UnitName='" + SUnit + "'");
            }
            if (SSelectType.equalsIgnoreCase("Depo")) {
                sb.append("  and regularorder.rorderno like 'DM%'");
            }
            if (SSelectType.equalsIgnoreCase("Party")) {
                sb.append("   and regularorder.rorderno not like 'DM%'");
            }
            if (!SOrderNo1.equals("All")) {
                sb.append("   and regularorder.rorderno='" + SOrderNo1 + "' ");
            }
            if (!SProcessTypeCode.equals("All")) {
                if (SProcessTypeCode.equals("BR-C Orders")) {
                    sb.append("  and ProcessingType.OEStatus = 1 ");
                } else if (SProcessTypeCode.equals("Other than BR-C Orders")) {
                    sb.append(" and ProcessingType.OEStatus = 0 ");
                } else {
                    sb.append("  and RegularOrder.ProcessTypeCode = " + SProcessTypeCode);
                }
            }
            sb.append("  group by RegularOrder.RorderNo order by RegularOrder.RorderNo");
            JDBCConnection  jdbc    = JDBCConnection.getJDBCConnection();
            Connection      conn    = jdbc.getConnection();
            Statement       stat    = conn.createStatement();
            ResultSet       res     = stat.executeQuery(sb.toString());

            while (res.next()) {
                VDOrderNo   . addElement(res.getString(1));
                VPacking    . addElement(res.getString(2));
            }
            res.close();
            stat.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    private String getPackWt(String OrderNo) {
        String SWt = "";
        for (int i = 0; i < VDOrderNo.size(); i++) {
            if (OrderNo.equals((String) VDOrderNo.elementAt(i))) {
                SWt = (String) VPacking.elementAt(i);
            }
        }
        return SWt;
    }
}