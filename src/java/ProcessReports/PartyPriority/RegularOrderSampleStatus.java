/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProcessReports.PartyPriority;

import ProcessReports.jdbc.*;
import ProcessReports.util.*;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

/**
 *
 * @author Administrator
 */
public class RegularOrderSampleStatus extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    Common          common;
    HttpSession     session;
    int             iEnDate = 0;
    int             iStDate = 0;
    String          SUnit   = "";
    String          bgColor, bgHead, bgUom, bgBody, fgHead, fgUom, fgBody;
    String          SStDate, SEnDate;
    Vector          VOrderDate, VOrderNo, VPartyName, VOrderWt, VCount, VRMOrder, VShade, VCotPer, VVisPer, VPolyPer, VDepth, VBasicOrderNo, VStatus, VOrderType, VEntryDate, VLightSource, VRemarks, VYarnType, VReferenceNo;
    Vector          VSampleOrderNo;

    int             iPDLine         = 0;
    int             iDDLine         = 0;
    String          DotLine         = "";
    int             iNineDays       = 0;
    int             iThreeDays      = 0;
    int             iPageCtr        = 0, iLCtr = 100;
    String          sl              = "";
    int             TLine           = 0;
    String          Str1            = "";
    String          Str2            = "";
    String          Str3            = "";
    String          Str4            = "";
    String          Unit            = "";
    String          SSelectType     = "";

    double          dPartyWtL       = 0;
    double          dPartyWtH       = 0;
    double          dDepoWtL        = 0;
    double          dDepoWtH        = 0;
    double          dGrantPartyWtL  = 0;
    double          dGrantPartyWtH  = 0;

    Vector          VFibreCode, VWeight, VDyeOrderNo, VDeliveryDate, VFibreRate, VFRDate, VFibreStatus, VPartyPriority;
    Vector          VLastDate;
    Vector          VTDate, VFDDate, VSDate, VFIDate;
    Vector          VRDOrderNo, VRDepth;

    FileWriter      FW;
    String          SLine   = "";
    String          SLine1  = "";
    String          SLine2  = "";

    String          Empty   = "";
    String          SEnd    = "";

    String          SHead   = "";
    int             iTLine  = 0;
    int             iPage   = 1;
    Vector          VDSOrderNo, VDDepth;
    String          Head1, Head2, Head3, Head4, Head5;
    boolean         bDepo;
    boolean         bParty;
    Vector          VUnitCode, VUnitName;
    int             iUnitCode = 0;
    Vector          VSampleNo, VSampleDepth;
    Vector          VRegOrderNo, VRegRMOrderNo, VOrderStatus;
    Vector          VDepthWt, VDepthOrd;

    Vector          theGIVector, theReceiptVector;
    int             iDyeingStatus = 0;

    String          SProcessTypeCode, SProcessTypeName;
    String          SServer, SShadeCardRefNo, SShadeCardRefOrderNo;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

                    response. setContentType("text/html;charset=UTF-8");
        PrintWriter out     = response.getWriter();

        common          = new Common();
        bgColor         = common.bgColor;
        bgHead          = common.bgHead;
        bgUom           = common.bgUom;
        bgBody          = common.bgBody;
        fgHead          = common.fgHead;
        fgUom           = common.fgUom;
        fgBody          = common.fgBody;

        iEnDate         = common.toInt(common.pureDate((String) request.getParameter("TEnDate")));

        SUnit           = request.getParameter("Unit");
        SSelectType     = common.parseNull((String) request.getParameter("Select"));

        try {
            StringTokenizer ST = new StringTokenizer(request.getParameter("processType"), "#");
            while (ST.hasMoreTokens()) {
                SProcessTypeCode = ST.nextToken();
                SProcessTypeName = ST.nextToken();
            }
        } catch (Exception ex) {
            SProcessTypeCode = "All";
            SProcessTypeName = "All";
        }
        System.out.println("1");
        initValue();
        System.out.println("2");
        getReceiptDetails();
        System.out.println("3");
        getGIDetails();

        try {
            System.out.println("4");
            setDepthVect();
            System.out.println("5");
            setVectorData();
            System.out.println("6");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        setRegDepth();
        System.out.println("7");
        setSampleDepth();
        System.out.println("8");
        PremDepth();
        System.out.println("9");
        setRMOrderNos();
        System.out.println("10");
        FibreWeightData();
        System.out.println("12");
        DateAlign();
        System.out.println("13");

        try {
            setHtml(out);
            System.out.println("14");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        getPrintWriter();
        out.close();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void initValue() {
        iTLine      = 0;
        iPage       = 1;
        dPartyWtL   = 0;
        dPartyWtH   = 0;
        dDepoWtL    = 0;
        dDepoWtH    = 0;
        bDepo       = false;
        bParty      = false;
        iNineDays   = 0;
        iThreeDays  = 0;
        iLCtr       = 100;
    }

    private void setHtml(PrintWriter out) throws Exception {
        initValue();
        System.out.println("start print");
        SStDate = common.parseDate(String.valueOf(iStDate));
        SEnDate = common.parseDate(String.valueOf(iEnDate));

        out.println("<html>");
        out.println("<head>");
        out.println("<title>InvPro</title>");
        out.println("</head>");
        out.println("<body bgcolor='" + bgColor + "' text = '#0000FF'>");
        out.println("<p align='center'><font size='5'><b><u>Regular Order Sample Status Report</u> ");
        out.println(" As On:" + SEnDate + "</b></font></p>");

        out.println("<p><b>Process Type : " + SProcessTypeName + "</b></p>");

        out.println("<div align='center'>");
        out.println("  <table border='0' bordercolor='" + bgColor + "'>");
        out.println("    <tr>");
        out.println("      <td  rowspan='2' align='center' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>SL.No</b></font></td>");
        out.println("      <td  rowspan='2'align='center' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>OrderDate</b></font></td>");
        out.println("      <td  rowspan='2'align='center' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>OrderNo</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>PartyName</b></font></td>");
        out.println("      <td  colspan='2' align='center' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>OrderWeight</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>Count</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>RMOrderNo</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>Status</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>FD-LD Date</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>Total Days</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>FD Days</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>Sample Days</b></font></td>");
        for (int i = 0; i <= 3; i++) {
            out.println("      <td  rowspan='2' align='center' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>Dyeing Code/Kgs</b></font></td>");
        }
        out.println("      <td  rowspan='2' align='center' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>Shade</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>Light Source</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>Yarn Type</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>Blend</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>Depth</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>Bleached Cotton Depth</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>Basic OrderNo</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>Remarks</b></font></td>");
        out.println("    </tr>");
        out.println("    <tr>");
        out.println("      <td   align='center' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>Below 1500 (kgs)</b></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>Above 1500 (kgs)</b></font></td>");
        out.println("    </tr>");
        out.println("    <tr>");
        out.println("      <td colspan='25' align='Left' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>PartyOrders</b></font></td>");
        out.println("    </tr>");

        String  SPriorityStatus = "";
        int     iSNo            = 0;

        double dGrantPartyWtL   = 0;
        double dGrantPartyWtH   = 0;

        double dBelowCountTotal = 0;
        double dAboveCountTotal = 0;

        ArrayList   theCountList= null;
                    theCountList= new ArrayList();
                    theCountList. clear();

        for (int i = 0; i < VOrderNo.size(); i++) {
            if (!common.parseNull((String) VPartyPriority.elementAt(i)).equals(SPriorityStatus)) {
                if (i != 0) {
                    setCountTotal(out, dBelowCountTotal, dAboveCountTotal);
                    dBelowCountTotal = 0;
                    dAboveCountTotal = 0;
                }

                if (i != 0) {
                    out.println("<tr>");
                    out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
                    out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
                    out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
                    out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'><b>Total</b></font></td>");
                    out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'><b>" + common.getRound(dPartyWtL, 3) + "</b></font></td>");
                    out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'><b>" + common.getRound(dPartyWtH, 3) + "</b></font></td>");
                    out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
                    out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
                    out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
                    out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
                    out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
                    out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
                    out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
                    out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
                    out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
                    out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
                    out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
                    out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
                    out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
                    out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
                    out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
                    out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
                    out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
                    out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
                    out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
                    out.println("</tr>");
                    dPartyWtL   = 0;
                    dPartyWtH   = 0;
                    iSNo        = 0;
                }
                out.println("    <tr>");
                out.println("      <td colspan=25 width='30' bgcolor='" + bgBody + "' align='Left'><font color='" + fgBody + "'>" + ((String) VPartyPriority.elementAt(i)) + "</font></td>");
                out.println("    </tr>");
                theCountList.clear();
            }
            String SEntryDt = common.parseNull((String) VEntryDate.elementAt(i));
            try {
                SEntryDt = SEntryDt.substring(11, SEntryDt.length());
            } catch (Exception ex) {
                SEntryDt = "";
            }
            String  SOrderNo        = (String) VOrderNo.elementAt(i);
            String  SParty          = (String) VPartyName.elementAt(i);
            String  SShade          = (String) VShade.elementAt(i);
            String  SLightSource    = (String) VLightSource.elementAt(i);
            int     iOrderType      = common.toInt((String) VOrderType.elementAt(i));

            if (!SOrderNo.startsWith("DM") && !SParty.startsWith("AMARJOTHI")) {
                String SCountName = common.parseNull((String) VCount.elementAt(i));
                // set Count Wise Total..
                if (theCountList.size() > 0 && !theCountList.contains(SCountName)) {
                    setCountTotal(out, dBelowCountTotal, dAboveCountTotal);

                    dBelowCountTotal = 0;
                    dAboveCountTotal = 0;
                }
                        iSNo        = iSNo + 1;
                        bParty      = true;
                String  SVisPer     = (String) VVisPer.elementAt(i);
                String  SCotPer     = (String) VCotPer.elementAt(i);
                String  SPolyPer    = (String) VPolyPer.elementAt(i);
               
                System.out.println("prnn===>"+(String) VOrderWt.elementAt(i));
                
                double  dOrderWt    = common.toDouble((String)  VOrderWt        . elementAt(i));
                String  SBasic      = common.parseNull((String) VBasicOrderNo   . elementAt(i));
                String  SStatus     = common.parseNull((String) VStatus         . elementAt(i));

                out.println("<tr>");
                out.println("      <td   align='center' bgcolor='" + bgBody + "'><font color='" + fgBody + "'>" + String.valueOf(iSNo) + "</font></td>");
                out.println("      <td   align='center' bgcolor='" + bgBody + "'><font color='" + fgBody + "'>" + common.parseDate((String) VOrderDate.elementAt(i)) + "  " + SEntryDt + "</font></td>");
                out.println("      <td   align='center' bgcolor='" + bgBody + "'><font color='" + fgBody + "'>" + SOrderNo + "</td>");
                out.println("      <td   align='center' bgcolor='" + bgBody + "'><font color='" + fgBody + "'>" + SParty + "</font></td>");
                if (dOrderWt <= 1500) {
                    dPartyWtL           = dPartyWtL + dOrderWt;
                    dGrantPartyWtL      += dOrderWt;
                    dBelowCountTotal    += dOrderWt;

                    out.println("      <td   align='center' bgcolor='" + bgBody + "'><font color='" + fgBody + "'>" + common.getRound(dOrderWt, 3) + "</font></td>");
                    out.println("      <td   align='center' bgcolor='" + bgBody + "'><font color='" + fgBody + "'></font></td>");
                } else {
                    dPartyWtH           = dPartyWtH + dOrderWt;
                    dGrantPartyWtH      += dOrderWt;
                    dAboveCountTotal    += dOrderWt;

                    out.println("      <td   align='center' bgcolor='" + bgBody + "'><font color='" + fgBody + "'></font></td>");
                    out.println("      <td   align='center' bgcolor='" + bgBody + "'><font color='" + fgBody + "'>" + dOrderWt + "</font></td>");
                }
                out.println("      <td   align='center' bgcolor='" + bgBody + "'><font color='" + fgBody + "'>" + (String) VCount.elementAt(i) + "</font></td>");
                out.println("      <td   align='center' bgcolor='" + bgBody + "'><font color='" + fgBody + "'>" + getRMOrderNo(i, SOrderNo) + "</font></td>");
                out.println("      <td   align='center' bgcolor='" + bgBody + "'><font color='" + fgBody + "'>" + SStatus + "</font></td>");
                out.println("      <td   align='center' bgcolor='" + bgBody + "'><font color='" + fgBody + "'>" + getFibreDyeingDate(SOrderNo, SStatus) + "</font></td>");

                out.println("      <td   align='center' bgcolor='" + bgBody + "'><font color='" + fgBody + "'>" + (String) VTDate.elementAt(i) + "</font></td>");
                out.println("      <td   align='center' bgcolor='" + bgBody + "'><font color='" + fgBody + "'>" + (String) VFDDate.elementAt(i) + "</font></td>");
                out.println("      <td   align='center' bgcolor='" + bgBody + "'><font color='" + fgBody + "'>" + (String) VSDate.elementAt(i) + "</font></td>");
                getFibreRate(SOrderNo, SStatus);
                for (int j = 0; j <= 3; j++) {
                    out.println("      <td   align='center' bgcolor='" + bgBody + "'><font color='" + fgBody + "'>" + common.parseNull((String) VFibreRate.elementAt(j)) + "</font></td>");
                }
                out.println("      <td   align='center' bgcolor='" + bgBody + "'><font color='" + fgBody + "'>" + SShade + "</font></td>");
                out.println("      <td   align='center' bgcolor='" + bgBody + "'><font color='" + fgBody + "'>" + SLightSource + "</font></td>");
                out.println("      <td   align='center' bgcolor='" + bgBody + "'><font color='" + fgBody + "'>" + (String) VYarnType.elementAt(i) + "</font></td>");

//                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>Cot%:"+SCotPer+"Vis%:"+SVisPer+"Poly%:"+SPolyPer+"</font></td>");
                out.println("      <td   align='center' bgcolor='" + bgBody + "'><font color='" + fgBody + "'>" + SCotPer + "</font></td>");
                out.println("      <td   align='center' bgcolor='" + bgBody + "'><font color='" + fgBody + "'>" + getDepth(SBasic) + "</font></td>");
                out.println("      <td   align='center' bgcolor='" + bgBody + "'><font color='" + fgBody + "'>" + getDepthVal(SBasic) + "</font></td>");

                String SReferenceNo = common.parseNull((String) VReferenceNo.elementAt(i));
                String SBasicLink   = "";

                if (SReferenceNo.equals("1")) {
                    SBasicLink = "<a href='" + SServer + "Mixing.sordermixing3.RegularOrderMixing?OrderNo=" + SBasic + "'>" + SBasic + "</a>";
                } else if (SReferenceNo.equals("2")) {
                    SBasicLink = "<a href='" + SServer + "Mixing.sordermixing3.SampleOrderMixingList?OrderNo=" + SBasic + "'>" + SBasic + "</a>";
                } else if (SReferenceNo.equals("3")) {
                    setShadeCardRefNo(SBasic);
                    /*
                        if(SShadeCardRefNo.equals("1"))
                        {
                             SBasicLink= "<a href='"+SServer+"Mixing.sordermixing3.RegularOrderMixing1?OrderNo="+SShadeCardRefOrderNo+"'>"+SBasic+"</a>";
                        }
                        else
                        {
                             SBasicLink= "<a href='"+SServer+"Mixing.sordermixing3.SampleOrderMixing1?OrderNo="+SShadeCardRefOrderNo+"'>"+SBasic+"</a>";
                        }
                    */
                    if (SShadeCardRefNo.equals("1")) {
                        SBasicLink = "<a href='" + SServer + "Mixing.sordermixing3.RegularOrderMixing?OrderNo=" + SShadeCardRefOrderNo + "'>" + SBasic + "</a>";
                    } else {
                        SBasicLink = "<a href='" + SServer + "Mixing.sordermixing3.SampleOrderMixingList?OrderNo=" + SShadeCardRefOrderNo + "'>" + SBasic + "</a>";
                    }
                }
                out.println("      <td   align='center' bgcolor='" + bgBody + "'><font color='" + fgBody + "'>" + SBasicLink + "</font></td>");
                out.println("      <td   align='center' bgcolor='" + bgBody + "'><font color='" + fgBody + "'>" + (String) VRemarks.elementAt(i) + "</font></td>");
                out.println("</tr>");
                theCountList.add(SCountName);
            }
            SPriorityStatus = common.parseNull((String) VPartyPriority.elementAt(i));
        }

        setCountTotal(out, dBelowCountTotal, dAboveCountTotal);
        dBelowCountTotal = 0;
        dAboveCountTotal = 0;
        out.println("<tr>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'><b>Total</b></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'><b>" + common.getRound(dPartyWtL, 3) + "</b></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'><b>" + common.getRound(dPartyWtH, 3) + "</b></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("</tr>");

        out.println("<tr>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'><b>Grand Total</b></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'><b>" + common.getRound(dGrantPartyWtL + dGrantPartyWtH, 3) + "</b></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("</tr>");

        dGrantPartyWtL = 0;
        dGrantPartyWtH = 0;

        setDepoHtml(out);

        out.println("  </table>");
        out.println("  </center>");
        out.println("</div>");
        out.println("");
        out.println("</body>");
        out.println("");
        out.println("</html>");
        out.close();
    }

    private void setDepoHtml(PrintWriter out) {
        int     iSNo            = 0;

        out.println("    <tr>");
        out.println("      <td colspan='25' align='left' bgcolor='" + bgHead + "'><font color='" + fgHead + "'><b>DepoOrders</b></font></td>");
        out.println("    </tr>");

        double  dBelowCountTotal = 0;
        double  dAboveCountTotal = 0;

        ArrayList   theCountList    = null;
                    theCountList    = new ArrayList();
                    theCountList    . clear();

        for (int i = 0; i < VOrderNo.size(); i++) {

            String  SOrderNo    = (String) VOrderNo.elementAt(i);
            String  SParty      = (String) VPartyName.elementAt(i);
            String  SShade      = (String) VShade.elementAt(i);
            String  SLightSource= (String) VLightSource.elementAt(i);
            int     iOrderType  = common.toInt((String) VOrderType.elementAt(i));
            String  SEntryDt    = common.parseNull((String) VEntryDate.elementAt(i));
            try {
                SEntryDt = SEntryDt.substring(11, SEntryDt.length());
            } catch (Exception ex) {
                SEntryDt = "";
            }
            if (SOrderNo.startsWith("DM") || SParty.startsWith("AMARJOTHI") && SShade.startsWith("LGREY")) {
                String SCountName = common.parseNull((String) VCount.elementAt(i));

                // set Count Wise Total..
                if (theCountList.size() > 0 && !theCountList.contains(SCountName)) {
                    setCountTotal(out, dBelowCountTotal, dAboveCountTotal);
                    dBelowCountTotal = 0;
                    dAboveCountTotal = 0;
                }

                        bDepo   = true;
                        iSNo    = iSNo + 1;
                String  SBasic  = "";
                String  SVisPer = (String) VVisPer.elementAt(i);
                String  SCotPer = (String) VCotPer.elementAt(i);
                String  SPolyPer= (String) VPolyPer.elementAt(i);
                double  dOrderWt= common.toDouble((String) VOrderWt.elementAt(i));
                        SBasic  = common.parseNull((String) VBasicOrderNo.elementAt(i));
                String  SStatus = common.parseNull((String) VStatus.elementAt(i));
                out.println("<tr>");
                out.println("      <td   align='center' bgcolor='" + bgBody + "'><font color='" + fgBody + "'>" + String.valueOf(iSNo) + "</font></td>");
                out.println("      <td   align='center' bgcolor='" + bgBody + "'><font color='" + fgBody + "'>" + common.parseDate((String) VOrderDate.elementAt(i)) + "  " + SEntryDt + "</font></td>");
                out.println("      <td   align='center' bgcolor='" + bgBody + "'><font color='" + fgBody + "'>" + SOrderNo + "</td>");
                out.println("      <td   align='center' bgcolor='" + bgBody + "'><font color='" + fgBody + "'>" + (String) VPartyName.elementAt(i) + "</font></td>");
                if (dOrderWt <= 1500) {
                    dDepoWtL            = dDepoWtL + dOrderWt;
                    dBelowCountTotal    += dOrderWt;
                    out.println("      <td   align='center' bgcolor='" + bgBody + "'><font color='" + fgBody + "'>" + common.getRound(dOrderWt, 3) + "</font></td>");
                    out.println("      <td   align='center' bgcolor='" + bgBody + "'><font color='" + fgBody + "'></font></td>");
                } else {
                    dDepoWtH            = dDepoWtH + dOrderWt;
                    dAboveCountTotal    += dOrderWt;
                    out.println("      <td   align='center' bgcolor='" + bgBody + "'><font color='" + fgBody + "'></font></td>");
                    out.println("      <td   align='center' bgcolor='" + bgBody + "'><font color='" + fgBody + "'>" + (String) VOrderWt.elementAt(i) + "</font></td>");
                }
                out.println("      <td   align='center' bgcolor='" + bgBody + "'><font color='" + fgBody + "'>" + (String) VCount.elementAt(i) + "</font></td>");
                out.println("      <td   align='center' bgcolor='" + bgBody + "'><font color='" + fgBody + "'>" + (String) getRMOrderNo(i, SOrderNo) + "</font></td>");
                out.println("      <td   align='center' bgcolor='" + bgBody + "'><font color='" + fgBody + "'>" + SStatus + "</font></td>");
                out.println("      <td   align='center' bgcolor='" + bgBody + "'><font color='" + fgBody + "'>" + getFibreDyeingDate(SOrderNo, SStatus) + "</font></td>");
                out.println("      <td   align='center' bgcolor='" + bgBody + "'><font color='" + fgBody + "'>" + (String) VTDate.elementAt(i) + "</font></td>");
                out.println("      <td   align='center' bgcolor='" + bgBody + "'><font color='" + fgBody + "'>" + (String) VFDDate.elementAt(i) + "</font></td>");
                out.println("      <td   align='center' bgcolor='" + bgBody + "'><font color='" + fgBody + "'>" + (String) VSDate.elementAt(i) + "</font></td>");
                getFibreRate(SOrderNo, SStatus);
                for (int j = 0; j <= 3; j++) {
                    out.println("      <td   align='center' bgcolor='" + bgBody + "'><font color='" + fgBody + "'>" + common.parseNull((String) VFibreRate.elementAt(j)) + "</font></td>");
                }
                out.println("      <td   align='center' bgcolor='" + bgBody + "'><font color='" + fgBody + "'>" + (String) VShade.elementAt(i) + "</font></td>");
                out.println("      <td   align='center' bgcolor='" + bgBody + "'><font color='" + fgBody + "'>" + (String) VLightSource.elementAt(i) + "</font></td>");
                out.println("      <td   align='center' bgcolor='" + bgBody + "'><font color='" + fgBody + "'>" + (String) VYarnType.elementAt(i) + "</font></td>");

//                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>Cot%:"+SCotPer+"Vis%:"+SVisPer+"Poly%:"+SPolyPer+"</font></td>");
                out.println("      <td   align='center' bgcolor='" + bgBody + "'><font color='" + fgBody + "'>" + SCotPer + "</font></td>");
                out.println("      <td   align='center' bgcolor='" + bgBody + "'><font color='" + fgBody + "'>" + getDepth(SBasic) + "</font></td>");
                String SReferenceNo = common.parseNull((String) VReferenceNo.elementAt(i));
                String SBasicLink   = "";
                if (SReferenceNo.equals("1")) {
                    SBasicLink = "<a href='" + SServer + "Mixing.sordermixing3.RegularOrderMixing?OrderNo=" + SBasic + "'>" + SBasic + "</a>";
                } else if (SReferenceNo.equals("2")) {
                    SBasicLink = "<a href='" + SServer + "Mixing.sordermixing3.SampleOrderMixingList?OrderNo=" + SBasic + "'>" + SBasic + "</a>";
                } else if (SReferenceNo.equals("3")) {
                    setShadeCardRefNo(SBasic);
                    /*
                        if(SShadeCardRefNo.equals("1"))
                        {
                             SBasicLink= "<a href='"+SServer+"Mixing.sordermixing3.RegularOrderMixing1?OrderNo="+SShadeCardRefOrderNo+"'>"+SBasic+"</a>";
                        }
                        else
                        {
                             SBasicLink= "<a href='"+SServer+"Mixing.sordermixing3.SampleOrderMixing1?OrderNo="+SShadeCardRefOrderNo+"'>"+SBasic+"</a>";
                        }
                    */
                    if (SShadeCardRefNo.equals("1")) {
                        SBasicLink = "<a href='" + SServer + "Mixing.sordermixing3.RegularOrderMixing?OrderNo=" + SShadeCardRefOrderNo + "'>" + SBasic + "</a>";
                    } else {
                        SBasicLink = "<a href='" + SServer + "Mixing.sordermixing3.SampleOrderMixingList?OrderNo=" + SShadeCardRefOrderNo + "'>" + SBasic + "</a>";
                    }
                }

                out.println("      <td   align='center' bgcolor='" + bgBody + "'><font color='" + fgBody + "'>" + SBasicLink + "</font></td>");
                out.println("      <td   align='center' bgcolor='" + bgBody + "'><font color='" + fgBody + "'>" + getDepthVal(SBasic) + "</font></td>");
                out.println("      <td   align='center' bgcolor='" + bgBody + "'><font color='" + fgBody + "'>" + (String) VRemarks.elementAt(i) + "</font></td>");
                out.println("</tr>");
                theCountList.add(SCountName);
            }
        }

        setCountTotal(out, dBelowCountTotal, dAboveCountTotal);
        dBelowCountTotal = 0;
        dAboveCountTotal = 0;
        out.println("<tr>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'><b>Total</b></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'><b>" + common.getRound(dDepoWtL, 3) + "</b></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'><b>" + common.getRound(dDepoWtH, 3) + "</b></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("</tr>");

        out.println("<tr>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'><b>Grand Total</b></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'><b>" + common.getRound(dDepoWtL + dDepoWtH, 3) + "</b></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'><b></b></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("</tr>");
    }

    private void setCountTotal(PrintWriter out, double dBelowCountTotal, double dAboveCountTotal) {
        out.println("<tr>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'><b>Count Wise Total</b></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'><b>" + common.getRound(dBelowCountTotal, 3) + "</b></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'><b>" + common.getRound(dAboveCountTotal, 3) + "</b></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("      <td   align='center' bgcolor='" + bgUom + "'><font color='" + fgUom + "'></font></td>");
        out.println("</tr>");
    }

    private void setVectorData() {

        VOrderDate      = new Vector();
        VOrderNo        = new Vector();
        VPartyName      = new Vector();
        VOrderWt        = new Vector();
        VCount          = new Vector();
        VRMOrder        = new Vector();
        VShade          = new Vector();
        VLightSource    = new Vector();
        VYarnType       = new Vector();
        VCotPer         = new Vector();
        VVisPer         = new Vector();
        VPolyPer        = new Vector();
        VDepth          = new Vector();
        VBasicOrderNo   = new Vector();
        VStatus         = new Vector();
        VOrderType      = new Vector();
        VEntryDate      = new Vector();
        VRemarks        = new Vector();
        VSampleOrderNo  = new Vector();
        VPartyPriority  = new Vector();
        VReferenceNo    = new Vector();

        String  QS  =    " Select  T.OrderDate,T.ROrderNo,PartyName,t.Weight,CountName,"
                        + //           " CotPer,VisPer,PolyPer,T.RefOrderNo,Depth,subStr(SOrderNo,0,length(SOrderNo)-1),"+
                        " Get_Blend_From_ROrd_Det(t.ROrderId),VisPer,PolyPer,T.RefOrderNo,Depth,subStr(SOrderNo,0,length(SOrderNo)-1),"
                        + " Color.ColorName,Generalstatus.Status,T.OrderType,T.AuthDate,t.SourceName,t.Instruction,T.SampleOrderNo,t.Name,t.Code, t.FormName, t.ReferenceNo "
                        + " From "
                        + " ("
                        + " Select RegularOrder.AuthDate,RegularOrder.OrderDate, RegularOrder.ROrderNo,PartyMaster.PartyName,"
                        + " RegularOrder.Weight,YarnCount.CountName,"
                        + " CotPer,VisPer,PolyPer,RegularOrder.RefOrderNo,RMixir.Depth,Max(RegularOrderStatus.id) as id, "
                        + " RegularOrder.ColorCode,max(SampleOrder.Orderid) as Orderid ,"
                        + " RegularOrder.UnitCode,RegularOrder.OrderType,LightSource.SourceName,RegularOrder.Instruction, "
                        + " SampleToRegularAuth.SampleOrderNo,PartyPriority.Name,PartyPriority.Code, RegularOrder.Orderid as ROrderId, FibreForm.FormName, RegularOrder.ReferenceNo "
                        + " from RegularORder "
                        + " Inner Join Unit On Unit.UnitCode = regularorder.UnitCode and RegularOrder.Status=1";
                if (!SUnit.equals("All")) {
                    QS = QS + " and Unit.UnitName ='" + SUnit + "'";
                }
                QS  = QS    + " Inner Join PartyMaster On PartyMaster.PartyCode = RegularOrder.PartyCode"
                            + " Inner Join PartyPriority On PartyPriority.Code = nvl(PartyMaster.PriorityStatus,0) "
                            + " inner join ProcessingType on ProcessingType.ProcessCode = regularorder.ProcessTypeCode "
                            + " Inner Join FibreForm on FibreForm.FormCode = RegularOrder.FibreFormCode "
                            + " left join SampleToRegularAuth on SampleToRegularAuth.RegularORderNo=RegularORder.ROrderNO "
                            + " Left  Join SampleOrder On SampleOrder.ForOrder = RegularOrder.ROrderNo"
                            + " Inner Join YarnCount On YarnCount.CountCode = RegularOrder.CountCode"
                            + " Inner Join Blend On Blend.BlendCode = RegularOrder.BlendCode"
                            + " Left  Join RMixir On RmixIr.OrdNo = RegularOrder.RefOrderNo and Rmixir.CorrectionMixing=0"
                            + " Left Join RegularOrderStatus On  RegularOrderStatus.Orderid = RegularOrder.Orderid"
                            + " Left join LightSource on LightSource.SourceCode= Regularorder.LightCode";

        QS += " WHere  RegularOrder.OrderDate >= 20061001 and RegularOrder.OrderDate < = " + common.getNextDate(iEnDate) + " and RegularOrder.Shortage<>2 And RegularOrder.RorderNo not Like 'T%'and RegularOrder.RorderNo not Like 'PR%'"
                + " and (SampleToRegularAuth.status is Null Or SampleToRegularAuth.Status=0)"
                + " and RegularOrder.AgeOrderNo is null ";
        if (!SProcessTypeCode.equals("All")) {
            if (SProcessTypeCode.equals("BR-C Orders")) {
                QS += " and ProcessingType.OEStatus = 1 ";
            } else if (SProcessTypeCode.equals("Other than BR-C Orders")) {
                QS += " and ProcessingType.OEStatus = 0 ";
            } else {
                QS += " and RegularOrder.ProcessTypeCode = " + SProcessTypeCode;
            }
        }
                QS +=  " group by  RegularOrder.AuthDate, RegularOrder.OrderDate, RegularOrder.ROrderNo,PartyMaster.PartyName,"
                + " RegularOrder.Weight,YarnCount.CountName,"
                + " CotPer,VisPer,PolyPer,RegularOrder.RefOrderNo,RMixir.Depth,RegularOrder.ColorCode,RegularOrder.Unitcode,RegularOrder.OrderType,LightSource.SourceName,RegularOrder.Instruction,SampleToRegularAuth.SampleOrderNo,PartyPriority.Name,PartyPriority.Code, RegularOrder.Orderid, FibreForm.FormName, RegularOrder.ReferenceNo "
                + " )t"
                + " Left join  OrderGroupDetails on OrderGroupDetails.ORderno=t.ROrderNo"
                + " Left  Join SampleOrder On SampleOrder.OrderId = T.OrderId"
                + " Left Join Color On Color.ColorCode = T.ColorCode"
                + " Left Join  RegularOrderStatus On RegularOrderStatus.id = T.id"
                + " Left  Join GeneralStatus On GeneralStatus.Statuscode = RegularorderStatus.StatusCode"
                //+ " Inner Join Unit On Unit.UnitCode = T.UnitCode"
                + " Where T.RorderNo not in('DM02312') and OrderGroupDetails.OrderNo is Null";
        if (SSelectType.equalsIgnoreCase("Depo")) {
            QS = QS + " and t.rorderno like 'DM%'";
        }
        if (SSelectType.equalsIgnoreCase("Party")) {
            QS = QS + " and t.rorderno not like 'DM%'";
        }
        QS = QS + " Group By"
                + " T.OrderDate,T.ROrderNo,PartyName,t.Weight,CountName,"
                + " CotPer,VisPer,PolyPer,T.RefOrderNo,Depth,T.id ,subStr(SOrderNo,0,length(SOrderNo)-1), "
                + " GeneralStatus.Status,Color.ColorName,T.OrderType,T.AuthDate,t.SourceName,t.Instruction, "
                + " T.SampleOrderNo,t.Name,t.Code, t.ROrderId, t.FormName, t.ReferenceNo "
                + " Order By  t.Code desc,CountName,T.OrderDate ";

        System.out.println(QS);
        try {
            /*
            FileWriter FW = new FileWriter("/software/MixPrint/RegularOrderSampleStatus.txt");
            FW.write(QS);
            FW.close();
*/
            JDBCConnection  jdbc    = JDBCConnection.getJDBCConnection();
            Connection      conn    = jdbc.getConnection();
            Statement       stat    = conn.createStatement();
            ResultSet       res     = stat.executeQuery(QS);

            while (res.next()) {
                VOrderDate      . addElement(res.getString(1));
                VOrderNo        . addElement(res.getString(2));
                VPartyName      . addElement(res.getString(3));
                VOrderWt        . addElement(res.getString(4));
                VCount          . addElement(res.getString(5));
                VCotPer         . addElement(res.getString(6));
                VVisPer         . addElement(res.getString(7));
                VPolyPer        . addElement(res.getString(8));
                VBasicOrderNo   . addElement(res.getString(9));
                VDepth          . addElement(res.getString(10));
                VRMOrder        . addElement(res.getString(11));
                VShade          . addElement(res.getString(12));
                VStatus         . addElement(res.getString(13));
                VOrderType      . addElement(res.getString(14));
                VEntryDate      . addElement(res.getString(15));
                VLightSource    . addElement(res.getString(16));
                VRemarks        . addElement(common.parseNull(res.getString(17)));
                VSampleOrderNo  . addElement(common.parseNull(res.getString(18)));
                VPartyPriority  . addElement(common.parseNull(res.getString(19)));
                VYarnType       . addElement(common.parseNull(res.getString(21)));
                VReferenceNo    . addElement(common.parseNull(res.getString(22)));
            }
            res.close();
            stat.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void FibreWeightData() {

        VFibreCode      = new Vector();
        VWeight         = new Vector();
        VDyeOrderNo     = new Vector();
        VDeliveryDate   = new Vector();
        VFRDate         = new Vector();
        VFIDate         = new Vector();
        VOrderStatus    = new Vector();

        String  QS =    " Select  ROrderNo ,FibreCode, sum(weight) ,max(OrderDate),max(DeliveryDate),max(gidate),OrderCompleted"
                    +   " From ( Select ROrderNo,DyeingOrderDetails.FibreCode,Sum(DyeingOrderDetails.Weight)  as weight ,DyeingOrderNo,OrderCompleted"
                    +   " From RegularORder "
                    +   " Left join SampleToRegularAuth on SampleToRegularAuth.RegularORderNo=RegularORder.ROrderNO"
                    +   " Left Join DyeingOrderDetails On  DyeingOrderDetails.OrderNo = RegularOrder.ROrderNo"
                    +   " WHere  OrderDate>=20061001 and REgularOrder.Status=1 "
                    +   " And RegularOrder.Shortage<>2 And RegularOrder.RorderNo not Like 'T%' and RegularOrder.RorderNo not Like 'PR%'"
                    +   " and (SampleToRegularAuth.status is Null Or SampleToRegularAuth.Status=0)"
                    +   " and RegularOrder.AgeOrderNo is null "
                    +   " group by ROrderNo ,DyeingOrderDetails.FibreCode,DyeingOrderNo,OrderCompleted )t"
                    +   " Inner Join DyeingOrder On DyeingOrder.OrderNo = T.DyeingOrderNo"
                    +   " Left Join Agndetails On AgnDetails.OrderNo = T.DyeingOrderNo"
                    +   " Left Join Agn On Agn.agnNo = AgnDetails.AgnNo"
                    +   " group by ROrderNo,FibreCode,OrderCompleted";

        try {
            JDBCConnection  jdbc    = JDBCConnection.getJDBCConnection();
            Connection      conn    = jdbc.getConnection();
            Statement       stat    = conn.createStatement();
            ResultSet       res     = stat.executeQuery(QS);
            while (res.next()) {
                VDyeOrderNo     . addElement(res.getString(1));
                VFibreCode      . addElement(res.getString(2));
                VWeight         . addElement(res.getString(3));
                VFIDate         . addElement(res.getString(4));
                VDeliveryDate   . addElement(res.getString(5));
                VFRDate         . addElement(res.getString(6));
                VOrderStatus    . addElement(res.getString(7));
            }
            res.close();
            stat.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    private String getFibreDyeingDate(String SOrderNo, String Status) {
        Status = common.parseNull(Status);
        if (Status.equals("FD")) {
            for (int i = 0; i < VDyeOrderNo.size(); i++) {
                String DyeingOrderNo = (String) VDyeOrderNo.elementAt(i);
                if (SOrderNo.equals(DyeingOrderNo)) {
                    return common.parseDate((String) VDeliveryDate.elementAt(i));
                }
            }
        }
        return "";
    }

    private String getFibreReciptDate(String SOrderNo) {

        String SDate = "";
        for (int i = 0; i < VDyeOrderNo.size(); i++) {
            String DyeingOrderNo = (String) VDyeOrderNo.elementAt(i);
            if (SOrderNo.equals(DyeingOrderNo)) {
                SDate = (String) VFRDate.elementAt(i);
            }
        }
        return SDate;
    }

    private String getFibreIssueDate(String SOrderNo) {

        String SDate = "";
        for (int i = 0; i < VDyeOrderNo.size(); i++) {
            String DyeingOrderNo = (String) VDyeOrderNo.elementAt(i);
            if (SOrderNo.equals(DyeingOrderNo)) {
                SDate = (String) VFIDate.elementAt(i);
            }
        }
        return SDate;
    }

    private void DateAlign() {

        try {
            VTDate  = new Vector();
            VFDDate = new Vector();
            VSDate  = new Vector();

            for (int i = 0; i < VOrderNo.size(); i++) {
                int     iFibreDy    = 0;
                int     iIssueDt    = 0;
                int     iReceiptDt  = 0;

                String  Order       = common.parseNull((String) VOrderNo.elementAt(i));
                int     iOrderDt    = common.toInt((String) VOrderDate.elementAt(i));
                int     iRepDt      = common.toInt(common.getServerDate());
                        iIssueDt    = common.toInt(getFibreIssueDate(Order));
                        iReceiptDt  = common.toInt(getFibreReciptDate(Order));
                /*
                      if(iReceiptDt!=0 && iIssueDt!=0)
                      {
                         iFibreDy  = common.getDateDiff(iReceiptDt,iIssueDt);
                      }
                      else if(iIssueDt!=0 && iReceiptDt==0)
                      {
                         iFibreDy  = common.getDateDiff(iRepDt,iIssueDt);
                      }
                 */
                String  SStatus     = common.parseNull((String) VStatus.elementAt(i));
                int     iTotalDy    = common.getDateDiff(iRepDt, iOrderDt) + 1;
                        iFibreDy    = getGIDate(Order, (String) VSampleOrderNo.elementAt(i), common.parseNull((String) VOrderDate.elementAt(i)));

                VTDate.addElement(String.valueOf(iTotalDy));

                if (iFibreDy > 0) {
                    VFDDate.addElement(String.valueOf(iFibreDy));
                    VSDate.addElement(String.valueOf(iTotalDy - iFibreDy));
                } else {
                    if (SStatus.equals("FD")) {
                        VFDDate.addElement(String.valueOf(iTotalDy));
                        VSDate.addElement("0");
                    } else {
                        VFDDate.addElement("0");
                        VSDate.addElement(String.valueOf(iTotalDy));
                    }
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    private void getPrintWriter() {
        try {

//            FW = new FileWriter("/software/MixPrint/RegularOrderSampleStatusNew.prn");
            FW = new FileWriter("d://RegularOrderSampleStatusNew.prn");

            iTLine = 0;

            prnHead();
            if (bParty) {
                prnBody();
            }
            if (bDepo) {
                DepoBody();
            }
            FW.close();

        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
        }
    }

    private void prnHead() {

        try {

            if (iLCtr < 56) {
                return;
            }

            if (iPageCtr > 0) {
                FW.write(sl + "\n" + "");
            }
            iPageCtr++;

            String  SPitch  = "M\n";
            String  Str1    = "Company   : AMARJOTHI SPINNING MILLS LIMITED\n";
            String  Str2    = "Document   : Regular Order Sample Status  \n";
            String  Str3    = "As On Date : " + common.parseDate(SEnDate) + "\n";
            String  Unit    = "Unit       : " + SUnit + ", Process Type : " + SProcessTypeName + "\n";
            String  Str4    = "Page No    : " + iPage + "\n M";

                    SHead   = "|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|";
                    Head1   = "|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n";
                    Head2   = "|    |          |             |                         |Ord.Weight(kgs)|     |          |         |          |     |     |     |          |          |          |          |           |           |          |              |      |Bleach|          |                    |\n";
                    Head3   = "|    |  Order   |  OrderNo    |           Party Name    |---------------|Count|RM OrderNo|  Status |  FD-LD   |Total|  FD |Samp.| Dyeing   | Dyeing   | Dyeing   | Dyeing   |   Shade   |   Light   |   Yarn   |     Blend    | Depth|Cotton|  Basic   |    Remarks         |\n";
                    Head4   = "|S.No|   Date   |             |                         | Below | Above |     |          |         |   Date   |Days | Days|Days | Code/Kgs | Code/Kgs | Code/Kgs | Code/Kgs |           |  Source   |   Type   |              |      |Depth | OrderNo  |                    |\n";
                    Head5   = "|    |          |             |                         | 1500  | 1500  |     |          |         |          |     |     |     |          |          |          |          |           |           |          |              |      |      |          |                    |\n";
                    SLine   = "|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|";
            String  SLineHead   = "|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|";
                    SLine1  = "|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|";
                    SEnd    = "|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|";

//                SLine2 = "|=====-====================-=============-=============================-==========-==========-=========-==========-==========================-===========-======-======-======-==========-==========-==========-=========--===========-===========-==============-======-=======-==========-==============================|";

            /*                 Empty      ="|"+common.Space(4)+"|"
                                  +common.Space(10)+"|"
                                  +common.Space(13)+"|"
                                  +common.Space(29)+"|"
                                  +common.Space(10)+"|"
                                  +common.Space(10)+"|"
                                  +common.Space(9)+"|"
                                  +common.Space(10)+"|"
                                  +common.Space(26)+"|"
                                  +common.Space(11)+"|"
                                  +common.Space(6)+"|"
                                  +common.Space(6)+"|"
                                  +common.Space(6)+"|"
                                  +common.Space(10)+"|"
                                  +common.Space(10)+"|"
                                  +common.Space(10)+"|"
                                  +common.Space(10)+"|"
                                  +common.Space(11)+"|"
                                  +common.Space(11)+"|"
                                  +common.Space(14)+"|"
                                  +common.Space(6)+"|"
                                  +common.Space(7)+"|"
                                  +common.Space(10)+"|"
                                  +common.Space(30)+"|";  */
            prnWriter(Str1 + Str2 + Str3 + Unit + Str4);
            if (bParty) {
                if (SUnit.equals("Sample_Unit")) {
                    prnWriter("SAMPLE UNIT REGULARORDER SAMPLING STATUS ");
                } else {
                    prnWriter("PARTY ORDER DETAILS");
                }
                prnWriter(Head1 + Head2 + Head3 + Head4 + Head5 + SLineHead);
                iLCtr = 10;
            }
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }

    private void prnBody() {
        int     iSNo            = 0;
                iTLine          = 0;
        String  SPriorityStatus = "";

        double  dGrantPartyWtL  = 0;
        double  dGrantPartyWtH  = 0;
        double  dPartyWtL       = 0;
        double  dPartyWtH       = 0;
        double dBelowCountTotal = 0;
        double dAboveCountTotal = 0;

        ArrayList   theCountList    = null;
                    theCountList    = new ArrayList();
                    theCountList    . clear();

        for (int i = 0; i < VOrderNo.size(); i++) {
            if (!common.parseNull((String) VPartyPriority.elementAt(i)).equals(SPriorityStatus)) {
                if (i != 0) {
                    setCountTotal(dBelowCountTotal, dAboveCountTotal);
                    dBelowCountTotal = 0;
                    dAboveCountTotal = 0;
                }

                if (i != 0) {
                    String STotal = "|" + common.Space(4) + "|"
                            + common.Space(10) + "|"
                            + common.Space(13) + "|"
                            + common.Cad("Total", 25) + "|"
                            + common.Rad(common.getRound(dPartyWtL, 0), 7) + "|"
                            + common.Rad(common.getRound(dPartyWtH, 0), 7) + "|"
                            + common.Space(5) + "|"
                            + common.Space(10) + "|"
                            + common.Space(9) + "|"
                            + common.Space(10) + "|"
                            + common.Space(5) + "|"
                            + common.Space(5) + "|"
                            + common.Space(5) + "|"
                            + common.Space(10) + "|"
                            + common.Space(10) + "|"
                            + common.Space(10) + "|"
                            + common.Space(10) + "|"
                            + common.Space(11) + "|"
                            + common.Space(11) + "|"
                            + common.Space(10) + "|"
                            + common.Space(14) + "|"
                            + common.Space(6) + "|"
                            + common.Space(6) + "|"
                            + common.Space(10) + "|"
                            + common.Space(20) + "|";

                    dPartyWtL = 0;
                    dPartyWtH = 0;
                    iSNo = 0;
                    //                   iLCtr=iLCtr+2;
                    //               iTLine =iTLine+10;
                    iLCtr = iLCtr + 15;
                    prnWriter(STotal);
                }

                String SPartyStatus = "|" + common.Space(4) + "|"
                        + common.Space(10) + "|"
                        + common.Space(13) + "|"
                        + common.Cad(common.parseNull((String) VPartyPriority.elementAt(i)), 25) + "|"
                        + common.Space(7) + "|"
                        + common.Space(7) + "|"
                        + common.Space(5) + "|"
                        + common.Space(10) + "|"
                        + common.Space(9) + "|"
                        + common.Space(10) + "|"
                        + common.Space(5) + "|"
                        + common.Space(5) + "|"
                        + common.Space(5) + "|"
                        + common.Space(10) + "|"
                        + common.Space(10) + "|"
                        + common.Space(10) + "|"
                        + common.Space(10) + "|"
                        + common.Space(11) + "|"
                        + common.Space(11) + "|"
                        + common.Space(10) + "|"
                        + common.Space(14) + "|"
                        + common.Space(6) + "|"
                        + common.Space(6) + "|"
                        + common.Space(10) + "|"
                        + common.Space(20) + "|";

                /*
                                       +common.Space(13)+"|"
                                       +common.Cad(common.parseNull((String)VPartyPriority.elementAt(i)),29)+"|"
                                    "|"+common.Pad(common.parseNull((String)VPartyPriority.elementAt(i)),27)+"@M"+ common.Space(84)+"|"+common.Space(10)+
                                       +common.Space(10)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(9)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(26)+"|"
                                       +common.Space(11)+"|"
                                       +common.Space(6)+"|"
                                       +common.Space(6)+"|"
                                       +common.Space(6)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(11)+"|"
                                       +common.Space(11)+"|"
                                       +common.Space(14)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(6)+"|"
                                       +common.Space(7)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(20)+"|";
                 */
                String S1 = "|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|";
                prnWriter(S1);
                prnWriter(SPartyStatus);
                prnWriter(S1);
                theCountList.clear();
            }
            String SEntryDt = common.parseNull((String) VEntryDate.elementAt(i));
            try {
                SEntryDt = SEntryDt.substring(11, SEntryDt.length());
            } catch (Exception ex) {
                SEntryDt = "";
            }
            String  SOrderNo        = (String) VOrderNo.elementAt(i);
            String  SBasic          = common.parseNull((String) VBasicOrderNo.elementAt(i));
            String  Party           = (String) VPartyName.elementAt(i);
            String  Shade           = (String) VShade.elementAt(i);
            String  SLightSource    = (String) VLightSource.elementAt(i);

            if (!SOrderNo.startsWith("DM") && !Party.startsWith("AMARJOTHI")) {

                String SCountName = common.parseNull((String) VCount.elementAt(i));

                // set Count Wise Total..
                if (theCountList.size() > 0 && !theCountList.contains(SCountName)) {
                    setCountTotal(dBelowCountTotal, dAboveCountTotal);

                    dBelowCountTotal = 0;
                    dAboveCountTotal = 0;
                }
                String SVisPer          = (String) VVisPer.elementAt(i);
                String SCotPer          = (String) VCotPer.elementAt(i);
                String SPolyPer         = (String) VPolyPer.elementAt(i);

//                              String SBlend  =SCotPer+"%C,"+SVisPer+"%V,"+SPolyPer+"%P";
                String  SBlend          = SCotPer;
                        iSNo            = iSNo + 1;
                String  SOrdWt1         = "";
                String  SOrdWt2         = "";
                String  SGrantOrdWt1    = "";
                String  SGrantOrdWt2    = "";

                double  dOrdWt = common.toDouble((String) VOrderWt.elementAt(i));

                if (dOrdWt <= 1500) {
                    SOrdWt1             = common.getRound(dOrdWt, 0);
                    SGrantOrdWt1        = common.getRound(dOrdWt, 0);
                    dPartyWtL           += dOrdWt;
                    dGrantPartyWtL      += dOrdWt;
                    dBelowCountTotal    += dOrdWt;
                } else {
                    SOrdWt2             = common.getRound(dOrdWt, 0);
                    SGrantOrdWt2        = common.getRound(dOrdWt, 0);

                    dPartyWtH           += dOrdWt;
                    dGrantPartyWtH      += dOrdWt;
                    dAboveCountTotal    += dOrdWt;
                }
                String  SNo             = "|" + common.Cad(String.valueOf(iSNo), 4) + "|";
                String  SOrderDate      = common.Cad(common.parseDate((String) VOrderDate.elementAt(i)), 10) + "|";
                String  SOrderNo1       = common.Pad(SOrderNo, 13) + "|";
                //String SOrderDate = common.Cad(common.parseDate((String) VOrderDate.elementAt(i)) + "  " + SEntryDt, 10) + "|";
                String  SParty          = common.Pad((String) VPartyName.elementAt(i), 25) + "|";
                String  SCount          = common.Cad((String) VCount.elementAt(i), 5) + "|";
                String  SRMOrder        = common.Pad(getRMOrderNo(i, SOrderNo), 10) + "|";
                String  SStatus         = common.Pad((String) VStatus.elementAt(i), 9) + "|";
                String  SFibreDt        = common.Pad(getFibreDyeingDate(SOrderNo, (String) VStatus.elementAt(i)), 10) + "|";
                String  STDate          = common.Cad((String) VTDate.elementAt(i), 5) + "|";
                String  SFDDate         = common.Cad((String) VFDDate.elementAt(i), 5) + "|";
                String  SDate           = common.Cad((String) VSDate.elementAt(i), 5) + "|";
                String  SShade          = common.Pad(common.parseNull((String) VShade.elementAt(i)), 11) + "|";
                        SLightSource    = common.Pad(common.parseNull((String) VLightSource.elementAt(i)), 11) + "|";
                String  SRemarks        = common.Pad((String) VRemarks.elementAt(i), 20) + "|";
                String  SYarnType       = common.Pad((String) VYarnType.elementAt(i), 10) + "|";
                String  SBDepth         = common.Rad(getDepthVal(SBasic), 6) + "|";
                        SBlend          = common.Pad(SBlend, 14) + "|";
                String  SDepth          = common.Cad(getDepth(SBasic), 6) + "|";
                        SBasic          = common.Pad(SBasic, 10) + "|";
                getFibreRate(SOrderNo, common.parseNull((String) VStatus.elementAt(i)));
                String  SFibreRW1       = common.Rad((String) VFibreRate.elementAt(0), 10) + "|";
                String  SFibreRW2       = common.Rad((String) VFibreRate.elementAt(1), 10) + "|";
                String  SFibreRW3       = common.Rad((String) VFibreRate.elementAt(2), 10) + "|";
                String  SFibreRW4       = common.Rad((String) VFibreRate.elementAt(3), 10) + "|";
                        SOrdWt1         = common.Rad(SOrdWt1, 7) + "|";
                        SOrdWt2         = common.Rad(SOrdWt2, 7) + "|";
                getDaysLine((String) VTDate.elementAt(i));
                prnWriter(SNo + SOrderDate + SOrderNo1 + SParty + SOrdWt1 + SOrdWt2 + SCount + SRMOrder + SStatus + SFibreDt + STDate + SFDDate + SDate + SFibreRW1 + SFibreRW2 + SFibreRW3 + SFibreRW4 + SShade + SLightSource + SYarnType + SBlend + SDepth + SBDepth + SBasic + SRemarks);
                prnWriter(SLine1);
                theCountList.add(SCountName);
            }
            SPriorityStatus = common.parseNull((String) VPartyPriority.elementAt(i));
        }
        setCountTotal(dBelowCountTotal, dAboveCountTotal);

        dBelowCountTotal = 0;
        dAboveCountTotal = 0;

        String STotal = "|" + common.Space(4) + "|"
                + common.Space(10) + "|"
                + common.Space(13) + "|"
                + common.Cad("Total", 25) + "|"
                + common.Rad(common.getRound(dPartyWtL, 0), 7) + "|"
                + common.Rad(common.getRound(dPartyWtH, 0), 7) + "|"
                + common.Space(5) + "|"
                + common.Space(10) + "|"
                + common.Space(9) + "|"
                + common.Space(10) + "|"
                + common.Space(5) + "|"
                + common.Space(5) + "|"
                + common.Space(5) + "|"
                + common.Space(10) + "|"
                + common.Space(10) + "|"
                + common.Space(10) + "|"
                + common.Space(10) + "|"
                + common.Space(11) + "|"
                + common.Space(11) + "|"
                + common.Space(10) + "|"
                + common.Space(14) + "|"
                + common.Space(6) + "|"
                + common.Space(6) + "|"
                + common.Space(10) + "|"
                + common.Space(20) + "|";

        String SGTotal = "|" + common.Space(4) + "|"
                + common.Space(10) + "|"
                + common.Space(13) + "|"
                + common.Cad("Grand Total", 25) + "|"
                + common.Rad(common.getRound(dGrantPartyWtL + dGrantPartyWtH, 0), 7) + "|"
                + common.Rad("", 7) + "|"
                + common.Space(5) + "|"
                + common.Space(10) + "|"
                + common.Space(9) + "|"
                + common.Space(10) + "|"
                + common.Space(5) + "|"
                + common.Space(5) + "|"
                + common.Space(5) + "|"
                + common.Space(10) + "|"
                + common.Space(10) + "|"
                + common.Space(10) + "|"
                + common.Space(10) + "|"
                + common.Space(11) + "|"
                + common.Space(11) + "|"
                + common.Space(10) + "|"
                + common.Space(14) + "|"
                + common.Space(6) + "|"
                + common.Space(6) + "|"
                + common.Space(10) + "|"
                + common.Space(20) + "|";

        dGrantPartyWtL  = 0;
        dGrantPartyWtH  = 0;
        dPartyWtL       = 0;
        dPartyWtH       = 0;

        try {
            prnWriter(STotal);
            prnWriter(SLine);
            prnWriter(SGTotal);
            iSNo++;
//                      prnWriter(SEnd+"");
            prnWriter(SEnd + "\n");
            prnWriter(" Report Taken As On  Date " + common.parseDate(common.getServerDate()) + " Time : " + common.getServerTime() + "");
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    private void DepoBody() {

        iTLine = 0;

        prnWriter("DEPO ORDER DETAILS");
        prnWriter(Str1 + Str2 + Str3 + Unit + Str4);
        prnWriter(Head1 + Head2 + Head3 + Head4 + Head5 + SLine);

        int         iSNo                = 0;

        double      dBelowCountTotal    = 0;
        double      dAboveCountTotal    = 0;

        ArrayList   theCountList= null;
                    theCountList= new ArrayList();
                    theCountList. clear();

        for (int i = 0; i < VOrderNo.size(); i++) {

            String SOrderNo     = (String) VOrderNo.elementAt(i);
            String SBasic       = common.parseNull((String) VBasicOrderNo.elementAt(i));
            String Party        = (String) VPartyName.elementAt(i);
            String Shade        = (String) VShade.elementAt(i);
            String SLightSource = (String) VLightSource.elementAt(i);
            String SEntryDt     = common.parseNull((String) VEntryDate.elementAt(i));
            try {
                SEntryDt = SEntryDt.substring(11, SEntryDt.length());
            } catch (Exception ex) {
                SEntryDt = "";
            }
            if (SOrderNo.startsWith("DM") || Party.startsWith("AMARJOTHI") && Shade.startsWith("LGREY")) {
                String SCountName = common.parseNull((String) VCount.elementAt(i));

                // set Count Wise Total..
                if (theCountList.size() > 0 && !theCountList.contains(SCountName)) {
                    setCountTotal(dBelowCountTotal, dAboveCountTotal);

                    dBelowCountTotal = 0;
                    dAboveCountTotal = 0;
                }
                String SVisPer = (String) VVisPer.elementAt(i);
                String SCotPer = (String) VCotPer.elementAt(i);
                String SPolyPer = (String) VPolyPer.elementAt(i);
//                              String SBlend  =SCotPer+"%C,"+SVisPer+"%V,"+SPolyPer+"%P";
                String SBlend = SCotPer;
                iSNo = iSNo + 1;
                String SOrdWt1 = "";
                String SOrdWt2 = "";
                double dOrdWt = common.toDouble((String) VOrderWt.elementAt(i));
                if (dOrdWt <= 1500) {
                    SOrdWt1 = common.getRound(dOrdWt, 0);
                    dBelowCountTotal += dOrdWt;
                } else {
                    SOrdWt2 = common.getRound(dOrdWt, 0);
                    dAboveCountTotal += dOrdWt;
                }
                String  SNo             = "|" + common.Cad(String.valueOf(iSNo), 4) + "|";
                String  SOrderNo1       = common.Pad(SOrderNo, 13) + "|";
//                String SOrderDate = common.Cad(common.parseDate((String) VOrderDate.elementAt(i)) + "  " + SEntryDt, 20) + "|";
                String  SOrderDate      = common.Cad(common.parseDate((String) VOrderDate.elementAt(i)), 10) + "|";
                String  SParty          = common.Pad((String) VPartyName.elementAt(i), 25) + "|";
                String  SCount          = common.Cad((String) VCount.elementAt(i), 5) + "|";
                String  SRMOrder        = common.Pad(getRMOrderNo(i, SOrderNo), 10) + "|";
                String  SStatus         = common.Pad((String) VStatus.elementAt(i), 9) + "|";
                String  SFibreDt        = common.Pad(getFibreDyeingDate(SOrderNo, (String) VStatus.elementAt(i)), 10) + "|";
                String  STDate          = common.Cad((String) VTDate.elementAt(i), 5) + "|";
                String  SFDDate         = common.Cad((String) VFDDate.elementAt(i), 5) + "|";
                String  SDate           = common.Cad((String) VSDate.elementAt(i), 5) + "|";
                String  SCode           = common.Cad("", 10) + "|";
                String  SShade          = common.Pad(common.parseNull((String) VShade.elementAt(i)), 11) + "|";
                        SLightSource    = common.Pad(common.parseNull((String) VLightSource.elementAt(i)), 11) + "|";
                String  SYarnType       = common.Pad((String) VYarnType.elementAt(i), 10) + "|";
                        SBlend          = common.Pad(SBlend, 14) + "|";
                String  SDepth          = common.Cad(getDepth(SBasic), 6) + "|";
                        SBasic          = common.Pad(SBasic, 10) + "|";
                String  SRemarks        = common.Pad((String) VRemarks.elementAt(i), 20) + "|";

                String  SBDepth         = common.Rad(getDepthVal(SBasic), 6) + "|";

                getFibreRate(SOrderNo, common.parseNull((String) VStatus.elementAt(i)));
                String  SFibreRW1       = common.Rad((String) VFibreRate.elementAt(0), 10) + "|";
                String  SFibreRW2       = common.Rad((String) VFibreRate.elementAt(1), 10) + "|";
                String  SFibreRW3       = common.Rad((String) VFibreRate.elementAt(2), 10) + "|";
                String  SFibreRW4       = common.Rad((String) VFibreRate.elementAt(3), 10) + "|";

                        SOrdWt1         = common.Rad(SOrdWt1, 7) + "|";
                        SOrdWt2         = common.Rad(SOrdWt2, 7) + "|";
                getDaysLine((String) VTDate.elementAt(i));
                prnWriter(SNo + SOrderDate + SOrderNo1 + SParty + SOrdWt1 + SOrdWt2 + SCount + SRMOrder + SStatus + SFibreDt + STDate + SFDDate + SDate + SFibreRW1 + SFibreRW2 + SFibreRW3 + SFibreRW4 + SShade + SLightSource + SYarnType + SBlend + SDepth + SBDepth + SBasic + SRemarks);
                prnWriter(SLine);
                theCountList.add(SCountName);
            }
        }

        setCountTotal(dBelowCountTotal, dAboveCountTotal);
        dBelowCountTotal = 0;
        dAboveCountTotal = 0;

        String STotal = "|" + common.Space(4) + "|"
                + common.Space(10) + "|"
                + common.Space(13) + "|"
                + common.Cad("Total", 25) + "|"
                + common.Rad(common.getRound(dDepoWtL, 0), 7) + "|"
                + common.Rad(common.getRound(dDepoWtH, 0), 7) + "|"
                + common.Space(5) + "|"
                + common.Space(10) + "|"
                + common.Space(9) + "|"
                + common.Space(10) + "|"
                + common.Space(5) + "|"
                + common.Space(5) + "|"
                + common.Space(5) + "|"
                + common.Space(10) + "|"
                + common.Space(10) + "|"
                + common.Space(10) + "|"
                + common.Space(10) + "|"
                + common.Space(11) + "|"
                + common.Space(11) + "|"
                + common.Space(10) + "|"
                + common.Space(14) + "|"
                + common.Space(6) + "|"
                + common.Space(6) + "|"
                + common.Space(10) + "|"
                + common.Space(20) + "|";

        String SGTotal = "|" + common.Space(4) + "|"
                + common.Space(10) + "|"
                + common.Space(13) + "|"
                + common.Cad("Grand Total", 25) + "|"
                + common.Rad(common.getRound(dDepoWtL + dDepoWtH, 0), 7) + "|"
                + common.Rad("", 7) + "|"
                + common.Space(5) + "|"
                + common.Space(10) + "|"
                + common.Space(9) + "|"
                + common.Space(10) + "|"
                + common.Space(5) + "|"
                + common.Space(5) + "|"
                + common.Space(5) + "|"
                + common.Space(10) + "|"
                + common.Space(10) + "|"
                + common.Space(10) + "|"
                + common.Space(10) + "|"
                + common.Space(11) + "|"
                + common.Space(11) + "|"
                + common.Space(10) + "|"
                + common.Space(14) + "|"
                + common.Space(6) + "|"
                + common.Space(6) + "|"
                + common.Space(10) + "|"
                + common.Space(20) + "|";

        TLine = TLine + 10;
        iLCtr = iLCtr + 9;

        try {
            prnWriter(SLine);
            prnWriter(STotal);
            prnWriter(SGTotal);
            iSNo++;
            iLCtr = iLCtr + 2;

            prnWriter(SEnd);
            prnWriter(" Report Taken As On  Date " + common.parseDate(common.getServerDate()) + " Time : " + common.getServerTime() + "");
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    private void setCountTotal(double dBelowCountTotal, double dAboveCountTotal) {
        String STotal = "|" + common.Space(4) + "|"
                + common.Space(10) + "|"
                + common.Space(13) + "|"
                + common.Cad("Count Wise Total", 25) + "|"
                + common.Rad(common.getRound(dBelowCountTotal, 0), 7) + "|"
                + common.Rad(common.getRound(dAboveCountTotal, 0), 7) + "|"
                + common.Space(5) + "|"
                + common.Space(10) + "|"
                + common.Space(9) + "|"
                + common.Space(10) + "|"
                + common.Space(5) + "|"
                + common.Space(5) + "|"
                + common.Space(5) + "|"
                + common.Space(10) + "|"
                + common.Space(10) + "|"
                + common.Space(10) + "|"
                + common.Space(10) + "|"
                + common.Space(11) + "|"
                + common.Space(11) + "|"
                + common.Space(10) + "|"
                + common.Space(14) + "|"
                + common.Space(6) + "|"
                + common.Space(6) + "|"
                + common.Space(10) + "|"
                + common.Space(20) + "|";

        iLCtr = iLCtr + 7;

        prnWriter(STotal);
        prnWriter(SLine);
    }

    private void prnWriter(String Str) {
        try {
            iTLine = iTLine + 1;
            if (iTLine < 56) {
                FW.write(Str + "\n");
            } else {
                iPage = iPage + 1;
                iTLine = 0;
                FW.write(SEnd + "\n");
                prnWriter(" Report Taken As On  Date " + common.parseDate(common.getServerDate()) + " Time : " + common.getServerTime() + "");
                prnHead();
                prnWriter(Head1 + Head2 + Head3 + Head4 + Head5 + SLine);
                prnWriter(Str);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    private void getFibreRate(String SOrderNo, String Status) {

        VFibreRate = new Vector();
        VFibreStatus = new Vector();
        if (Status.equals("FD")) {
            for (int i = 0; i < VFibreCode.size(); i++) {
                String SFibre = (String) VFibreCode.elementAt(i);
                String SWt = (String) VWeight.elementAt(i);
                String SOrdNo = (String) VDyeOrderNo.elementAt(i);

                if (SOrderNo.equals(SOrdNo)) {
                    if (getFDStatus(SOrderNo) == 0) {
                        VFibreRate.addElement(SFibre + "/" + SWt);
                    }
                }
            }
        }
        int iSize = VFibreRate.size();
        for (int j = iSize; j <= 3; j++) {
            String Str = "";
            VFibreRate.add(j, "");
        }
    }

    private String getDepth(String SOrderNo) {

        String SDepth = "";
        if (SOrderNo.equals("DIRECT")) {
            return "";
        }
        SDepth = getRegOrdDepth(SOrderNo);
        if (!SDepth.equals("")) {
            return common.parseNull(SDepth);
        } else {
            SDepth = getPremDepth(SOrderNo);
            if (common.parseNull(SDepth).equals("")) {
                SDepth = getSampleDepth(SOrderNo);
            }
        }
        return SDepth;
    }

    private String getRegOrdDepth(String SOrderNo) {
        String SDepth = "";
        for (int i = 0; i < VRDOrderNo.size(); i++) {
            String SampleNo = common.parseNull((String) VRDOrderNo.elementAt(i));
            if (SampleNo.equals(SOrderNo)) {
                SDepth = common.parseNull((String) VRDepth.elementAt(i));
                return SDepth;
            }
        }
        return SDepth;
    }

    private String getSampleDepth(String SOrderNo) {
        String SDepth = "";
        for (int i = 0; i < VSampleNo.size(); i++) {
            String SampleNo = common.parseNull((String) VSampleNo.elementAt(i));
            if (SampleNo.equals(SOrderNo)) {
                SDepth = common.parseNull((String) VSampleDepth.elementAt(i));
                return SDepth;
            }
        }
        return SDepth;
    }

    private void setSampleDepth() {

        VSampleNo = new Vector();
        VSampleDepth = new Vector();

        String QS = " Select  ROrderNo ,OrdNo,Sum(MRat) from RegularOrder"
                + " Inner Join Smixir  On Smixir.OrdNo  = RegularOrder.RefOrderNo"
                + " Inner Join SBlenr On SBlenr.MixNo   = SMixir.MixNo"
                + " Inner Join Fibre On Fibre.FibreCode = SBlenr.FibCd"
                + " Left Join  SampleToRegularAuth On SampleToRegularAuth.RegularOrderNo=RegularOrder.ROrderNo"
                + " Where  RegularOrder.OrderDate > = 20061001 and RegularOrder.Status=1 and FibCd Not Like 'C%' and Fibre.ColorCode <>241 and Fibre.ColorCode <> 561 "
                + " and (SampleToRegularAuth.status is Null Or SampleToRegularAuth.Status=0)"
                + " and RegularOrder.AgeOrderNo is null "
                + " and RegularOrder.Orderdate < =" + iEnDate
                + " group by ROrderNo,OrdNo";

        try {
            JDBCConnection  jdbc    = JDBCConnection.getJDBCConnection();
            Connection      conn    = jdbc.getConnection();
            Statement       stat    = conn.createStatement();
            ResultSet       res     = stat.executeQuery(QS);
            while (res.next()) {
                VSampleNo.addElement(res.getString(2));
                VSampleDepth.addElement(res.getString(3));
            }
            res.close();
            stat.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    private void setRegDepth() {

        VRDOrderNo = new Vector();
        VRDepth = new Vector();

        String QS = " Select RefOrderNo ,MRate from ( Select   ROrderNo,RefOrderNo,Sum(MRat) as MRate from RegularOrder"
                + " Inner Join Rmixir On RMixir.OrdNo = RegularOrder.RefOrderNo"
                + " Inner Join  Rblenr On RBlenr.mixNo = Rmixir.MixNo"
                + " Inner Join Fibre on Fibre.Fibrecode=RBlenr.FibCd "
                + " Where OrderDate >20061101  and REgularOrder.Status=1 and  FibCd  Not Like 'C%'and fibcd not like ('U%') and CorrectionMixing=0 and Fibre.ColorCode<>241 and Fibre.ColorCode<>561 "
                + " and RegularOrder.AgeOrderNo is null "
                + " group by ROrderNo,RefOrderNo) Group by RefOrderNo ,MRate";
        try {
            JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
            Connection conn = jdbc.getConnection();
            Statement stat = conn.createStatement();
            ResultSet res = stat.executeQuery(QS);
            while (res.next()) {
                VRDOrderNo.addElement(res.getString(1));
                VRDepth.addElement(res.getString(2));
            }
            res.close();
            stat.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    private void PremDepth() {

        VDSOrderNo = new Vector();
        VDDepth = new Vector();

        String QS = " Select  RegularOrder.ROrderNo,RegularOrder.RefOrderNo ,Sum(MRat) from RegularOrder"
                + " Inner Join ShadeCardNo On ShadeCardNo.ORderNo = RegularOrder.RefOrderNo"
                + " Inner Join RmixIr On RmixIr.OrdNo = ShadeCardNo.RefOrderNo"
                + " Inner Join Rblenr On RBlenr.MixNo   = Rmixir.MixNo"
                + " Left Join  SampleToRegularAuth On SampleToRegularAuth.RegularOrderNo=RegularOrder.ROrderNo"
                + " Where RegularOrder.OrderDate > = 20061001 and REgularOrder.Status=1 and RegularOrder.Orderdate < =" + iEnDate + " and  RBlenr.FibCd not Like 'C%' "
                + " and (SampleToRegularAuth.status is Null Or SampleToRegularAuth.Status=0)"
                + " and RegularOrder.AgeOrderNo is null "
                + " Group by RegularOrder.ROrderNo ,RegularOrder.RefOrderNo";

        try {
            JDBCConnection  jdbc    = JDBCConnection.getJDBCConnection();
            Connection      conn    = jdbc.getConnection();
            Statement       stat    = conn.createStatement();
            ResultSet       res     = stat.executeQuery(QS);
            while (res.next()) {
                VDSOrderNo  . addElement(res.getString(2));
                VDDepth     . addElement(res.getString(3));
            }
            res.close();
            stat.close();
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    private void setRMOrderNos() {

        VRegOrderNo     = new Vector();
        VRegRMOrderNo   = new Vector();
        String QS = "Select ROrderNo ,SOrderNo from RegOrderSampleNo Order by ROrderNo";
        try {
            JDBCConnection  jdbc    = JDBCConnection.getJDBCConnection();
            Connection      conn    = jdbc.getConnection();
            Statement       stat    = conn.createStatement();
            ResultSet       res     = stat.executeQuery(QS);
            while (res.next()) {
                VRegOrderNo.addElement(res.getString(1));
                VRegRMOrderNo.addElement(res.getString(2));
            }
            res.close();
            stat.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    private String getPremDepth(String SOrderNo) {
        String SDepth = "";
        for (int i = 0; i < VDSOrderNo.size(); i++) {
            String DSOrderNo = common.parseNull((String) VDSOrderNo.elementAt(i));
            if (DSOrderNo.equals(SOrderNo)) {
                SDepth = common.parseNull((String) VDDepth.elementAt(i));
                return SDepth;
            }
        }
        return SDepth;
    }

    private String getRMOrderNo(int iRow, String OrdNo) {
        String SRMOrder = common.parseNull((String) VRMOrder.elementAt(iRow));
        if (SRMOrder.equals("")) {
            for (int i = 0; i < VRegOrderNo.size(); i++) {
                String SRegNo = common.parseNull((String) VRegOrderNo.elementAt(i));
                if (SRegNo.equals(OrdNo)) {
                    return SRMOrder = common.parseNull((String) VRegRMOrderNo.elementAt(i));
                }
            }
        }
        return SRMOrder;
    }

    private int getFDStatus(String SOrderNo) {
        int iStatus = 1;
        for (int i = 0; i < VDyeOrderNo.size(); i++) {
            String DyeingOrderNo = (String) VDyeOrderNo.elementAt(i);
            if (SOrderNo.equals(DyeingOrderNo)) {
                return Integer.parseInt((String) VOrderStatus.elementAt(i));
            }
        }
        return iStatus;
    }

    private void getDaysLine(String STotalDt) {
        int iTotalDt = common.toInt(STotalDt);
        if (iTotalDt <= 9 && iNineDays == 0) {
//             prnWriter(SLine1);
//             prnWriter(SLine1);
//             prnWriter(SLine2);
//             prnWriter(SLine2);

            iNineDays = 1;
        }
        if (iTotalDt <= 3 && iThreeDays == 0) {
//             prnWriter(SLine1);
//             prnWriter(SLine2);

            iThreeDays = 1;
        }
    }

    private void getGIDetails() {
        theGIVector = new Vector();

        try {
            JDBCConnection1     jdbc            = JDBCConnection1.getJDBCConnection1();
            Connection          theConnection   = jdbc.getConnection();
            Statement           theStatement    = theConnection.createStatement();
            ResultSet           rs              = theStatement.executeQuery(getGIDetailsQS());

            while (rs.next()) {
//                    RMSMDetailsReportClass theClass = null;
//                    theClass            = new RMSMDetailsReportClass();
//                    theClass            . setGIDate(common.parseNull(rs.getString(1)));
//                    theClass            . setOrderNo(common.parseNull(rs.getString(2)));
//                    theGIVector         . addElement(theClass);
            }
            rs.close();
            theStatement.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private String getGIDetailsQS() {
        StringBuffer SB = new StringBuffer();

        /*
          SB.append(" Select Max(Agn.GIDate) as GIDate,DyeingOrderDetails.OrderNo from DyeingOrderDetails ");
          SB.append(" left Join  AgnDetails on DyeingOrderDetails.DyeingOrderNo = AgnDetails.Orderno ");
          SB.append(" Left Join Agn  on AgnDetails.AgnNo = Agn.AgnNo ");
          SB.append(" Group by DyeingOrderDetails.OrderNo ");
          SB.append(" Order by 1 Desc ");
         */
        SB.append(" Select Max(Agn.GIDate) as GIDate,t.OrderNo,t.DyeingOrderNo from ");
        SB.append(" ( ");
        SB.append(" Select Max(DyeingOrderNo) as DyeingOrderNo,OrderNo from DyeingOrderDetails ");
        SB.append(" Group by OrderNo ");
        SB.append(" )t ");
        SB.append(" Inner Join  AgnDetails on t.DyeingOrderNo = AgnDetails.Orderno ");
        SB.append(" Inner Join Agn  on AgnDetails.AgnNo = Agn.AgnNo ");
        SB.append(" Group by t.OrderNo,t.DyeingOrderNo ");
        SB.append(" Order by 1 Desc ");

        System.out.println(SB);
        return SB.toString();
    }

    public void getReceiptDetails() {
        theReceiptVector = new Vector();
        try {
            JDBCConnection1     jdbc            = JDBCConnection1.getJDBCConnection1();
            Connection          theConnection   = jdbc.getConnection();
            Statement           theStatement    = theConnection.createStatement();
            ResultSet           rs              = theStatement.executeQuery(getReceiptDetailsQS());

            while (rs.next()) {
//                    RMSMDetailsReportClass theClass = null;

//                    theClass            = new RMSMDetailsReportClass();
//                    theClass            . setGIDate(common.parseNull(rs.getString(1)));
//                    theClass            . setOrderNo(common.parseNull(rs.getString(2)));
//                    theReceiptVector    . addElement(theClass);
            }

            rs.close();
            theStatement.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private String getReceiptDetailsQS() {
        
        StringBuffer    SB = new StringBuffer();

                        SB.append(" Select Max(GIDate) as GIDate, OrdNo from ");
                        SB.append(" ( ");
                        SB.append(" Select Max(GIDate) as GIDate,GINO,AgnDetails.AgnNo,AgnDetails.BaseCode,DyerWeight,SMixIr.OrdNo from AgnDetails ");
                        SB.append(" Inner join Agn on Agn.AgnNo=AgnDetails.AgnNo ");
                        SB.append(" Inner Join Fibre on Fibre.FibreCode = AgnDetails.BaseCode ");
                        SB.append(" Inner Join SBlenr on SBlenr.FibCd = Fibre.FibreCode ");
                        SB.append(" Inner Join SMixir on SMixir.MixNo = SBlenr.MixNo ");
                        SB.append(" where substr(SMixIr.MixingDate,0,6) >= " + common.getPreviousMonth(common.toInt(String.valueOf(iEnDate).substring(0, 6))));
                        SB.append(" Group by GINo,AgnDetails.AgnNo,AgnDetails.BaseCode,DyerWeight,SMixIr.OrdNo ");
                        SB.append(" Union All ");
                        SB.append(" Select Max(AgnDate) as GIDate,GINO,PurchaseAgn.AgnNo,Fibre.FibreCode,0 as DyerWeight,SMixIr.OrdNo From PurchaseAgn ");
                        SB.append(" Inner join Fibre on Fibre.FibreCode=PurchaseAgn.FibreCode ");
                        SB.append(" Inner Join SBlenr on SBlenr.FibCd = Fibre.FibreCode ");
                        SB.append(" Inner Join SMixir on SMixir.MixNo = SBlenr.MixNo ");
                        SB.append(" where substr(SMixIr.MixingDate,0,6) >= " + common.getPreviousMonth(common.toInt(String.valueOf(iEnDate).substring(0, 6))));
                        SB.append(" Group by AgnDate,GINO,PurchaseAgn.AgnNo,Fibre.FibreCode,SMixIr.OrdNo ");
                        SB.append(" ) ");
                        SB.append(" Group by OrdNo ");
                        SB.append(" order by 1 Desc ");

        return SB.toString();
    }

    private int getGIDate(String SOrderNo, String SSampleOrderNo, String SOrderDate) {
        for (int i = 0; i < theGIVector.size(); i++) {
            /*               RMSMDetailsReportClass theClass = (RMSMDetailsReportClass)theGIVector.elementAt(i);

               if(SOrderNo.equals(theClass.getOrderNo()))
               {
                    int iFDDays    = common.getDateDiff(common.toInt(theClass.getGIDate()),common.toInt(SOrderDate));

                    System.out.println("Gate Inward----"+common.toInt(theClass.getGIDate()));

                    return iFDDays;
               } */
        }
        for (int i = 0; i < theReceiptVector.size(); i++) {
            /*               RMSMDetailsReportClass theClass = (RMSMDetailsReportClass)theReceiptVector.elementAt(i);

               if(SSampleOrderNo.equals(theClass.getOrderNo()))
               {
                    int iFDDays    = common.getDateDiff(common.toInt(theClass.getGIDate()),common.toInt(SOrderDate));

                    System.out.println("Receipt ----"+common.toInt(theClass.getGIDate()));

                    return iFDDays;
               } */
        }
        return 0;
    }

    private void setDepthVect() {
        VDepthWt    = new Vector();
        VDepthOrd   = new Vector();
        try {
            String QS = " Select BleachedCotton,OrdNo from "
                    + " ( "
                    + " select sum(mrat) as BleachedCotton,SMixir.OrdNo from sblenr "
                    + " Inner Join fibre on fibre.fibrecode = sblenr.fibcd "
                    + " Inner Join SMixir on SMixir.MixNo = SBlenr.MixNo "
                    + " where fibre.colorcode=561 "
                    + " Group by SMixir.OrdNo "
                    + " Union All "
                    + " select sum(mrat) as BleachedCotton,RMixir.OrdNo from Rblenr "
                    + " Inner Join fibre on fibre.fibrecode = Rblenr.fibcd "
                    + " Inner Join RMixir on RMixir.MixNo = RBlenr.MixNo "
                    + " where fibre.colorcode=561 "
                    + " Group by RMixir.OrdNo "
                    + " ) ";

            JDBCConnection  jdbc    = JDBCConnection.getJDBCConnection();
            Connection      conn    = jdbc.getConnection();
            Statement       stat    = conn.createStatement();
            ResultSet       res     = stat.executeQuery(QS);
            while (res.next()) {
                VDepthWt.addElement(res.getString(1));
                VDepthOrd.addElement(res.getString(2));
            }
            res.close();
            stat.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    private String getDepthVal(String SBasic) {
        for (int i = 0; i < VDepthWt.size(); i++) {
            if (((String) VDepthOrd.elementAt(i)).equals(SBasic)) {
                return ((String) VDepthWt.elementAt(i));
            }
        }
        return "";
    }

    private void setShadeCardRefNo(String SOrderNo) {
        SShadeCardRefNo         = "";
        SShadeCardRefOrderNo    = "";
        try {
            JDBCConnection      theConnection   = JDBCConnection.getJDBCConnection();
            Connection          connection      = theConnection.getConnection();
            PreparedStatement   st              = connection.prepareStatement(" Select RefNo, RefOrderNo from shadecardNo where OrderNo='" + SOrderNo + "' ");
            ResultSet           rs              = st.executeQuery();
            while (rs.next()) {
                SShadeCardRefNo     = common.parseNull(rs.getString(1));
                SShadeCardRefOrderNo= common.parseNull(rs.getString(2));
            }
            rs.close();
            st.close();
        } catch (Exception ex) {
        }
    }
}
