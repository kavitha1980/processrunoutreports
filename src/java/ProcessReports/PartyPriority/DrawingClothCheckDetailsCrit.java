/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProcessReports.PartyPriority;

import ProcessReports.jdbc.*;
import ProcessReports.util.*;

import java.io.*;
import java.sql.*;           
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

/**
 *
 * @author Administrator
 */
public class DrawingClothCheckDetailsCrit extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    HttpSession session;
    Common common =new Common();
    Vector VUnitCode,VUnitName;
    JDBCConnection theConnect;
    Connection  connect      ;
    String SToday = "";
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        
        response.setContentType("text/html;charset=UTF-8");
        SToday = common.parseDate(common.getCurrentDate());
        PrintWriter out = response.getWriter();
        try {
        
            setDataintoVector();

            out.println("<html>");
            out.println("<body bgcolor='#9AA8D6'>");
            out.println("");
            out.println("<base target='topmain'>");
            out.println("<form method='GET' action='DrawingClothCheckDetails'>");

            out.println("<table>");

            out.println("</tr>");

            out.println("<tr>");
            out.println("<td><font color='#FFFF66'><b>From Date: </b></font></td>");
            out.println("<td><input type='text' name='TStartDate' value="+SToday+" size='9'></td>");
            out.println("<td><font color='#FFFF66'><b>To Date: </b></font></td>");
            out.println("<td><input type='text' name='TEnDate' value="+SToday+" size='9'></td>");
            out.println("<tr>");

            out.println("<tr>");
            out.println("<td><font color='#FFFF66'><b>Unit: </b></font></td>");
            out.println("      <td><select size=1 name=Unit>");
            out.println("      <option value='All'>All</option>");
            for(int i=0;i<VUnitCode.size();i++) {
                out.println("      <option value='"+VUnitName.elementAt(i)+"'>"+VUnitName.elementAt(i)+"</option>");
            }
            out.println("    <tr>");
            out.println("   <tr>");
            out.println("     <td><input type='submit' value='Submit' name='B1' style='color: #003300; font-size: 10pt; font-weight: bold'></td>");
            out.println("     <td><input type='reset' value='Reset' name='B2' style='color: #800000; font-size: 10pt; font-weight: bold'></td>");
            out.println("   </tr>");

            out.println("</table>");
            out.println("</form>");
            out.println("</body>");
            out.println("</html>");
        } catch(Exception ex){
            ex.printStackTrace();
        }finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
     public void setDataintoVector()
     {
         VUnitCode   = new Vector();
         VUnitName   = new Vector();

          try
          {
               theConnect	= JDBCConnection.getJDBCConnection();
               connect      = theConnect.getConnection();
               Statement st = connect.createStatement();
               ResultSet rs = st.executeQuery("select unitcode,unitname from unit where unitcode in (1,2,10) order by 2 ");
			   
               while(rs.next())
               {
                    VUnitCode.addElement(rs.getString(1));
                    VUnitName.addElement(rs.getString(2));
               }
               rs.close();
			   st.close();
			   
          }
          catch(Exception e)
          {
            System.out.println(e);

          }
     }
}
