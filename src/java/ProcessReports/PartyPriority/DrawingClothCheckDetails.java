/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProcessReports.PartyPriority;

import ProcessReports.jdbc.*;
import ProcessReports.util.*;

import java.io.*;
import java.sql.*;           
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

/**
 *
 * @author Administrator
 */
public class DrawingClothCheckDetails extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    Common         common;
    HttpSession    session;
    
    Connection 	   theProcessConnection  = null;    
    Connection 	   theProcessaConnection = null;    

    String SStDate,SEnDate;
    String bgColor,bgHead,bgUom,bgBody,fgHead,fgUom,fgBody;
    String SUnit="";
    String SParty="";

	String SSelectType="";

    String SCount="",SNote="",SNote1="", SNote2="";
    Vector VARNo;
    FileWriter  FW;
    int TLine=0;
    int iPage=1;
    
    int iStDate=0;
    int iEnDate=0;
    int iSlNo=0;
    
    double dTOrdWt=0;
    double dPTOrdWt=0;
    
    int SNo    = 0;
    
    double dGrandOrdWt=0;
 
    Vector    VOrderNo, VCountName, VShade, VReason, VStatus, VTpi, VIssueDate, VIssueUser, VReceiveDate, VReceiveUser,
    	      VCompleteDate,VCompleteUser, VClothCheckRemarks, VTimeDiff;
 
	
    String End="";
    String End1="";
    String Str7="";
	
	//Pdf

     private   Document       document;
     private   PdfPTable      thePdfTable;
    
     private   float 		  fHeight = 0;
     
     private   int            iAbsWidth[]    = {10, 30, 25, 30, 35, 30, 30, 20, 25, 30, 25, 30, 25, 30, 30};

     private   Font           bigbold        = FontFactory.getFont("TIMES_ROMAN", 10, Font.BOLD);
     private   Font           mediumbold     = FontFactory.getFont("TIMES_ROMAN", 8, Font.BOLD);
     private   Font           mediumnormal   = FontFactory.getFont("TIMES_ROMAN", 8, Font.NORMAL);
	
     private   String         SFileName, SFileOpenPath, SFileWritePath;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            response.setContentType("text/html;charset=UTF-8");
            common = new Common();

            bgColor = common.bgColor;
            bgHead  = common.bgHead;
            bgUom   = common.bgUom;
            bgBody  = common.bgBody;
            fgHead  = common.fgHead;
            fgUom   = common.fgUom;
            fgBody  = common.fgBody;
            PrintWriter out = response.getWriter();
            iStDate        = common.toInt(common.pureDate((String)request.getParameter("TStartDate")));
            iEnDate        = common.toInt(common.pureDate((String)request.getParameter("TEnDate")));
            SUnit    	   = request.getParameter("Unit");

            initValues(request);

            setData();
            setHtml(out);

            document . add(new Paragraph("\nReport Taken Date Time : "+common.getSerDateTime(), mediumnormal));
            document . close();
		   
            out.close();
        } catch(Exception ex){
            ex.printStackTrace();
        }finally {
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

	
	private void initValues(HttpServletRequest request){
		
	    VOrderNo          = new Vector();
	    VCountName        = new Vector();
	    VShade            = new Vector();
	    VReason     	  = new Vector();
	    VStatus		      = new Vector();
	    VTpi              = new Vector();
	    VIssueDate        = new Vector();
	    VIssueUser        = new Vector();
		
	    VReceiveDate      = new Vector();
	    VReceiveUser      = new Vector();
	    
		VCompleteDate     = new Vector();
	    VCompleteUser     = new Vector();
		
	    VClothCheckRemarks= new Vector();
	    VTimeDiff         = new Vector();
		
		
		try{
               //SFileName           = "pdfreports/DrawingClothCheck.pdf";
               //SFileOpenPath       = "http://"+request.getServerName()+":"+request.getServerPort()+"/examples/"+SFileName;
               //SFileWritePath      = request.getRealPath("/")+SFileName;
		
               SFileWritePath       = "D://DrawingClothCheck.pdf";
               document            = new Document(PageSize.A4.rotate());
               PdfWriter writer    = PdfWriter.getInstance(document, new FileOutputStream(SFileWritePath));

               document            . open();
			   
               // print Details..

               thePdfTable         = new PdfPTable(15);
               thePdfTable         . setWidths(iAbsWidth);
               thePdfTable         . setWidthPercentage(100);
               thePdfTable         . setHeaderRows(1);
               
			   
		}catch(Exception ex){
			ex.printStackTrace();
		}		
		
	}

    private void setHtml(PrintWriter out) throws  ServletException
    {

    try{
        SStDate            = common.parseDate(String.valueOf(iStDate));
        SEnDate            = common.parseDate(String.valueOf(iEnDate));
        
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Drawing Cloth Check Details</title>");
        out.println("</head>");
        out.println("<body bgcolor='"+bgColor+"' text = '#0000FF'>");
		
	    out.println("<p>");
	    out.println("  <a href='"+SFileOpenPath+"' target='_blank'><b>View this Report in Pdf</b></a>");
	    out.println("</p>");
		
        out.println("<p align='center'><font size='5'><b>Drawing Cloth Check Details -   ");
        out.println(" From : "+SStDate+" To : "+SEnDate+"</b></font></p>");
        out.println("<p align='center'><font size='5'><b>Unit : <u>"+SUnit+" </u></font></p>  ");
        
	    Paragraph paragraph;
	    paragraph = new Paragraph("Drawing Cloth Check Details - From : "+SStDate+" To : "+SEnDate+"\n Unit : "+SUnit+"\n\n", bigbold);
	    paragraph . setAlignment(Element.ALIGN_CENTER);                                                                                                                    
	    document  . add(paragraph);
		paragraph = new Paragraph("\n", bigbold);
		document  . add(paragraph);
		
		out.println("<div align='center'>");
        
        out.println("<p></p>");
        
        out.println("  <table border='1' bordercolor='"+bgColor+"'>");
        out.println("    <tr>");
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>SL.No</b></font></td>");
        out.println("      <td  rowspan='1'align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>OrderNo</b></font></td>");
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Count Name</b></font></td>");
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Shade</b></font></td>");
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Reason</b></font></td>");
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Status</b></font></td>");
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Cloth Check Remarks</b></font></td>");
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Tpi</b></font></td>");
        
		out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Issue Date</b></font></td>");
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Issue User</b></font></td>");
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Receive Date</b></font></td>");
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Receive User</b></font></td>");
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Completed Date</b></font></td>");
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Completed User</b></font></td>");
		
        out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Time Diff.(Checked-Issue)</b></font></td>");
        out.println("    </tr>");

        
        drawPdfText("Sl No", 0, 0, "C", mediumbold, "ALL", fHeight);
	    drawPdfText("Order No", 0, 0, "C", mediumbold, "ALL", fHeight);
	    drawPdfText("Count Name", 0, 0, "C", mediumbold, "ALL", fHeight);
	    drawPdfText("Shade", 0, 0, "C", mediumbold, "ALL", fHeight);
	    drawPdfText("Reason", 0, 0, "C", mediumbold, "ALL", fHeight);
	    drawPdfText("Status", 0, 0, "C", mediumbold, "ALL", fHeight);
	    drawPdfText("Cloth Check Remarks", 0, 0, "C", mediumbold, "ALL", fHeight);
	    drawPdfText("Tpi", 0, 0, "C", mediumbold, "ALL", fHeight);
	    drawPdfText("Issue Date", 0, 0, "C", mediumbold, "ALL", fHeight);
	    drawPdfText("Issue User", 0, 0, "C", mediumbold, "ALL", fHeight);
	    drawPdfText("Receive Date", 0, 0, "C", mediumbold, "ALL", fHeight);
	    drawPdfText("Receive User", 0, 0, "C", mediumbold, "ALL", fHeight);
	    drawPdfText("Complete Date", 0, 0, "C", mediumbold, "ALL", fHeight);
	    drawPdfText("Complete User", 0, 0, "C", mediumbold, "ALL", fHeight);
	    drawPdfText("Time Diff.(Checked-Issue)", 0, 0, "C", mediumbold, "ALL", fHeight);
	
        int iSlNo = 1;
        
        
        for(int i=0;i<VOrderNo.size();i++)
        {
            out.println("    <tr>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>"+String.valueOf(iSlNo)+"</font></td>");
            out.println("      <td width='40' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VOrderNo.elementAt(i)+"</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VCountName.elementAt(i)+"</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VShade.elementAt(i)+"</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VReason.elementAt(i)+"</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VStatus.elementAt(i)+"</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+VClothCheckRemarks.elementAt(i)+"</td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VTpi.elementAt(i)+"</font></td>");

            out.println("      <td width='40' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VIssueDate.elementAt(i)+"</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VIssueUser.elementAt(i)+"</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VReceiveDate.elementAt(i)+"</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VReceiveUser.elementAt(i)+"</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VCompleteDate.elementAt(i)+"</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VCompleteUser.elementAt(i)+"</font></td>");
			
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+VTimeDiff.elementAt(i)+"</td>");
            out.println("    </tr>");
            
			drawPdfText(String.valueOf(iSlNo), 0, 0, "R", mediumnormal, "ALL", fHeight);
			drawPdfText(common.parseNull((String)VOrderNo.elementAt(i)), 0, 0, "L", mediumnormal, "ALL", fHeight);
			drawPdfText(common.parseNull((String)VCountName.elementAt(i)), 0, 0, "L", mediumnormal, "ALL", fHeight);
			drawPdfText(common.parseNull((String)VShade.elementAt(i)), 0, 0, "L", mediumnormal, "ALL", fHeight);
			drawPdfText(common.parseNull((String)VReason.elementAt(i)), 0, 0, "L", mediumnormal, "ALL", fHeight);
			drawPdfText(common.parseNull((String)VStatus.elementAt(i)), 0, 0, "L", mediumnormal, "ALL", fHeight);
			drawPdfText(common.parseNull((String)VClothCheckRemarks.elementAt(i)), 0, 0, "L", mediumnormal, "ALL", fHeight);
			drawPdfText(common.parseNull((String)VTpi.elementAt(i)), 0, 0, "L", mediumnormal, "ALL", fHeight);
			drawPdfText(common.parseNull((String)VIssueDate.elementAt(i)), 0, 0, "L", mediumnormal, "ALL", fHeight);
			drawPdfText(common.parseNull((String)VIssueUser.elementAt(i)), 0, 0, "L", mediumnormal, "ALL", fHeight);
			drawPdfText(common.parseNull((String)VReceiveDate.elementAt(i)), 0, 0, "L", mediumnormal, "ALL", fHeight);
			drawPdfText(common.parseNull((String)VReceiveUser.elementAt(i)), 0, 0, "L", mediumnormal, "ALL", fHeight);
			drawPdfText(common.parseNull((String)VCompleteDate.elementAt(i)), 0, 0, "L", mediumnormal, "ALL", fHeight);
			drawPdfText(common.parseNull((String)VCompleteUser.elementAt(i)), 0, 0, "L", mediumnormal, "ALL", fHeight);
			drawPdfText(common.parseNull((String)VTimeDiff.elementAt(i)), 0, 0, "L", mediumnormal, "ALL", fHeight);
			
            iSlNo++;
        }
		document. add(thePdfTable);
			
        out.println("  </table>");
        out.println("  </center>");
        out.println("</div>");
        out.println("");
        out.println("</body>");
        out.println("");
        out.println("</html>");
        out.println("");
        out.close();
		
     }catch(Exception ex){
	    ex.printStackTrace();
     }

    }


    private void setData() {

	    VOrderNo          = new Vector();
	    VCountName        = new Vector();
	    VShade            = new Vector();
	    VReason     	  = new Vector();
	    VStatus		      = new Vector();
	    VTpi              = new Vector();
	    VIssueDate        = new Vector();
	    VIssueUser        = new Vector();
		
	    VReceiveDate      = new Vector();
	    VReceiveUser      = new Vector();
	    
		VCompleteDate     = new Vector();
	    VCompleteUser     = new Vector();
		
	    VClothCheckRemarks= new Vector();
	    VTimeDiff         = new Vector();


        StringBuffer sb = new StringBuffer();
		
                     sb . append(" select rorderno, countname, yshnm, CLOTHCHECKREASONNAME, ");
                     sb . append(" ClothStatus, CLOTHCHECKTPI, ");
                     sb . append(" IssueDate, ISSUETOSUNIT_USER, ReceiveDate, CLOTHCHKSUNITREC_USER, ");
                     sb . append(" CheckedDate,  SUNITCLOTHCHK_USER, REMARKS, ");
                     //sb . append("  Iss_Rec_HourDiff, ");
                     sb . append(" Iss_Chk_HourDiff, MATERIAL, CLOTHCHECK_HOURS, IssChkDiffHours, IssChkDiffMinutes, ");
                     sb . append(" weight  from  ( ");
		
			if(SUnit.trim().equals("A_Unit") || SUnit.trim().equals("All")){
                     sb . append(" select processingofindent.rorderno, processingofindent.countname, processingofindent.yshnm, CLOTHCHECKREASON.CLOTHCHECKREASONNAME, ");
                     sb . append(" PRO_DRAWING_CLOTHCHECK_STATUS.STATUS as ClothStatus, processingofindent.CLOTHCHECKTPI, ");
                     sb . append(" to_char(processingofindent.CLOTHCHKISSUETOSUNIT_DATE,'DD.MM.YYYY hh24:mi') as IssueDate, processa.GETUSERNAME(processingofindent.CLOTHCHKISSUETOSUNIT_UCODE) as ISSUETOSUNIT_USER, ");
                     sb . append(" to_char(processingofindent.CLOTHCHKSUNITREC_DATE,'DD.MM.YYYY hh24:mi') as ReceiveDate, processa.GETUSERNAME(processingofindent.CLOTHCHKSUNITREC_UCODE) as CLOTHCHKSUNITREC_USER, ");
                     sb . append(" to_char(processingofindent.SUNITCLOTHCHK_DATE,'DD.MM.YYYY hh24:mi') as CheckedDate,  processa.GETUSERNAME(processingofindent.SUNITCLOTHCHK_UCODE) as SUNITCLOTHCHK_USER, PRO_CLOTHSTATUS_REMARKS.REMARKS, ");
                     //sb . append(" floor(24*(processingofindent.CLOTHCHKSUNITREC_DATE-processingofindent.CLOTHCHKISSUETOSUNIT_DATE))||' Hours '|| ");
                     //sb . append(" floor((((processingofindent.CLOTHCHKSUNITREC_DATE-processingofindent.CLOTHCHKISSUETOSUNIT_DATE)*24*60*60) - floor(((processingofindent.CLOTHCHKSUNITREC_DATE-processingofindent.CLOTHCHKISSUETOSUNIT_DATE)*24*60*60)/3600)*3600)/60)||' Minutes' as  Iss_Rec_HourDiff, ");
                     sb . append(" floor(24*(processingofindent.SUNITCLOTHCHK_DATE-processingofindent.CLOTHCHKISSUETOSUNIT_DATE))||' Hours '|| ");
                     sb . append(" floor((((processingofindent.SUNITCLOTHCHK_DATE-processingofindent.CLOTHCHKISSUETOSUNIT_DATE)*24*60*60) - floor(((processingofindent.SUNITCLOTHCHK_DATE-processingofindent.CLOTHCHKISSUETOSUNIT_DATE)*24*60*60)/3600)*3600)/60)||' Minutes' as  Iss_Chk_HourDiff, ");
                     
                     sb . append(" CLOTHCHK_MATERIALTYPE.MATERIAL, CLOTHCHK_MATERIALTYPE.CLOTHCHECK_HOURS, floor(24*(processingofindent.SUNITCLOTHCHK_DATE-processingofindent.CLOTHCHKISSUETOSUNIT_DATE)) as IssChkDiffHours,   ");
                     sb . append(" floor((((processingofindent.SUNITCLOTHCHK_DATE-processingofindent.CLOTHCHKISSUETOSUNIT_DATE)*24*60*60) - floor(((processingofindent.SUNITCLOTHCHK_DATE-processingofindent.CLOTHCHKISSUETOSUNIT_DATE)*24*60*60)/3600)*3600)/60) as IssChkDiffMinutes, ");
                     sb . append(" processingofindent.weight  from  processa.processingofindent ");
					 
                     sb . append(" inner join process.CLOTHCHECKREASON on CLOTHCHECKREASON.CLOTHCHECKREASONCODE  = processingofindent.CLOTHCHECKREASONCODE ");
                     sb . append(" inner join process.CLOTHCHK_MATERIALTYPE on CLOTHCHK_MATERIALTYPE.ID  = processingofindent.CLOTHCHECKMATERIALTYPE ");
                     sb . append(" INNER JOIN process.PRO_CLOTHSTATUS_REMARKS ON PRO_CLOTHSTATUS_REMARKS.ID = processingofindent.SUNITCLOTHCHKSTATUS_REMARKS ");
                     sb . append(" INNER JOIN process.PRO_DRAWING_CLOTHCHECK_STATUS ON PRO_DRAWING_CLOTHCHECK_STATUS.CODE = PROCESSINGOFINDENT.SUNITCLOTHCHKSTATUS  ");
                     sb . append(" where to_char(processingofindent.SUNITCLOTHCHK_DATE,'YYYYMMDD')>='"+iStDate+"'  and to_char(processingofindent.SUNITCLOTHCHK_DATE,'YYYYMMDD')<= '"+iEnDate+"' ");
			}
			
			if(SUnit.trim().equals("All")){
					 sb . append(" Union All ");
			}

			if(SUnit.trim().equals("B_Unit") || SUnit.trim().equals("All")){
                     sb . append(" select processingofindent.rorderno, processingofindent.countname, processingofindent.yshnm, CLOTHCHECKREASON.CLOTHCHECKREASONNAME,  ");
                     sb . append(" PRO_DRAWING_CLOTHCHECK_STATUS.STATUS as ClothStatus, processingofindent.CLOTHCHECKTPI, ");
                     sb . append(" to_char(processingofindent.CLOTHCHKISSUETOSUNIT_DATE,'DD.MM.YYYY hh24:mi') as IssueDate, PROCESS.GETUSERNAME(processingofindent.CLOTHCHKISSUETOSUNIT_UCODE) as ISSUETOSUNIT_USER, ");
                     sb . append(" to_char(processingofindent.CLOTHCHKSUNITREC_DATE,'DD.MM.YYYY hh24:mi') as ReceiveDate, PROCESS.GETUSERNAME(processingofindent.CLOTHCHKSUNITREC_UCODE) as CLOTHCHKSUNITREC_USER, ");
                     sb . append(" to_char(processingofindent.SUNITCLOTHCHK_DATE,'DD.MM.YYYY hh24:mi') as CheckedDate,  PROCESS.GETUSERNAME(processingofindent.SUNITCLOTHCHK_UCODE) as SUNITCLOTHCHK_USER, PRO_CLOTHSTATUS_REMARKS.REMARKS, ");
                     //sb . append(" floor(24*(processingofindent.CLOTHCHKSUNITREC_DATE-processingofindent.CLOTHCHKISSUETOSUNIT_DATE))||' Hours '|| ");
                     //sb . append(" floor((((processingofindent.CLOTHCHKSUNITREC_DATE-processingofindent.CLOTHCHKISSUETOSUNIT_DATE)*24*60*60) - floor(((processingofindent.CLOTHCHKSUNITREC_DATE-processingofindent.CLOTHCHKISSUETOSUNIT_DATE)*24*60*60)/3600)*3600)/60)||' Minutes' as  Iss_Rec_HourDiff, ");
                     sb . append(" floor(24*(processingofindent.SUNITCLOTHCHK_DATE-processingofindent.CLOTHCHKISSUETOSUNIT_DATE))||' Hours '|| ");
                     sb . append(" floor((((processingofindent.SUNITCLOTHCHK_DATE-processingofindent.CLOTHCHKISSUETOSUNIT_DATE)*24*60*60) - floor(((processingofindent.SUNITCLOTHCHK_DATE-processingofindent.CLOTHCHKISSUETOSUNIT_DATE)*24*60*60)/3600)*3600)/60)||' Minutes' as  Iss_Chk_HourDiff, ");
                     
                     sb . append(" CLOTHCHK_MATERIALTYPE.MATERIAL, CLOTHCHK_MATERIALTYPE.CLOTHCHECK_HOURS, floor(24*(processingofindent.SUNITCLOTHCHK_DATE-processingofindent.CLOTHCHKISSUETOSUNIT_DATE)) as IssChkDiffHours,   ");
                     sb . append(" floor((((processingofindent.SUNITCLOTHCHK_DATE-processingofindent.CLOTHCHKISSUETOSUNIT_DATE)*24*60*60) - floor(((processingofindent.SUNITCLOTHCHK_DATE-processingofindent.CLOTHCHKISSUETOSUNIT_DATE)*24*60*60)/3600)*3600)/60) as IssChkDiffMinutes, ");
                     sb . append(" processingofindent.weight  from  processingofindent ");
                     sb . append(" inner join CLOTHCHECKREASON on CLOTHCHECKREASON.CLOTHCHECKREASONCODE  = processingofindent.CLOTHCHECKREASONCODE ");
                     sb . append(" inner join CLOTHCHK_MATERIALTYPE on CLOTHCHK_MATERIALTYPE.ID  = processingofindent.CLOTHCHECKMATERIALTYPE ");
                     sb . append(" INNER JOIN PRO_CLOTHSTATUS_REMARKS ON PRO_CLOTHSTATUS_REMARKS.ID = processingofindent.SUNITCLOTHCHKSTATUS_REMARKS ");
                     sb . append(" INNER JOIN PRO_DRAWING_CLOTHCHECK_STATUS ON PRO_DRAWING_CLOTHCHECK_STATUS.CODE = PROCESSINGOFINDENT.SUNITCLOTHCHKSTATUS  ");
                     sb . append(" where to_char(processingofindent.SUNITCLOTHCHK_DATE,'YYYYMMDD')>='"+iStDate+"'  and to_char(processingofindent.SUNITCLOTHCHK_DATE,'YYYYMMDD')<= '"+iEnDate+"' ");
                     //sb . append(" Order by 7 ");
			}
			
			if(SUnit.trim().equals("All")){
					 sb . append(" Union All ");
			}
			
			if(SUnit.trim().equals("C_Unit") || SUnit.trim().equals("All")){
                     sb . append(" select cunit_processgofindent.rorderno, cunit_processgofindent.countname, cunit_processgofindent.yshnm, CLOTHCHECKREASON.CLOTHCHECKREASONNAME,  ");
                     sb . append(" PRO_DRAWING_CLOTHCHECK_STATUS.STATUS as ClothStatus, cunit_processgofindent.CLOTHCHECKTPI, ");
                     sb . append(" to_char(cunit_processgofindent.CLOTHCHKISSUETOSUNIT_DATE,'DD.MM.YYYY hh24:mi') as IssueDate, PROCESS.GETCUNITUSERNAME(cunit_processgofindent.CLOTHCHKISSUETOSUNIT_UCODE) as ISSUETOSUNIT_USER, ");
                     sb . append(" to_char(cunit_processgofindent.CLOTHCHKSUNITREC_DATE,'DD.MM.YYYY hh24:mi') as ReceiveDate, PROCESS.GETCUNITUSERNAME(cunit_processgofindent.CLOTHCHKSUNITREC_UCODE) as CLOTHCHKSUNITREC_USER, ");
                     sb . append(" to_char(cunit_processgofindent.SUNITCLOTHCHK_DATE,'DD.MM.YYYY hh24:mi') as CheckedDate,  PROCESS.GETCUNITUSERNAME(cunit_processgofindent.SUNITCLOTHCHK_UCODE) as SUNITCLOTHCHK_USER, PRO_CLOTHSTATUS_REMARKS.REMARKS, ");
                     //sb . append(" floor(24*(cunit_processgofindent.CLOTHCHKSUNITREC_DATE-cunit_processgofindent.CLOTHCHKISSUETOSUNIT_DATE))||' Hours '|| ");
                     //sb . append(" floor((((cunit_processgofindent.CLOTHCHKSUNITREC_DATE-cunit_processgofindent.CLOTHCHKISSUETOSUNIT_DATE)*24*60*60) - floor(((cunit_processgofindent.CLOTHCHKSUNITREC_DATE-cunit_processgofindent.CLOTHCHKISSUETOSUNIT_DATE)*24*60*60)/3600)*3600)/60)||' Minutes' as  Iss_Rec_HourDiff, ");
                     sb . append(" floor(24*(cunit_processgofindent.SUNITCLOTHCHK_DATE-cunit_processgofindent.CLOTHCHKISSUETOSUNIT_DATE))||' Hours '|| ");
                     sb . append(" floor((((cunit_processgofindent.SUNITCLOTHCHK_DATE-cunit_processgofindent.CLOTHCHKISSUETOSUNIT_DATE)*24*60*60) - floor(((cunit_processgofindent.SUNITCLOTHCHK_DATE-cunit_processgofindent.CLOTHCHKISSUETOSUNIT_DATE)*24*60*60)/3600)*3600)/60)||' Minutes' as  Iss_Chk_HourDiff, ");
                     
                     sb . append(" CLOTHCHK_MATERIALTYPE.MATERIAL, CLOTHCHK_MATERIALTYPE.CLOTHCHECK_HOURS, floor(24*(cunit_processgofindent.SUNITCLOTHCHK_DATE-cunit_processgofindent.CLOTHCHKISSUETOSUNIT_DATE)) as IssChkDiffHours,   ");
                     sb . append(" floor((((cunit_processgofindent.SUNITCLOTHCHK_DATE-cunit_processgofindent.CLOTHCHKISSUETOSUNIT_DATE)*24*60*60) - floor(((cunit_processgofindent.SUNITCLOTHCHK_DATE-cunit_processgofindent.CLOTHCHKISSUETOSUNIT_DATE)*24*60*60)/3600)*3600)/60) as IssChkDiffMinutes, ");
                     sb . append(" cunit_processgofindent.weight  from  cunit_processgofindent ");
                     sb . append(" inner join CLOTHCHECKREASON on CLOTHCHECKREASON.CLOTHCHECKREASONCODE  = cunit_processgofindent.CLOTHCHECKREASONCODE ");
                     sb . append(" inner join CLOTHCHK_MATERIALTYPE on CLOTHCHK_MATERIALTYPE.ID  = cunit_processgofindent.CLOTHCHECKMATERIALTYPE ");
                     sb . append(" INNER JOIN PRO_CLOTHSTATUS_REMARKS ON PRO_CLOTHSTATUS_REMARKS.ID = cunit_processgofindent.SUNITCLOTHCHKSTATUS_REMARKS ");
                     sb . append(" INNER JOIN PRO_DRAWING_CLOTHCHECK_STATUS ON PRO_DRAWING_CLOTHCHECK_STATUS.CODE = cunit_processgofindent.SUNITCLOTHCHKSTATUS  ");
                     sb . append(" where to_char(cunit_processgofindent.SUNITCLOTHCHK_DATE,'YYYYMMDD')>='"+iStDate+"'  and to_char(cunit_processgofindent.SUNITCLOTHCHK_DATE,'YYYYMMDD')<= '"+iEnDate+"' ");
                     //sb . append(" Order by 7 ");
			}
			
			
					 sb . append(" ) Order by 7 ");


        try{
			Statement stat      =  null;
			
			if(SUnit.trim().equals("A_Unit")){
			
				if(theProcessaConnection==null){
					JDBCProcessaConnection jdbc = JDBCProcessaConnection.getJDBCProcessaConnection();
					theProcessaConnection       = jdbc.getConnection();
				}
				
				stat      = theProcessaConnection.createStatement();
				
			}else{
				
				if(theProcessConnection==null){
					JDBCProcessConnection jdbc = JDBCProcessConnection.getJDBCProcessConnection();
					theProcessConnection       = jdbc.getConnection();
				}
				
				stat      = theProcessConnection.createStatement();
				
			}
            ResultSet res       = stat.executeQuery(sb.toString());
            
            while(res.next())
            {
		        VOrderNo          . addElement(common.parseNull(res.getString(1)));
		        VCountName        . addElement(common.parseNull(res.getString(2)));
		        VShade            . addElement(common.parseNull(res.getString(3)));
		        VReason           . addElement(common.parseNull(res.getString(4)));
		        VStatus           . addElement(common.parseNull(res.getString(5)));
		        VTpi 		      . addElement(common.parseNull(res.getString(6)));
				
		        VIssueDate        . addElement(common.parseNull(res.getString(7)));
		        VIssueUser        . addElement(common.parseNull(res.getString(8)));
				
		        VReceiveDate      . addElement(common.parseNull(res.getString(9)));
		        VReceiveUser      . addElement(common.parseNull(res.getString(10)));
		        
				VCompleteDate     . addElement(common.parseNull(res.getString(11)));
		        VCompleteUser     . addElement(common.parseNull(res.getString(12)));
				
		        VClothCheckRemarks. addElement(common.parseNull(res.getString(13)));
		        VTimeDiff         . addElement(common.parseNull(res.getString(14)));
            }
            res.close();
            stat.close();
            
        }catch(Exception ex) {
            ex.printStackTrace();
        }

    }

     private void drawPdfText(String SText, int iRowSpan, int iColSpan, String SAlign, 
                Font theFont, String SBorderType, float fHeight)
     {
          try
          {
               PdfPCell c1    = new PdfPCell(new Phrase(SText, theFont));
            
               if(SAlign.equals("L"))
               {
                    c1        . setHorizontalAlignment(Element.ALIGN_LEFT);
               }

               if(SAlign.equals("C"))
               {
                    c1        . setHorizontalAlignment(Element.ALIGN_CENTER);
                    c1        . setVerticalAlignment(Element.ALIGN_MIDDLE);
               }

               if(SAlign.equals("R"))
               {
                    c1        . setHorizontalAlignment(Element.ALIGN_RIGHT);
               }

               if(iRowSpan > 0)
               {
                    c1        . setRowspan(iRowSpan);
               }
            
               if(iColSpan > 0)
               {
                    c1        . setColspan(iColSpan);
               }

               if(SBorderType.equals("ALL"))
               {
                    c1        . setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
               }

               if(SBorderType.equals("LR") || SBorderType.equals("RL"))
               {
                    c1        . setBorder(Rectangle.LEFT | Rectangle.RIGHT);
               }

               if(SBorderType.equals("LRT") || SBorderType.equals("RLT"))
               {
                    c1        . setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP);
               }

               if(SBorderType.equals("LRB") || SBorderType.equals("RLB"))
               {
                    c1        . setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
               }

               if(SBorderType.equals("LB"))
               {
                    c1        . setBorder(Rectangle.LEFT | Rectangle.BOTTOM);
               }

               if(SBorderType.equals("RB"))
               {
                    c1        . setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
               }

               if(SBorderType.equals("L"))
               {
                    c1        . setBorder(Rectangle.LEFT);
               }

               if(SBorderType.equals("B")) {
                    c1        . setBorder(Rectangle.BOTTOM);
               }

               if(SBorderType.equals("R"))
               {
                    c1        . setBorder(Rectangle.RIGHT);
               }

               if(SBorderType.equals("NO"))
               {
                    c1        . setBorder(Rectangle.NO_BORDER);
               }

               if(fHeight == 0)
               {
                    c1        . hasMinimumHeight();
               }else
               {
                    c1        . setFixedHeight(fHeight);
               }
           
//               c1             . setPadding(3f);
            
               thePdfTable    . addCell(c1);
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }
}
