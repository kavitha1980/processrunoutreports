/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProcessReports.PartyPriority;

import ProcessReports.jdbc.*;
import ProcessReports.util.*;

import java.io.*;
import java.sql.*;           
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

/**
 *
 * @author Administrator
 */
public class RegMixingPendingCritNew extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    HttpSession     session;
    Common          common = new Common();
    Vector          VUnitCode,VUnitName,VROrderNo,VCountName,VShadeName,VOrderType,VPartyCode,VPartyName;
    Vector          AProcessTypeCode, AProcessTypeName;
    JDBCConnection  theConnect;
    Connection      connect;
    String          SToday = "";
    
    Vector AGroupCode, AGroupName; 

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        SToday = common.parseDate(common.getCurrentDate());
        try {
                        response. setContentType("text/html;charset=UTF-8");
            PrintWriter out     = response.getWriter();
          setDataintoVector();

          out.println("<html>");

          out.println("<SCRIPT LANGUAGE='JavaScript'>");

          out.println("function func1(xyz,a1)");
          out.println("{");
          out.println(" typedstring = a1.value;");
          out.println(" for(ctr=0;ctr<xyz.options.length;ctr++)");
          out.println(" {");
          out.println(" str=xyz.options[ctr].value;");
          out.println(" if(typedstring==str.substring(0,typedstring.length))");
          out.println(" break;");
          out.println(" }");
          out.println(" if (ctr < xyz.options.length)");
          out.println(" xyz.options[ctr].selected=true;");
          out.println(" return;");
          out.println("}");
          out.println("</Script>");

          out.println("</head>");

          out.println("<body bgcolor='#9AA8D6'>");
          out.println("");
          out.println("<base target='topmain'>");
          out.println("<form method='GET' action='RegMixingPendingNew1'>");

          out.println("<table>");

          out.println("</tr>");

          out.println("<tr>");
          out.println("<td><font color='#FFFF66'><b>As On Date: </b></font></td>");
          out.println("     <td><input type='text' name='TEnDate'value="+SToday+" size='9'></td>");
          out.println("<tr>");

          out.println("<tr>");
          out.println("<td><font color='FFEEFE'><b>Unit: </b></font></td>");
          out.println("      <td><select size=1 name=Unit>");
          out.println("      <option value='All'>All</option>");
          for(int i=0;i<VUnitCode.size();i++)
          {
               out.println("      <option value='"+VUnitName.elementAt(i)+"'>"+VUnitName.elementAt(i)+"</option>");
          }


        
		   out.println("    </tr>");
           out.println("    <tr>");

          out.println("       <td><font color='#FFFF66'><b>Type</b></font></td>");
          out.println("       <td><select name='type'>");
          out.println("            <option value='All'>All</option>");
          out.println("            <option value='Organic'>Organic Type</option>");
          out.println("            <option value='BCI'>BCI Type</option>");

          for(int i=0;i<AGroupCode.size();i++)
          {
               out.println("      <option value='"+AGroupCode.get(i)+"'>"+AGroupName.get(i)+"</option>");
          }
		  out.println("       </select></td>");
		  out.println("<tr>");
          out.println("<td><font color='FFEEFE'><b>Process Type: </b></font></td>");
          out.println("      <td><select size=1 name='processType'>");
          out.println("      <option value='All#All'>All</option>");
          out.println("      <option value='BR-C Orders#BR-C Orders'>BR-C Orders</option>");
          out.println("      <option value='Other than BR-C Orders#Other than BR-C Orders'>Other than BR-C Orders</option>");
          for(int i=0;i<AProcessTypeCode.size();i++)
          {
               out.println("      <option value='"+AProcessTypeCode.get(i)+"#"+AProcessTypeName.get(i)+"'>"+AProcessTypeName.get(i)+"</option>");
          }
		   out.println("      </select></td>");
		    out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td><font color='FFEEFE'><b>Order No: </b></font></td>");
          out.println("      <td><input type='text' size='10' name='ROrderNoText' onKeyUp='func1(this.form.ROrderNo,this.form.ROrderNoText)'>");
          out.println("      <select size=1 name=ROrderNo>");
          out.println("      <Option Value='All'>ALL</option>");
          for(int i=0;i<VROrderNo.size();i++)
          {
               out.println("      <option value='"+VROrderNo.elementAt(i)+"'>"+VROrderNo.elementAt(i)+"</option>");
          }
          out.println("      </select>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td><font color='FFEEFE'><b>Order Type: </b></font></td>");
          out.println("      <td><input type='text' size='10' name='OrderTypeText' onKeyUp='func1(this.form.OrderType,this.form.OrderTypeText)'>");
          out.println("      <select size=1 name=OrderType>");
          out.println("      <Option Value='All'>ALL</option>");
          for(int i=0;i<VOrderType.size();i++)
          {
               out.println("      <option value='"+VOrderType.elementAt(i)+"'>"+VOrderType.elementAt(i)+"</option>");
          }
          out.println("      </select>");
          out.println("    </tr>");            
          out.println("    <tr>");
          out.println("      <td><font color='FFEEFE'><b>Count: </b></font></td>");
          out.println("      <td><input type='text' size='10' name='CountNameText' onKeyUp='func1(this.form.CountName,this.form.CountNameText)'>");
          out.println("      <select size=1 name=CountName>");
          out.println("      <Option Value='All'>ALL</option>");
          for(int i=0;i<VCountName.size();i++)
          {
               out.println("      <option value='"+VCountName.elementAt(i)+"'>"+VCountName.elementAt(i)+"</option>");
          }
          out.println("      </select>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td><font color='FFEEFE'><b>Shade Name: </b></font></td>");
          out.println("      <td><input type='text' size='10' name='ShadeNameText' onKeyUp='func1(this.form.ShadeName,this.form.ShadeNameText)'>");
          out.println("      <select size=1 name=ShadeName>");
          out.println("      <Option Value='All'>ALL</option>");
          for(int i=0;i<VShadeName.size();i++)
          {
               out.println("      <option value='"+VShadeName.elementAt(i)+"'>"+VShadeName.elementAt(i)+"</option>");
          }

          out.println("    <tr>");
          out.println("      <td><font color='FFEEFE'><b>Party: </b></font></td>");
          out.println("      <td><input type='text' size='10' name='PartyNameText' onKeyUp='func1(this.form.PartyName,this.form.PartyNameText)'>");
          out.println("      <select size=1 name=PartyName>");
          out.println("      <Option Value='All'>ALL</option>");
          for(int i=0;i<VPartyCode.size();i++)
          {
               out.println("      <option value='"+VPartyName.elementAt(i)+"'>"+VPartyName.elementAt(i)+"</option>");
          }

          out.println("    <tr>");
          out.println("      <td><font color='FFEEFE'><b>Delay Days</b></font></td>");
          out.println("      <td><input type='text' size='10' name='Delay'><font color='#FFFF66'></font></td>");
          out.println("    </tr>");

          out.println("<tr>");
          out.println("<center>");
          out.println("<div align='center'>");
          out.println("<table border=1 width='600' height='14'>");

          out.println("       <td><b><font color='FFEEFE'>All</font> </b><input type='RADIO' name='Select' value= 'All' Checked></td>");
          out.println("       <td><b><font color='FFEEFE'>Depo Order</font> </b><input type='RADIO' name='Select' value= 'Depo' ></td>");
          out.println("       <td><b><font color='FFEEFE'>Party Order</font> </b><input type='RADIO' name='Select' value='Party'></td>");
          out.println("    </tr>");
          out.println("    </center>");
          out.println("  </table>");
          out.println("</div>");

          out.println("</tr>");

          out.println("<tr>");

          out.println("     <td><input type='submit' value='Submit' name='B1' style='color: #003300; font-size: 10pt; font-weight: bold'></td>");
          out.println("     <td><input type='reset' value='Reset' name='B2' style='color: #800000; font-size: 10pt; font-weight: bold'></td>");
          out.println("      </select>");

          out.println("</table>");
          out.println("</form>");
          out.println("</body>");
          out.println("</html>");
            out.close();
        } catch(Exception ex ){
            ex.printStackTrace();
        }finally {
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

     public void setDataintoVector()
     {
         VUnitCode   = new Vector();
         VUnitName   = new Vector();
         VROrderNo   = new Vector();
         VCountName  = new Vector();
         VShadeName  = new Vector();
         VOrderType  = new Vector();
         VPartyCode  = new Vector();
         VPartyName  = new Vector();
		 AProcessTypeCode     = new Vector();
         AProcessTypeName     = new Vector();


        
         AGroupCode  = new Vector();
         AGroupName  = new Vector();

       

          try
          {
               theConnect   = JDBCConnection.getJDBCConnection();
               connect      = theConnect.getConnection();
               Statement st = connect.createStatement();
               ResultSet rs; 
               rs             =st.executeQuery("select UnitCode, Unitname from Unit order by 2");
               while(rs.next())
               {
                    VUnitCode.addElement(rs.getString(1));
                    VUnitName.addElement(rs.getString(2));
               }
               rs.close();
               rs=st.executeQuery("select distinct ROrderNo from RegularOrder");
               while(rs.next())
               {
                    VROrderNo.addElement(rs.getString(1));
               }
               rs.close();
               rs=st.executeQuery("select distinct CountName from YarnCount");
               while(rs.next())
               {
                    VCountName.addElement(rs.getString(1));
               }
               rs.close();

               
               rs=st.executeQuery("select  Yshnm from yarnm ");
               while(rs.next())
               {
                    VShadeName.addElement(rs.getString(1));
               }
               rs.close();

               rs=st.executeQuery("select  Name from OrderType ");
               while(rs.next())
               {
                    VOrderType.addElement(rs.getString(1));
               }
               rs.close();
               rs             =st.executeQuery("select PartyCode, Partyname from PartyMaster order by 1");
               while(rs.next())
               {
                    VPartyCode.addElement(rs.getString(1));
                    VPartyName.addElement(rs.getString(2));
               }
			   rs = st.executeQuery("Select ProcessCode, ProcessType from ProcessingType Order by 2 ");
			   while(rs.next())
			   {
	               AProcessTypeCode.addElement(common.parseNull(rs.getString(1)));
    	           AProcessTypeName.addElement(common.parseNull(rs.getString(2)));
               }
              

               rs = st.executeQuery(" Select GroupCode, GroupName from FibreFormGroup Order by 2");
             

               while(rs.next())
               {
                   AGroupCode . addElement(common.parseNull(rs.getString(1)));
                   AGroupName . addElement(common.parseNull(rs.getString(2)));
               }
                rs.close();   
               st.close();
          }
          catch(Exception ex)
          {
           System.out.println(ex);
           ex.printStackTrace();
          }
     }
}
