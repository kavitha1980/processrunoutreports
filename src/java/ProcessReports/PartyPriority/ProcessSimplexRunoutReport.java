/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProcessReports.PartyPriority;

import ProcessReports.jdbc.*;
import ProcessReports.util.*;

import java.io.*;
import java.sql.*;           
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

/**
 *
 * @author Administrator
 */
public class ProcessSimplexRunoutReport extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */

    Common              common;
    HttpSession         session;

    String              SStDate,SEnDate;
    String              bgColor,bgHead,bgUom,bgBody,fgHead,fgUom,fgBody;
    String              SUnit="";
    String              SParty="";
    String              SOrderNo="";
    String              SOrderNo1="";
    String              SCount="",SNote="",SNote1="";
    String              SProcessTypeCode, SProcessTypeName;
    String              SSelectType="";
    Vector              VARNo;
    FileWriter          FW;
    int                 TLine       = 0;
    int                 iPage       = 1;

    int                 iStDate     = 0;
    int                 iEnDate     = 0;
    int                 iSlNo       = 0;

    double              dTOrdWt     = 0;
    double              dTMixWt     = 0;
    double              dTPackWt    = 0;

    double              dPTOrdWt    = 0;
    double              dPTMixWt    = 0;
    double              dPTPackWt   = 0;

    int                 SNo         = 0;

//  double dGrandTotal=0;

    double              dGrandOrdWt=0;
    double              dGrandMixWt  =0;
    double              dGrandPackWt =0;

    Vector              VOrderNo, VOrderDate, VMillDueDate, VOrderWeight, VCount, VParty, VMixDate, VMixWt, VPackWt, VBColor, VRColor, VCColor, VCleerDate, VCorrectionMix, VRMixDate, VRemarks, VStatus, VPartyPriority, VProcessRemarks;
    Vector              VDOrderNo,VPacking,VDespatch;
    Vector              VDueDepo,VDueMill,VDLMix,VDRMix,VPTotal,VDFMix,VRunDelay,VCleerDelay;

    Vector              VInsOrderNo,VInsShadeName,VInsRefNo,VShortage;

    String              End="";
    String              End1="";

    ResultSet           theResult;
    ResultSetMetaData   rsmd;

    int                 iRunOutTarget;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        valueInt();
        common = new Common();

        bgColor = common.bgColor;
        bgHead  = common.bgHead;
        bgUom   = common.bgUom;
        bgBody  = common.bgBody;
        fgHead  = common.fgHead;
        fgUom   = common.fgUom;
        fgBody  = common.fgBody;

        try {
                        response    . setContentType("text/html;charset=UTF-8");
            PrintWriter out         = response.getWriter();
            //        iStDate            = common.toInt((String)session.getValue("StartDate"));
            iEnDate             = common.toInt(common.pureDate((String)request.getParameter("TEnDate")));
            SUnit               = request.getParameter("Unit");
            SParty              = request.getParameter("PartyName");
            SOrderNo1           = request.getParameter("ROrderNo");
            SCount              = request.getParameter("Count");
            SSelectType         = common.parseNull((String)request.getParameter("Select"));
            SProcessTypeCode    = null;
            SProcessTypeName    = null;

            StringTokenizer ST  = new StringTokenizer(request.getParameter("processType"), "#");
            while(ST.hasMoreTokens())   {
               SProcessTypeCode    = ST.nextToken();
               SProcessTypeName    = ST.nextToken();
            }

            // set Target Days..
            iRunOutTarget       = 7;
            if(SProcessTypeCode.equals("BR-C Orders"))    {
                iRunOutTarget  = 5;
            }
            valueInt();
            setVector();
            setInstruction();
            DespatchDetails(iEnDate);
            getCorrectionMix();
            DataAlign();
            setHtml(out);
            FileWriter();
            out.close();
        } catch(Exception ex){
            ex.printStackTrace();
        }finally {
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    private void valueInt()
    {
        TLine           = 0;
        iPage           = 1;
        dTOrdWt         = 0;
        dTMixWt         = 0;
        dTPackWt        = 0;
        dPTOrdWt        = 0;
        dPTMixWt        = 0;
        dPTPackWt       = 0;
        dGrandOrdWt     = 0;
        dGrandMixWt     = 0;
        dGrandPackWt    = 0;
        SNo             = 0;
    }

    private void setHtml(PrintWriter out) throws  ServletException
    {
        SStDate = common.parseDate(String.valueOf(iStDate));
        SEnDate = common.parseDate(String.valueOf(iEnDate));

        out.println("<html>");
        out.println("<head>");
        out.println("<title>InvPro</title>");
        out.println("</head>");
        out.println("<body bgcolor='"+bgColor+"' text = '#0000FF'>");
        out.println("<p align='center'><font size='5'><b><u>"+SUnit+" - SIMPLEX RUNOUT</u>  ");
        out.println(" ( As On Date: "+SEnDate+" )"+"</b></font></p>");
        out.println("<p><b>Process Type : "+SProcessTypeName+", Target Days : "+iRunOutTarget+"</b></p>");
        out.println("<div align='center'>");
        out.println("<p></p>");
        if(SUnit.equals("Sample_Unit")) {
            out.println("<table bgColor='LightGreen' border='1' borderColor='LightGreen'>");
            out.println("<tr>");
            out.println("  <td><b>Target : 6 Days</b></td>");
            out.println("</tr>");
            out.println("</table>");
        }

        out.println("  <table border='0' bordercolor='"+bgColor+"'>");
        out.println("    <tr>");
        out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>SL.No</b></font></td>");
        out.println("      <td  rowspan='2'align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>OrderNo</b></font></td>");
        out.println("      <td  rowspan='2'align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>OrderDate</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>AsPer mill DueDate</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Ordered Weight</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Count</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Name of the Buyer</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>LastMixing Date</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>UptoDate MixingWeight</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Balance to Mix</b></font></td>");
        out.println("      <td  colspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>BobinPassing</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Packed Weight</b></font></td>");
        /*
        out.println("      <td  Colspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Simplex</b></font></td>");
        out.println("      <td  colspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Spinning</b></font></td>");
        */
        out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Process Instruction(BlRoom toSpinning)</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>DueDate(Dept)</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>DueDate(Mill)</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>F.Mix DelayDays</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>L.Mix DelayDays</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>R.Mix DelayDays</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Creeling DelayDays</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Process DelayDays</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Runout DelayDays</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Remarks</b></font></td>");
        out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Process Remarks</b></font></td>");
        out.println("    </tr>");
        out.println("    <tr>");
        out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Bobin Nos</b></font></td>");
        out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Bobin Kgs</b></font></td>");
        /*
        out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Bobin Color</b></font></td>");
        out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Bobin Ring Color</b></font></td>");
        out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>CopsColor</b></font></td>");
        out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Creeling Date</b></font></td>");
        */
        out.println("    </tr>");

        String  SPriorityStatus = "";
        int     iSlNo           = 1;

        for(int i=0;i<VOrderNo.size();i++)  {
            if(!common.parseNull((String)VPartyPriority.elementAt(i)).equals(SPriorityStatus))  {
                if(i!=0)    {
                    out.println("    <tr>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("      <td   align='Right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>"+common.getRound(dTOrdWt,3)+"</b></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>"+common.getRound(dTMixWt,3)+"</b></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp;</font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>"+common.getRound(dTPackWt,3)+"</b></font></td>");
                    /*
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    */
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp;</font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp;</font></td>");
                    out.println("    </tr>");
                    dTOrdWt     = 0;
                    dTMixWt     = 0;
                    dTPackWt    = 0;
                    iSlNo       = 1;
                }
                out.println("    <tr>");
                out.println("      <td colspan=16 width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>"+((String)VPartyPriority.elementAt(i))+"</font></td>");
                //out.println("      <td colspan=19 width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>"+((String)VPartyPriority.elementAt(i))+"</font></td>");
                out.println("    </tr>");
            }
            double  dOrdWt      = common.toDouble((String)VOrderWeight.elementAt(i));
            double  dMixWt      = common.toDouble((String)VMixWt.elementAt(i));
            double  dPackWt     = common.toDouble(getPackWt((String)VOrderNo.elementAt(i)));

                    dTOrdWt     = dTOrdWt+dOrdWt;
                    dTMixWt     = dTMixWt+dMixWt;
                    dTPackWt    = dTPackWt+dPackWt;
                    dGrandOrdWt = dGrandOrdWt+dOrdWt;
                    dGrandMixWt = dGrandMixWt+dMixWt;
                    dGrandPackWt= dGrandPackWt+dPackWt;

            out.println("    <tr>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>"+String.valueOf(iSlNo)+"</font></td>");
            out.println("      <td width='40' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VOrderNo.elementAt(i)+"</font></td>");
            out.println("      <td width='180' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+common.parseDate((String)VOrderDate.elementAt(i))+"</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+common.parseDate((String)VMillDueDate.elementAt(i))+"</font></td>");
            out.println("      <td width='40' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>"+common.getRound(dOrdWt,3)+"</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VCount.elementAt(i)+"</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VParty.elementAt(i)+"</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+common.parseDate((String)VMixDate.elementAt(i))+"</font></td>");
            out.println("      <td width='40' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>"+common.getRound(dMixWt,3)+"</font></td>");
            out.println("      <td width='40' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>0.00</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>-</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>-</font></td>");
            out.println("      <td width='40' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>"+common.getRound(dPackWt,3)+"</font></td>");

            /*
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VBColor.elementAt(i)+"</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VRColor.elementAt(i)+"</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VCColor.elementAt(i)+"</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+common.parseDate((String)VCleerDate.elementAt(i))+"</font></td>");
            */
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='center'><font color='"+fgBody+"'>"+getInstrucion((String)VOrderNo.elementAt(i))+"</td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='center'><font color='"+fgBody+"'>"+(String)VDueDepo.elementAt(i)+"</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='center'><font color='"+fgBody+"'>"+(String)VDueMill.elementAt(i)+"</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='center'><font color='"+fgBody+"'>"+(String)VDFMix.elementAt(i)+"</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='center'><font color='"+fgBody+"'>"+(String)VDLMix.elementAt(i)+"</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='center'><font color='"+fgBody+"'>"+(String)VDRMix.elementAt(i)+"</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='center'><font color='"+fgBody+"'>"+(String)VCleerDelay.elementAt(i)+"</td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='center'><font color='"+fgBody+"'>"+(String)VPTotal.elementAt(i)+"</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='center'><font color='"+fgBody+"'>"+VRunDelay.elementAt(i)+"</td>");

            if(SUnit.equalsIgnoreCase("Sample_Unit"))   {
                out.println("  <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VStatus.elementAt(i)+"</font></td>");
            }else   {
                out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VRemarks.elementAt(i)+"</font></td>");
            }
            SPriorityStatus = common.parseNull((String)VPartyPriority.elementAt(i));
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VProcessRemarks.elementAt(i)+"</font></td>");
            out.println("    </tr>");
            iSlNo++;
        }
        out.println("    <tr>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='Right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>"+common.getRound(dTOrdWt,3)+"</b></font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>"+common.getRound(dTMixWt,3)+"</b></font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp;</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>"+common.getRound(dTPackWt,3)+"</b></font></td>");
        /*
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        */
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp;</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp;</font></td>");
        out.println("    </tr>");
        out.println("    <tr>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>GRAND TOTAL</font></td>");
        out.println("      <td   align='Right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>"+common.getRound(dGrandOrdWt,3)+"</b></font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>"+common.getRound(dGrandMixWt,3)+"</b></font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp;</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>"+common.getRound(dGrandPackWt,3)+"</b></font></td>");
        /*
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        */
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp;</font></td>");
        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp;</font></td>");
        out.println("    </tr>");
        dTOrdWt         = 0;
        dTMixWt         = 0;
        dTPackWt        = 0;
        dGrandOrdWt     = 0;
        dGrandMixWt     = 0;
        dGrandPackWt    = 0;
        out.println("  </table>");
        out.println("  </center>");
        out.println("</div>");
        //out.println(getBUnitQs());
        out.println("");
        out.println("</body>");
        out.println("");
        out.println("</html>");
        out.println("");
        out.close();
    }

    public void setVector() {

        VOrderNo            = new Vector();
        VOrderDate          = new Vector();
        VMillDueDate        = new Vector();
        VOrderWeight        = new Vector();
        VCount              = new Vector();
        VParty              = new Vector();
        VMixDate            = new Vector();
        VMixWt              = new Vector();
        VBColor             = new Vector();
        VRColor             = new Vector();
        VCColor             = new Vector();
        VCleerDate          = new Vector();
        VCorrectionMix      = new Vector();
        VRMixDate           = new Vector();
        VRemarks            = new Vector();
        VStatus             = new Vector();
        VPartyPriority      = new Vector();
        VProcessRemarks     = new Vector();

        try {
            String      SQs     = "";
            Connection  conn    = null;
            if(SUnit.equals("A_Unit")){
                SQs  = getAUnitQs();
                JDBCProcessaConnection  jdbc = JDBCProcessaConnection.getJDBCProcessaConnection();
                                        conn = jdbc . getConnection();
            }
            if(SUnit.equals("B_Unit")){
                SQs  = getBUnitQs();
                JDBCProcessConnection   jdbc = JDBCProcessConnection.getJDBCProcessConnection();
                                        conn = jdbc . getConnection();
            }
            java.sql.Statement stat      = conn.createStatement();
            java.sql.ResultSet res       = stat.executeQuery(SQs);

            while(res.next())   {
                String SFibreForm   = ","+common.parseNull(res.getString(17));
                if(SFibreForm.indexOf("NONE") != -1) {
                    SFibreForm     = "";
            }
            VOrderNo            . addElement(res.getString(1));
            VOrderDate          . addElement(res.getString(2));
            VMillDueDate        . addElement(res.getString(3));
            VOrderWeight        . addElement(res.getString(4));
            VCount              . addElement(res.getString(5));
            VParty              . addElement(res.getString(6));
            VMixDate            . addElement(res.getString(7));
            VMixWt              . addElement(res.getString(8));
            VBColor             . addElement(common.parseNull(res.getString(9)));
            VRColor             . addElement(common.parseNull(res.getString(10)));
            VCColor             . addElement(common.parseNull(res.getString(11)));
            VCleerDate          . addElement(res.getString(12));
            VCorrectionMix      . addElement(res.getString(13));

            VRemarks            . addElement(common.parseNull(res.getString(14))+SFibreForm);
            VStatus             . addElement(res.getString(15)+SFibreForm);

            VRMixDate           . addElement(String.valueOf(0));
            VPartyPriority      . addElement(common.parseNull(res.getString(16)));
            VProcessRemarks     . addElement(common.parseNull(res.getString(19)));
        }
        res.close();
        stat.close();
        }   catch(Exception ex) {
            ex.printStackTrace();
        }
    }
    private  String getAUnitQs(){

        StringBuffer    sb  = new StringBuffer(); 
                        sb  . append("  Select DISTINCT rorderno, OrderDate, DeliveryDate, Weight, CountName, PartyName, MixingDate, MixingWeight, SimplexPumb, ");
                        sb  . append("  SimplexRing, SpinningPumb, SpinningStatusStartDate, CorrectionMixing,  ProcessA.GetSimplexRunoutStatus(BaseOrder) as Remarks, ");
                        sb  . append("  GenStatus, PriorityName, FormName, PriorityCode, ProcessRemarks  From ( ");
                        sb  . append("  Select  DISTINCT REGULARORDER.rorderno, Regularorder.OrderDate, DeliveryDate, Regularorder.Weight, YarnCount.CountName, ");
                        sb  . append("  PartyMaster.PartyName, Rmixir.MixingDate, Rmixir.MixingWeight, ''  as SimplexPumb, ");
                        sb  . append("  '' as SimplexRing, '' as SpinningPumb, '' as SpinningStatusStartDate, Rmixir.CorrectionMixing,  ");
                        sb  . append("  decode(OrderSplitDetails.baseorderno,'',RegularOrder.rorderno, OrderSplitDetails.baseorderno) as BaseOrder,  ");
                        sb  . append("  scm.GetGeneralStatus(RegularOrder.OrderId) as GenStatus, ");
            //            sb.append("  PartyPriority.Name as PriorityName, ");
                        sb  . append("  deCode(RegularOrder.Shortage, 1, 'Shortage', PartyPriority.Name) as PriorityName, ");
                        sb  . append("  FibreForm.FormName, PartyPriority.Code as PriorityCode, PROCESSINGOFINDENT.REMARKS as ProcessRemarks   From SCM.REGULARORDER ");
                        sb  . append("  Inner Join PROCESSA.PROCESSINGOFINDENT ON PROCESSINGOFINDENT.RORDERNO = REGULARORDER.RORDERNO ");
                        sb  . append("  Left Join processa.pro_process_base on pro_process_base.rorderno = processingofindent.rorderno  ");
                        sb  . append("  Left Join processa.pro_carding_can_details on pro_carding_can_details.rorderno = processingofindent.rorderno  ");
                        sb  . append("  Left Join processa.pro_drawing_can_details on pro_drawing_can_details.rorderno = processingofindent.rorderno   ");
                        sb  . append("  Inner Join scm.YarnCount On  YarnCount.CountCode = RegularOrder.CountCode  ");
                        sb  . append("  Inner Join scm.PartyMaster On  Partymaster.partyCode = RegularOrder.partyCode  ");
                        sb  . append("  Inner Join scm.PartyPriority On PartyPriority.Code = nvl(PartyMaster.PriorityStatus,0)  ");
                        sb  . append("  inner join scm.unit on unit.unitcode= RegularOrder.UnitCode  ");
                        sb  . append("  Inner Join scm.ProcessingType on ProcessingType.ProcessCode = RegularOrder.ProcessTypeCode  ");
                        sb  . append("  Inner Join scm.RMixir On  Rmixir.OrdNo = REGULARORDER.RORDERNO ");
                        sb  . append("  Inner Join scm.FibreForm on FibreForm.FormCode = nvl(RegularOrder.FibreFormCode, 0)  ");
                        sb  . append("  Left Join scm.OrderSplitDetails On OrderSplitDetails.splitorderno = Regularorder.rorderno and OrderSplitDetails.deptcode!=0  ");
                        sb  . append("  Where Nvl(RMixir.CorrectionMixing,0)=0 and  ");
                        sb  . append("  ( ");
                        sb  . append("  pro_process_base.MIXSTKSTATUS = 0  ");
                        sb  . append("  or ");
                        sb  . append("  pro_carding_can_details.orderfollowstatus = 0 ");
                        sb  . append("  or ");
                        sb  . append("  ( ");
                        sb  . append("  (PROCESSINGTYPE.OESTATUS = 0  AND pro_drawing_can_details.orderfollowstatus = 0) ");
                        sb  . append("  OR  ");
                        sb  . append("  (PROCESSINGTYPE.OESTATUS = 1  ");
                        sb  . append("  AND pro_drawing_can_details.processlevel in ( ");
                        sb  . append("  SELECT AUNIT_ORDERDRAWINGSETTINGDLS.PROCESSLEVEL FROM PROCESSA.AUNIT_ORDERDRAWINGSETTINGDLS WHERE RORDERNO = Regularorder.rorderno AND   ");
                        sb  . append(" AUNIT_ORDERDRAWINGSETTINGDLS.ID != ( SELECT MAX(AUNIT_ORDERDRAWINGSETTINGDLS.ID)  FROM PROCESSA.AUNIT_ORDERDRAWINGSETTINGDLS WHERE RORDERNO = pro_drawing_can_details.rorderno AND AUNIT_ORDERDRAWINGSETTINGDLS.PROCESSLEVEL!='none' AND AUNIT_ORDERDRAWINGSETTINGDLS.PROCESSLEVEL!='NONE' ) ");
                        sb  . append("  AND AUNIT_ORDERDRAWINGSETTINGDLS.PROCESSLEVEL!='none' AND AUNIT_ORDERDRAWINGSETTINGDLS.PROCESSLEVEL!='NONE' ");
                        sb  . append("  ) ");
                        sb  . append("  AND pro_drawing_can_details.orderfollowstatus = 0 ");
                        sb  . append("    )");
                        sb  . append("  )");
                        sb  . append("  )");

        if(!SUnit.equals("All")){
            sb.append("   and Unit.UnitName='"+SUnit+"' ");
        }
            if(SSelectType.equalsIgnoreCase("Depo")){
            sb.append("   and regularorder.rorderno like 'DM%'");
        }
        if(SSelectType.equalsIgnoreCase("Party")){
            sb.append("   and regularorder.rorderno not like 'DM%'");
        }
        if(!SOrderNo1.equals("All")){
            sb.append("  and regularorder.rorderno='"+SOrderNo1+"' ");
        }
        if(!SProcessTypeCode.equals("All")){
            if(SProcessTypeCode.equals("BR-C Orders")){
                sb.append("  and ProcessingType.OEStatus = 1 ");
            }   else if(SProcessTypeCode.equals("Other than BR-C Orders")){
                sb.append("  and ProcessingType.OEStatus = 0 ");
            }   else{
                sb.append("  and RegularOrder.ProcessTypeCode = "+SProcessTypeCode);
            }
        }
        sb.append("  ) Order By 16 desc, OrderDate , rorderno ");
        return sb.toString();
    }

    private  String getBUnitQs() {
        
        StringBuffer    sb  = new StringBuffer(); 
                        sb  . append(" Select DISTINCT rorderno, OrderDate, DeliveryDate, Weight, CountName, PartyName, MixingDate, MixingWeight, SimplexPumb,  ");
                        sb  . append(" SimplexRing, SpinningPumb, SpinningStatusStartDate, CorrectionMixing,  Process.GetSimplexRunoutStatus(rorderno) as Remarks, "); 
                        sb  . append(" GenStatus, PriorityName, FormName, PriorityCode, '' as ProcessRemarks  From (  ");
                        sb  . append(" Select  REGULARORDER.rorderno, Regularorder.OrderDate, DeliveryDate, Regularorder.Weight, YarnCount.CountName,  ");
                        sb  . append(" PartyMaster.PartyName, Rmixir.MixingDate, Rmixir.MixingWeight, ''  as SimplexPumb,  ");
                        sb  . append(" '' as SimplexRing, '' as SpinningPumb, '' as SpinningStatusStartDate, Rmixir.CorrectionMixing,   ");
                        sb  . append(" decode(OrderSplitDetails.baseorderno,'',RegularOrder.rorderno, OrderSplitDetails.baseorderno) as BaseOrder,   ");
                        sb  . append(" scm.GetGeneralStatus(RegularOrder.OrderId) as GenStatus, deCode(RegularOrder.Shortage, 1, 'Shortage', PartyPriority.Name) as PriorityName, FibreForm.FormName, PartyPriority.Code as PriorityCode   From SCM.REGULARORDER  ");
                        sb  . append(" Inner Join PROCESS.PROCESSINGOFINDENT ON PROCESSINGOFINDENT.RORDERNO = REGULARORDER.RORDERNO and processingofindent.simpcomp = 0 ");
                        sb  . append(" Left Join process.pro_process_base on pro_process_base.rorderno = processingofindent.rorderno   ");
                        sb  . append(" Left Join process.pro_blowroom_lapslip on pro_blowroom_lapslip.rorderno = processingofindent.rorderno   ");
                        sb  . append(" Left Join process.pro_carding_can_details on pro_carding_can_details.rorderno = processingofindent.rorderno   ");
                        sb  . append(" Left Join process.pro_drawing_can_details on pro_drawing_can_details.rorderno = processingofindent.rorderno ");   
                        sb  . append(" Inner Join scm.YarnCount On  YarnCount.CountCode = RegularOrder.CountCode   ");
                        sb  . append(" Inner Join scm.PartyMaster On  Partymaster.partyCode = RegularOrder.partyCode   ");
                        sb  . append(" Inner Join scm.PartyPriority On PartyPriority.Code = nvl(PartyMaster.PriorityStatus,0)   ");
                        sb  . append(" inner join scm.unit on unit.unitcode= RegularOrder.UnitCode  and RegularOrder.UnitCode  = 2 ");
                        sb  . append(" Inner Join scm.ProcessingType on ProcessingType.ProcessCode = RegularOrder.ProcessTypeCode   ");
                        sb  . append(" Inner Join scm.RMixir On  Rmixir.OrdNo = REGULARORDER.RORDERNO  ");
                        sb  . append(" Inner Join scm.FibreForm on FibreForm.FormCode = nvl(RegularOrder.FibreFormCode, 0)   ");
                        sb  . append(" Left Join scm.OrderSplitDetails On OrderSplitDetails.splitorderno = Processingofindent.rorderno ");
                        sb  . append(" Where Nvl(RMixir.CorrectionMixing,0)=0 and ");
                        sb  . append(" (  ");
                        sb  . append(" pro_process_base.MIXSTKSTATUS = 0 ");  
                        sb  . append(" or  ");
                        sb  . append(" pro_blowroom_lapslip.orderfollowstatus = 0  ");
                        sb  . append(" or  ");
                        sb  . append(" pro_carding_can_details.orderfollowstatus = 0  ");
                        sb  . append(" or  ");
                        sb  . append("  pro_drawing_can_details.orderfollowstatus = 0  ");
                        sb  . append("  ) ");
                        sb  . append(" AND ");
                        sb  . append(" ( ");
                        sb  . append("   ordersplitdetails.DEPTCODE is null ");
                        sb  . append(" or ");
                        sb  . append(" ( ");
                        sb  . append(" ordersplitdetails.DEPTCODE is not null  ");
                        sb  . append(" and  ");
                        sb  . append(" (ordersplitdetails.DEPTCODE=16 or ordersplitdetails.DEPTCODE<=3)  ");
                        sb  . append(" ) ");
                        sb  . append(" ) ");
            if(!SUnit.equals("All")){
                sb  . append("   and Unit.UnitName='"+SUnit+"' ");
            }
            if(SSelectType.equalsIgnoreCase("Depo")){
                sb  . append("   and regularorder.rorderno like 'DM%'");
            }
            if(SSelectType.equalsIgnoreCase("Party")){
                sb  . append("   and regularorder.rorderno not like 'DM%'");
            }
            if(!SOrderNo1.equals("All")){
                sb  . append("  and regularorder.rorderno='"+SOrderNo1+"' ");
            }
            if(!SProcessTypeCode.equals("All")){
                if(SProcessTypeCode.equals("BR-C Orders")){
                    sb  . append("  and ProcessingType.OEStatus = 1 ");
                }else if(SProcessTypeCode.equals("Other than BR-C Orders")){
                    sb  .append("  and ProcessingType.OEStatus = 0 ");
                }else{
                    sb  . append("  and RegularOrder.ProcessTypeCode = "+SProcessTypeCode);
                }
            }
            sb  . append("  ) Order By 16 desc, OrderDate , rorderno ");
            System.out.println(sb.toString());
        return sb.toString();    
    }    

    public void DespatchDetails(int OnDate) {
        
        VDOrderNo       = new Vector();
        VPacking        = new Vector();
        try {
            StringBuffer    sb  = new StringBuffer(); 
                            sb  . append("  Select RegularOrder.RorderNo,nvl(Sum(Balepacking.NetWeight),0) as Packing From RegularOrder");
                            sb  . append("  Left  Join Balepacking On Balepacking.Orderid = RegularOrder.Orderid and Balepacking.Flag<>1");
                            sb  . append("  inner join unit on unit.unitcode= RegularOrder.UnitCode ");
                            sb  . append("  Inner Join ProcessingType on ProcessingType.ProcessCode=RegularOrder.ProcessTypeCode ");
            if(SUnit.equals("A_Unit")){
                sb.append("  Inner Join processa.processingofindent on processingofindent.rorderno = regularorder.rorderno ");
            }
            if(SUnit.equals("B_Unit")){
                sb.append("  Inner Join process.processingofindent on processingofindent.rorderno = regularorder.rorderno ");
            }
            sb.append("  Where processingofindent.packcomp =  0 ");
            if(!SUnit.equals("All")){
                sb.append("  and Unit.UnitName='"+SUnit+"'");
            }
            if(SSelectType.equalsIgnoreCase("Depo")){
                sb.append("  and regularorder.rorderno like 'DM%'");
            }
            if(SSelectType.equalsIgnoreCase("Party")){
                sb.append("   and regularorder.rorderno not like 'DM%'");
            }
            if(!SOrderNo1.equals("All")){
                sb.append("   and regularorder.rorderno='"+SOrderNo1+"' ");
            } 
            if(!SProcessTypeCode.equals("All")){
                if(SProcessTypeCode.equals("BR-C Orders")){
                    sb.append("  and ProcessingType.OEStatus = 1 ");
                }   else if(SProcessTypeCode.equals("Other than BR-C Orders")){
                        sb.append(" and ProcessingType.OEStatus = 0 ");
                }   else{
                    sb.append("  and RegularOrder.ProcessTypeCode = "+SProcessTypeCode);
                }
            }
            sb.append("  group by RegularOrder.RorderNo order by RegularOrder.RorderNo");

            JDBCConnection  jdbc    = JDBCConnection.getJDBCConnection();
            Connection      conn    = jdbc.getConnection();
            Statement       stat    = conn.createStatement();
            ResultSet       res     = stat.executeQuery(sb.toString());
            while(res.next()){
                VDOrderNo   . addElement(res.getString(1));
                VPacking    . addElement(res.getString(2));
            }
            res . close();
            stat. close();
        }   catch(Exception e)  {
            e.printStackTrace();
        }
    }

    private void DataAlign()    {
        
        VDueDepo    = new Vector();
        VDueMill    = new Vector();
        VDLMix      = new Vector();
        VDRMix      = new Vector();
        VDFMix      = new Vector();
        VPTotal     = new Vector();
        VRunDelay   = new Vector();
        VCleerDelay = new Vector();

        for(int i=0;i<VOrderNo.size();i++)  {
            try {
                int iOrderDate  = common.toInt((String)VOrderDate.elementAt(i));
                int iDueDate    = common.toInt((String)VMillDueDate.elementAt(i));
                int iMixDate    = common.toInt((String)VMixDate.elementAt(i));
                int iRepDate    = common.toInt(common.getServerDate());
                int iReMix      = common.toInt((String)VRMixDate.elementAt(i));
                int iCreer      = common.toInt((String)VCleerDate.elementAt(i));

                int iDueDep     = common.getDateDiff(iDueDate,iRepDate);
                int iDueMill    = common.getDateDiff(iRepDate,iOrderDate);
                int iDLMix      = common.getDateDiff(iMixDate,iOrderDate);
                int iProDate    = common.getDateDiff(iRepDate,iMixDate);
                int iRMixDate   = common.getDateDiff(iReMix,iRepDate);
                int iCreerD     = common.getDateDiff(iRepDate,iCreer);

                    VDueDepo    . addElement(Integer.toString(iDueDep));
                    VDueMill    . addElement(Integer.toString(iDueMill));
                    VDLMix      . addElement(Integer.toString(iDLMix));
                    VDFMix      . addElement(Integer.toString(iDLMix));
                    VDRMix      . addElement(Integer.toString(iRMixDate));
                    VPTotal     . addElement(Integer.toString(iProDate));
    //                VRunDelay.addElement(Integer.toString(iProDate-7));
                    VRunDelay   . addElement(Integer.toString(iProDate - iRunOutTarget));
                    VCleerDelay . addElement(Integer.toString(iCreerD));
            }   catch(Exception e)  {
                e.printStackTrace();
            }
        }
    }

    private void getCorrectionMix() {

        int     iCorrectionMix  = 0;
        double  dCMixWt         = 0;
        String  CDate           = "";
        String  COrderNo        = "";

        for(int i=0; i<VOrderNo.size();i++) {
            iCorrectionMix = common.toInt((String)VCorrectionMix.elementAt(i));
            if(iCorrectionMix==1)   {
                COrderNo    =(String)VOrderNo.elementAt(i);
                dCMixWt     = common.toDouble((String)VMixWt.elementAt(i));
                CDate       = (String)VMixDate.elementAt(i);
                for(int j=0;j<VOrderNo.size();j++)  {
                        SOrderNo    = (String)VOrderNo.elementAt(j);
                    int iStatus     = common.toInt((String)VCorrectionMix.elementAt(j));
                    if(SOrderNo.equals(COrderNo) && iStatus!=1) {
                        double  dWt         = common.toDouble((String)VMixWt.elementAt(j))+dCMixWt;
                                VRMixDate   . set(j,CDate);
                                VMixWt      . set(j,common.getRound(dWt,3));
                    }
                }
                VOrderNo        . remove(i);
                VOrderDate      . remove(i);
                VMillDueDate    . remove(i);
                VOrderWeight    . remove(i);
                VCount          . remove(i);
                VParty          . remove(i);
                VMixDate        . remove(i);
                VMixWt          . remove(i);
                VBColor         . remove(i);
                VRColor         . remove(i);
                VCColor         . remove(i);
                VCleerDate      . remove(i);
                VCorrectionMix  . remove(i);
                VRMixDate       . remove(i);
                VRemarks        . remove(i);
            }
        }
    }

    private void Head() {

        String  SPitch      = "M";
        String  Head1       = "Company  :AMARJOTHI SPG MILLS Ltd \n";
        String  Head2       = "Document :Simplex RunOut  & Follow Up Report";
        String  SAsOn       = "As On Date  "+common.parseDate(SEnDate);
        String  Head3       = "Unit : "+SUnit+" ,Process Type :  "+SProcessTypeName+", Target Days : "+iRunOutTarget+"\n";
        String  Head4       = "Page :"+iPage;

        String  Str1        = "",Str2="",Str3="",Str4="",Str5="",Str6="";

        String  STarget1    = common.Space(250)+"|----------------------\n";
        String  STarget2    = common.Space(250)+"|   Target : 6 Days   |\n";
        String  STarget3    = common.Space(250)+"----------------------|\n";

                Str1        = "|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n";
                Str2        = "|     |           |          |  As Per  |        |     |                           |          | UpTo   | Balance|Bobin Passing |           |      Process      |  Due | Due  | F.Mix| L.Mix| R.Mix|Creelg|Proces|Runout|                         |                        |\n";
                Str3        = "|Sl.No| OrderNo   | OrderDate|   Mill   | Order  |Count|    Name Of the Buyer      |LastMixing| Date   |  To    |--------------| Packed    |    Instruction    | Date | Date | Delay| Delay| Delay| Delay| Days |Delay |         Remarks         |     Process Remarks    |\n";
                Str4        = "|     |           |          | DueDate  | Weight |     |                           |    Date  | Mixed  |  Mix   |      |       | Weight    |    BlowRoom to    | AsPer| AsPer| Days | Days | Days | Days |      |Days  |                         |                        |\n";
                Str5        = "|     |           |          |          |        |     |                           |          | Weight |  (Kgs) | Nos  |  Kgs  |  Kgs      |     Spinning      | Dept | Mills|      |      |      |      |      |      |                         |                        |\n";
                Str6        = "|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|";
                End         = "|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n";
                SNote       = " \nFoot Note : Due Date Dly Days=Rep Dt - Due Dt /*/ First Mixing Delay Days = F.Mix - Ord Dt /*/Last Mixing Delay Days = R.Mix Dt - Ord Dt/*/ Process Days = Rep Dt - L.Mix ";
                SNote1      = " \n            Creeling days = Rep Dt - Spg Clr Dt /*/ Runout Delay = Rep Dt - L.Mix - 7 Days ";
                End1        = End+SNote+SNote1;
        prnWriter(Head1+Head2);
        prnWriter(SAsOn);
        prnWriter(Head3+Head4+SPitch);

        if(SUnit.equals("Sample_Unit")) {
            prnWriter(STarget1+STarget2+STarget3);
        }
        prnWriter(Str1+Str2+Str3+Str4+Str5+Str6);
    }

    private void Body() {

        String  SPriorityStatus = "";
        int     slno            = 0;
        for(int i=0;i<VOrderNo.size();i++)  {
            slno++;
            try {
                if(!common.parseNull((String)VPartyPriority.elementAt(i)).equals(SPriorityStatus))  {

                    if(i!=0){

                        FW.write("|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|"+"\n");
                        FW.write("|TOTAL|           |          |          |"+common.Rad(common.getRound(dTOrdWt,0),8)+"|     |                           |          |"+common.Rad(common.getRound(dTMixWt,0),8)+"|        |      |       |"+common.Rad(common.getRound(dTPackWt,3),11)+"|                   |      |      |      |      |      |      |      |      |                         |                        |"+"\n");
                        dTOrdWt = 0;
                        dTMixWt = 0;
                        dTPackWt= 0;
                        slno    = 1;
//                        SNo = 1;
                        TLine =TLine+10;
                    }
                    String SPartyStatus="|"+common.Space(5)+
                                        "|"+common.Space(11)+
                                        "|"+common.Space(10)+
                                        "|"+common.Space(10)+
                                        "|"+common.Space(8)+
                                        "|"+common.Space(5)+
                                        "|E"+common.Pad(common.parseNull((String)VPartyPriority.elementAt(i)),27)+"F"+
                                        "|"+common.Space(10)+
                                        "|"+common.Space(8)+
                                        "|"+common.Space(8)+
                                        "|"+common.Space(6)+
                                        "|"+common.Space(7)+
                                        "|"+common.Space(11)+
                                        "|"+common.Space(19)+
                                        "|"+common.Space(6)+
                                        "|"+common.Space(6)+
                                        "|"+common.Space(6)+
                                        "|"+common.Space(6)+
                                        "|"+common.Space(6)+
                                        "|"+common.Space(6)+
                                        "|"+common.Space(6)+
                                        "|"+common.Space(6)+
                                        "|"+common.Space(25)+
                                        "|"+common.Space(24)+"|";

                    String S1  = "|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|";
                    String S2  = "|     |           |          |          |        |     |                           |          |        |       | Nos  |  Kgs  |  Kgs      |     Spinning      |      |      |      |      |      |      |      |      |                         |                        |";
                    String S3  = "|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|";

                    prnWriter(S1);
                    prnWriter(SPartyStatus);
                    prnWriter(S3);

                }
            }   catch(Exception e) {
                e.printStackTrace();
            }
            double  dOrdWt      = common.toDouble((String)VOrderWeight.elementAt(i));
            double  dMixWt      = common.toDouble((String)VMixWt.elementAt(i));
            double  dPackWt     = common.toDouble(getPackWt((String)VOrderNo.elementAt(i)));

//            iSNo =iSNo+1;
                    dTOrdWt     = dTOrdWt+dOrdWt;
                    dTMixWt     = dTMixWt+dMixWt;
                    dTPackWt    = dTPackWt+dOrdWt;
                    dGrandOrdWt = dGrandOrdWt+dOrdWt;
                    dGrandMixWt = dGrandMixWt+dMixWt;
                    dGrandPackWt= dGrandPackWt+dOrdWt;

            String SNo          = "|"+common.Cad(String.valueOf(slno),5)+"|";
            String OrdNo        = common.Pad((String)VOrderNo.elementAt(i),11)+"|";
            String OrdDate      = common.Pad(common.parseDate((String)VOrderDate.elementAt(i)),10)+"|";
            String Due          = common.Pad(common.parseDate((String)VMillDueDate.elementAt(i)),10)+"|";
            String OrdWt        = common.Rad(common.getRound(dOrdWt,0)+" ",8)+"|";
            String Count        = common.Cad(common.parseNull((String)VCount.elementAt(i)),5)+"|";
            String Party        = common.Pad((String)VParty.elementAt(i),27)+"|";
            String MixDate      = common.Pad(common.parseDate((String)VMixDate.elementAt(i)),10)+"|";
            String MixWt        = common.Rad(common.getRound(dMixWt,0)+" ",8)+"|";
            String Bal          = common.Rad(common.getRound("",3),8)+"|";
            String BWt          = common.Pad(common.getRound("",3),6)+"|";
            String BNo          = common.Pad(common.getRound("",0),7)+"|";
            String Emp          = common.Pad("",6)+"|";
            String Emp1         = common.Pad("",7)+"|";
            String Pack         = common.Rad(common.getRound(dPackWt,3),11)+"|";
            String BColor       = common.Cad(getNilRemark(common.parseNull((String)VBColor.elementAt(i))),20)+"|";
            String RColor       = common.Cad(getNilRemark(common.parseNull((String)VRColor.elementAt(i))),12)+"|";
            String CColor       = common.Cad(getNilRemark(common.parseNull((String)VCColor.elementAt(i))),11)+"|";
            String CleerD       = common.Cad(common.parseDate((String)VCleerDate.elementAt(i)),10)+"|";
            String Process      = common.Rad(getInstrucion((String)VOrderNo.elementAt(i)),19)+"|";
            String DueDepo      = common.Rad(common.parseNull((String)VDueDepo.elementAt(i))+" ",6)+"|";
            String DueMill      = common.Rad(common.parseNull((String)VDueMill.elementAt(i))+" ",6)+"|";
            String FDelay       = common.Rad(common.parseNull((String)VDLMix.elementAt(i))+" ",6)+"|";
            String RDelay       = common.Rad(common.parseNull((String)VDFMix.elementAt(i))+" ",6)+"|";
            String RMix         = common.Rad(common.getRound("",3),6)+"|";
            String CreelDays    = common.Rad(common.parseNull((String)VCleerDelay.elementAt(i))+" ",6)+"|";
            String PDelay       = common.Rad(common.parseNull((String)VPTotal.elementAt(i))+" ",6)+"|";
            String RODelay      = common.Rad(common.parseNull((String)VRunDelay.elementAt(i))+" ",6)+"|";
            String SRemarks     = "";

            if(SUnit.equalsIgnoreCase("Sample_Unit"))   {
                SRemarks       = common.Pad((String)VStatus.elementAt(i),25)+"|";
            }   else   {
                SRemarks       = common.Pad(getNilRemark(common.parseNull((String)VRemarks.elementAt(i))),25)+"|";
            }
            String SProcessRemarks  = common.Pad(getNilRemark(common.parseNull((String)VProcessRemarks.elementAt(i))),24)+"|";
            String SStrSpace        =   "|"+common.Space(5)+
                                        "|"+common.Space(11)+
                                        "|"+common.Space(10)+
                                        "|"+common.Space(10)+
                                        "|"+common.Space(8)+
                                        "|"+common.Space(5)+
                                        "|"+common.Space(27)+
                                        "|"+common.Space(10)+
                                        "|"+common.Space(8)+
                                        "|"+common.Space(8)+
                                        "|"+common.Space(6)+
                                        "|"+common.Space(7)+
                                        "|"+common.Space(11)+
                                        "|"+common.Space(19)+
                                        "|"+common.Space(6)+
                                        "|"+common.Space(6)+
                                        "|"+common.Space(6)+
                                        "|"+common.Space(6)+
                                        "|"+common.Space(6)+
                                        "|"+common.Space(6)+
                                        "|"+common.Space(6)+
                                        "|"+common.Space(6)+
                                        "|"+common.Space(25)+
                                        "|"+common.Space(24)+"|";

            prnWriter(SNo+OrdNo+OrdDate+Due+OrdWt+Count+Party+MixDate+MixWt+Bal+Emp+Emp1+Pack+Process+DueDepo+DueMill+FDelay+RDelay+RMix+CreelDays+PDelay+RODelay+SRemarks+SProcessRemarks);
            //prnWriter(SNo+OrdNo+OrdDate+Due+OrdWt+Count+Party+MixDate+MixWt+Bal+Emp+Emp1+Pack+BColor+RColor+CColor+CleerD+Process+DueDepo+DueMill+FDelay+RDelay+RMix+CreelDays+PDelay+RODelay+SRemarks);
            prnWriter(SStrSpace);
            SPriorityStatus = common.parseNull((String)VPartyPriority.elementAt(i));

        }
    }

    private void setTotal() {
        try {
            FW.write("|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|"+"\n");
            FW.write("|  Total============>                   |"+common.Rad(common.getRound(dTOrdWt,0),8)+"|"+common.Space(44)+"|"+common.Rad(common.getRound(dTMixWt,0),8)+"|"+common.Space(23)+"|"+common.Rad(common.getRound(dTPackWt,3),11)+"|"+common.Space(126)+"|"+"\n");
            FW.write("|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|"+"\n");
            FW.write("|  GRAND TOTAL                          |"+common.Rad(common.getRound(dGrandOrdWt,0),8)+"|"+common.Space(44)+"|"+common.Rad(common.getRound(dGrandMixWt,0),8)+"|"+common.Space(23)+"|"+common.Rad(common.getRound(dGrandPackWt,3),11)+"|"+common.Space(126)+"|"+"\n");

            dTOrdWt     = 0;
            dTMixWt     = 0;
            dTPackWt    = 0;
            dGrandOrdWt = 0;
            dGrandMixWt = 0;
            dGrandPackWt= 0;
        }   catch(Exception e) {
            e.printStackTrace();
        }
    }

    private void FileWriter()   {
        try {

//          FW = new FileWriter("//"+SCRIPTHOST+"/d/"+SCRIPTFOLDER+"/src/Temp/NewSimplexRunout.prn");
//          FW = new FileWriter("d:/NewSimplexRunout.prn");
            FW = new FileWriter("/software/MixPrint/NewSimplexRunout.prn");
            Head();
            Body();
            setTotal();
            prnWriter(End1);
            FW.close();
        }   catch(Exception e) {
            System.out.println(e);
            e.printStackTrace();
        }
    }

    private void prnWriter(String Str)  {
        try {
            TLine =TLine+1;
            if(TLine<53)    {
                FW.write(Str+"\n");
            }
            else    {
                iPage=iPage+1;
                FW.write(Str+"\n");
                FW.write(End1+"\n");
                TLine =0;
                Head();
            }
        }   catch(Exception e)  {
            e.printStackTrace();
        }
    }

    private String getPackWt(String OrderNo)    {
        String SWt  = "";
        for(int i=0; i <VDOrderNo.size();i++)   {
            if( OrderNo.equals((String)VDOrderNo.elementAt(i))) {
                SWt  = (String)VPacking.elementAt(i);
            }
        }
        return SWt;
    }

    private String getNilRemark(String Str)
    {
        if(Str.equals("Nil")|| Str.equals("NIL")){
            return "";
        }
        return Str;
    }

    public void setInstruction()
    {
        VInsOrderNo   = new Vector();
        VInsShadeName = new Vector();
        VInsRefNo     = new Vector();
        VShortage     = new Vector();

        try {
            StringBuffer    sb  = new StringBuffer();
                            sb  . append("  Select  RegularOrder.RorderNo, ShadeGroupName , RefOrderNo, Shortage  from RegularOrder ");
            if(SUnit.equals("A_Unit")){
                sb . append("  Inner Join processa.processingofindent on processingofindent.rorderno = regularorder.rorderno and processingofindent.packcomp = 0");
            }
            if(SUnit.equals("B_Unit")){
                sb . append("  Inner Join process.processingofindent on processingofindent.rorderno = regularorder.rorderno and processingofindent.packcomp = 0");
            }
            sb . append("  Left  Join OrderShadeGroup On OrderShadeGroup.id =RegularOrder.Shadeid");
            sb . append("  inner join unit on unit.unitcode= RegularOrder.UnitCode  ");

            if(!SUnit.equals("All")){
                sb . append("  Where Unit.UnitName='"+SUnit+"' ");
            }
//		sb.append("  where  RegularOrder.unitcode = 1");
            JDBCConnection  jdbc    = JDBCConnection.getJDBCConnection();
            Connection      conn    = jdbc.getConnection();
            Statement       stat    = conn.createStatement();
            ResultSet       res     = stat.executeQuery(sb.toString());

            while(res.next())   {
                VInsOrderNo     . addElement(res.getString(1));
                VInsShadeName   . addElement(res.getString(2));
                VInsRefNo       . addElement(res.getString(3));
                VShortage       . addElement(res.getString(4));
            }
            res . close();
            stat. close();
        }catch(Exception e) {
            e.printStackTrace();
        }
    }

    private String getInstrucion(String SOrderNo)   {
        
        for(int i =0;i< VInsOrderNo.size();i++) {

            String OrdNo = (String)VInsOrderNo.elementAt(i);
            if(SOrderNo.equals(OrdNo))  {
                if(SOrderNo.startsWith("DM"))   {
                    return (String)VInsShadeName.elementAt(i);
                }   else    {
                    int     iNo         = 0;
                    String  RefNo       = common.parseNull((String)VInsRefNo.elementAt(i));
                    String  Shortage    = common.parseNull((String)VShortage.elementAt(i));

                    if(RefNo.startsWith("PREM")){
                        return "AS PER SHADE CARD"; 
                    }
                    if(RefNo.startsWith("SM")){
                        return "AS PER SAMPLES";
                    }
                    if((RefNo.startsWith("DM") || RefNo.startsWith("LM")) && !Shortage.startsWith("2")){
                        return "AS PER REGULAR";
                    }
                    if(RefNo.startsWith("DM") || RefNo.startsWith("LM")|| Shortage.startsWith("2")){
                        return "Shortage Order";
                    }
                    if(RefNo.startsWith("DIRECT")){
                        return "DIRECT MATCHING";
                    }
                    if(RefNo.length() > 2 ){
                        iNo =common.toInt(RefNo.substring(0,1));
                    }
                    if(iNo > 0){
                        return "AS PER SAMPLES";
                    }
                }
            }
        }
        return "";
    }
}
