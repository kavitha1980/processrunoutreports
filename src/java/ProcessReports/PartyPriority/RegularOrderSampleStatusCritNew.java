/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProcessReports.PartyPriority;


import ProcessReports.jdbc.*;
import ProcessReports.util.*;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

/**
 *
 * @author Administrator
 */
public class RegularOrderSampleStatusCritNew extends HttpServlet {

    HttpSession     session;
    Vector          VUnitCode,VUnitName;
    String          SToday  = "";
    Common          common  = new Common();
    ArrayList       AProcessTypeCode, AProcessTypeName;


    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            
            SToday= common.parseDate(common.getCurrentDate());
                        response. setContentType("text/html;charset=UTF-8");
            PrintWriter out     = response.getWriter();
            
            setDataintoVector();


            out.println("<html>");
            out.println("<body bgcolor='#9AA8D6'>");
            out.println("");
            out.println("<base target='topmain'>");
            out.println("<form method='GET' action='RegularOrderSampleStatus'>");

            out.println("<table>");

            out.println("</tr>");
            out.println("<tr>");
            out.println("<td><font color='#FFFF66'><b>As On Date: </b></font></td>");
            out.println("     <td><input type='text' name='TEnDate'value="+SToday+" size='12'></td>");
            out.println("<tr>");

            out.println("<tr>");
            out.println("<td><font color='#FFFF66'><b>Unit: </b></font></td>");
            out.println("      <td><select size=1 name=Unit>");
            out.println("      <option value='All'>All</option>");
            for(int i=0;i<VUnitCode.size();i++) {
                 out.println("      <option value='"+VUnitName.elementAt(i)+"'>"+VUnitName.elementAt(i)+"</option>");
            }
            out.println("      </select></td>");

            out.println("<td><font color='#FFFF66'><b>Process Type : </b></font></td>");

            out.println("      <td><select size=1 name='processType'>");
            out.println("      <option value='All#All'>All</option>");
            out.println("      <option value='BR-C Orders#BR-C Orders'>BR-C Orders</option>");
            out.println("      <option value='Other than BR-C Orders#Other than BR-C Orders'>Other than BR-C Orders</option>");
            for(int i=0;i<AProcessTypeCode.size();i++)    {
                out.println("<option value='"+AProcessTypeCode.get(i)+"#"+AProcessTypeName.get(i)+"'>"+AProcessTypeName.get(i)+"</option>");
            }
            out.println("      </select></td>");
                out.println("    </tr>");
		out.println("    <tr>");
		out.println("<div align='center'>");
            out.println("<table border=1 width='600' height='14'>");

            out.println("       <td><b><font color='FFEEFE'>All</font> </b><input type='RADIO' name='Select' value= 'All' Checked></td>");
            out.println("       <td><b><font color='FFEEFE'>Depo Order</font> </b><input type='RADIO' name='Select' value= 'Depo' ></td>");
		  
            out.println("       <td><b><font color='FFEEFE'>Party Order</font> </b><input type='RADIO' name='Select' value='Party'></td>");
            out.println("    </tr>");
            out.println("    </center>");
            out.println("  </table>");
            out.println("</div>");
            out.println("    </tr>");
            out.println("<tr>");

            out.println("     <td><input type='submit' value='Submit' name='B1' style='color: #003300; font-size: 10pt; font-weight: bold'></td>");
            out.println("     <td><input type='reset' value='Reset' name='B2' style='color: #800000; font-size: 10pt; font-weight: bold'></td>");
            out.println("</tr>");

            out.println("</table>");
            out.println("</form>");
            out.println("</body>");
            out.println("</html>");

            out.close();

        } catch(Exception ex){
            ex.printStackTrace();
        }finally {
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    public void setDataintoVector() {

        VUnitCode           = new Vector();
        VUnitName           = new Vector();
        AProcessTypeCode    = new ArrayList();
        AProcessTypeName    = new ArrayList();

        try {

            Class.forName("oracle.jdbc.OracleDriver");
            Connection theConnection = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","scm","rawmat");

            Statement st             =theConnection.createStatement();
            ResultSet rs             =st.executeQuery(" select unitcode,unitname from unit order by 2 ");

            while(rs.next())    {
                VUnitCode   . addElement(rs.getString(1));
                VUnitName   . addElement(rs.getString(2));
            }
           rs.close();

            rs = st.executeQuery("Select ProcessCode, ProcessType from ProcessingType Order by 2 ");
            while(rs.next())    {
                AProcessTypeCode    . add(common.parseNull(rs.getString(1)));
                AProcessTypeName    . add(common.parseNull(rs.getString(2)));
            }
            rs          . close();
            st          . close();
            theConnection. close();
        }catch(Exception e) {
            System.out.println(e);
        }
    }
}
