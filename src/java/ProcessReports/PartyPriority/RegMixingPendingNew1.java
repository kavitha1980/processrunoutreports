/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProcessReports.PartyPriority;

import ProcessReports.jdbc.*;
import ProcessReports.util.*;

import java.io.*;
import java.sql.*;           
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

/**
 *
 * @author Administrator
 */
public class RegMixingPendingNew1 extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
Common common;
     HttpSession    session;

     String bgColor,bgHead,bgUom,bgBody,fgHead,fgUom,fgBody;
     String SPitch;
     int iLen;
     FileWriter  FW;
     String SUnit="";
     String SCount="";
     String SOrderType="";
     String SShade="";
     String SParty="";

     String line="",totl="",sl="";
     String SFromDate="";
     String SOrderNo,SRefOrderNo,SRate,SPrem1,SRefOrderNo1,SRate1,SMixingDate,SMixingWeight;
     double dRate1,dRate,Diff;
     Vector VOrderNo,VOrderDate,VApprovalDate,VBuyer,VEndUse,VOrderWeight,VRealizationPer,VMixWeight,VCount,VMixDate,VDespDate,VRemarks,VApprovalCall,VDeliveryDate,VPartyPriority;
     String SOrdNo ="" ;
     String SOrdDt ="";
     String SWeig  ="";
     String SMrat  ="";
     double dDepth1=0;
     double dDepth2=0;
     double DepthDiff=0;
     int iSlNo=1;
     int TLine=0;


     String SLastEnOrd ="";
     String SLastDate  ="";

     double dTOrdWt=0;
     double dTMixWt  =0;
     double dTPackWt =0;
     int SNo    = 0;

     String Empty="";
     String sbV="";
     String sbT="";
     String  sbGT="";
     String  sbGT1="";

     String SLastOrderNo      = "";
     String SLastOrderDate    = "";
     String SCountOfOrderNo   = "";
     String SLastMixNo        = "";
     String SLastDyeingOrderNo= "",SLastAgnNo="";
     String SSelectType="";
     String SAppWeight="";
     String SNotAppWeight="";
	 String SProcessTypeCode, SProcessTypeName;
     int iDays,iFromDate =0;

     double dDepth0=0;
     double dDepth3=0;
     double DepthDifference=0;
     double TotOrderWeight=0;
     double TotMixWeight=0;
     double GrandTot=0;
     double GrandTotal=0;
     int LC=59,k=1;
     int iPageCtr=0,iLCtr=100;
     String List1="",List2="",List3="",List4="",List5="",List6="",List7="",List8="",List9="",List10="",List11="",List12="",List13="",List14="",List15="",List16="",List17="",List18="",List19="";

     String SType   = "";    
     protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
          common = new Common();

          bgColor = common.bgColor;
          bgHead  = common.bgHead;
          bgUom   = common.bgUom;
          bgBody  = common.bgBody;
          fgHead  = common.fgHead;
          fgUom   = common.fgUom;
          fgBody  = common.fgBody;

        try {
            
                    response    . setContentType("text/html;charset=UTF-8");
        PrintWriter out         = response.getWriter();
          session            = request.getSession(false);
          SUnit           = common.parseNull((String)request.getParameter("Unit"));
          SCount          = common.parseNull((String)request.getParameter("CountName"));
          SOrderNo          = common.parseNull((String)request.getParameter("ROrderNo"));
          SFromDate       = common.pureDate((String)request.getParameter("TEnDate")); 
          iFromDate       = common.toInt(common.pureDate(request.getParameter("TEnDate")));
          SSelectType     = common.parseNull((String)request.getParameter("Select"));
          iDays           = common.toInt((String)request.getParameter("Delay"));
          SOrderType      = common.parseNull((String)request.getParameter("OrderType"));
          SType          = common.parseNull((String)request.getParameter("type"));

          SShade          = request.getParameter("ShadeName");
          SParty          = common.parseNull((String)request.getParameter("PartyName"));
		  SProcessTypeCode    = null;
          SProcessTypeName    = null;

          StringTokenizer ST  = new StringTokenizer(request.getParameter("processType"), "#");
          while(ST.hasMoreTokens())
          {
               SProcessTypeCode    = ST.nextToken();
               SProcessTypeName    = ST.nextToken();
          }


          iPageCtr=0;
          iLCtr=100;

          setData();
          setHtml(out);
          printFile();
          out.close();

        } finally {
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
     private void setHtml(PrintWriter out) throws  ServletException
     {

          dRate=common.toDouble(SRate);
          dRate1=common.toDouble(SRate1);
          Diff=dRate-dRate1;

          out.println("<html>");
          out.println("<head>");
          out.println("<title>InvPro</title>");
          out.println("</head>");
          out.println("<body bgcolor='"+bgColor+"' text = '#0000FF'>");
          out.println("<p align='center'><font size='5'><b><u>RegularOrder Mixing Pending Details on Date of "+common.parseDate(SFromDate)+" ");
		  out.println("<p><b>Process Type : "+SProcessTypeName+"</b></p>");
          out.println("<div align='center'>");
          out.println("  <table border='0' bordercolor='"+bgColor+"'>");
          out.println("    <tr>");
          out.println("      <td    align='center' rowspan='2' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>SL.No</b></font></td>");
          out.println("      <td    align='center' rowspan='2' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>OrderNo</b></font></td>");
          out.println("      <td    align='center' rowspan='2' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>OrderDate</b></font></td>");
          out.println("      <td    align='center' rowspan='2' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>ApprovalDate</b></font></td>");
          out.println("      <td    align='center' rowspan='2' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>NameOf the Buyer</b></font></td>");
          out.println("      <td   align='center' rowspan='2' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>EndUser</b></font></td>");
          out.println("      <td   align='center' rowspan='2' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Count</b></font></td>");
          out.println("      <td   align='center' rowspan='2' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>OrderedWeight</b></font></td>");
          out.println("      <td   align='center' rowspan='2' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Was to Allow</b></font></td>");
          out.println("      <td    align='center' rowspan='2' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Weight AsPer Table Kgs</b></font></td>");
          out.println("      <td    align='center' rowspan='2' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>UptoDate Mixing</b></font></td>");
          out.println("      <td   align='center' rowspan='2' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>BalanceToMix</b></font></td>");
          out.println("      <td   align='center' rowspan='2' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>DueDate For Mixing</b></font></td>");
          out.println("      <td   align='center' rowspan='2' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>DueDate For DesPatch</b></font></td>");
          out.println("      <td   align='center' colspan='3' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Delayed Days</b></font></td>");
          out.println("      <td   align='center' rowspan='2' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Mixing DelayDays</b></font></td>");
          out.println("      <td   align='center' rowspan='2' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Remarks</b></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td   align='center'  bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>OrderDate of Mix</b></font></td>");
          out.println("      <td   align='center'  bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>DueDateOf Mix</b></font></td>");
          out.println("      <td   align='center'  bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>DueDate of Dps</b></font></td>");
          out.println("    </tr>");

          String SPriorityStatus="";

          int iSlNo = 1;

          for(int i=0;i<VOrderNo.size();i++)
          {
              if(!common.parseNull((String)VPartyPriority.elementAt(i)).equals(SPriorityStatus))
              {
                 if(i!=0)
                 {

                      out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>&nbsp</font></td>");
                      out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>Total</font></td>");
                      out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>&nbsp</font></td>");
                      out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>&nbsp</font></td>");
                      out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>&nbsp</font></td>");
                      out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>&nbsp</font></td>");
                      out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>&nbsp</font></td>");
                      out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>"+common.getRound(TotOrderWeight,2)+"</font></td>");
                      out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>&nbsp</font></td>");
                      out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>"+common.getRound(TotMixWeight,2)+"</font></td>");
                      out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>&nbsp</font></td>");
                      out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>"+common.getRound(TotOrderWeight,2)+"</font></td>");
                      out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>&nbsp</font></td>");
                      out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>&nbsp</font></td>");
                      out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>&nbsp</font></td>");
                      out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>&nbsp</font></td>");
                      out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>&nbsp</font></td>");
                      out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>&nbsp</font></td>");
                      out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>&nbsp</font></td>");
                      out.println("    </tr>");

                        TotOrderWeight=0;
                        TotMixWeight=0;
                        iSlNo    = 1;

                 }

                 out.println("    <tr>");
                 out.println("      <td colspan=19 width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>"+((String)VPartyPriority.elementAt(i))+"</font></td>");
                 out.println("    </tr>");
              }

              int CurrentDate= common.getIntCurrentDate();
              int OrderDate  =common.toInt((String)VOrderDate.elementAt(i));
              int ApprovalDate=common.toInt((String)VApprovalCall.elementAt(i));
              int MixDate    =common.toInt((String)VMixDate.elementAt(i));
              int DespDate    =common.toInt((String)VDespDate.elementAt(i));

              int OrderDateOfMix=common.getDateDiff(CurrentDate,OrderDate);
              //int DueDateOfMix=common.getDateDiff(CurrentDate,MixDate);
              //int DueDateOfDesp=common.getDateDiff(CurrentDate,DespDate);
              int DueDateMixingPen=common.getDateDiff(CurrentDate,ApprovalDate);

              String DueDateMixing = common.pureDate((String)VDeliveryDate.elementAt(i));

              for(int j=0;j<15;j++)
              {
                   DueDateMixing   = common.getPrevDate(DueDateMixing);
              }
              int DueDateOfMix=common.getDateDiff(iFromDate,common.toInt(DueDateMixing));
              int DueDateOfDesp=common.getDateDiff(iFromDate,common.toInt(common.pureDate((String)VDeliveryDate.elementAt(i))));

              if(DueDateMixingPen>=iDays)
              {
            out.println("    <tr>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>"+(iSlNo)+"</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>"+VOrderNo.elementAt(i)+"</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>"+common.parseDate((String)VOrderDate.elementAt(i))+"</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>"+common.parseDate((String)VApprovalDate.elementAt(i))+"</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>"+common.parseNull((String)VBuyer.elementAt(i))+"</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>"+common.parseDate((String)VEndUse.elementAt(i))+"</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>"+common.parseNull((String)VCount.elementAt(i))+"</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>"+common.getRound(common.toDouble((String)VOrderWeight.elementAt(i)),2)+"</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>"+common.parseDate((String)VRealizationPer.elementAt(i))+"</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>"+common.getRound(common.toDouble((String)VMixWeight.elementAt(i)),2)+"</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>0</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>"+common.getRound(common.toDouble((String)VOrderWeight.elementAt(i)),2)+"</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>"+common.parseDate(String.valueOf(DueDateMixing))+"</font></td>");
         // out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>"+common.parseDate((String)VMixDate.elementAt(i))+"</font></td>");
         // out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>"+common.parseDate((String)VDespDate.elementAt(i))+"</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>"+VDeliveryDate.elementAt(i)+"</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>"+OrderDateOfMix+"</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>"+DueDateOfMix+"</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>"+DueDateOfDesp+"</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>"+DueDateMixingPen+"</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>"+common.parseNull((String)VRemarks.elementAt(i))+"</font></td>");
            out.println("    </tr>");

            TotOrderWeight=TotOrderWeight+common.toDouble((String)VOrderWeight.elementAt(i));
            TotMixWeight=TotMixWeight+common.toDouble((String)VMixWeight.elementAt(i));

            GrandTot +=common.toDouble((String)VOrderWeight.elementAt(i));
            GrandTotal +=common.toDouble((String)VMixWeight.elementAt(i));


               if(i==VOrderNo.size()-1)
               {
                    SLastEnOrd = (String)VOrderNo.elementAt(i);
                    SLastDate  = (String)VOrderDate.elementAt(i);
               }
               iSlNo++;


               SPriorityStatus = common.parseNull((String)VPartyPriority.elementAt(i));

             }

           }

            out.println("    <tr>");
     
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>&nbsp</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>Total</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>&nbsp</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>&nbsp</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>&nbsp</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>&nbsp</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>&nbsp</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>"+common.getRound(TotOrderWeight,2)+"</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>&nbsp</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>"+common.getRound(TotMixWeight,2)+"</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>&nbsp</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>"+common.getRound(TotOrderWeight,2)+"</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>&nbsp</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>&nbsp</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>&nbsp</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>&nbsp</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>&nbsp</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>&nbsp</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>&nbsp</font></td>");
            out.println("    </tr>");

            out.println("    <tr>");
     
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>&nbsp</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>GrandTotal</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>&nbsp</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>&nbsp</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>&nbsp</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>&nbsp</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>&nbsp</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>"+common.getRound(GrandTot,2)+"</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>&nbsp</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>"+common.getRound(GrandTotal,2)+"</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>&nbsp</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>"+common.getRound(GrandTot,2)+"</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>&nbsp</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>&nbsp</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>&nbsp</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>&nbsp</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>&nbsp</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>&nbsp</font></td>");
            out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>&nbsp</font></td>");
            out.println("    </tr>");

            TotOrderWeight=0;
            TotMixWeight =0;
            GrandTot=0;
            GrandTotal=0;
            iSlNo=1;
          
          out.println("  </table>");

          out.println("  </center>");
          out.println("</div>");
          out.println("");
          out.println("</body>");
          out.println("");
          out.println("</html>");
          out.println("");
          out.close();
     }

     public void setData()
     {
          VOrderNo        = new Vector();
          VOrderDate      = new Vector();
          VApprovalDate   = new Vector();
          VBuyer          = new Vector();
          VEndUse         = new Vector();
          VCount          = new Vector();
          VOrderWeight    = new Vector();
          VRealizationPer = new Vector();
          VMixWeight      = new Vector();
          VMixDate        = new Vector();
          VDespDate       = new Vector();
          VRemarks        = new Vector();
          VApprovalCall   = new Vector();
          VDeliveryDate   = new Vector();
          VPartyPriority  = new Vector();

            String QS = "  select regularorder.Rorderno,orderdate,to_Char(sampletoregularAuth.lastmodified,'DD.MM.YYYY hh24:mi') as Approvaldate,"+
                        "  partymaster.partyname,EndUse.Prefixname,yarncount.countname,regularorder.weight as orderedweight,"+
                        "  decode(sampletoregularAuth.realisationper,'',RegularOrder.RealizationPer,sampletoregularAuth.realisationper),"+
                        " ((regularorder.weight)/(decode(sampletoregularAuth.realisationper,'0',RegularOrder.RealizationPer,sampletoregularauth.realisationper))) * 100 as mixweight,"+
                        "  To_Char(To_Date(OrderDate,'YYYYMMDD')+5,'YYYYMMDD') as Mixingdate,To_Char(To_Date(OrderDate,'YYYYMMDD')+20,'YYYYMMDD') as DespatchDate,SampletoregularAuth.SampleOrderNo as remarks,"+
                        "  to_Char(sampletoregularauth.lastmodified,'YYYYMMDD') as ApprovaldateCal, "+
                        "  RegularOrder.DeliveryDate, "+

//                        "  PartyPriority.Name "+

                        "  deCode(RegularOrder.Shortage, 1, 'Shortage', PartyPriority.Name) as PartyPriority "+
                        "  from regularorder"+
                        "  left join sampletoregularAuth on sampletoregularAuth.regularorderno=regularorder.rorderno and SampleTORegularAuth.Status=1"+
                        "  inner join partymaster on partymaster.partycode=regularorder.partycode"+
                        "  Inner Join PartyPriority On PartyPriority.Code = nvl(PartyMaster.PriorityStatus,0) "+
                        "  inner join yarncount on yarncount.countcode=regularorder.countcode"+
                        "  inner join unit on unit.unitcode=regularorder.unitcode"+
                        "  left join enduse on enduse.endusecode=regularorder.endusecode"+
                        "  Left join RMixir on Rmixir.ORDNO=RegularOrder.ROrderNo"+
                        "  Left Join Yarnm On Yarnm.Yshcd = Rmixir.YarnShadeCode "+
                        "  inner join ordertype on ordertype.code=regularorder.ordertype "+
                        "  inner join ProcessingType on ProcessingType.ProcessCode = regularorder.ProcessTypeCode "+
                        "  Left Join FibreForm on FibreForm.FormCode = RegularOrder.FibreFormCode "+
                        "  where orderdate>=20070111 and OrderDate<="+SFromDate+" and RMixir.ORDNO is null";

                   if(!SUnit.equalsIgnoreCase("All"))
                   {
                        QS=QS+" and unit.unitname='"+SUnit+"'";
                   }
				   if(!SProcessTypeCode.equals("All"))
					{
						if(SProcessTypeCode.equals("BR-C Orders"))
						{
						QS += " and ProcessingType.OEStatus = 1 ";
						}
						else if(SProcessTypeCode.equals("Other than BR-C Orders"))
						{
						QS += " and ProcessingType.OEStatus = 0 ";
						}
						else
						{
						QS += " and RegularOrder.ProcessTypeCode = "+SProcessTypeCode;
						}
					}
				   
                   if(!SOrderType.equalsIgnoreCase("All"))
                   {
                        QS=QS+" and ordertype.name='"+SOrderType+"'";
                   }
                   if(!SCount.equalsIgnoreCase("All"))
                   {
                        QS=QS+" and yarncount.countname='"+SCount+"'";
                   }
                   if(!SOrderNo.equalsIgnoreCase("All"))
                   {
                        QS=QS+" and regularorder.rorderno='"+SOrderNo+"'";

                   }
                   if(SSelectType.equalsIgnoreCase("Depo"))
                   {
                        QS=QS+" and regularorder.rorderno like 'DM%'";
                   }
                   if(SSelectType.equalsIgnoreCase("Party"))
                   {
                        QS=QS+" and regularorder.rorderno not like 'DM%'";
                   }
                   if(!SShade.equals("All"))
                   {
                        QS = QS + " and Yarnm.Yshnm='"+SShade+"'"; 
                   }
                   if(!SParty.equals("All"))
                   {
                        QS = QS + " and PartyMaster.PartyName = '"+SParty+"'";
                   } 
                    
                       if(!SType.equalsIgnoreCase("All") && !SType.equals("Organic") && !SType.equals("BCI"))
                         {
//                              QS = QS +  " and FibreForm.FormName like '%"+SType+"%' ";

                              QS = QS +  " and FibreForm.GroupCode = "+SType;
                         }
 

//                        QS=QS+ " order by PartyPriority.Code desc,2,1 ";

                        QS=QS+ " order by 15 desc,2,1 ";

          System.out.println(QS);

            String QS1 = " select max(regularorder.Rorderno) as orderno,"+
                         " max(orderdate) as orderdate,"+
                         " count(regularorder.Rorderno) as NoofOrder "+
                         " from regularorder"+
                         " left join Rmixir on Rmixir.ordno=RegularOrder.Rorderno"+
                         " left join sampletoregularauth on sampletoregularauth.regularorderno=regularorder.rorderno and SampleToRegularAuth.Status=1"+
                         " inner join partymaster on partymaster.partycode=regularorder.partycode"+
                         " inner join yarncount on yarncount.countcode=regularorder.countcode"+
                         " inner join unit on unit.unitcode=regularorder.unitcode"+
                         "  inner join ordertype on ordertype.code=regularorder.ordertype "+
						 "  inner join ProcessingType on ProcessingType.ProcessCode = regularorder.ProcessTypeCode "+
                         " left join enduse on enduse.endusecode=regularorder.endusecode"+
                         " where orderdate>=20060801 and Rorderno not Like 'PRE%' and  OrderDate<="+SFromDate+" and regularorder.rorderno not in (select ordno from rmixir)";

                   if(!SUnit.equals("All"))
                   {
                        QS1 = QS1 + " and unit.unitname='"+SUnit+"'";
                   }
				   if(!SProcessTypeCode.equals("All"))
					{
						if(SProcessTypeCode.equals("BR-C Orders"))
						{
						QS1 += " and ProcessingType.OEStatus = 1 ";
						}
						else if(SProcessTypeCode.equals("Other than BR-C Orders"))
						{
						QS1 += " and ProcessingType.OEStatus = 0 ";
						}
						else
						{
						QS1 += " and RegularOrder.ProcessTypeCode = "+SProcessTypeCode;
						}
					}
                   if(!SCount.equalsIgnoreCase("All"))
                   {
                        QS1=QS1+" and yarncount.countname='"+SCount+"'";
                   }
                   if(!SOrderNo.equalsIgnoreCase("All"))
                   {
                        QS1=QS1+" and regularorder.rorderno='"+SOrderNo+"'";
                   }
                   if(SSelectType.equalsIgnoreCase("Depo"))
                   {
                        QS1=QS1+" and regularorder.rorderno like 'DM%'";
                   }
                   if(SSelectType.equalsIgnoreCase("Party"))
                   {
                        QS1=QS1+" and regularorder.rorderno not like 'DM%'";
                   }
                   if(!SParty.equals("All"))
                   {
                        QS1 = QS1 + " and PartyMaster.PartyName = '"+SParty+"'";
                   } 

                   QS1 = QS1 + " group by Rmixir.Mixno order by 2,1";

           String QS2 = "  select sum(orderedweight) from ( "+
                        "  select regularorder.Rorderno,orderdate,to_Char(sampletoregularAuth.lastmodified,'DD.MM.YYYY hh24:mi') as Approvaldate,"+
                        "  partymaster.partyname,EndUse.Prefixname,yarncount.countname,regularorder.weight as orderedweight,"+
                        "  decode(sampletoregularAuth.realisationper,'',RegularOrder.RealizationPer,sampletoregularAuth.realisationper),"+
                        " ((regularorder.weight)/(decode(sampletoregularAuth.realisationper,'0',RegularOrder.RealizationPer,sampletoregularauth.realisationper))) * 100 as mixweight,"+
                        "  To_Char(To_Date(OrderDate,'YYYYMMDD')+5,'YYYYMMDD') as Mixingdate,To_Char(To_Date(OrderDate,'YYYYMMDD')+20,'YYYYMMDD') as DespatchDate,SampletoregularAuth.SampleOrderNo as remarks,"+
                        "  to_Char(sampletoregularauth.lastmodified,'YYYYMMDD') as ApprovaldateCal, "+
                        "  RegularOrder.DeliveryDate "+
                        "  from regularorder"+
                        "  left join sampletoregularAuth on sampletoregularAuth.regularorderno=regularorder.rorderno and SampleToRegularAuth.Status=1"+
                        "  inner join partymaster on partymaster.partycode=regularorder.partycode"+
                        "  inner join yarncount on yarncount.countcode=regularorder.countcode"+
                        "  inner join unit on unit.unitcode=regularorder.unitcode"+
                        "  left join enduse on enduse.endusecode=regularorder.endusecode"+
                        "  Left join RMixir on Rmixir.ORDNO=RegularOrder.ROrderNo"+
                        "  inner join ordertype on ordertype.code=regularorder.ordertype "+
						"  inner join ProcessingType on ProcessingType.ProcessCode = regularorder.ProcessTypeCode "+
                        "  Left Join Yarnm On Yarnm.Yshcd = Rmixir.YarnShadeCode "+ 
                        "  where orderdate>=20070111 and OrderDate<="+SFromDate+" and RMixir.ORDNO is null and to_Char(sampletoregularAuth.lastmodified,'DD.MM.YYYY hh24:mi') is null   ";

                   if(!SUnit.equalsIgnoreCase("All"))
                   {
                        QS2=QS2+" and unit.unitname='"+SUnit+"'";
                   }
				   if(!SProcessTypeCode.equals("All"))
					{
						if(SProcessTypeCode.equals("BR-C Orders"))
						{
						QS2 += " and ProcessingType.OEStatus = 1 ";
						}
						else if(SProcessTypeCode.equals("Other than BR-C Orders"))
						{
						QS2 += " and ProcessingType.OEStatus = 0 ";
						}
						else
						{
						QS2 += " and RegularOrder.ProcessTypeCode = "+SProcessTypeCode;
						}
					}
                   if(SSelectType.equalsIgnoreCase("Depo"))
                   {
                        QS2=QS2+" and regularorder.rorderno like 'DM%'";
                   }
                   if(!SOrderType.equalsIgnoreCase("All"))
                   {
                        QS2=QS2+" and ordertype.name='"+SOrderType+"'";
                   }
                   if(SSelectType.equalsIgnoreCase("Party"))
                   {
                        QS2=QS2+" and regularorder.rorderno not like 'DM%'  ";
                   }
                   if(!SParty.equals("All"))
                   {
                        QS2 = QS2 + " and PartyMaster.PartyName = '"+SParty+"'";
                   } 

                   QS2= QS2+" ) ";

           String QS3 = "  select sum(orderedweight) from ( "+
                        "  select regularorder.Rorderno,orderdate,to_Char(sampletoregularAuth.lastmodified,'DD.MM.YYYY hh24:mi') as Approvaldate,"+
                        "  partymaster.partyname,EndUse.Prefixname,yarncount.countname,regularorder.weight as orderedweight,"+
                        "  decode(sampletoregularAuth.realisationper,'',RegularOrder.RealizationPer,sampletoregularAuth.realisationper),"+
                        " ((regularorder.weight)/(decode(sampletoregularAuth.realisationper,'0',RegularOrder.RealizationPer,sampletoregularauth.realisationper))) * 100 as mixweight,"+
                        "  To_Char(To_Date(OrderDate,'YYYYMMDD')+5,'YYYYMMDD') as Mixingdate,To_Char(To_Date(OrderDate,'YYYYMMDD')+20,'YYYYMMDD') as DespatchDate,SampletoregularAuth.SampleOrderNo as remarks,"+
                        "  to_Char(sampletoregularauth.lastmodified,'YYYYMMDD') as ApprovaldateCal, "+
                        "  RegularOrder.DeliveryDate "+
                        "  from regularorder"+
                        "  left join sampletoregularAuth on sampletoregularAuth.regularorderno=regularorder.rorderno and SampleToRegularAuth.Status=1"+
                        "  inner join partymaster on partymaster.partycode=regularorder.partycode"+
                        "  inner join yarncount on yarncount.countcode=regularorder.countcode"+
                        "  inner join unit on unit.unitcode=regularorder.unitcode"+
                        "  left join enduse on enduse.endusecode=regularorder.endusecode"+
                        "  inner join ordertype on ordertype.code=regularorder.ordertype "+
						"  inner join ProcessingType on ProcessingType.ProcessCode = regularorder.ProcessTypeCode "+
                        "  Left join RMixir on Rmixir.ORDNO=RegularOrder.ROrderNo"+
                        "  Left Join Yarnm On Yarnm.Yshcd = Rmixir.YarnShadeCode "+ 
                        "  where orderdate>=20070111 and OrderDate<="+SFromDate+" and RMixir.ORDNO is null and to_Char(sampletoregularAuth.lastmodified,'DD.MM.YYYY hh24:mi') is not null  ";

                   if(!SUnit.equalsIgnoreCase("All"))
                   {
                        QS3=QS3+" and unit.unitname='"+SUnit+"'";
                   }
				   if(!SProcessTypeCode.equals("All"))
					{
						if(SProcessTypeCode.equals("BR-C Orders"))
						{
						QS3 += " and ProcessingType.OEStatus = 1 ";
						}
						else if(SProcessTypeCode.equals("Other than BR-C Orders"))
						{
						QS3 += " and ProcessingType.OEStatus = 0 ";
						}
						else
						{
						QS3 += " and RegularOrder.ProcessTypeCode = "+SProcessTypeCode;
						}
					}
                   if(!SOrderType.equalsIgnoreCase("All"))
                   {
                        QS3=QS3+" and ordertype.name='"+SOrderType+"'";
                   }
                   if(SSelectType.equalsIgnoreCase("Depo"))
                   {
                        QS3=QS3+" and regularorder.rorderno like 'DM%'";
                   }
                   if(SSelectType.equalsIgnoreCase("Party"))
                   {
                        QS3=QS3+" and regularorder.rorderno not like 'DM%'  ";
                   }
                   if(!SParty.equals("All"))
                   {
                        QS3 = QS3 + " and PartyMaster.PartyName = '"+SParty+"'";
                   } 

                   QS3= QS3+" ) ";


          try
          {
               FileWriter fw = new FileWriter("d:\\me.sql");
               fw.write(QS);
               fw.close();

               JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
               Connection     conn = jdbc.getConnection();

               Statement stat      = conn.createStatement();
               ResultSet res       = stat.executeQuery(QS);
               while(res.next())
               {
                    VOrderNo       .addElement(common.parseNull(res.getString(1)));
                    VOrderDate     .addElement(common.parseNull(res.getString(2)));
                    VApprovalDate  .addElement(common.parseNull(res.getString(3)));
                    VBuyer         .addElement(common.parseNull(res.getString(4)));
                    VEndUse        .addElement(common.parseNull(res.getString(5)));
                    VCount         .addElement(common.parseNull(res.getString(6)));
                    VOrderWeight   .addElement(common.parseNull(res.getString(7)));
                    VRealizationPer.addElement(common.parseNull(res.getString(8)));
                    VMixWeight     .addElement(common.parseNull(res.getString(9)));
                    VMixDate       .addElement(common.parseNull(res.getString(10)));
                    VDespDate      .addElement(common.parseNull(res.getString(11)));
                    VRemarks       .addElement(common.parseNull(res.getString(12)));
                    VApprovalCall  .addElement(common.parseNull(res.getString(13)));
                    VDeliveryDate  .addElement(common.parseNull(common.parseDate(res.getString(14))));
                    VPartyPriority .addElement(common.parseNull(res.getString(15)));
               }
               res.close();

               res       = stat.executeQuery(QS1);
               while(res.next())
               {
                    SLastOrderNo    = (String)res.getString(1);
                    SLastOrderDate  = common.parseDate((String)res.getString(2));
                    SCountOfOrderNo = (String)res.getString(3);
               }
               res.close();

               res       = stat.executeQuery(QS2);
               while(res.next())
               {
                    SNotAppWeight   = (String)res.getString(1);
               }
               res.close();

               res       = stat.executeQuery(QS3);
               while(res.next())
               {
                    SAppWeight    = (String)res.getString(1);
               }
               res.close();

               res       = stat.executeQuery(" select max(mixno) from Rmixir ");
               while(res.next())
               {
                    SLastMixNo     = res.getString(1);
               }
               res.close();

               res       = stat.executeQuery(" select max(Orderno) from DyeingOrder where ORderType=0 ");
               while(res.next())
               {
                    SLastDyeingOrderNo   = res.getString(1);
               }
               res.close();

               res       = stat.executeQuery(" select max(GiNo) from Agn ");
               while(res.next())
               {
                    SLastAgnNo   = res.getString(1);
               }
               res.close();

               stat.close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

      private void printHead()
      {
          try
          {

               if(iLCtr<56)
                  return;

               if(iPageCtr>0)
               {
                    FW.write(sl+"\n"+"");
               }
               iPageCtr++;
              
               String SPitch="P\n";
               String Amar= "Company  :AMARJOTHI SPINNING MILLS Ltd. \n" ;
               String Doc = "Document :Regular Order Mixing Pending Report-As onDate  " ;
                    
               FW.write(SPitch);
               FW.write("Company  :AMARJOTHI SPINNING MILLS Ltd. \n\n");
               FW.write("Document :Regular Order Mixing Pending Report-As onDate  "+common.parseDate(SFromDate)+"\n");
               FW.write("Unit : "+SUnit+", Process Type : "+SProcessTypeName+"\n");

               FW.write("Page : "+iPageCtr+"\n");

               
               String s1  = "|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n";
               String s2  = "|    |          |          |                |                           |     |       |              |           |            |       |              |          |          |  Delayed Days   |            |               |\n";
               String s3  = "|    |          |          |                |                           |     |       |              | Waste To  | Weight     |Up To  | Balance To   |Due Date  |Due Date  |                 |Mixing Delay|               |\n";
               String s4  = "|S.No| Order No | Order    |  Approval Date |    Name of the Buyer      | End | Count | Order Weight |  Allow    | As Per     |DateMix|    Mix       |   For    |   For    |-----------------|   Days     |   Remarks     |\n";
               String s5  = "|    |          | Date     |                |                           | Use |       |              |           | Table Kgs  |       |              | Mixing   |Despatch  |OrdDt|DueDt|DueDt|            |               |\n";
               String s6  = "|    |          |          |                |                           |KWSR |       |              |    (%)    |            |       |              |          |          |OfMix|OfMix|OfDes|            |               |\n";
               String s7  = "|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n";

               FW.write(s1);
               FW.write(s2);
               FW.write(s3);
               FW.write(s4);
               FW.write(s5);
               FW.write(s6);
               FW.write(s7);

               iLCtr=10;

          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
       }
       private void printFile()
       {
           try
           {

                 FW = new FileWriter("/software/MixPrint/RegularMixingPendingNew.prn");
                 //FW = new FileWriter("d://RegularMixingPendingNew.prn");


               printHead();
               printBody();
               printFoot();

               int i=0;

               for(int j=0;j<VApprovalDate.size();j++)
               {
                    if(VApprovalDate.elementAt(j).equals(""))
                    {
                         i++;
                    }
               }
               System.out.println("count of i-"+i);
               System.out.println(SLastOrderNo);
               FW.write(" Last Entry   : Order No: "+SLastEnOrd+" / "+common.parseDate(SLastDate)+"        Dyeing Order No :"+SLastDyeingOrderNo+"          G.I.No:"+SLastAgnNo+"         Mixing No :"+SLastMixNo+"            Delivery Note:-              Approval Pendings:"+i+" "+"\n");
               FW.write(" EApproved Kgs : "+SAppWeight+"      Waiting For Approval Kgs  :"+SNotAppWeight+"F "+"\n");

               FW.close();
          }
          catch(Exception  e)
          {
               System.out.println(e);
          }

     }

     private void printBody()
     {


                sl   = "--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|";


                totl = "|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n";

                line = "--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n";

          int sno = 1;     

          try
          {
               String SPriorityStatus="";
               int slno  = 1;

               for(int i=0;i<VOrderNo.size();i++)
               {
               slno++;

                   if(!common.parseNull((String)VPartyPriority.elementAt(i)).equals(SPriorityStatus))
                   {
                     if(i!=0)
                     {

                    sbGT  =    "|"+common.Pad(Empty,4)+
                         "|"+ common.Pad("Total",10)+
                         "|"+ common.Space(10)+
                         "|"+ common.Space(16)+
                         "|"+ common.Space(27)+
                         "|"+ common.Space(5)+
                         "|"+ common.Space(7)+
                         "|"+common.Rad(common.getRound(TotOrderWeight,2),14)+ 
                         "|"+ common.Space(11)+
                         "|"+common.Rad(common.getRound(TotMixWeight,2),12)+ 
                         "|"+ common.Space(7)+
                         "|"+common.Rad(common.getRound(TotOrderWeight,2),14)+ 
                         "|"+ common.Space(10)+
                         "|"+ common.Rad(Empty,10)+
                         "|"+ common.Space(5)+
                         "|"+ common.Space(5)+
                         "|"+ common.Space(5)+
                         "|"+ common.Space(12)+
                         "|"+ common.Space(15)+"|"+"";

                         FW.write(totl);
                         FW.write(sbGT+"\n");

                    TotOrderWeight=0;
                    TotMixWeight =0;
                    iSlNo=1;
                     TLine =TLine+10;

                     iLCtr=iLCtr+9;



                     }

                     String SPartyStatus= "|"+common.Space(4)+
                              "|"+common.Space(10)+
                              "|"+common.Space(10)+
                              "|"+common.Space(16)+
                              "|E"+common.Cad(common.parseNull((String)VPartyPriority.elementAt(i)),27)+
                            "F|"+common.Space(5)+
                              "|"+common.Space(7)+
                              "|"+common.Space(14)+
                              "|"+common.Space(11)+
                              "|"+common.Space(12)+
                              "|"+common.Space(7)+   
                              "|"+common.Space(14)+
                              "|"+common.Space(10)+
                              "|"+common.Space(10)+
                              "|"+common.Space(5)+
                              "|"+common.Space(5)+
                              "|"+common.Space(5)+
                              "|"+common.Space(12)+
                              "|"+common.Space(15)+"|"+"\n";


                    String S1 = "|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n";


                    FW.write(S1);
                    FW.write(SPartyStatus);
                    FW.write(S1);

                   }

                        int CurrentDate= common.getIntCurrentDate();
                        int OrderDate  =common.toInt((String)VOrderDate.elementAt(i));
                        int ApprovalDate=common.toInt((String)VApprovalCall.elementAt(i));
                        int MixDate    =common.toInt((String)VMixDate.elementAt(i));
                        int DespDate    =common.toInt((String)VDespDate.elementAt(i));

                        int OrderDateOfMix=common.getDateDiff(CurrentDate,OrderDate);
                        //int DueDateOfMix=common.getDateDiff(CurrentDate,MixDate);
                        //int DueDateOfDesp=common.getDateDiff(CurrentDate,DespDate);
                        int DueDateMixingPen=common.getDateDiff(CurrentDate,ApprovalDate);

                        String DueDateMixing = common.pureDate((String)VDeliveryDate.elementAt(i));

                        for(int j=0;j<15;j++)
                        {
                             DueDateMixing   = common.getPrevDate(DueDateMixing);
                        }
                        int DueDateOfMix=common.getDateDiff(iFromDate,common.toInt(DueDateMixing));
                        int DueDateOfDesp=common.getDateDiff(iFromDate,common.toInt(common.pureDate((String)VDeliveryDate.elementAt(i))));

              if(DueDateMixingPen>=iDays)
              {



                      TotOrderWeight=TotOrderWeight+common.toDouble((String)VOrderWeight.elementAt(i));
                      TotMixWeight  =TotMixWeight+common.toDouble((String)VMixWeight.elementAt(i));

                      GrandTot=GrandTot+common.toDouble((String)VOrderWeight.elementAt(i));
                      GrandTotal  =GrandTotal+common.toDouble((String)VMixWeight.elementAt(i));



                        List1=String.valueOf(iSlNo);
                        List2=(String)VOrderNo.elementAt(i);
                        List3=common.parseDate((String)VOrderDate.elementAt(i));
                        List4=common.parseDate((String)VApprovalDate.elementAt(i));
                        List5=(String)VBuyer.elementAt(i);
                        List6=(String)VEndUse.elementAt(i);
                        List7=(String)VCount.elementAt(i);
                        List8=common.getRound(common.toDouble((String)VOrderWeight.elementAt(i)),2);
                        List9=(String)VRealizationPer.elementAt(i);
                        List10=common.getRound(common.toDouble((String)VMixWeight.elementAt(i)),2);
                        List11="0";
                        List12=common.getRound(common.toDouble((String)VOrderWeight.elementAt(i)),2);
                        List13=common.parseDate(DueDateMixing);
                        //List13=common.parseDate((String)VMixDate.elementAt(i));
                        //List14=common.parseDate((String)VDespDate.elementAt(i));
                        List14=(String)VDeliveryDate.elementAt(i);
                        List15=Integer.toString(OrderDateOfMix);
                        List16=Integer.toString(DueDateOfMix);
                        List17=Integer.toString(DueDateOfDesp);
                        List18=Integer.toString(DueDateMixingPen);
                        List19=common.parseNull((String)VRemarks.elementAt(i));


                      
     
     
     
                      sbV =   "|"+common.Cad(List1,4)+
                              "|"+common.Pad(List2,10)+
                              "|"+common.Pad(List3,10)+
                              "|"+common.Pad(List4,16)+
                              "|"+common.Pad(List5,27)+
                              "|"+common.Pad(List6,5)+
                              "|"+common.Pad(List7,7)+
                              "|"+common.Rad(List8,14)+
                              "|"+common.Rad(List9,11)+
                              "|"+common.Rad(List10,12)+
                              "|"+common.Rad(List11,7)+   //yarn/cloth
                              "|"+common.Rad(List12,14)+
                              "|"+common.Pad(List13,10)+
                              "|"+common.Pad(List14,10)+
                              "|"+common.Rad(List15,5)+
                              "|"+common.Rad(List16,5)+
                              "|"+common.Rad(List17,5)+
                              "|"+common.Rad(List18,12)+
                              "|"+common.Pad(List19,15)+"|"+"\n";

     
                     String SLineSpace= "|"+common.Space(4)+
                              "|"+common.Space(10)+
                              "|"+common.Space(10)+
                              "|"+common.Space(16)+
                              "|"+common.Space(27)+
                              "|"+common.Space(5)+
                              "|"+common.Space(7)+
                              "|"+common.Space(14)+
                              "|"+common.Space(11)+
                              "|"+common.Space(12)+
                              "|"+common.Space(7)+   //yarn/cloth
                              "|"+common.Space(14)+
                              "|"+common.Space(10)+
                              "|"+common.Space(10)+
                              "|"+common.Space(5)+
                              "|"+common.Space(5)+
                              "|"+common.Space(5)+
                              "|"+common.Space(12)+
                              "|"+common.Space(15)+"|"+"\n";

                         FW.write(sbV);
                         FW.write(SLineSpace);
                         iSlNo++;
                         iLCtr=iLCtr+2;
                         printHead();
                         SPriorityStatus = common.parseNull((String)VPartyPriority.elementAt(i));

                     }

                    }
            } 
            catch(Exception e)
            {
                 e.printStackTrace();
            }

     }

     private void printFoot()
     {

          try
          {

               String gtot="Total";
     
               sbGT  =    "|"+common.Pad(Empty,4)+
                    "|"+ common.Pad(gtot,10)+
                    "|"+ common.Space(10)+
                    "|"+ common.Space(16)+
                    "|"+ common.Space(27)+
                    "|"+ common.Space(5)+
                    "|"+ common.Space(7)+
                    "|"+common.Rad(common.getRound(TotOrderWeight,2),14)+ 
                    "|"+ common.Space(11)+
                    "|"+common.Rad(common.getRound(TotMixWeight,2),12)+ 
                    "|"+ common.Space(7)+
                    "|"+common.Rad(common.getRound(TotOrderWeight,2),14)+ 
                    "|"+ common.Space(10)+
                    "|"+ common.Rad(Empty,10)+
                    "|"+ common.Space(5)+
                    "|"+ common.Space(5)+
                    "|"+ common.Space(5)+
                    "|"+ common.Space(12)+
                    "|"+ common.Space(15)+"|"+"\n";



               String gtot1="GrandTotal";
     
               sbGT1  =    "|"+common.Pad(Empty,4)+
                    "|"+ common.Pad(gtot1,10)+
                    "|"+ common.Space(10)+
                    "|"+ common.Space(16)+
                    "|"+ common.Space(27)+
                    "|"+ common.Space(5)+
                    "|"+ common.Space(7)+
                    "|"+common.Rad(common.getRound(GrandTot,2),14)+ 
                    "|"+ common.Space(11)+
                    "|"+common.Rad(common.getRound(GrandTotal,2),12)+ 
                    "|"+ common.Space(7)+
                    "|"+common.Rad(common.getRound(GrandTot,2),14)+ 
                    "|"+ common.Space(10)+
                    "|"+ common.Rad(Empty,10)+
                    "|"+ common.Space(5)+
                    "|"+ common.Space(5)+
                    "|"+ common.Space(5)+
                    "|"+ common.Space(12)+
                    "|"+ common.Space(15)+"|"+"\n";


                    FW.write(totl);
                    FW.write(sbGT);
                    FW.write(totl);


                    FW.write(sbGT1);
                    FW.write(line);


                    TotOrderWeight=0;
                    TotMixWeight =0;

                    GrandTot=0;
                    GrandTotal=0;
                    iSlNo=1;
          
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }

      }

     private void set80Pitch(int iPageLength)
     {
          if(iPageLength<=86)
               SPitch="P\n";
          if(iPageLength>86 && iPageLength<=107)
               SPitch="M\n";
          if(iPageLength>107 && iPageLength<=136)
               SPitch="g\n";
          if(iPageLength>136 && iPageLength<=153)
               SPitch="P\n";
          if(iPageLength>153 && iPageLength<=173)
               SPitch="M\n";
          if(iPageLength>173 && iPageLength<=192)
               SPitch="g\n";
     }
     private void set130Pitch(int iPageLength)
     {
          if(iPageLength<=134)
               SPitch="P\n";
          if(iPageLength>134 && iPageLength<=163)
               SPitch="M\n";
          if(iPageLength>163 && iPageLength<=204)
               SPitch="g\n";
          if(iPageLength>204 && iPageLength<=217)
               SPitch="P\n";
          if(iPageLength>217 && iPageLength<=261)
               SPitch="M\n";
          if(iPageLength>261)
               SPitch="g\n";
    }  

}
