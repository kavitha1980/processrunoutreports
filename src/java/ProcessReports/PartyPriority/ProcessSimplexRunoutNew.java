/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProcessReports.PartyPriority;

import ProcessReports.jdbc.*;
import ProcessReports.util.*;

import java.io.*;
import java.sql.*;           
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

/**
 *
 * @author Administrator
 */
public class ProcessSimplexRunoutNew extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    Common              common;
    HttpSession         session;

    Connection          theProcessConnection  = null;    
    Connection          theProcessaConnection = null;    

    String              SStDate,SEnDate;
    String              bgColor,bgHead,bgUom,bgBody,fgHead,fgUom,fgBody;
    String              SUnit    = "";
    String              SParty   = "";

    String              SProcessTypeCode, SProcessTypeName;
    String              SSelectType="";

    String              SOrderNo="";
    String              SOrderNo1="";

    String              SCount="",SNote="",SNote1="", SNote2="";
    Vector              VARNo;
    FileWriter          FW;
    int                 TLine   = 0;
    int                 iPage   = 1;

    int                 iStDate = 0;
    int                 iEnDate = 0;
    int                 iSlNo   = 0;

    double              dTOrdWt = 0;
    double              dPTOrdWt= 0;
    String              stm     = "";
    int                 SNo     = 0;

    double              dGrandOrdWt=0;

    Vector              VOrderNo, VOrderdate, VOrderdateDays, VCountName, VShade, VOrdWeight, VDepth, VBlend, VProcessType, VBrSetting, VCardSetting,
                        VDrdarkrsb,VDrdarkdo2s, VDrdarkdo6s, VDrdarkd11s,VDrwhitersb,VDrwhitedo2s, VDrwhitedo6s, 
                        VDrwhited11s, VSimplexTpi, VClothcheck,VMixingdate,VOrderStatus, VPartyPriority,VCardSettingShort, 
                        VClothCheckStatus, VSimplexRunningMachine, VDrawingRunningMachine;
	
    Vector              VMixNo;  //Aunit
    Vector              VCdFlatSpeed, VCdDoffSpeed;  //Aunit - Carding Setting
    String              End="";
    String              End1="";
    String              Str7="";
	
    java.util.List[]    theAbstractList = null;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            valueInt();
            common = new Common();
        
            bgColor = common.bgColor;
            bgHead  = common.bgHead;
            bgUom   = common.bgUom;
            bgBody  = common.bgBody;
            fgHead  = common.fgHead;
            fgUom   = common.fgUom;
            fgBody  = common.fgBody;

                        response. setContentType("text/html;charset=UTF-8");
            PrintWriter out     = response.getWriter();
            iEnDate        = common.toInt(common.pureDate((String)request.getParameter("TEnDate")));
            SUnit    	   = request.getParameter("Unit");
            SParty   	   = request.getParameter("PartyName");
            SOrderNo1 	   = request.getParameter("ROrderNo");
            SCount    	   = request.getParameter("Count");

            SSelectType     = common.parseNull((String)request.getParameter("Select"));

            StringTokenizer ST  = new StringTokenizer(request.getParameter("processType"), "#");
            while(ST.hasMoreTokens())   {
                SProcessTypeCode    = ST.nextToken();
                SProcessTypeName    = ST.nextToken();
            }

            valueInt();
        
	    if(SUnit.trim().equals("A_Unit")){
                SetAUnit_Data();
                setAUnit_Html(out);
	    }
	    
	    if(SUnit.trim().equals("B_Unit")){
                stm = SetBUnit_Data();
		//System.out.println(stm);
                setBUnit_Html(out);
	    }
	    
	    if(SUnit.trim().equals("C_Unit") || SUnit.trim().equals("A_Unit_A")){
                SetCUnit_Data();
                setCUnit_Html(out);
	    }
	    
	    FileWriter();
            out.close();
        } catch(Exception ex){
            ex.printStackTrace();
        }finally {
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    private void valueInt() {
        
        TLine           = 0;
        iPage           = 1;
        dTOrdWt         = 0;
        dPTOrdWt        = 0;
        dGrandOrdWt     = 0;
        SNo             =  0;
        theAbstractList = new java.util.ArrayList[5];
    }

    private void setAUnit_Html(PrintWriter out) throws  ServletException
    {
        try{
            SStDate            = common.parseDate(String.valueOf(iStDate));
            SEnDate            = common.parseDate(String.valueOf(iEnDate));
        
            out.println("<html>");
            out.println("<head>");
            out.println("<title>InvPro</title>");
            out.println("</head>");
            out.println("<body bgcolor='"+bgColor+"' text = '#0000FF'>");
            out.println("<p align='center'><font size='5'><b><u>"+SUnit+" - SimplexRunout</u>  ");
            out.println(" As On Date: "+SEnDate+"</b></font></p>");
            out.println("<div align='center'>");

            out.println("<p></p>");

            out.println("  <table border='0' bordercolor='"+bgColor+"'>");
            out.println("    <tr>");
            out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>SL.No</b></font></td>");
            out.println("      <td  rowspan='1'align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>OrderNo</b></font></td>");
            out.println("      <td  rowspan='1'align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>OrderDate</b></font></td>");
            out.println("      <td  rowspan='1'align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>OrderDate Days</b></font></td>");
            out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Count Name</b></font></td>");
            out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Shade</b></font></td>");
            out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Depth</b></font></td>");
            out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Order Weight</b></font></td>");
            out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Process Type</b></font></td>");
            out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Blend</b></font></td>");

            out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Mix No</b></font></td>");
            out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Blowroom Setting</b></font></td>");
            out.println("      <td  colspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Carding Setting</b></font></td>");

            out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Simplex Tpi</b></font></td>");
            out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Cloth Check</b></font></td>");
            out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Process Days</b></font></td>");
            out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Remarks</b></font></td>");
            out.println("    </tr>");

            out.println("    <tr>");
            out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>&nbsp</font></td>");
            out.println("      <td   align='Right' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>&nbsp</font></td>");

            out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Mach Line</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Flat Speed</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Doffer Speed</font></td>");

            out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>&nbsp;</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>&nbsp</font></td>");
            out.println("    </tr>");

            String SPriorityStatus="";
            int iSlNo = 1;
            java.util.List theOrderList = new java.util.ArrayList();
        
            for(int i=0;i<VOrderNo.size();i++)  {
                if(!common.parseNull((String)VPartyPriority.elementAt(i)).equals(SPriorityStatus))  {
                    if(i!=0)    {
                        out.println("    <tr>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("      <td   align='Right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>"+common.getRound(dTOrdWt,3)+"</b></font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("    </tr>");
                    
                        dTOrdWt  = 0;
                        iSlNo    = 1;
                    }
                    out.println("    <tr>");
                    out.println("      <td colspan=18 width='30' bgcolor='"+bgBody+"' align='center'><font color='"+fgBody+"'>"+((String)VPartyPriority.elementAt(i))+"</font></td>");
                    out.println("    </tr>");
                }
                double dOrdWt = common.toDouble((String)VOrdWeight.elementAt(i));
                if(!theOrderList.contains(common.parseNull((String)VOrderNo.elementAt(i)))) {
                    dTOrdWt	    = dTOrdWt+dOrdWt;
                    dGrandOrdWt     = dGrandOrdWt+dOrdWt;
                    theOrderList    . add(common.parseNull((String)VOrderNo.elementAt(i)));
                }
                int iMixDate    = common.toInt((String)VMixingdate.elementAt(i));
                int iRepDate    = common.toInt(common.getServerDate());
                int iProDate    = common.getDateDiff(iRepDate,iMixDate);

                out.println("    <tr>");
                out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>"+String.valueOf(iSlNo)+"</font></td>");
                out.println("      <td width='40' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VOrderNo.elementAt(i)+"</font></td>");
                out.println("      <td width='180' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+common.parseDate((String)VOrderdate.elementAt(i))+"</font></td>");
                out.println("      <td width='180' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+common.parseNull((String)VOrderdateDays.elementAt(i))+"</font></td>");
                out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VCountName.elementAt(i)+"</font></td>");
                out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VShade.elementAt(i)+"</font></td>");
                out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>"+(String)VDepth.elementAt(i)+"</font></td>");
                out.println("      <td width='40' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>"+common.getRound(dOrdWt,3)+"</font></td>");
                out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VProcessType.elementAt(i)+"</font></td>");
                out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VBlend.elementAt(i)+"</font></td>");

                out.println("      <td width='40' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VMixNo.elementAt(i)+"</font></td>");
                out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VBrSetting.elementAt(i)+"</font></td>");
                out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VCdFlatSpeed.elementAt(i)+"</font></td>");
                out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VCdDoffSpeed.elementAt(i)+"</font></td>");

                out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VSimplexTpi.elementAt(i)+"</font></td>");
                out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VClothcheck.elementAt(i)+"</font></td>");
                out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+String.valueOf(iProDate)+"</font></td>");
                out.println("      <td width='30' bgcolor='"+bgBody+"' align='center'><font color='"+fgBody+"'>"+VOrderStatus.elementAt(i)+"</td>");
                SPriorityStatus = common.parseNull((String)VPartyPriority.elementAt(i));
                out.println("    </tr>");
                iSlNo++;
            }
            out.println("    <tr>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='Right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>"+common.getRound(dTOrdWt,3)+"</b></font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("    </tr>");

            out.println("    <tr>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>GRAND TOTAL</font></td>");
            out.println("      <td   align='Right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>"+common.getRound(dGrandOrdWt,3)+"</b></font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("    </tr>");
            dTOrdWt         = 0;
            dGrandOrdWt     = 0;
            out.println("  </table>");
            out.println("  </center>");
            out.println("</div>");
            out.println("");
            out.println("</body>");
            out.println("");
            out.println("</html>");
            out.println("");
            out.close();
        }catch(Exception ex){
	    ex.printStackTrace();
        }
    }

    private void setBUnit_Html(PrintWriter out) throws  ServletException    {

        try{
            SStDate            = common.parseDate(String.valueOf(iStDate));
            SEnDate            = common.parseDate(String.valueOf(iEnDate));
        
            out.println("<html>");
            out.println("<head>");
            out.println("<title>InvPro</title>");
            out.println("</head>");
            out.println("<body bgcolor='"+bgColor+"' text = '#0000FF'>");
            out.println("<p align='center'><font size='5'><b><u>SimplexRunout</u>  ");
            out.println(" As On Date: "+SEnDate+"</b></font></p>");
            out.println("<div align='center'>");
        
            out.println("<p></p>");
        
            out.println("  <table border='0' bordercolor='"+bgColor+"'>");
            out.println("    <tr>");
            out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>SL.No</b></font></td>");
            out.println("      <td  rowspan='1'align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>OrderNo</b></font></td>");
            out.println("      <td  rowspan='1'align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>OrderDate</b></font></td>");
            out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Count Name</b></font></td>");
            out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Shade</b></font></td>");
            out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Depth</b></font></td>");
            out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Order Weight</b></font></td>");
            out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Process Type</b></font></td>");
            out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Mix No</b></font></td>");
            out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Blowroom Setting</b></font></td>");
            out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Carding Setting</b></font></td>");
            out.println("      <td  colspan='4' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Drawing Setting</b></font></td>");
            out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Simplex Tpi</b></font></td>");
            out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Cloth Check</b></font></td>");
            out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Cloth Check Status</b></font></td>");
            out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Process Days</b></font></td>");
            out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Drawing Run Machine</b></font></td>");
            out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Simplex Run Machine</b></font></td>");
            out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Remarks</b></font></td>");
            out.println("    </tr>");
        
            String SPriorityStatus="";
            int iSlNo = 1;

            for(int i=0;i<VOrderNo.size();i++)  {
                if(!common.parseNull((String)VPartyPriority.elementAt(i)).equals(SPriorityStatus))  {
                    if(i!=0)    {
                        out.println("    <tr>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("      <td   align='Right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>"+common.getRound(dTOrdWt,3)+"</b></font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("    </tr>");
                        dTOrdWt  = 0;
                        iSlNo    = 1;
                    }
                    out.println("    <tr>");
                    out.println("      <td colspan=22 width='30' bgcolor='"+bgBody+"' align='center'><font color='"+fgBody+"'>"+((String)VPartyPriority.elementAt(i))+"</font></td>");
                    out.println("    </tr>");
                }
                double  dOrdWt      = common.toDouble((String)VOrdWeight.elementAt(i));
                        dTOrdWt     = dTOrdWt+dOrdWt;
                        dGrandOrdWt = dGrandOrdWt+dOrdWt;
		int     iMixDate    = common.toInt((String)VMixingdate.elementAt(i));
                int     iRepDate    = common.toInt(common.getServerDate());
                int     iProDate    = common.getDateDiff(iRepDate,iMixDate);
                out.println("    <tr>");
                out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>"+String.valueOf(iSlNo)+"</font></td>");
                out.println("      <td width='40' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VOrderNo.elementAt(i)+"</font></td>");
                out.println("      <td width='180' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+common.parseDate((String)VOrderdate.elementAt(i))+"</font></td>");
                out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VCountName.elementAt(i)+"</font></td>");
                out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VShade.elementAt(i)+"</font></td>");
                out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>"+(String)VDepth.elementAt(i)+"</font></td>");
                out.println("      <td width='40' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>"+common.getRound(dOrdWt,3)+"</font></td>");
                out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VProcessType.elementAt(i)+"</font></td>");
                out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VMixNo.elementAt(i)+"</font></td>");
                out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VBrSetting.elementAt(i)+"</font></td>");
                out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VCardSetting.elementAt(i)+"</font></td>");
                out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VDrdarkrsb.elementAt(i)+"</font></td>");
                out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VDrdarkdo2s.elementAt(i)+"</font></td>");

                out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VDrwhitersb.elementAt(i)+"</font></td>");
                out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VDrwhitedo2s.elementAt(i)+"</font></td>");
                out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VSimplexTpi.elementAt(i)+"</font></td>");
                out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VClothcheck.elementAt(i)+"</font></td>");
                out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VClothCheckStatus.elementAt(i)+"</font></td>");
                out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+String.valueOf(iProDate)+"</font></td>");
                out.println("      <td width='30' bgcolor='"+bgBody+"' align='center'><font color='"+fgBody+"'>"+(String)VDrawingRunningMachine.elementAt(i)+"</td>");
                out.println("      <td width='30' bgcolor='"+bgBody+"' align='center'><font color='"+fgBody+"'>"+(String)VSimplexRunningMachine.elementAt(i)+"</td>");
                out.println("      <td width='30' bgcolor='"+bgBody+"' align='center'><font color='"+fgBody+"'>"+VOrderStatus.elementAt(i)+"</td>");

                SPriorityStatus = common.parseNull((String)VPartyPriority.elementAt(i));
                out.println("    </tr>");
                iSlNo++;
            }
        
            out.println("    <tr>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='Right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>"+common.getRound(dTOrdWt,3)+"</b></font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("    </tr>");
        
            out.println("    <tr>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>GRAND TOTAL</font></td>");
            out.println("      <td   align='Right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>"+common.getRound(dGrandOrdWt,3)+"</b></font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("    </tr>");

            dTOrdWt     = 0;
            dGrandOrdWt = 0;
            out.println("  </table>");
            out.println("  </center>");
            out.println("</div>");
		//out.println("stm==>"+stm);
            out.println("");
            out.println("</body>");
            out.println("");
            out.println("</html>");
            out.println("");
            out.close();
        }catch(Exception ex){
	    ex.printStackTrace();
        }
    }
    
    private void setCUnit_Html(PrintWriter out) throws  ServletException    {
        try {
            SStDate     = common.parseDate(String.valueOf(iStDate));
            SEnDate     = common.parseDate(String.valueOf(iEnDate));
        
            out.println("<html>");
            out.println("<head>");
            out.println("<title>InvPro</title>");
            out.println("</head>");
            out.println("<body bgcolor='"+bgColor+"' text = '#0000FF'>");
            out.println("<p align='center'><font size='5'><b><u>"+SUnit+" - SimplexRunout</u>  ");
            out.println(" As On Date: "+SEnDate+"</b></font></p>");
            out.println("<div align='center'>");
        
            out.println("<p></p>");
        
            out.println("  <table border='0' bordercolor='"+bgColor+"'>");
            out.println("    <tr>");
            out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>SL.No</b></font></td>");
            out.println("      <td  rowspan='1'align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>OrderNo</b></font></td>");
            out.println("      <td  rowspan='1'align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>OrderDate</b></font></td>");
            out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Count Name</b></font></td>");
            out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Shade</b></font></td>");
            out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Order Weight</b></font></td>");
            out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Process Type</b></font></td>");
            out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Blend</b></font></td>");

            out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Mix No</b></font></td>");
            out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Blowroom Setting</b></font></td>");
            out.println("      <td  colspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Carding Setting</b></font></td>");

            out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Simplex Tpi</b></font></td>");
            out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Cloth Check</b></font></td>");
            out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Process Days</b></font></td>");
            out.println("      <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Remarks</b></font></td>");
            out.println("    </tr>");
            out.println("    <tr>");
            out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>&nbsp</font></td>");
            out.println("      <td   align='Right' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>&nbsp</font></td>");

            out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Setting</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Flat Speed</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Doffer Speed</font></td>");

            out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>&nbsp;</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>&nbsp</font></td>");
            out.println("    </tr>");

            String          SPriorityStatus = "";
            int             iSlNo           = 1;
            java.util.List theOrderList     = new java.util.ArrayList();
            for(int i=0;i<VOrderNo.size();i++)  {
                if(!common.parseNull((String)VPartyPriority.elementAt(i)).equals(SPriorityStatus))  {
                    if(i!=0)    {
                        out.println("    <tr>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("      <td   align='Right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>"+common.getRound(dTOrdWt,3)+"</b></font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                        out.println("    </tr>");
                        dTOrdWt  = 0;
                        iSlNo    = 1;
                    }
                    out.println("    <tr>");
                    out.println("      <td colspan=16 width='30' bgcolor='"+bgBody+"' align='center'><font color='"+fgBody+"'>"+((String)VPartyPriority.elementAt(i))+"</font></td>");
                    out.println("    </tr>");
                }
                double dOrdWt = common.toDouble((String)VOrdWeight.elementAt(i));
                if(!theOrderList.contains(common.parseNull((String)VOrderNo.elementAt(i)))) {
                    dTOrdWt         = dTOrdWt+dOrdWt;
                    dGrandOrdWt     = dGrandOrdWt+dOrdWt;
                    theOrderList    . add(common.parseNull((String)VOrderNo.elementAt(i)));
                }
                int iMixDate    = common.toInt((String)VMixingdate.elementAt(i));
                int iRepDate    = common.toInt(common.getServerDate());
                int iProDate    = common.getDateDiff(iRepDate,iMixDate);

                out.println("    <tr>");
                out.println("      <td width='30' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>"+String.valueOf(iSlNo)+"</font></td>");
                out.println("      <td width='40' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VOrderNo.elementAt(i)+"</font></td>");
                out.println("      <td width='180' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+common.parseDate((String)VOrderdate.elementAt(i))+"</font></td>");
                out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VCountName.elementAt(i)+"</font></td>");
                out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VShade.elementAt(i)+"</font></td>");
                out.println("      <td width='40' bgcolor='"+bgBody+"' align='right'><font color='"+fgBody+"'>"+common.getRound(dOrdWt,3)+"</font></td>");
                out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VProcessType.elementAt(i)+"</font></td>");
                out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VBlend.elementAt(i)+"</font></td>");

                out.println("      <td width='40' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VMixNo.elementAt(i)+"</font></td>");
                out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VBrSetting.elementAt(i)+"</font></td>");
                out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VCdFlatSpeed.elementAt(i)+"</font></td>");
                out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VCdDoffSpeed.elementAt(i)+"</font></td>");

                out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VSimplexTpi.elementAt(i)+"</font></td>");
                out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+(String)VClothcheck.elementAt(i)+"</font></td>");
                out.println("      <td width='30' bgcolor='"+bgBody+"' align='left'><font color='"+fgBody+"'>"+String.valueOf(iProDate)+"</font></td>");
                out.println("      <td width='30' bgcolor='"+bgBody+"' align='center'><font color='"+fgBody+"'>"+VOrderStatus.elementAt(i)+"</td>");
                SPriorityStatus = common.parseNull((String)VPartyPriority.elementAt(i));
                out.println("    </tr>");
            
                iSlNo++;
            }

            out.println("    <tr>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='Right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>"+common.getRound(dTOrdWt,3)+"</b></font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("    </tr>");

            out.println("    <tr>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>GRAND TOTAL</font></td>");
            out.println("      <td   align='Right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>"+common.getRound(dGrandOrdWt,3)+"</b></font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
            out.println("    </tr>");


            dTOrdWt =0;

            dGrandOrdWt     =   0;

            out.println("  </table>");
            out.println("  </center>");
            out.println("</div>");
            out.println("");
            out.println("</body>");
            out.println("");
            out.println("</html>");
            out.println("");
            out.close();
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    private String SetBUnit_Data(){

        VOrderNo          = new Vector();
        VOrderdate        = new Vector();
        VCountName        = new Vector();
        VShade            = new Vector();
        VOrdWeight        = new Vector();
        VDepth            = new Vector();
        VProcessType      = new Vector();
        VBrSetting        = new Vector();
        VCardSetting      = new Vector();
        VDrdarkrsb        = new Vector();
        VDrdarkdo2s       = new Vector();
        VDrwhitersb       = new Vector();
        VDrwhitedo2s      = new Vector();
        VSimplexTpi       = new Vector();
        VClothcheck       = new Vector();
        VMixingdate       = new Vector();
        VOrderStatus      = new Vector();
        VPartyPriority	  = new Vector();
        VCardSettingShort = new Vector();
        VClothCheckStatus      = new Vector();
        VSimplexRunningMachine = new Vector();
        VDrawingRunningMachine = new Vector();
        VMixNo            = new Vector();

        theAbstractList[0] = new java.util.ArrayList();
        theAbstractList[1] = new java.util.ArrayList();
        theAbstractList[2] = new java.util.ArrayList();
        theAbstractList[3] = new java.util.ArrayList();
        theAbstractList[4] = new java.util.ArrayList();
		
        String  QS =    " ";
     
                QS =    " SELECT DISTINCT REGULARORDER.RORDERNO, REGULARORDER.ORDERDATE, YARNCOUNT.COUNTNAME, PROCESSINGOFINDENT.YSHNM, REGULARORDER.WEIGHT, "+
                        " PROCESSINGTYPE.PROCESSTYPE,  "+
                        " ''||ORDERBLOWROOMSETTING.TBB1||','||ORDERBLOWROOMSETTING.TBB2||'/'||ORDERBLOWROOMSETTING.KB1||','||ORDERBLOWROOMSETTING.KB2 as Bsetting,  "+
                        " ORDERCARDINGSETTING.CARDSET, ''||ORDERDRAWINGSETTING.RSBSET1||','||ORDERDRAWINGSETTING.RSBSET2 as DarkRsb,''||ORDERDRAWINGSETTING.DO2SSET1||','  "+
                        " ||ORDERDRAWINGSETTING.DO2SSET2 as DarkDo2s, ''||ORDERDRAWINGSETTING.WHITERSBSET1||','||ORDERDRAWINGSETTING.WHITERSBSET2 as WhiteRsb, "+   
                        "  ''||ORDERDRAWINGSETTING.WHITEDO2SSET1||','||ORDERDRAWINGSETTING.WHITEDO2SSET2 as WhiteDo2s, ORDERSIMPLEXSETTING.SIMPTPI, "+
                        " decode(PROCESSINGOFINDENT.CLOTHCHECKSTATUS,0,'NO',1,'YES',PROCESSINGOFINDENT.CLOTHCHECKSTATUS) as ClothCheck, "+
                        " Rmixir.MixingDate, GETSIMPLEXRUNOUTSTATUS(REGULARORDER.RORDERNO) as RunoutStatus, "+
                        " deCode(RegularOrder.Shortage, 1, 'Shortage', PartyPriority.Name) as PartyPriority, "+
                        " cardsettingtype.CARDINGSETTING, PartyPriority.Code, "+
                        " decode(PROCESSINGOFINDENT.CLOTHCHECKSTATUS,1,decode(PROCESSINGOFINDENT.SUNITCLOTHCHKSTATUS,0,decode(PROCESSINGOFINDENT.CLOTHCHKSUNITRECEIVED,1,'Received',decode(PROCESSINGOFINDENT.CLOTHCHKISSUETOSUNIT,1,'Issued',0,'To be Given')),PRO_DRAWING_CLOTHCHECK_STATUS.STATUS)) as ClothCheck, "+
                        //" decode(PROCESSINGOFINDENT.CLOTHCHECKSTATUS,1,decode(PROCESSINGOFINDENT.SUNITCLOTHCHKSTATUS,1,decode(PROCESSINGOFINDENT.SUNITCLOTHCHKSTATUS_REMARKS,0,'OK', decode(PROCESSINGOFINDENT.CLOTHAUTH,1, 'OK',0,'SMB Auth Pending')),2,'Not OK',decode(PROCESSINGOFINDENT.CLOTHCHKSUNITRECEIVED,1,'Received',decode(PROCESSINGOFINDENT.CLOTHCHKISSUETOSUNIT,1,'Issued','To be Given'))),'') as ClothCheck, "+
                        " PROCESSINGOFINDENT.DEPTH, to_number(PROCESSINGOFINDENT_DETAILS.MIX_NAME), Regularorder.Fibreformcode, FibreForm.FormName, ProcessingOfIndent.ProcessGroupCode, PROCESSINGOFINDENT.GroupName, "+
                        " RegularOrder.ObaCode, RegularOrder.ResCorrectionOrder  FROM REGULARORDER "+
                        " INNER JOIN SCM.YARNCOUNT ON YARNCOUNT.COUNTCODE = REGULARORDER.COUNTCODE "+
                        " INNER JOIN SCM.PROCESSINGTYPE ON PROCESSINGTYPE.PROCESSCODE = REGULARORDER.PROCESSTYPECODE and REGULARORDER.unitcode = 2 "+
                        " INNER JOIN SCM.FIBREFORM ON FIBREFORM.FORMCODE = REGULARORDER.FIBREFORMCODE "+
                        //" INNER JOIN PROCESSINGOFINDENT ON PROCESSINGOFINDENT.RORDERNO =  REGULARORDER.RORDERNO  AND PROCESSINGOFINDENT.FIBREISSUEGODOWNSTATUS = 1 "+
                        " INNER JOIN PROCESSINGOFINDENT ON PROCESSINGOFINDENT.RORDERNO =  REGULARORDER.RORDERNO  AND PROCESSINGOFINDENT.INDDETAILSMIXXINGISSUESTATUS = 1  AND PROCESSINGOFINDENT.PACKCOMP = 0 "+
                        " INNER JOIN PRO_DRAWING_CLOTHCHECK_STATUS ON PRO_DRAWING_CLOTHCHECK_STATUS.CODE      = PROCESSINGOFINDENT.SUNITCLOTHCHKSTATUS "+
                        " LEFT JOIN PROCESS.PROCESSINGOFINDENT_DETAILS ON PROCESSINGOFINDENT_DETAILS.RORDERNO = PROCESSINGOFINDENT.RORDERNO "+
                        //" AND PROCESSINGOFINDENT_DETAILS.FIBREISSUEGODOWNSTATUS = 1 "+
                        " AND PROCESSINGOFINDENT_DETAILS.INDDETAILSMIXXINGISSUESTATUS = 1 "+

                        " LEFT JOIN ORDERBLOWROOMSETTING ON ORDERBLOWROOMSETTING.RORDERNO = REGULARORDER.RORDERNO AND ORDERBLOWROOMSETTING.SETTINGTYPE = 0 "+
                        " LEFT JOIN ORDERCARDINGSETTING  ON ORDERCARDINGSETTING.RORDERNO  = REGULARORDER.RORDERNO  AND ORDERCARDINGSETTING.SETTINGTYPE = 0 "+
                        " LEFT JOIN cardsettingtype      ON cardsettingtype.CARDSETTINGTYPENAME = ORDERCARDINGSETTING.CARDSET "+
                        " LEFT JOIN ORDERDRAWINGSETTING  ON ORDERDRAWINGSETTING.RORDERNO  = REGULARORDER.RORDERNO  AND ORDERDRAWINGSETTING.SETTINGTYPE = 0 "+
                        " LEFT JOIN ORDERSIMPLEXSETTING  ON ORDERSIMPLEXSETTING.RORDERNO  = REGULARORDER.RORDERNO  AND ORDERSIMPLEXSETTING.SETTINGTYPE = 0 "+

                        " AND "+
                        " ( "+
                        "   ( PROCESSINGOFINDENT.MULTIMIX = 1 AND PROCESSINGOFINDENT_DETAILS.MIX_STAGE = 3 AND ORDERSIMPLEXSETTING.MIXNO =  to_number(PROCESSINGOFINDENT_DETAILS.MIX_NAME) ) "+
                        "  OR "+
                        "   ( PROCESSINGOFINDENT.MULTIMIX = 1 AND PROCESSINGOFINDENT_DETAILS.MIX_STAGE = 5 AND to_number(PROCESSINGOFINDENT_DETAILS.MIX_NAME) = ORDERSIMPLEXSETTING.MIXNO ) "+
                        "  OR "+
                        "   ( PROCESSINGOFINDENT.MULTIMIX = 0 AND ORDERSIMPLEXSETTING.MIXNO = 1 ) "+
                        " ) "+

                        " INNER JOIN RMixir On  Rmixir.OrdNo = REGULARORDER.RORDERNO  AND Nvl(RMixir.CorrectionMixing,0)= 0  "+ 
                        " Left join SCM.YarnM on YarnM.YSHCD = Rmixir.YarnShadeCOde "+

                        " Left Join process.pro_process_base on pro_process_base.rorderno = processingofindent.rorderno and pro_process_base.order_type = 0 "+
                        " AND (  "+
                        " 		( PROCESSINGOFINDENT.MULTIMIX = 1 AND to_number(PROCESSINGOFINDENT_DETAILS.MIX_NAME) = pro_process_base.MIXNO ) "+
                        " 		 OR "+
                        " 		( PROCESSINGOFINDENT.MULTIMIX = 0 AND pro_process_base.MIXNO = 1 ) "+
                        " ) "+
                        " Left Join process.pro_blowroom_lapslip on pro_blowroom_lapslip.rorderno = processingofindent.rorderno and pro_blowroom_lapslip.type = 0 "+
                        " AND (  "+
                        " 		  ( PROCESSINGOFINDENT.MULTIMIX = 1 AND to_number(PROCESSINGOFINDENT_DETAILS.MIX_NAME) = pro_blowroom_lapslip.MIXNO ) "+
                        " 		  OR "+
                        " 		  ( PROCESSINGOFINDENT.MULTIMIX = 0 AND pro_blowroom_lapslip.MIXNO = 1 ) "+
                        "     ) "+
                        " Left Join process.pro_carding_can_details on pro_carding_can_details.rorderno = processingofindent.rorderno and pro_carding_can_details.type = 0 "+
                        " AND (  "+
                        " 		  ( PROCESSINGOFINDENT.MULTIMIX = 1 AND to_number(PROCESSINGOFINDENT_DETAILS.MIX_NAME) = pro_carding_can_details.MIXNO ) "+
                        " 		  OR "+
                        " 		  ( PROCESSINGOFINDENT.MULTIMIX = 0 AND pro_carding_can_details.MIXNO = 1 ) "+
                        " 	   ) "+
                        " Left Join process.pro_drawing_can_details on pro_drawing_can_details.rorderno = processingofindent.rorderno and pro_drawing_can_details.type = 0 "+
                        " AND (  "+
                        " 		( PROCESSINGOFINDENT.MULTIMIX = 1 AND to_number(PROCESSINGOFINDENT_DETAILS.MIX_NAME) = pro_drawing_can_details.MIXNO ) "+
                        " 		OR "+
                        " 		( PROCESSINGOFINDENT.MULTIMIX = 0 AND pro_drawing_can_details.MIXNO = 1 ) "+
                        " ) "+
                        " Inner Join SCM.PartyMaster On  Partymaster.partyCode = RegularOrder.partyCode "+
                        " Inner Join SCM.PartyPriority On PartyPriority.Code = nvl(PartyMaster.PriorityStatus,0) "+
                        " Left  Join SCM.OrderSplitDetails on OrderSplitDetails.SplitOrderNo = Processingofindent.rorderno "+
                        //" INNER JOIN SCM.SIMPLEXSTATUS ON SIMPLEXSTATUS.ORDERNO = REGULARORDER.RORDERNO  "+
                        //" Left Join SCM.SimplexDetails On SimplexDetails.Proid = SimplexStatus.ProId  "+
                        //" WHERE REGULARORDER.UNITCODE = 2 AND Nvl(RMixir.CorrectionMixing,0)=0 and SIMPLEXSTATUS.STARTDATE<='"+iEnDate+"' AND PROCESSINGOFINDENT.PACKCOMP = 0 And "+
                        //" WHERE REGULARORDER.UNITCODE = 2 AND Nvl(RMixir.CorrectionMixing,0)=0  and SimplexStatus.RunoutStatus = 0  "+
                        //" And SimplexStatus.CurStatus=1 and SimplexDetails.Status=1 AND PROCESSINGOFINDENT.PACKCOMP = 0 And "+ 
                        //" WHERE PROCESSINGOFINDENT.entrydate>=20170101  AND Nvl(RMixir.CorrectionMixing,0)=0  and "+
                        " WHERE PROCESSINGOFINDENT.entrydate>=20170101 AND "+ 	
                        " (  "+
                        "    ( PROCESSINGOFINDENT.MULTIMIX =1 AND  "+
                        " 	    to_number(PROCESSINGOFINDENT_DETAILS.MIX_NAME) = ORDERBLOWROOMSETTING.MIXNO AND  "+
                        " 	    to_number(PROCESSINGOFINDENT_DETAILS.MIX_NAME) = ORDERCARDINGSETTING.MIXNO AND  "+
                        " 	    to_number(PROCESSINGOFINDENT_DETAILS.MIX_NAME) = ORDERDRAWINGSETTING.MIXNO  "+
                        "    ) "+
                        " OR "+
                        "   ( PROCESSINGOFINDENT.MULTIMIX = 0 and "+
                        " 	    ORDERBLOWROOMSETTING.MIXNO = 1 AND  "+
                        " 	    ORDERCARDINGSETTING.MIXNO = 1  AND  "+
                        " 	    ORDERDRAWINGSETTING.MIXNO = 1  "+
                        " 	 ) "+
                        " ) "+
                        " AND  "+
                        " (  "+
                        "    PROCESSINGOFINDENT.MIXINGENTRYSTATUS = 0 "+
                        " or  "+
                        "    pro_process_base.MIXSTKSTATUS = 0 "+
                        " or  "+
                        "    pro_blowroom_lapslip.orderfollowstatus = 0 "+
                        " or  "+
                        "    pro_carding_can_details.orderfollowstatus = 0  "+
                        " or  "+
                        "    pro_drawing_can_details.orderfollowstatus = 0  "+
		 
                        /* //Commented on 09.03.2019 - reason order dept completion auto problem
                        " or  "+
                        "    PROCESSINGOFINDENT.BLOWCOMP = 0  "+
                        " or  "+
                        "    PROCESSINGOFINDENT.CARDCOMP = 0  "+
                        " or  "+
                        "    PROCESSINGOFINDENT.DRAWCOMP = 0  "+
                        */
                        " ) "+
                        " AND "+
                        " ( "+
                        "   ordersplitdetails.DEPTCODE is null "+
                        " or "+
			" ( "+
                        " ordersplitdetails.DEPTCODE is not null  "+
                        " and  "+
                        " (ordersplitdetails.DEPTCODE=16 or ordersplitdetails.DEPTCODE<=3)  "+
                        " ) "+
                        " ) ";
        if(!SProcessTypeCode.equals("All")) {
            if(SProcessTypeCode.equals("BR-C Orders")){
                QS += " and ProcessingType.OEStatus = 1 ";
            }else if(SProcessTypeCode.equals("Other than BR-C Orders")){
                QS += " and ProcessingType.OEStatus = 0 ";
            }else   {
                QS += " and RegularOrder.ProcessTypeCode = "+SProcessTypeCode;
            }
        }
        if(SSelectType.equalsIgnoreCase("Depo"))   {
            QS=QS+" and regularorder.rorderno like 'DM%'";
        }
        if(SSelectType.equalsIgnoreCase("Party"))   {
            QS=QS+" and regularorder.rorderno not like 'DM%'";
        }
        if(!SOrderNo1.equals("All"))    {
            QS = QS + " and regularorder.rorderno='"+SOrderNo1+"' ";
        }
        QS = QS + " Order By 17 desc, Rmixir.MixingDate, REGULARORDER.RORDERNO ";

		System.out.println(QS);

        try{
            if(theProcessConnection==null){
                JDBCProcessConnection   jdbc                    = JDBCProcessConnection.getJDBCProcessConnection();
                                	theProcessConnection    = jdbc.getConnection();
            }
            Statement stat      = theProcessConnection.createStatement();
            ResultSet res       = stat.executeQuery(QS);
            java.util.HashMap theMap = new java.util.HashMap();
            while(res.next())   {
                VOrderNo          . addElement(common.parseNull(res.getString(1)));
                VOrderdate        . addElement(common.parseNull(res.getString(2)));
                VCountName        . addElement(common.parseNull(res.getString(3)));
                VShade            . addElement(common.parseNull(res.getString(4)));
                VOrdWeight        . addElement(common.parseNull(res.getString(5)));
                VProcessType      . addElement(common.parseNull(res.getString(6)));
		        
                VBrSetting        . addElement(common.parseNull(res.getString(7)));
                VCardSetting      . addElement(common.parseNull(res.getString(8)));
		        
                VDrdarkrsb        . addElement(common.parseNull(res.getString(9)));
                VDrdarkdo2s       . addElement(common.parseNull(res.getString(10)));
                VDrwhitersb       . addElement(common.parseNull(res.getString(11)));
                VDrwhitedo2s      . addElement(common.parseNull(res.getString(12)));
		        
                VSimplexTpi       . addElement(common.parseNull(res.getString(13)));

                VClothcheck       . addElement(common.parseNull(res.getString(14)));
                VMixingdate       . addElement(common.parseNull(res.getString(15)));
                VOrderStatus      . addElement(common.parseNull(res.getString(16)));
                VPartyPriority    . addElement(common.parseNull(res.getString(17)));
                VCardSettingShort . addElement(common.parseNull(res.getString(18)));
                VClothCheckStatus . addElement(common.parseNull(res.getString(20)));
                VDepth            . addElement(common.parseNull(res.getString(21)));
                VMixNo            . addElement(common.parseNull(res.getString(22)));
		        
                if(common.parseNull(res.getString(16)).trim().equals("Drawing") || common.parseNull(res.getString(16)).trim().equals("Simplex")) {
                    VSimplexRunningMachine . addElement(common.parseNull(getSimplexRunningMachine(res.getString(1))));
                    VDrawingRunningMachine . addElement(common.parseNull(getDrawingRunningMachine(res.getString(1))));
                }else{
                    VSimplexRunningMachine . addElement("");
                    VDrawingRunningMachine . addElement("");
                }
                theMap = new java.util.HashMap();
                theMap.put("RORDERNO",  res.getString(1));
                theMap.put("COUNTNAME", res.getString(3));
                theMap.put("SHADE",     res.getString(4));
                theMap.put("WEIGHT",    res.getString(5));
                theMap.put("DEPTH",     res.getString(21));
                theMap.put("GROUPNAME", res.getString(26));
                if(res.getInt(28)>0) {	//Correction  
                    theAbstractList[0].add(theMap);
                }else if(res.getString(24).contains("FANCY")){	//Fancy
                    theAbstractList[1].add(theMap);
                }else if(res.getInt(23)==2 || res.getInt(23)==73 || res.getInt(23)==9){	//Ball Neps
                    theAbstractList[2].add(theMap);
                }else if(res.getString(24).contains("NEPS")){	//Other Neps (Comber Neps)
                    theAbstractList[3].add(theMap);
                }else if(res.getInt(27)==1) {	//OBA 
                    theAbstractList[4].add(theMap);
                }
            }
            res.close();
            stat.close();
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return QS;
    }
    private String getSimplexRunningMachine(String SOrderNo) {
        String QS = " SELECT wm_concat(MACHINE.MACH_NAME) as RunningMachine FROM PRO_SIMPLEX "+
                    " INNER JOIN MACHINE ON MACHINE.MACH_CODE = PRO_SIMPLEX.MACH_CODE AND MACHINE.UNIT_CODE = 2 AND MACHINE.DEPT_CODE = 6 "+
                    " WHERE PRO_SIMPLEX.RORDERNO  = '"+SOrderNo+"' AND COMPSTATUS = 0 ";

        try{
            if(theProcessConnection==null){
            	JDBCProcessConnection   jdbc                    = JDBCProcessConnection.getJDBCProcessConnection();
                                        theProcessConnection    = jdbc.getConnection();
            }
            Statement stat      = theProcessConnection.createStatement();
            ResultSet res       = stat.executeQuery(QS);
            while(res.next()){
                return res.getString(1);
            }
            res . close();
            stat. close();
        
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return "";
    } 

    private String getDrawingRunningMachine(String SOrderNo){
        String SRunMachine  = "";
        String QS           =   " SELECT wm_concat(MACHINE.MACH_NAME||'-'||PRO_DRAWING.PROCESSLEVEL) as RunningMachine FROM PRO_DRAWING "+
                                " INNER JOIN MACHINE ON MACHINE.MACH_CODE = PRO_DRAWING.MACH_CODE AND MACHINE.UNIT_CODE = 2 AND MACHINE.DEPT_CODE = 5 "+
                                " WHERE PRO_DRAWING.RORDERNO  = '"+SOrderNo+"' AND COMPSTATUS = 0 ";
        try{
            if(theProcessConnection==null){
            	JDBCProcessConnection   jdbc                = JDBCProcessConnection.getJDBCProcessConnection();
                                        theProcessConnection= jdbc.getConnection();
            }
            Statement stat      = theProcessConnection.createStatement();
            ResultSet res       = stat.executeQuery(QS);
            while(res.next()){
                SRunMachine = res.getString(1);
            }
            res . close();
            stat. close();

            if(common.parseNull(SRunMachine).trim().length()==0){
                 QS = "";
                 QS = " SELECT PROCESSNAME FROM DRAWINGPROCESS WHERE (PRIORITY,id) = ( "+
                      " SELECT MAX(PRIORITY), MAX(DRAWINGPROCESS.ID) FROM PRO_DRAWING "+
                      " INNER JOIN PROCESSINGOFINDENT ON PROCESSINGOFINDENT.RORDERNO = PRO_DRAWING.RORDERNO AND PROCESSINGOFINDENT.DRAWCOMP = 0 "+
                      " INNER JOIN ORDERDRAWINGSETTING ON ORDERDRAWINGSETTING.RORDERNO = PRO_DRAWING.RORDERNO AND ORDERDRAWINGSETTING.MIXNO = PRO_DRAWING.MIXNO "+
                      " INNER JOIN ORDERDRAWINGSETTINGDETAILS ON ORDERDRAWINGSETTINGDETAILS.RORDERNO = ORDERDRAWINGSETTING.RORDERNO AND "+
                      " ORDERDRAWINGSETTINGDETAILS.MIXNO = ORDERDRAWINGSETTING.MIXNO AND ORDERDRAWINGSETTINGDETAILS.PROCESSLEVEL = PRO_DRAWING.PROCESSLEVEL "+
                      " INNER JOIN DRAWINGPROCESS ON DRAWINGPROCESS.PROCESSNAME = PRO_DRAWING.PROCESSLEVEL "+
                      " WHERE PRO_DRAWING.COMPSTATUS = 1 AND PRO_DRAWING.RORDERNO = '"+SOrderNo+"' "+
                      " AND (SELECT COUNT(*) FROM PRO_DRAWING WHERE  RORDERNO = '"+SOrderNo+"' AND COMPSTATUS =0) = 0 ) ";

                 stat      = theProcessConnection.createStatement();
                 res       = stat.executeQuery(QS);
                while(res.next()){
                    SRunMachine = "STK-"+res.getString(1);
                }
                res . close();
                stat. close();
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return SRunMachine;
    } 

    private void SetAUnit_Data() {

        VOrderNo          = new Vector();
        VOrderdate        = new Vector();
        VOrderdateDays    = new Vector();
        VCountName        = new Vector();
        VShade            = new Vector();
        VOrdWeight        = new Vector();
        VProcessType      = new Vector();
        VBlend            = new Vector();
        VMixNo            = new Vector();

        VBrSetting        = new Vector();
        VCardSetting      = new Vector();
        VCdFlatSpeed      = new Vector();
        VCdDoffSpeed      = new Vector();

        VSimplexTpi       = new Vector();
        VClothcheck       = new Vector();
        VMixingdate       = new Vector();
        VOrderStatus      = new Vector();
        VPartyPriority	  = new Vector();
        VCardSettingShort = new Vector();
        VDepth            = new Vector();

        //, round(Sysdate-to_date(RegularOrder.ORDERDATE,'yyyymmdd')) as OrderDays
        String  QS =    " ";
                QS =    " SELECT distinct RORDERNO, ORDERDATE, COUNTNAME, YSHNM, WEIGHT, PROCESSTYPE, "+
			" Blend, MIXNO, MACH_LINE, "+
			" CARDSET, CARDINGSETTING, FlatSpd, DOFFERSPEED, SIMPTPI, ClothCheck, "+
 			" MixingDate, sts, PartyPriority, Code, DEPTH, round(SYSDATE-to_date(ORDERDATE,'YYYYMMDD')) as NoofDays "+
			" From ( "+
                        " SELECT distinct REGULARORDER.RORDERNO, REGULARORDER.ORDERDATE, YARNCOUNT.COUNTNAME, YARNM.YSHNM, REGULARORDER.WEIGHT, PROCESSINGTYPE.PROCESSTYPE,  "+
                        " 'C: '||PROCESSINGOFINDENT.COTTONPER||' V:'||PROCESSINGOFINDENT.VISCOSE||' P:'||PROCESSINGOFINDENT.POLYSTER as Blend, PROCESSINGOFINDENT_DETAILS.MIXNO, AUNIT_ORDERBLOWROOMSETTING.MACH_LINE as MACH_LINE, "+
                        "  AUNIT_ORDERCARDINGSETTING_NEW.CARDSET, AUNIT_CARDSETTINGTYPE.CARDINGSETTING, AUNIT_ORDERCARDINGSETTING_NEW.FLATSPEED||'-'||AUNIT_CARDFLATSPEED.FLATSPEED_MM as FlatSpd, AUNIT_ORDERCARDINGSETTING_NEW.DOFFERSPEED, AUNIT_ORDERSIMPLEXSETTING.SIMPTPI,  "+
                        " decode(PROCESSINGOFINDENT.CLOTHCHECKSTATUS,0,'NO',1,'YES',PROCESSINGOFINDENT.CLOTHCHECKSTATUS) as ClothCheck,  "+
                        " Rmixir.MixingDate, PROCESSA.GETSIMPLEXRUNOUTSTATUS(REGULARORDER.RORDERNO) as sts, deCode(RegularOrder.Shortage, 1, 'Shortage', PartyPriority.Name) as PartyPriority, PartyPriority.Code, PROCESSINGOFINDENT.DEPTH  FROM REGULARORDER "+
                        " INNER JOIN SCM.YARNCOUNT ON YARNCOUNT.COUNTCODE = REGULARORDER.COUNTCODE  "+
                        " INNER JOIN SCM.PROCESSINGTYPE ON PROCESSINGTYPE.PROCESSCODE = REGULARORDER.PROCESSTYPECODE "+ 
                        " INNER JOIN PROCESSA.PROCESSINGOFINDENT ON PROCESSINGOFINDENT.RORDERNO =  REGULARORDER.RORDERNO  AND PROCESSINGOFINDENT.INDDETAILSMIXXINGISSUESTATUS=1  "+
                        " LEFT JOIN PROCESSA.PROCESSINGOFINDENT_DETAILS ON PROCESSINGOFINDENT_DETAILS.RORDERNO =  PROCESSINGOFINDENT.RORDERNO "+
                        " AND PROCESSINGOFINDENT_DETAILS.INDDETAILSMIXXINGISSUESTATUS=1 "+
                        " Left Join processa.pro_process_base on pro_process_base.rorderno = processingofindent.rorderno  and pro_process_base.order_type = 0 "+
                        " AND (  "+
                        " ( PROCESSINGOFINDENT.MULTIMIX = 1 AND PROCESSINGOFINDENT_DETAILS.MIXNO = pro_process_base.MIXNO ) "+
                        " OR "+
                        " ( PROCESSINGOFINDENT.MULTIMIX = 0 AND pro_process_base.MIXNO = 1 ) "+
                        " ) "+
                        " Left Join processa.pro_carding_can_details on pro_carding_can_details.rorderno = processingofindent.rorderno  and pro_carding_can_details.type = 0 "+
                        " AND (  "+
                        " ( PROCESSINGOFINDENT.MULTIMIX = 1 AND PROCESSINGOFINDENT_DETAILS.MIXNO = pro_carding_can_details.MIXNO ) "+
                        " OR "+
                        " ( PROCESSINGOFINDENT.MULTIMIX = 0 AND pro_carding_can_details.MIXNO = 1 ) "+
                        " ) "+
                        " Left Join processa.pro_drawing_can_details on pro_drawing_can_details.rorderno = processingofindent.rorderno   and pro_drawing_can_details.type = 0 "+
                        " AND (  "+
                        " ( PROCESSINGOFINDENT.MULTIMIX = 1 AND PROCESSINGOFINDENT_DETAILS.MIXNO = pro_drawing_can_details.MIXNO ) "+
                        " OR "+
                        " ( PROCESSINGOFINDENT.MULTIMIX = 0 AND pro_drawing_can_details.MIXNO = 1 ) "+
                        " ) "+
                        " LEFT JOIN PROCESSA.AUNIT_ORDERBLOWROOMSETTING ON AUNIT_ORDERBLOWROOMSETTING.RORDERNO = REGULARORDER.RORDERNO AND AUNIT_ORDERBLOWROOMSETTING.SETTINGTYPE = 0   "+
                        " LEFT JOIN PROCESSA.AUNIT_ORDERCARDINGSETTING_NEW ON AUNIT_ORDERCARDINGSETTING_NEW.RORDERNO = REGULARORDER.RORDERNO  AND AUNIT_ORDERCARDINGSETTING_NEW.SETTINGTYPE = 0  "+
                        " LEFT JOIN PROCESSA.AUNIT_CARDSETTINGTYPE ON AUNIT_CARDSETTINGTYPE.CARDSETTINGTYPENAME = decode(AUNIT_ORDERCARDINGSETTING_NEW.CARDSET,'none','0',AUNIT_ORDERCARDINGSETTING_NEW.CARDSET)  "+
                        " LEFT JOIN PROCESSA.AUNIT_CARDFLATSPEED ON AUNIT_CARDFLATSPEED.FLATSPEEDNAME = decode(AUNIT_ORDERCARDINGSETTING_NEW.FLATSPEED,'none','0',AUNIT_ORDERCARDINGSETTING_NEW.FLATSPEED)  "+
                        " LEFT JOIN PROCESSA.AUNIT_ORDERDRAWINGSETTING ON AUNIT_ORDERDRAWINGSETTING.RORDERNO = REGULARORDER.RORDERNO AND AUNIT_ORDERDRAWINGSETTING.SETTINGTYPE = 0 "+
                        " LEFT JOIN PROCESSA.AUNIT_ORDERSIMPLEXSETTING ON AUNIT_ORDERSIMPLEXSETTING.RORDERNO = REGULARORDER.RORDERNO AND AUNIT_ORDERSIMPLEXSETTING.SETTINGTYPE = 0 "+
                        " AND "+
                        " ( "+
                        "   ( PROCESSINGOFINDENT.MULTIMIX = 1 AND PROCESSINGOFINDENT_DETAILS.MIX_STAGE = 3 AND AUNIT_ORDERSIMPLEXSETTING.MIXNO =  PROCESSINGOFINDENT_DETAILS.MIXNO ) "+
                        "  OR "+
                        "   ( PROCESSINGOFINDENT.MULTIMIX = 1 AND PROCESSINGOFINDENT_DETAILS.MIX_STAGE = 5 AND PROCESSINGOFINDENT_DETAILS.MIXNO = AUNIT_ORDERSIMPLEXSETTING.MIXNO ) "+
                        "  OR "+
                        "   ( PROCESSINGOFINDENT.MULTIMIX = 0 AND AUNIT_ORDERSIMPLEXSETTING.MIXNO = 1 ) "+
                        " ) "+
                        " LEFT  JOIN RMixir On  Rmixir.OrdNo = REGULARORDER.RORDERNO "+
                        " Left join SCM.YarnM on YarnM.YSHCD=Rmixir.YarnShadeCOde  "+
                        " INNER JOIN SCM.SIMPLEXSTATUS ON SIMPLEXSTATUS.ORDERNO = REGULARORDER.RORDERNO  "+
                        " Inner Join SCM.PartyMaster On  Partymaster.partyCode = RegularOrder.partyCode  "+
                        " Inner Join SCM.PartyPriority On PartyPriority.Code = nvl(PartyMaster.PriorityStatus,0) "+
                        " Left Join SCM.SimplexDetails On SimplexDetails.Proid = SimplexStatus.ProId  "+
                        " WHERE REGULARORDER.UNITCODE = 1 AND Nvl(RMixir.CorrectionMixing,0)=0 and SIMPLEXSTATUS.STARTDATE<='"+iEnDate+"' and SimplexStatus.RunoutStatus = 0  "+
                        " and SimplexStatus.CurStatus=1 and SimplexDetails.Status=1 and "+ 
                        " (  "+
                        "    ( PROCESSINGOFINDENT.MULTIMIX =1 AND  "+
                        " 	    PROCESSINGOFINDENT_DETAILS.MIXNO = AUNIT_ORDERBLOWROOMSETTING.MIXNO AND  "+
                        " 	    PROCESSINGOFINDENT_DETAILS.MIXNO = AUNIT_ORDERCARDINGSETTING_NEW.MIXNO AND  "+
                        " 	    PROCESSINGOFINDENT_DETAILS.MIXNO = AUNIT_ORDERDRAWINGSETTING.MIXNO  "+
                        "  ) "+
                        " OR "+
                        "   ( PROCESSINGOFINDENT.MULTIMIX = 0 and "+
                        " 	    AUNIT_ORDERBLOWROOMSETTING.MIXNO = 1 AND  "+
                        " 	    AUNIT_ORDERCARDINGSETTING_NEW.MIXNO = 1  AND  "+
                        " 	    AUNIT_ORDERDRAWINGSETTING.MIXNO = 1  "+
                        " 	 ) "+
                        " ) "+
                        "  AND  "+
                        "    ( ( PROCESSINGTYPE.OESTATUS = 0  AND PROCESSINGOFINDENT.SIMPCOMP = 0 ) "+
                        "       OR "+
                        "      ( PROCESSINGTYPE.OESTATUS = 1  AND PROCESSINGOFINDENT.OECOMP = 0 ) ) "+
                        "  AND  "+
                        "  ( "+
                         " PROCESSINGOFINDENT.MIXINGENTRYSTATUS = 0 "+
                         " or  "+
                        "  pro_process_base.MIXSTKSTATUS = 0  "+
                        "  or "+
                        "  pro_carding_can_details.orderfollowstatus = 0 "+
                        "  or "+
                        "  ( "+
                        "    ( PROCESSINGTYPE.OESTATUS = 0  AND pro_drawing_can_details.orderfollowstatus = 0 ) "+
                        "   OR  "+
                        "    ( PROCESSINGTYPE.OESTATUS = 1  AND pro_drawing_can_details.processlevel in  "+
                        "      ( SELECT nvl(AUNIT_ORDERDRAWINGSETTINGDLS.PROCESSLEVEL,'') FROM PROCESSA.AUNIT_ORDERDRAWINGSETTINGDLS   "+
                        "        WHERE RORDERNO = Regularorder.rorderno AND  AUNIT_ORDERDRAWINGSETTINGDLS.ID !=  "+
                        "        ( SELECT MAX(nvl(AUNIT_ORDERDRAWINGSETTINGDLS.ID,0))  FROM PROCESSA.AUNIT_ORDERDRAWINGSETTINGDLS  WHERE RORDERNO = pro_drawing_can_details.rorderno "+
                        "          AND AUNIT_ORDERDRAWINGSETTINGDLS.PROCESSLEVEL!='none' AND AUNIT_ORDERDRAWINGSETTINGDLS.PROCESSLEVEL!='NONE'  "+
                        "        )  "+
                        "        AND AUNIT_ORDERDRAWINGSETTINGDLS.PROCESSLEVEL!='none' AND AUNIT_ORDERDRAWINGSETTINGDLS.PROCESSLEVEL!='NONE' "+
                        "  	   ) "+
                        "  AND pro_drawing_can_details.orderfollowstatus = 0 "+
                        "    ) "+
                        "  ) "+
                        "  )";
		
        if(!SProcessTypeCode.equals("All")) {
            if(SProcessTypeCode.equals("BR-C Orders"))
            {
                QS += " and ProcessingType.OEStatus = 1 ";
            }else if(SProcessTypeCode.equals("Other than BR-C Orders")){
                QS += " and ProcessingType.OEStatus = 0 ";
            }   else{
                QS += " and RegularOrder.ProcessTypeCode = "+SProcessTypeCode;
            }
        }
        if(SSelectType.equalsIgnoreCase("Depo"))    {
            QS=QS+" and regularorder.rorderno like 'DM%'";
        }
        if(SSelectType.equalsIgnoreCase("Party"))   {
            QS=QS+" and regularorder.rorderno not like 'DM%'";
        }
        if(!SOrderNo1.equals("All"))    {
            QS = QS + " and regularorder.rorderno='"+SOrderNo1+"' ";
        } 
        //QS = QS + " Order By 18 desc, Rmixir.MixingDate, REGULARORDER.RORDERNO ";
        QS = QS + " ) Order By 18 desc, MixingDate, RORDERNO ";
        try{
            if(theProcessaConnection==null){
            	JDBCProcessaConnection  jdbc                    = JDBCProcessaConnection.getJDBCProcessaConnection();
                                        theProcessaConnection   = jdbc.getConnection();
            }
            Statement stat      = theProcessaConnection.createStatement();
            ResultSet res       = stat.executeQuery(QS);
            
            while(res.next())   {
                VOrderNo          . addElement(common.parseNull(res.getString(1)));
                VOrderdate        . addElement(common.parseNull(res.getString(2)));
                VCountName        . addElement(common.parseNull(res.getString(3)));
                VShade            . addElement(common.parseNull(res.getString(4)));
                VOrdWeight        . addElement(common.parseNull(res.getString(5)));
                VProcessType      . addElement(common.parseNull(res.getString(6)));
                VBlend 		      . addElement(common.parseNull(res.getString(7)));
                VMixNo            . addElement(common.parseNull(res.getString(8)));

                VBrSetting        . addElement(common.parseNull(res.getString(9)));
                VCardSetting      . addElement(common.parseNull(res.getString(10)));
                VCardSettingShort . addElement(common.parseNull(res.getString(11)));
                VCdFlatSpeed      . addElement(common.parseNull(res.getString(12)));
                VCdDoffSpeed      . addElement(common.parseNull(res.getString(13)));
                VSimplexTpi       . addElement(common.parseNull(res.getString(14)));

                VClothcheck       . addElement(common.parseNull(res.getString(15)));
                VMixingdate       . addElement(common.parseNull(res.getString(16)));
                VOrderStatus      . addElement(common.parseNull(res.getString(17)));
                VPartyPriority    . addElement(common.parseNull(res.getString(18)));
                VDepth            . addElement(common.parseNull(res.getString(20)));
                VOrderdateDays    . addElement(common.parseNull(res.getString(21)));
            }
            res.close();
            stat.close();
        }catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    private void SetCUnit_Data() {

        VOrderNo          = new Vector();
        VOrderdate        = new Vector();
        VCountName        = new Vector();
        VShade            = new Vector();
        VOrdWeight        = new Vector();
        VProcessType      = new Vector();
        VBlend            = new Vector();
        VMixNo            = new Vector();

        VBrSetting        = new Vector();
        VCardSetting      = new Vector();
        VCdFlatSpeed      = new Vector();
        VCdDoffSpeed      = new Vector();

        VSimplexTpi       = new Vector();
        VClothcheck       = new Vector();
        VMixingdate       = new Vector();
        VOrderStatus      = new Vector();
        VPartyPriority	  = new Vector();
        VCardSettingShort = new Vector();
		

        String  QS  =   " ";
                QS  =   " SELECT distinct REGULARORDER.RORDERNO, REGULARORDER.ORDERDATE, YARNCOUNT.COUNTNAME, YARNM.YSHNM, REGULARORDER.WEIGHT, PROCESSINGTYPE.PROCESSTYPE,  "+
                        " 'C: '||CUNIT_PROCESSGOFINDENT.COTTONPER||' V:'||CUNIT_PROCESSGOFINDENT.VISCOSE||' P:'||CUNIT_PROCESSGOFINDENT.POLYSTER as Blend, CUNIT_PROCESSGOFINDENT_DETAILS.MIXNO, CUNIT_ORDBLOWROOMSETTING.RK1||' , '||CUNIT_ORDBLOWROOMSETTING.RK2 as MACH_LINE, "+
                        "  CUNIT_ORDCARDINGSETTING.CARDSET, CUNIT_CARDSETTINGTYPE.CARDINGSETTING, CUNIT_ORDCARDINGSETTING.FLATSPEED as FlatSpd, CUNIT_ORDCARDINGSETTING.DOFFERSPEED, '' as SimpTpi,  "+
                        " decode(CUNIT_PROCESSGOFINDENT.CLOTHCHECKSTATUS,0,'NO',1,'YES',CUNIT_PROCESSGOFINDENT.CLOTHCHECKSTATUS) as ClothCheck,  "+
                        " Rmixir.MixingDate, '' as Status, deCode(RegularOrder.Shortage, 1, 'Shortage', PartyPriority.Name) as PartyPriority, PartyPriority.Code FROM REGULARORDER "+
                        " INNER JOIN SCM.YARNCOUNT ON YARNCOUNT.COUNTCODE = REGULARORDER.COUNTCODE  "+
                        " INNER JOIN SCM.PROCESSINGTYPE ON PROCESSINGTYPE.PROCESSCODE = REGULARORDER.PROCESSTYPECODE  AND PROCESSINGTYPE.OESTATUS = 1 "+ 
                        " INNER JOIN PROCESS.CUNIT_PROCESSGOFINDENT ON CUNIT_PROCESSGOFINDENT.RORDERNO =  REGULARORDER.RORDERNO  AND CUNIT_PROCESSGOFINDENT.INDDETAILSMIXXINGISSUESTATUS=1  "+
                        " LEFT JOIN PROCESS.CUNIT_PROCESSGOFINDENT_DETAILS ON CUNIT_PROCESSGOFINDENT_DETAILS.RORDERNO =  CUNIT_PROCESSGOFINDENT.RORDERNO "+
                        " AND CUNIT_PROCESSGOFINDENT_DETAILS.INDDETAILSMIXXINGISSUESTATUS=1 "+
                        " Left Join process.cunit_pro_process_base on cunit_pro_process_base.rorderno = CUNIT_PROCESSGOFINDENT.rorderno  and cunit_pro_process_base.order_type = 0 "+
                        " AND (  "+
                        " ( CUNIT_PROCESSGOFINDENT.MULTIMIX = 1 AND CUNIT_PROCESSGOFINDENT_DETAILS.MIXNO = cunit_pro_process_base.MIXNO ) "+
                        " OR "+
                        " ( CUNIT_PROCESSGOFINDENT.MULTIMIX = 0 AND cunit_pro_process_base.MIXNO = 1 ) "+
                        " ) "+
                        " Left Join process.cunit_pro_carding_can_details on cunit_pro_carding_can_details.rorderno = CUNIT_PROCESSGOFINDENT.rorderno  and cunit_pro_carding_can_details.type = 0 "+
                        " AND (  "+
                        " ( CUNIT_PROCESSGOFINDENT.MULTIMIX = 1 AND CUNIT_PROCESSGOFINDENT_DETAILS.MIXNO = cunit_pro_carding_can_details.MIXNO ) "+
                        " OR "+
                        " ( CUNIT_PROCESSGOFINDENT.MULTIMIX = 0 AND cunit_pro_carding_can_details.MIXNO = 1 ) "+
                        " ) "+
                        " Left Join process.cunit_pro_drawing_can_details on cunit_pro_drawing_can_details.rorderno = CUNIT_PROCESSGOFINDENT.rorderno   and cunit_pro_drawing_can_details.type = 0 "+
                        " AND (  "+
                        " ( CUNIT_PROCESSGOFINDENT.MULTIMIX = 1 AND CUNIT_PROCESSGOFINDENT_DETAILS.MIXNO = cunit_pro_drawing_can_details.MIXNO ) "+
                        " OR "+
                        " ( CUNIT_PROCESSGOFINDENT.MULTIMIX = 0 AND cunit_pro_drawing_can_details.MIXNO = 1 ) "+
                        " ) "+
                        " LEFT JOIN PROCESS.CUNIT_ORDBLOWROOMSETTING ON CUNIT_ORDBLOWROOMSETTING.RORDERNO = REGULARORDER.RORDERNO AND CUNIT_ORDBLOWROOMSETTING.SETTINGTYPE = 0   "+
                        " LEFT JOIN PROCESS.CUNIT_ORDCARDINGSETTING ON CUNIT_ORDCARDINGSETTING.RORDERNO = REGULARORDER.RORDERNO  AND CUNIT_ORDCARDINGSETTING.SETTINGTYPE = 0  "+
                        " LEFT JOIN PROCESS.CUNIT_CARDSETTINGTYPE ON CUNIT_CARDSETTINGTYPE.CARDSETTINGTYPENAME = CUNIT_ORDCARDINGSETTING.CARDSET  "+
                        " LEFT JOIN PROCESS.CUNIT_CARDFLATSPEED ON CUNIT_CARDFLATSPEED.FLATSPEEDNAME = CUNIT_ORDCARDINGSETTING.FLATSPEED  "+
                        " LEFT JOIN PROCESS.CUNIT_ORDDRAWINGSETTING ON CUNIT_ORDDRAWINGSETTING.RORDERNO = REGULARORDER.RORDERNO  AND CUNIT_ORDDRAWINGSETTING.SETTINGTYPE = 0  "+
                        " LEFT  JOIN RMixir On  Rmixir.OrdNo = REGULARORDER.RORDERNO "+
                        " Left join SCM.YarnM on YarnM.YSHCD=Rmixir.YarnShadeCOde  "+
                        " INNER JOIN SCM.SIMPLEXSTATUS ON SIMPLEXSTATUS.ORDERNO = REGULARORDER.RORDERNO  "+
                        " Inner Join SCM.PartyMaster On  Partymaster.partyCode = RegularOrder.partyCode  "+
                        " Inner Join SCM.PartyPriority On PartyPriority.Code = nvl(PartyMaster.PriorityStatus,0) "+
                        " Left Join SCM.SimplexDetails On SimplexDetails.Proid = SimplexStatus.ProId  "+
                        " WHERE REGULARORDER.UNITCODE in (10,12) AND Nvl(RMixir.CorrectionMixing,0)=0  AND CUNIT_PROCESSGOFINDENT.OECOMP = 0 AND SIMPLEXSTATUS.STARTDATE<='"+iEnDate+"' and SimplexStatus.RunoutStatus = 0  "+
                        " and SimplexStatus.CurStatus=1 and SimplexDetails.Status=1 and "+ 
                        " (  "+
                        "    ( CUNIT_PROCESSGOFINDENT.MULTIMIX =1 AND  "+
                        " 	    CUNIT_PROCESSGOFINDENT_DETAILS.MIXNO = CUNIT_ORDBLOWROOMSETTING.MIXNO AND  "+
                        " 	    CUNIT_PROCESSGOFINDENT_DETAILS.MIXNO = CUNIT_ORDCARDINGSETTING.MIXNO AND  "+
                        " 	    CUNIT_PROCESSGOFINDENT_DETAILS.MIXNO = CUNIT_ORDDRAWINGSETTING.MIXNO  "+
                        "  ) "+
                        " OR "+
                        "   ( CUNIT_PROCESSGOFINDENT.MULTIMIX = 0 and "+
                        " 	    CUNIT_ORDBLOWROOMSETTING.MIXNO = 1 AND  "+
                        " 	    CUNIT_ORDCARDINGSETTING.MIXNO = 1  AND  "+
                        " 	    CUNIT_ORDDRAWINGSETTING.MIXNO = 1  "+
                        " 	 ) "+
                        " ) "+
                        "  AND  "+
                        "  ( "+
                        " CUNIT_PROCESSGOFINDENT.MIXINGENTRYSTATUS = 0 "+
                        " or  "+
                        "  cunit_pro_process_base.MIXSTKSTATUS = 0  "+
                        "  or "+
                        "  cunit_pro_carding_can_details.orderfollowstatus = 0 "+
                        "  or "+
                        "  ( "+
                        "    ( PROCESSINGTYPE.OESTATUS = 0  AND cunit_pro_drawing_can_details.orderfollowstatus = 0 ) "+
                        "  OR  "+
                        "    ( PROCESSINGTYPE.OESTATUS = 1  AND cunit_pro_drawing_can_details.processlevel in ( "+
                        "  SELECT CUNIT_ORDDRAWSETTINGDETAILS.PROCESSLEVEL FROM PROCESS.CUNIT_ORDDRAWSETTINGDETAILS WHERE RORDERNO = Regularorder.rorderno AND   "+
                        "  CUNIT_ORDDRAWSETTINGDETAILS.ID != ( SELECT MAX(CUNIT_ORDDRAWSETTINGDETAILS.ID)  FROM PROCESS.CUNIT_ORDDRAWSETTINGDETAILS WHERE RORDERNO = cunit_pro_drawing_can_details.rorderno AND CUNIT_ORDDRAWSETTINGDETAILS.PROCESSLEVEL!='none' AND CUNIT_ORDDRAWSETTINGDETAILS.PROCESSLEVEL!='NONE' ) "+
                        "  AND CUNIT_ORDDRAWSETTINGDETAILS.PROCESSLEVEL!='none' AND CUNIT_ORDDRAWSETTINGDETAILS.PROCESSLEVEL!='NONE' "+
                        "  ) "+
                        "  AND cunit_pro_drawing_can_details.orderfollowstatus = 0 "+
                        "    )"+
        		"  )"+
                        "  )";
        if(!SProcessTypeCode.equals("All")) {
            if(SProcessTypeCode.equals("BR-C Orders"))  {
                QS += " and ProcessingType.OEStatus = 1 ";
            }else if(SProcessTypeCode.equals("Other than BR-C Orders")) {
                QS += " and ProcessingType.OEStatus = 0 ";
            }else   {
                QS += " and RegularOrder.ProcessTypeCode = "+SProcessTypeCode;
            }
        }

        if(SSelectType.equalsIgnoreCase("Depo"))    {
            QS=QS+" and regularorder.rorderno like 'DM%'";
        }
        if(SSelectType.equalsIgnoreCase("Party"))   {
            QS=QS+" and regularorder.rorderno not like 'DM%'";
        }
        if(!SOrderNo1.equals("All"))    {
            QS = QS + " and regularorder.rorderno='"+SOrderNo1+"' ";
        } 
        QS = QS + " Order By 18 desc, Rmixir.MixingDate, REGULARORDER.RORDERNO ";
        try{
            if(theProcessConnection==null){
                JDBCProcessConnection   jdbc                = JDBCProcessConnection.getJDBCProcessConnection();
                                        theProcessConnection= jdbc.getConnection();
            }
            Statement stat      = theProcessConnection.createStatement();
            ResultSet res       = stat.executeQuery(QS);

            while(res.next())   {
                VOrderNo          . addElement(common.parseNull(res.getString(1)));
                VOrderdate        . addElement(common.parseNull(res.getString(2)));
                VCountName        . addElement(common.parseNull(res.getString(3)));
                VShade            . addElement(common.parseNull(res.getString(4)));
                VOrdWeight        . addElement(common.parseNull(res.getString(5)));
                VProcessType      . addElement(common.parseNull(res.getString(6)));
                VBlend 		      . addElement(common.parseNull(res.getString(7)));
                VMixNo            . addElement(common.parseNull(res.getString(8)));

                VBrSetting        . addElement(common.parseNull(res.getString(9)));
                VCardSetting      . addElement(common.parseNull(res.getString(10)));
                VCardSettingShort . addElement(common.parseNull(res.getString(11)));
                VCdFlatSpeed      . addElement(common.parseNull(res.getString(12)));
                VCdDoffSpeed      . addElement(common.parseNull(res.getString(13)));
                VSimplexTpi       . addElement(common.parseNull(res.getString(14)));

                VClothcheck       . addElement(common.parseNull(res.getString(15)));
                VMixingdate       . addElement(common.parseNull(res.getString(16)));
                VOrderStatus      . addElement(common.parseNull(res.getString(17)));
                VPartyPriority    . addElement(common.parseNull(res.getString(18)));
            }
            res.close();
            stat.close();
        }catch(Exception ex)    {
            ex.printStackTrace();
        }
    }
    
    private void Head_AUnit()   {
        String  SPitch  = "P";
        String  Head1   = "Company  :AMARJOTHI SPG MILLS Ltd \n";
        String  Head2   = "Document :Simplex RunOut  & Follow Up Report";
        String  SAsOn   = "As On Date  "+common.parseDate(SEnDate);
        String  Head3   = "Unit : "+SUnit+"\n";
        String  Head4   = "Page :"+iPage;
        String  Str1    = "",Str2="",Str3="",Str4="",Str5="",Str6="";
                Str1  = "----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n";
                Str2  = "|     |         |     |          |     |     |           |       |         |                    |               |                   |    |    |    |     |     |     |     |     |\n";
                Str3  = "|Sl.No| OrderNo |MixNo| OrderDate|Order|Count|   Shade   | Order | Process |        Blend       |  B/R Setting  |  Carding Setting  |SM  |Clth|Pro |Order|Depth| Sh  | Sh  | Sh  |\n";
                Str4  = "|     |         |     |          |Date |     |           | Wgt   |  Type   |                    |               | Flat       |Doffer|TPI | chk|Days|Sts  |     |  I  | II  | III |\n";
                Str5  = "|     |         |     |          |Days |     |           |       |         |                    |               | Speed      |Speed |    |    |    |     |     |     |     |     |\n";
                Str6  = "|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|";
                Str7  = "|-----|---------|-----|----------|-----|-----|-----------|-------|---------|--------------------|---------------|------------|------|----|----|----|-----|-----|-----|-----|-----|";
        //5+9+10+5+11+7+9+20+15+12+6+4+4+4+5+5+5+5+5
                End   = "----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n";
                SNote = " /*/ Process Days = Rep Dt - L.Mix  /*/ Runout Delay = Rep Dt - L.Mix - 7 Days ";
                End1 =End+SNote;
        prnWriter(Head1+Head2+"\n");
        prnWriter(SAsOn+"\n");
        prnWriter(Head3+Head4+SPitch+"\n");
        prnWriter(Str1+Str2+Str3+Str4+Str5+Str6+"\n");
    }

    private void Head() {

        String  SPitch  = "P";
        String  Head1   = "Company  :AMARJOTHI SPG MILLS Ltd \n";
        String  Head2   = "Document :Simplex RunOut  & Follow Up Report";
        String  SAsOn   = "As On Date  "+common.parseDate(SEnDate);
        String  Head3   = "Unit : "+SUnit+"\n";
        String  Head4   = "Page :"+iPage;
        String  Str1    = "",Str2="",Str3="",Str4="",Str5="",Str6="";
                Str1    = "--------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n";
                Str2    = "|     |         |     |     |           |     |       |         |        |      |                       |    |    |      |    |       |    |     |     |     |     |     |\n";
                Str3    = "|Sl.No| OrderNo |MixNo|Count|   Shade   |Depth| Order | Process | B/R    | CD   |    Drawing Setting    |SM  |Clth|Cloth |Pro |Draw   |Simp|Order| Sh  | Sh  | Sh  | Sh  |\n";
                Str4    = "|     |         |     |     |           |     | Wgt   |  Type   | Setting| Sett |Dark |Dark |White|White|TPI | chk|Status|Days|Run    |Run |Sts  | III |  I  | II  | III |\n";
                Str5    = "|     |         |     |     |           |     |       |         | TBB|KB |      |Rsb  |Do2s |Rsb  |Do2s |    |    |      |    |Machine|Mach|     |     |     |     |     |\n";
                Str6    = "|------------------------------------------------------------------------------------------------------------------------------------------------------------------------|";
                Str7    = "|-----|---------|-----|-----|-----------|-----|-------|---------|--------|------|-----|-----|-----|-----|----|----|------|----|-------|----|-----|-----|-----|-----|-----|";
        //5+9+10+5+11+7+9+8+6+15+3+4+4+5+3+3+3+3
                End     = "--------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n";
                SNote   = " /*/ Process Days = Rep Dt - L.Mix  /*/ Runout Delay = Rep Dt - L.Mix - 7 Days ";
                End1 =End+SNote;
        prnWriter(Head1+Head2+"\n");
        prnWriter(SAsOn+"\n");
        prnWriter(Head3+Head4+SPitch+"\n");
        prnWriter(Str1+Str2+Str3+Str4+Str5+Str6+"\n");
    }

    private void Body_AUnit()   {

        String          SPriorityStatus     = "";
        int             slno                = 0;
        String          STotal              = "";

        java.util.List  theShadeList        = new java.util.ArrayList();
        java.util.List  theTotOrderNoList   = new java.util.ArrayList();

        for(int i=0;i<VOrderNo.size();i++)  {
            slno++;
            try {
                if(!common.parseNull((String)VPartyPriority.elementAt(i)).equals(SPriorityStatus))  {
                    if(i!=0)    {
                        FW.write("|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|"+"\n");
                        //5+9+10+5+11+7+9+20+15+12+6+4+4+4+5+5+5+5+5
                        STotal  =   "|"+common.Space(5)+
                                    "|"+common.Space(9)+
                                    "|"+common.Space(5)+
                                    "|"+common.Space(10)+
                                    "|"+common.Space(5)+
                                    "|"+common.Space(5)+
                                    "|"+common.Space(11)+
                                    "|"+common.Rad(common.parseNull(String.valueOf(dTOrdWt)),7)+
                                    "|"+common.Space(9)+
                                    "|"+common.Space(20)+
                                    "|"+common.Space(15)+
                                    "|"+common.Space(12)+
                                    "|"+common.Space(6)+
                                    "|"+common.Space(4)+
                                    "|"+common.Space(4)+
                                    "|"+common.Space(4)+
                                    "|"+common.Space(5)+
                                    "|"+common.Space(5)+
                                    "|"+common.Space(5)+
                                    "|"+common.Space(5)+
                                    "|"+common.Space(5);

                        FW.write(STotal+"|\n");
                        dTOrdWt = 0;
                        slno    = 1;
//                        SNo = 1;
                        TLine   = TLine+10;
                    }
                    String SPartyStatus =   "|"+common.Space(5)+
                                            "|"+common.Space(9)+
                                            "|"+common.Space(5)+
                                            "|"+common.Space(10)+
                                            "|"+common.Space(5)+
                                            "|"+common.Space(5)+
                                            "|"+common.Space(11)+
                                            "|"+common.Space(7)+
                                            "|"+common.Space(9)+
                                            "|"+common.Pad(common.parseNull((String)VPartyPriority.elementAt(i)),35)+
                                            ""+common.Pad("@P",26)+
                                            "|"+common.Space(4)+
                                            "|"+common.Space(5)+
                                            "|"+common.Space(5)+
                                            "|"+common.Space(5)+
                                            "|"+common.Space(5)+
                                            "|"+common.Space(5);
                                //"|"+common.Space(4)+
                                //"|"+common.Space(4)+
                                //"|"+common.Space(3)+

                    String S1  = "|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|";
                    String S2  = "|-----|---------|-----|----------|-----|-----|-----------|-------|---------|--------------------|---------------|------------|------|----|----|----|-----|-----|-----|-----|-----|";
                    String S3  = "|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|";

                    prnWriter(S1+"\n");
                    prnWriter(SPartyStatus+"|\n");
                    prnWriter(S3+"\n");
                }
                double dOrdWt=common.toDouble((String)VOrdWeight.elementAt(i));
                //            iSNo =iSNo+1;
                if(!theTotOrderNoList.contains(common.parseNull((String)VOrderNo.elementAt(i)))){
                    dTOrdWt             = dTOrdWt+dOrdWt;
                    dGrandOrdWt         = dGrandOrdWt+dOrdWt;
                    theTotOrderNoList   . add(common.parseNull((String)VOrderNo.elementAt(i)));
                }
                String SStrSpace    =   "|"+common.Space(5)+
                                    "|"+common.Space(9)+
                                    "|"+common.Space(5)+
                                    "|"+common.Space(10)+
                                    "|"+common.Space(5)+
                                    "|"+common.Space(5)+
                                    "|"+common.Space(11)+
                                    "|"+common.Space(7)+
                                    "|"+common.Space(9)+
                                    "|"+common.Space(20)+
                                    "|"+common.Space(15)+
                                    "|"+common.Space(12)+
                                    "|"+common.Space(6)+
                                    "|"+common.Space(4)+
                                    "|"+common.Space(4)+
                                    "|"+common.Space(4)+
                                    "|"+common.Space(5)+
                                    "|"+common.Space(5)+
                                    "|"+common.Space(5)+
                                    "|"+common.Space(5)+
                                    "|"+common.Space(5);
                //	    String SProcessType = common.parseNull((String)VProcessType.elementAt(i));
		String  SStatus         = common.parseNull((String)VOrderStatus.elementAt(i));
		String  SOrderStatus    = "";
		if(SStatus.trim().equals("Mixing"))                 { SOrderStatus = "MX";  }
		else if(SStatus.trim().equals("Blowroom"))          { SOrderStatus = "BR";  }
		else if(SStatus.trim().equals("Carding"))           { SOrderStatus = "CD";  }
		else if(SStatus.trim().equals("Drawing"))           { SOrderStatus = "DR";  }
		else if(SStatus.trim().equals("Drawing Runout"))    { SOrderStatus = "DR-R";  }
		else if(SStatus.trim().equals("Simplex"))           { SOrderStatus = "SM";  }
		else if(SStatus.trim().equals("SimplexRunout"))     { SOrderStatus = "SMR"; }
		theShadeList = new java.util.ArrayList();
		theShadeList = common.getShadeLines(common.parseNull((String)VShade.elementAt(i)),10);
                String SData = "";
		for(int x=0;x<theShadeList.size();x++) {
                    if(x==0){
                        //"|"+common.Pad(common.parseNull(SDrawsetting),23)+
                        SData = "|"+common.Cad(String.valueOf(slno),5)+
                                "|"+common.Pad(common.parseNull((String)VOrderNo.elementAt(i)),9)+
                                "|"+common.Pad(common.parseNull((String)VMixNo.elementAt(i)),5)+
                                "|"+common.Pad(common.parseNull(common.parseDate((String)VOrderdate.elementAt(i))),10)+
                                "|"+common.Rad(common.parseNull((String)VOrderdateDays.elementAt(i)),5)+
                                "|"+common.Cad(common.parseNull((String)VCountName.elementAt(i)),5)+
                                "|"+common.Pad(common.parseNull(theShadeList.get(x).toString()),11)+
                                "|"+common.Rad(common.parseNull((String)VOrdWeight.elementAt(i)),7)+
                                "|"+common.Pad(common.parseNull((String)VProcessType.elementAt(i)),9)+
                                "|"+common.Pad(common.parseNull((String)VBlend.elementAt(i)),20)+
                                "|"+common.Pad(common.parseNull((String)VBrSetting.elementAt(i)),15)+
                                "|"+common.Cad(common.parseNull((String)VCdFlatSpeed.elementAt(i)),12)+
                                "|"+common.Rad(common.parseNull((String)VCdDoffSpeed.elementAt(i)),6)+
                                "|"+common.Rad(common.parseNull((String)VSimplexTpi.elementAt(i)),4)+
                                "|"+common.Cad(common.parseNull((String)VClothcheck.elementAt(i)),4)+
                                "|"+common.Rad(String.valueOf(common.getDateDiff(common.toInt(common.getServerDate()),common.toInt((String)VMixingdate.elementAt(i)))),4)+
                                "|"+common.Cad(common.parseNull(SOrderStatus),5)+
                                "|"+common.Rad(common.parseNull((String)VDepth.elementAt(i)),5)+
                                "|"+common.Rad("",5)+
                                "|"+common.Rad("",5)+
                                "|"+common.Rad("",5)+"|\n";
                    }else{
                        SData +=    "|"+common.Cad("",5)+
                                    "|"+common.Pad("",9)+
                                    "|"+common.Pad("",5)+
                                    "|"+common.Pad("",10)+
                                    "|"+common.Cad("",5)+
                                    "|"+common.Cad("",5)+
                                    "|"+common.Pad(common.parseNull(theShadeList.get(x).toString()),11)+
                                    "|"+common.Rad("",7)+
                                    "|"+common.Pad("",9)+
                                    "|"+common.Pad("",20)+
                                    "|"+common.Pad("",15)+
                                    "|"+common.Pad("",12)+
                                    "|"+common.Pad("",6)+
                                    "|"+common.Cad("",4)+
                                    "|"+common.Cad("",4)+
                                    "|"+common.Cad("",4)+
                                    "|"+common.Cad("",5)+
                                    "|"+common.Rad("",5)+
                                    "|"+common.Rad("",5)+
                                    "|"+common.Rad("",5)+
                                    "|"+common.Rad("",5)+"|\n";
                    }
                }
                prnWriter(SData);
                prnWriter(Str7+"\n");
                //prnWriter(SStrSpace);
                SPriorityStatus = common.parseNull((String)VPartyPriority.elementAt(i));
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }

    private void Body() {
        String  SPriorityStatus = "";
        int     slno            = 0;
        String  STotal          = "";

        int     iBig            = 0;
        int     iShSize         = 0; 
        int     iDSize          = 0; 
        int     iSSize          = 0;
        
        String  SShadeName      = "";
        String  SDrawMachine    = "";
        String  SSimpMachine    = "";
        
        String  SData           = "";
	
        java.util.List theShadeList    = new java.util.ArrayList();
        java.util.List theDrawMachList = new java.util.ArrayList();
        java.util.List theSimpMachList = new java.util.ArrayList();

        for(int i=0;i<VOrderNo.size();i++)  {
            slno++;
            try {
                if(!common.parseNull((String)VPartyPriority.elementAt(i)).equals(SPriorityStatus))  {
                    if(i!=0)    {
                        FW.write("|------------------------------------------------------------------------------------------------------------------------------------------------------------------------|"+"\n");
                        //5+9+10+5+11+7+9+8+6+15+3+4+4+5+3+3+3+3
                        STotal  =   "|"+common.Space(5)+
                                    "|"+common.Space(9)+
                                    "|"+common.Space(5)+
                                    "|"+common.Space(5)+
                                    "|"+common.Space(11)+
                                    "|"+common.Space(5)+
                                    "|"+common.Rad(common.parseNull(String.valueOf(dTOrdWt)),7)+
                                    "|"+common.Space(9)+
                                    "|"+common.Space(8)+
                                    "|"+common.Space(6)+
                                    "|"+common.Space(23)+
                                    "|"+common.Space(4)+
                                    "|"+common.Space(4)+
                                    "|"+common.Space(6)+
                                    "|"+common.Space(4)+
                                    "|"+common.Space(7)+
                                    "|"+common.Space(4)+
                                    "|"+common.Space(5)+
                                    "|"+common.Space(5)+
                                    "|"+common.Space(5)+
                                    "|"+common.Space(5)+
                                    "|"+common.Space(5);
                        FW.write(STotal+"|\n");
                        dTOrdWt = 0;
                        slno    = 1;
//                        SNo = 1;
                        TLine   = TLine+10;
                    }
                    
                    String  SPartyStatus    =   "|"+common.Space(5)+
                                                "|"+common.Space(9)+
                                                "|"+common.Space(5)+
                                                "|"+common.Space(5)+
                                                "|"+common.Space(11)+
                                                "|"+common.Space(5)+
                                                "|"+common.Space(7)+
                                                "|"+common.Space(9)+
                                                "|"+common.Space(8)+
                                                "|"+common.Space(6)+
                                                "|"+common.Pad(common.parseNull((String)VPartyPriority.elementAt(i)),15)+
                                                ""+common.Pad("@P",12)+
                                                "|"+common.Space(4)+
                                                "|"+common.Space(6)+
                                                "|"+common.Space(4)+
                                                "|"+common.Space(7)+
                                                "|"+common.Space(4)+
                                                "|"+common.Space(5)+
                                                "|"+common.Space(5)+
                                                "|"+common.Space(5)+
                                                "|"+common.Space(5)+
                                                "|"+common.Space(5);
                                //"|"+common.Space(4)+
                                //"|"+common.Space(4)+
                                //"|"+common.Space(3)+
                    String S1  = "|------------------------------------------------------------------------------------------------------------------------------------------------------------------------|";
                    String S2  = "|-----|---------|-----|-----|-----------|-----|-------|---------|--------|------|-----|-----|-----|-----|----|----|------|----|-------|----|-----|-----|-----|-----|-----|";
                    String S3  = "|------------------------------------------------------------------------------------------------------------------------------------------------------------------------|";
                    prnWriter(S1+"\n");
                    prnWriter(SPartyStatus+"|\n");
                    prnWriter(S3+"\n");
                }
                double  dOrdWt      = common.toDouble((String)VOrdWeight.elementAt(i));
                //            iSNo =iSNo+1;
                        dTOrdWt     = dTOrdWt+dOrdWt;
                        dGrandOrdWt = dGrandOrdWt+dOrdWt;

                String  SStrSpace   =   "|"+common.Space(5)+
                                        "|"+common.Space(9)+
                                        "|"+common.Space(5)+
                                        "|"+common.Space(5)+
                                        "|"+common.Space(11)+
                                        "|"+common.Space(5)+
                                        "|"+common.Space(7)+
                                        "|"+common.Space(9)+
                                        "|"+common.Space(8)+
                                        "|"+common.Space(6)+
                                        "|"+common.Space(23)+
                                        "|"+common.Space(4)+
                                        "|"+common.Space(4)+
                                        "|"+common.Space(6)+
                                        "|"+common.Space(4)+
                                        "|"+common.Space(7)+
                                        "|"+common.Space(4)+
                                        "|"+common.Space(5)+
                                        "|"+common.Space(5)+
                                        "|"+common.Space(5)+
                                        "|"+common.Space(5)+
                                        "|"+common.Space(5);
                String SProcessType = common.parseNull((String)VProcessType.elementAt(i));
                String SDrawsetting = "";
		if(SProcessType.startsWith("B/R")){
                    SDrawsetting =  common.Pad((String)VDrdarkrsb.elementAt(i),5)+
                                    "|"+common.Pad((String)VDrdarkdo2s.elementAt(i),5)+
                                    "|"+common.Pad("",5)+
                                    "|"+common.Pad("",5);
		}else{
		  String SWhiteRsb  = "";
		  String SWhitedo2s = "";
		         SWhiteRsb  = common.parseNull((String)VDrwhitersb.elementAt(i));
		         SWhitedo2s = common.parseNull((String)VDrwhitedo2s.elementAt(i));
		  if(SWhiteRsb.trim().equals("none,none")){  SWhiteRsb   = ""; }
		  if(SWhitedo2s.trim().equals("none,none")){ SWhitedo2s = "";  }
		  SDrawsetting = common.Pad((String)VDrdarkrsb.elementAt(i),5)+
		  		"|"+common.Pad((String)VDrdarkdo2s.elementAt(i),5)+
				"|"+common.Pad(SWhiteRsb,5)+
				"|"+common.Pad(SWhitedo2s,5);
		}
		String SStatus = common.parseNull((String)VOrderStatus.elementAt(i));
		String SOrderStatus = "";
		if(SStatus.trim().equals("Mixing"))		{ SOrderStatus = "MX";  }
		else if(SStatus.trim().equals("Blowroom"))	{ SOrderStatus = "BR";  }
		else if(SStatus.trim().equals("Carding"))	{ SOrderStatus = "CD";  }
		else if(SStatus.trim().equals("Drawing"))	{ SOrderStatus = "DR";  }
		else if(SStatus.trim().equals("Simplex"))	{ SOrderStatus = "SM";  }
		else if(SStatus.trim().equals("SimplexRunout"))	{ SOrderStatus = "SMR"; }
		theShadeList    = new java.util.ArrayList();
		theShadeList    = common.getShadeLines(common.parseNull((String)VShade.elementAt(i)),10);
                theDrawMachList = new java.util.ArrayList();
                theDrawMachList = getCanLines(common.parseNull((String)VDrawingRunningMachine.elementAt(i)).split(","),7);
                theSimpMachList = new java.util.ArrayList();
                theSimpMachList = getCanLines(common.parseNull((String)VSimplexRunningMachine.elementAt(i)).split(","),4);
                iBig            = getMaxSize(theShadeList.size(),theDrawMachList.size(),theSimpMachList.size());
                iShSize         = theShadeList.size(); 
                iDSize          = theDrawMachList.size(); 
                iSSize          = theSimpMachList.size();
                SData           = "";
                for(int x=0;x<iBig;x++) {
			
                    SShadeName   = "";
                    SDrawMachine = "";
                    SSimpMachine = "";

                    if(iShSize>0 && x<iShSize){
                      SShadeName = common.parseNull(theShadeList.get(x).toString());
                    }
                    if(iSSize>0 && x<iSSize){
                      SSimpMachine = common.parseNull(theSimpMachList.get(x).toString());
                    }
                    if(iDSize>0 && x<iDSize){
                      SDrawMachine = common.parseNull(theDrawMachList.get(x).toString());
                    }
                    if(x==0){
		                        //"|"+common.Pad(common.parseNull(SDrawsetting),23)+
                        SData = "|"+common.Cad(String.valueOf(slno),5)+
                                "|"+common.Pad(common.parseNull((String)VOrderNo.elementAt(i)),9)+
                                "|"+common.Pad(common.parseNull((String)VMixNo.elementAt(i)),5)+
                                "|"+common.Cad(common.parseNull((String)VCountName.elementAt(i)),5)+
                                "|"+common.Pad(common.parseNull(SShadeName),11)+
                                "|"+common.Rad(common.parseNull((String)VDepth.elementAt(i)),5)+
                                "|"+common.Rad(common.parseNull((String)VOrdWeight.elementAt(i)),7)+
                                "|"+common.Pad(common.parseNull((String)VProcessType.elementAt(i)),9)+
                                "|"+common.Pad(common.parseNull((String)VBrSetting.elementAt(i)),8)+
                                "|"+common.Cad(common.parseNull((String)VCardSettingShort.elementAt(i)),6)+
                                "|"+SDrawsetting+
                                "|"+common.Cad(common.parseNull((String)VSimplexTpi.elementAt(i)),4)+
                                "|"+common.Cad(common.parseNull((String)VClothcheck.elementAt(i)),4)+
                                "|"+common.Cad(common.parseNull((String)VClothCheckStatus.elementAt(i)),6)+
                                "|"+common.Cad(String.valueOf(common.getDateDiff(common.toInt(common.getServerDate()),common.toInt((String)VMixingdate.elementAt(i)))),4)+
                                "|"+common.Pad(common.parseNull(SDrawMachine),7)+
                                "|"+common.Pad(common.parseNull(SSimpMachine),4)+
                                "|"+common.Cad(common.parseNull(SOrderStatus),5)+
                                "|"+common.Rad("",5)+
                                "|"+common.Rad("",5)+
                                "|"+common.Rad("",5)+
                                "|"+common.Rad("",5)+"|\n";
                    }else{
                        SData   +=  "|"+common.Cad("",5)+
                                    "|"+common.Pad("",9)+
                                    "|"+common.Cad("",5)+
                                    "|"+common.Cad("",5)+
                                    "|"+common.Pad(common.parseNull(SShadeName),11)+
                                    "|"+common.Rad("",5)+
                                    "|"+common.Rad("",7)+
                                    "|"+common.Pad("",9)+
                                    "|"+common.Pad("",8)+
                                    "|"+common.Cad("",6)+
                                    "|"+common.Pad("",5)+
                                    "|"+common.Pad("",5)+
                                    "|"+common.Pad("",5)+
                                    "|"+common.Pad("",5)+
                                    "|"+common.Cad("",4)+
                                    "|"+common.Cad("",4)+
                                    "|"+common.Cad("",6)+
                                    "|"+common.Cad("",4)+
                                    "|"+common.Pad(common.parseNull(SDrawMachine),7)+
                                    "|"+common.Pad(common.parseNull(SSimpMachine),4)+
                                    "|"+common.Cad("",5)+
                                    "|"+common.Rad("",5)+
                                    "|"+common.Rad("",5)+
                                    "|"+common.Rad("",5)+
                                    "|"+common.Rad("",5)+"|\n";
                    }
                }
                prnWriter(SData);
                prnWriter(Str7+"\n");
                //prnWriter(SStrSpace);
                SPriorityStatus = common.parseNull((String)VPartyPriority.elementAt(i));
            }catch(Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void setTotal_AUnit()   {
        try {
            //5+9+5+10+5+11+7+9+20+15+12+6+4+4+4+5+5+5+5+5 = >151
            //"----------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n";
	    //"|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|";
            //|-----|---------|-----|----------|-----|-----------|-------|---------|--------------------|---------------|------------|------|----|----|----|-----|-----|-----|-----|-----|
            //52+7+91+2=>172
            FW.write("|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|"+"\n");
            FW.write("| Total============>                                     |"+common.Rad(common.getRound(dTOrdWt,3),7)+"|"+common.Space(111)+"|\n");
            FW.write("|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|"+"\n");
            FW.write("| GRAND TOTAL                                            |"+common.Rad(common.getRound(dGrandOrdWt,3),7)+"|"+common.Space(111)+"|\n");
            dTOrdWt     = 0;
            dGrandOrdWt = 0;
        }   catch(Exception e)  {
            e.printStackTrace();
        }
    }
    private void setTotal() {
        try{
            FW.write("|------------------------------------------------------------------------------------------------------------------------------------------------------------------------|"+"\n");
            FW.write("| Total============>                          |"+common.Rad(common.getRound(dTOrdWt,3),7)+"|"+common.Space(114)+"|\n");
            FW.write("|------------------------------------------------------------------------------------------------------------------------------------------------------------------------|"+"\n");
            FW.write("| GRAND TOTAL                                 |"+common.Rad(common.getRound(dGrandOrdWt,3),7)+"|"+common.Space(114)+"|\n");
            dTOrdWt     = 0;
            dGrandOrdWt = 0;
        }   catch(Exception e)  {
            e.printStackTrace();
        }
    }

    private void FileWriter()   {
        try {
            
            FW = new FileWriter("/software/MixPrint/ProcessSimplexRunout.prn");
            //            FW = new FileWriter("d://ProcessSimplexRunout.prn");
	    if(SUnit.trim().equals("A_Unit") || SUnit.trim().equals("C_Unit") || SUnit.trim().equals("A_Unit_A")){
                Head_AUnit();
                Body_AUnit();
                setTotal_AUnit();
                prnWriter(End1+"\n");
	    }            
	    if(SUnit.trim().equals("B_Unit")){
                Head();
                Body();
                setTotal();
                prnWriter(End1+"\n");
                AbstractHead();
                AbstractBody();
                AbstractTotal();
	    }
            //prnWriter(End1+"\n");
            FW.close();
        }   catch(Exception e)  {
            System.out.println(e);
            e.printStackTrace();
        }
    }
    
    private void prnWriter(String Str)  {
        try {
            TLine =TLine+1;
            if(TLine<53)    {
                FW.write(Str);
            }   else    {
                iPage=iPage+1;
                FW.write(Str);
                FW.write(End1);
                TLine =0;
                if(SUnit.trim().equals("A_Unit") || SUnit.trim().equals("C_Unit") || SUnit.trim().equals("A_Unit_A")) {
                    Head_AUnit();
                }
                if(SUnit.trim().equals("B_Unit")){
                    Head();
                }
            }
        }   catch(Exception e)  {
            e.printStackTrace();
        }
    }

    public java.util.List getCanLines(String[] str, int iLength) {
        java.util.List  theCanList  = new java.util.ArrayList();
        StringBuffer    sb          = new StringBuffer();
        String          SData       = "";
        int iLen = 0;
        iLen = iLength;
        for (int i = 0; i < str.length; i++) {
            SData = common.parseNull((String) str[i]);
            if (i == (str.length - 1)) {
                if (SData.length() <= iLen) {
                    sb          . append(SData);
                    iLen        = iLen - SData.length();
                    theCanList  . add(sb);
                } else if (SData.length() > iLen) {
                    theCanList  . add(sb);
                    sb          = new StringBuffer();
                    iLen        = iLength;
                    sb          . append(SData);
                    iLen        = iLen - SData.length();
                    theCanList  . add(sb);
                }
            } else {
                if (SData.length() <= iLen) {
                    sb      . append(SData).append(",");
                    iLen    = iLen - (SData.length() + 1);
                } else if (SData.length() > iLen) {
                    theCanList  . add(sb);
                    sb          = new StringBuffer();
                    iLen        = iLength;
                    sb          . append(SData).append(",");
                    iLen        = iLen - (SData.length() + 1);
                }
            }
        }
        return theCanList;
    }
    public int getMaxSize(int isize1, int isize2, int isize3) {

        //int largest = java.util.Collections.max(java.util.Arrays.asList(isize1, isize2, isize3));
        if(isize1>=isize2 && isize1>=isize3){
           return isize1;
        } 
        if(isize2>=isize1 && isize2>=isize3){
           return isize2;
        } 
        if(isize3>=isize1 && isize3>=isize2){
           return isize3;
        } 
        return 1;
    }


    private void AbstractHead() {
        
        String  SPitch  = "P";
        String  Head1   = "Company  : AMARJOTHI SPG MILLS Ltd \n";
        String  Head2   = "Document : Simplex RunOut  & Follow Up Report - Abstract";
        String  SAsOn   = "As On Date  "+common.parseDate(SEnDate);
        String  Head3   = "Unit : "+SUnit+"\n";
        String  Head4   = "Page :"+iPage;
        
        String  Str1    = "",Str2="",Str3="";
                Str1    = "---------------------------------------------------------------\n";
                Str2    = "|Sl.No| OrderNo |MixNo|Count|   Shade   |Depth| Weight|Process|\n";
                Str3    = "|-----------------------------------------------------|-------|";
                Str7    = "|-----|---------|-----|-----|-----------|-----|-------|-------|";
        //5+9+10+5+11+7+9

        End     = "---------------------------------------------------------------\n";
        SNote   = " ";
        End1    = End+SNote;
        prnWriter(Head1+Head2+"\n");
        prnWriter(SAsOn+"\n");
        prnWriter(Head3+Head4+SPitch+"\n");
        prnWriter(Str1+Str2+Str3+"\n");
    }
	
    private void AbstractBody() {
        String  SData       = "";
        int     iSiNo       = 0;
		dTOrdWt     = 0;
		dGrandOrdWt = 0;
        try{
            if(theAbstractList[0].size()>0){
                prnAbsWriter("|"+common.Cad("Correction Orders",61)+"|\n");
                for(int i=0;i<theAbstractList[0].size();i++){
                    java.util.HashMap theMap = (java.util.HashMap)theAbstractList[0].get(i);
                    prnAbsWriter(Str7+"\n");
                    SData = "|"+common.Pad(String.valueOf(++iSiNo),5)+
                            "|"+common.Pad(common.parseNull((String)theMap.get("RORDERNO")),9)+
                            "|"+common.Pad(common.parseNull((String)theMap.get("MIXNO")),5)+
                            "|"+common.Pad(common.parseNull((String)theMap.get("COUNTNAME")),5)+
                            "|"+common.Pad(common.parseNull((String)theMap.get("SHADE")),11)+
                            "|"+common.Pad(common.parseNull((String)theMap.get("DEPTH")),5)+
                            "|"+common.Rad(common.parseNull((String)theMap.get("WEIGHT")),7)+
                            "|"+common.Pad(common.parseNull((String)theMap.get("GROUPNAME")),7)+"|\n";

                    dTOrdWt     += common.toDouble(common.parseNull((String)theMap.get("WEIGHT")));
                    dGrandOrdWt += common.toDouble(common.parseNull((String)theMap.get("WEIGHT")));

                    prnAbsWriter(SData);
                }
				
                FW.write("|-------------------------------------------------------------|"+"\n");
                FW.write("| Total============>                          |"+common.Rad(common.getRound(dTOrdWt,3),7)+"|"+common.Space(7)+"|\n");
                prnAbsWriter(End);
            }
            if(theAbstractList[1].size()>0){
                dTOrdWt = 0;
                iSiNo   = 0;
                prnAbsWriter("|"+common.Cad("Fancy",61)+"|\n");
                for(int i=0;i<theAbstractList[1].size();i++) {
                    java.util.HashMap theMap = (java.util.HashMap)theAbstractList[1].get(i);
                    prnAbsWriter(Str7+"\n");
                    SData   =   "|"+common.Pad(String.valueOf(++iSiNo),5)+
                                "|"+common.Pad(common.parseNull((String)theMap.get("RORDERNO")),9)+
                                "|"+common.Pad(common.parseNull((String)theMap.get("MIXNO")),5)+
                                "|"+common.Pad(common.parseNull((String)theMap.get("COUNTNAME")),5)+
                                "|"+common.Pad(common.parseNull((String)theMap.get("SHADE")),11)+
                                "|"+common.Pad(common.parseNull((String)theMap.get("DEPTH")),5)+
                                "|"+common.Rad(common.parseNull((String)theMap.get("WEIGHT")),7)+
                                "|"+common.Pad(common.parseNull((String)theMap.get("GROUPNAME")),7)+"|\n";
                    dTOrdWt     += common.toDouble(common.parseNull((String)theMap.get("WEIGHT")));
                    dGrandOrdWt += common.toDouble(common.parseNull((String)theMap.get("WEIGHT")));
                    prnAbsWriter(SData);
                }
                FW.write("|-------------------------------------------------------------|"+"\n");
                FW.write("| Total============>                          |"+common.Rad(common.getRound(dTOrdWt,3),7)+"|"+common.Space(7)+"|\n");
                prnAbsWriter(End);
            }
            if(theAbstractList[2].size()>0){
                dTOrdWt = 0;
                iSiNo   = 0;
                prnAbsWriter("|"+common.Cad("Ball Neps",61)+"|\n");
                for(int i=0;i<theAbstractList[2].size();i++){
                    java.util.HashMap theMap = (java.util.HashMap)theAbstractList[2].get(i);
                    prnAbsWriter(Str7+"\n");
                    SData   =   "|"+common.Pad(String.valueOf(++iSiNo),5)+
                                "|"+common.Pad(common.parseNull((String)theMap.get("RORDERNO")),9)+
                                "|"+common.Pad(common.parseNull((String)theMap.get("MIXNO")),5)+
                                "|"+common.Pad(common.parseNull((String)theMap.get("COUNTNAME")),5)+
                                "|"+common.Pad(common.parseNull((String)theMap.get("SHADE")),11)+
                                "|"+common.Pad(common.parseNull((String)theMap.get("DEPTH")),5)+
                                "|"+common.Rad(common.parseNull((String)theMap.get("WEIGHT")),7)+
                                "|"+common.Pad(common.parseNull((String)theMap.get("GROUPNAME")),7)+"|\n";
                    dTOrdWt     += common.toDouble(common.parseNull((String)theMap.get("WEIGHT")));
                    dGrandOrdWt += common.toDouble(common.parseNull((String)theMap.get("WEIGHT")));
                    prnAbsWriter(SData);
                }
                FW.write("|-------------------------------------------------------------|"+"\n");
                FW.write("| Total============>                          |"+common.Rad(common.getRound(dTOrdWt,3),7)+"|"+common.Space(7)+"|\n");
                prnAbsWriter(End);
            }
            if(theAbstractList[3].size()>0){
				
                dTOrdWt = 0;
                iSiNo   = 0;
                prnAbsWriter("|"+common.Cad("Other Neps",61)+"|\n");
                for(int i=0;i<theAbstractList[3].size();i++){
                    java.util.HashMap theMap = (java.util.HashMap)theAbstractList[3].get(i);
                    prnAbsWriter(Str7+"\n");
                    SData   =   "|"+common.Pad(String.valueOf(++iSiNo),5)+
                                "|"+common.Pad(common.parseNull((String)theMap.get("RORDERNO")),9)+
                                "|"+common.Pad(common.parseNull((String)theMap.get("MIXNO")),5)+
                                "|"+common.Pad(common.parseNull((String)theMap.get("COUNTNAME")),5)+
                                "|"+common.Pad(common.parseNull((String)theMap.get("SHADE")),11)+
                                "|"+common.Pad(common.parseNull((String)theMap.get("DEPTH")),5)+
                                "|"+common.Rad(common.parseNull((String)theMap.get("WEIGHT")),7)+
                                "|"+common.Pad(common.parseNull((String)theMap.get("GROUPNAME")),7)+"|\n";
                    dTOrdWt     += common.toDouble(common.parseNull((String)theMap.get("WEIGHT")));
                    dGrandOrdWt += common.toDouble(common.parseNull((String)theMap.get("WEIGHT")));
                    prnAbsWriter(SData);
                }
                FW.write("|-------------------------------------------------------------|"+"\n");
                FW.write("| Total============>                          |"+common.Rad(common.getRound(dTOrdWt,3),7)+"|"+common.Space(7)+"|\n");
                prnAbsWriter(End);
            }
            prnAbsWriter(Str7+"\n");
            if(theAbstractList[4].size()>0){
                dTOrdWt = 0;
                iSiNo   = 0;
                prnAbsWriter("|"+common.Cad("OBA Orders",61)+"|\n");
                for(int i=0;i<theAbstractList[4].size();i++){
                    java.util.HashMap theMap = (java.util.HashMap)theAbstractList[4].get(i);
                    prnAbsWriter(Str7+"\n");
                    SData   =   "|"+common.Pad(String.valueOf(++iSiNo),5)+
                                "|"+common.Pad(common.parseNull((String)theMap.get("RORDERNO")),9)+
                                "|"+common.Pad(common.parseNull((String)theMap.get("MIXNO")),5)+
                                "|"+common.Pad(common.parseNull((String)theMap.get("COUNTNAME")),5)+
                                "|"+common.Pad(common.parseNull((String)theMap.get("SHADE")),11)+
                                "|"+common.Pad(common.parseNull((String)theMap.get("DEPTH")),5)+
                                "|"+common.Rad(common.parseNull((String)theMap.get("WEIGHT")),7)+
                                "|"+common.Pad(common.parseNull((String)theMap.get("GROUPNAME")),7)+"|\n";
                    dTOrdWt     += common.toDouble(common.parseNull((String)theMap.get("WEIGHT")));
                    dGrandOrdWt += common.toDouble(common.parseNull((String)theMap.get("WEIGHT")));
                    prnAbsWriter(SData);
                }
                FW.write("|-------------------------------------------------------------|"+"\n");
                FW.write("| Total============>                          |"+common.Rad(common.getRound(dTOrdWt,3),7)+"|"+common.Space(7)+"|\n");
                prnAbsWriter(End);
            }
            //prnAbsWriter(End1+"\n");
        }catch(Exception ex){
                ex.printStackTrace();
        }
    }

    private void AbstractTotal()    {
        try{
            //FW.write("|-------------------------------------------------------------|"+"\n");
            FW.write("| GRAND TOTAL                                 |"+common.Rad(common.getRound(dGrandOrdWt,3),7)+"|"+common.Space(7)+"|\n");
            FW.write(End1+"\n");
            dTOrdWt     = 0;
            dGrandOrdWt = 0;
        }   catch(Exception e)  {
            e.printStackTrace();
        }
    }

    private void prnAbsWriter(String Str)   {
        try {
            TLine = TLine+1;
            if(TLine<53)    {
                FW.write(Str);
            }   else    {
                iPage   = iPage+1;
                FW      . write(Str);
                FW      . write(End1);
                TLine   = 0;
                AbstractHead();
            }
        }   catch(Exception e)  {
            e.printStackTrace();
        }
    }
}
