/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProcessReports.PartyPriority;

import java.io.*;
import java.util.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.Vector;
import ProcessReports.util.*;
import ProcessReports.jdbc.*;
import ProcessReports.rndi.*;

/**
 *
 * @author Administrator
 */
public class ProcessConeWindingRunOutNew extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
     Common common;
     HttpSession    session;
     Vector VDeliveryDate,VCount,VParty,VOrderWt,VMixingWt,VMixingDate,VOrderNo,VOrderDate,VCorrectionMix,VRMixDate,VRealisation;
     Vector VDOrderNo,VPacking,VDespatch;
     Vector VExWt,VExPer,VDueDepo,VDueMill,VDDye,VDLMix,VDRMix,VPTotal,VRSimple,VRBobbin,VRpining,VRCone,VRSpining,VRemarks, VProcessRemarks;
     Vector VCSimplex,VCSpinning,VSConeWinding,VPartyPriority;

     String SStDate,SEnDate;
     String bgColor,bgHead,bgUom,bgBody,fgHead,fgUom,fgBody;
     String SUnit="";
     String SParty="";
     String SOrderNo="";
     String SCount="";
     String SPitch;
	 String SSelectType="";

     double dGrantOrdTot=0;
     double dGrantPackTot=0;
     double dGrantDespTot=0;


     FileWriter  FW;
     int iStDate=0;
     int iEnDate=0;
     String End="";
     String End1="";
     double dOrdWt=0;
     double dPackWt=0;
     double dDespWt=0;
     ResultSet theResult;
     ResultSetMetaData rsmd;
     int iPage=1;
     int TLine=0;
     int iSlNo=0;
     String SNote="";
     String SNote1="";
     String SNote2="";
     String SNote3="";

     String SProcessTypeCode, SProcessTypeName;

        String SRunOutTarget;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
          common = new Common();
          valueInt();
          bgColor = common.bgColor;
          bgHead  = common.bgHead;
          bgUom   = common.bgUom;
          bgBody  = common.bgBody;
          fgHead  = common.fgHead;
          fgUom   = common.fgUom;
          fgBody  = common.fgBody;
                        response    . setContentType("text/html;charset=UTF-8");
        PrintWriter     out         = response.getWriter();
          iEnDate  =common.toInt(common.pureDate((String)request.getParameter("TEnDate")));
          SUnit    =request.getParameter("Unit");
		  SSelectType     = common.parseNull((String)request.getParameter("Select"));

          try
          {
               StringTokenizer ST  = new StringTokenizer(request.getParameter("processType"), "#");
               while(ST.hasMoreTokens())
               {
                    SProcessTypeCode    = ST.nextToken();
                    SProcessTypeName    = ST.nextToken();
               }
          }
          catch(Exception ex)
          {
               SProcessTypeCode    = "All";
               SProcessTypeName    = "All";
          }

          // set Target Days..

          SRunOutTarget       = "7 + 6 + 1 = 14";

          if(SProcessTypeCode.equals("BR-C Orders"))
          {
               SRunOutTarget  = "5 + 5 + 1 = 11";
          }
          System.out.println("1");
          valueInt();
          System.out.println("2");
          MixingDetails(iEnDate);
          System.out.println("3");
          DespatchDetails(iEnDate);
          System.out.println("4");
          getCorrectionMix();
          System.out.println("5");
          setDataVectors();
          System.out.println("6");
          setHtml(out);
          System.out.println("7");
          FileWriter();
            out.close();
        } catch(Exception ex){
            ex.printStackTrace();
        }finally {
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

     private void valueInt()
     {
                iPage=1;
                TLine=0;
                dOrdWt=0;
                dPackWt=0;
                dDespWt=0;

     }
     private void setHtml(PrintWriter out) throws  ServletException
     {
          SStDate            = common.parseDate(String.valueOf(iStDate));
          SEnDate            = common.parseDate(String.valueOf(iEnDate));

          out.println("<html>");
          out.println("<head>");
          out.println("<title>InvPro</title>");
          out.println("</head>");
          out.println("<body bgcolor='"+bgColor+"' text = '#0000FF'>");
          out.println("<p align='center'><font size='5'><b><u>ConeWinding Runout Pending Report</u> ");
          out.println(" As On Date: "+SEnDate+"</b></font></p>");

          out.println("<p><b>Process Type : "+SProcessTypeName+", Target Days : "+SRunOutTarget+"</b></p>");

          		out.println("<div align='center'>");
          		out.println("<table border='0' bordercolor='"+bgColor+"'>");
               	out.println("<tr>");
               	out.println("<td rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>SL.No</b></font></td>");
               	out.println("<td rowspan='2'align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>OrderNo</b></font></td>");
               	out.println("<td rowspan='2'align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>OrderDate</b></font></td>");
               	out.println("<td rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Count</b></font></td>");
               	out.println("<td rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>NameoftheBuyer</b></font></td>");
               	out.println("<td rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>OrderWeight</b></font></td>");
               	out.println("<td colspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>MixingQuantity</b></font></td>");
               	out.println("<td rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Packed Weight</b></font></td>");
               	out.println("<td rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Despatched Weight</b></font></td>");
               	out.println("<td colspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Excess Shortage</b></font></td>");
               	out.println("<td colspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>DueDate DeleayDays</b></font></td>");
               	out.println("<td rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Dyeing Delay Days</b></font></td>");
               	out.println("<td rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>LMix Delay Days</b></font></td>");
               	out.println("<td rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>RMix Delay Days</b></font></td>");
               	out.println("<td rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Total Process Days</b></font></td>");
               	out.println("<td rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Days For Simplex Runout</b></font></td>");
               	out.println("<td rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Bobin Rack Exhaus Days(Mill)</b></font></td>");
               	out.println("<td rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Days for Spinning Runout</b></font></td>");
               	out.println("<td rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>CWG Runout Delay days</b></font></td>");
               	out.println("<td rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Remarks</b></font></td>");
               	out.println("<td rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Process Remarks</b></font></td>");
               	out.println("</tr>");
               	out.println("<tr>");
               	out.println("<td align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>As for Table</b></font></td>");
               	out.println("<td align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>MixingWt</b></font></td>");
               	out.println("<td align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Weight Kgs</b></font></td>");
               	out.println("<td align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Percentage 2</b></font></td>");
               	out.println("<td align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>As Per Depot</b></font></td>");
               	out.println("<td align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>As Per Mill</b></font></td>");
               	out.println("</tr>");

               String SPriorityStatus="";

               int iSlNo =0;

               double dGrantTot=0;
               double dGrantTotal=0;

               for(int i=0; i < VOrderNo.size();i++)
               {
                   if(!common.parseNull((String)VPartyPriority.elementAt(i)).equals(SPriorityStatus))
                   {
                      if(i!=0)
                      {
                         out.println("<tr>");
                         out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                         out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                         out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                         out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>Total</font></td>");
                         out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                         out.println("<td align='Right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+common.getRound(dOrdWt,3)+"</font></td>");
                         out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp;</font></td>");
                         out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</td>");
                         out.println("<td align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+common.getRound(dPackWt,3)+"</font></td>");
                         out.println("<td align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+common.getRound(dDespWt,3)+"</font></td>");
                         out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</td>");
                         out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                         out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                         out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                         out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                         out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                         out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                         out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                         out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                         out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                         out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                         out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                         out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp;</font></td>");
                         out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp;</font></td>");
                         out.println("</tr>");

                          dOrdWt=0;
                          dPackWt=0;
                          dDespWt=0;
                          iSlNo=0;
                    }
                         out.println("<tr>");
                         out.println("<td colspan=20 width='30' bgcolor='"+bgBody+"' align='Left'><font color='"+fgBody+"'>"+((String)VPartyPriority.elementAt(i))+"</font></td>");
                         out.println("</tr>");
                    }

                    String OrdNo = (String)VOrderNo.elementAt(i);

                    dOrdWt  =dOrdWt+ common.toDouble((String)VOrderWt.elementAt(i));
                    double dOrderWt= common.toDouble((String)VOrderWt.elementAt(i));
                    dPackWt =dPackWt+getPackingWt(OrdNo);
                    dDespWt =dDespWt+getDespatchWt(OrdNo);
                    double dRealisation =common.toDouble((String)VRealisation.elementAt(i));
                    double dAsPerTable  =dOrderWt*100/dRealisation; 

                      dGrantOrdTot  += common.toDouble((String)VOrderWt.elementAt(i));
                      dGrantPackTot += getPackingWt(OrdNo);
                      dGrantDespTot += getDespatchWt(OrdNo);

                    out.println("<tr>");
                    out.println("<td align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+String.valueOf(i+1)+"</font></td>");
                    out.println("<td align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VOrderNo.elementAt(i)+"</font></td>");
                    out.println("<td align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.parseDate((String)VOrderDate.elementAt(i))+"</font></td>");
                    out.println("<td align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VCount.elementAt(i)+"</font></td>");
                    out.println("<td align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VParty.elementAt(i)+"</font></td>");
                    out.println("<td align='Right' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.getRound(dOrderWt,3)+"</font></td>");
                    out.println("<td align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.getRound(dAsPerTable,3)+"</font></td>");
                    out.println("<td align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.getRound(common.toDouble((String)VMixingWt.elementAt(i)),3)+"</font></td>");
                    out.println("<td align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.getRound(getPackingWt(OrdNo),3)+"</font></td>");
                    out.println("<td align='Right' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.getRound(getDespatchWt(OrdNo),3)+"</font></td>");
                    out.println("<td align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VExWt.elementAt(i)+"</font></td>");
                    out.println("<td align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VExPer.elementAt(i)+"%</font></td>");
                    out.println("<td align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VDueDepo.elementAt(i)+"</font></td>");
                    out.println("<td align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VDueMill.elementAt(i)+"</font></td>");
                    out.println("<td align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VDDye.elementAt(i)+"</font></td>");
                    out.println("<td align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VDLMix.elementAt(i)+"</font></td>");
                    out.println("<td align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VDRMix.elementAt(i)+"</font></td>");
                    out.println("<td align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VPTotal.elementAt(i)+"</font></td>");
                    out.println("<td align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VRSimple.elementAt(i)+"</font></td>");
                    out.println("<td align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VRBobbin.elementAt(i)+"</font></td>");
                    out.println("<td align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VRSpining.elementAt(i)+"</font></td>");
                    out.println("<td align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VRCone.elementAt(i)+"</font></td>");
                    out.println("<td align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+VRemarks.elementAt(i)+"</font></td>");
                    out.println("<td align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+VProcessRemarks.elementAt(i)+"</font></td>");
                    out.println("</tr>");

                    SPriorityStatus = common.parseNull((String)VPartyPriority.elementAt(i));

                    iSlNo++;
               }
                    out.println("<tr>");
                    out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>Total</font></td>");
                    out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("<td align='Right' 	bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+common.getRound(dOrdWt,3)+"</font></td>");
                    out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp;</font></td>");
                    out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</td>");
                    out.println("<td align='right' 	bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+common.getRound(dPackWt,3)+"</font></td>");
                    out.println("<td align='right' 	bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+common.getRound(dDespWt,3)+"</font></td>");
                    out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</td>");
                    out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp;</font></td>");
                    out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp;</font></td>");
                    out.println("</tr>");

                    out.println("<tr>");
                    out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>Grand Total</font></td>");
                    out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("<td align='Right' bgcolor='"+bgUom+"'><font  color='"+fgUom+"'>"+common.getRound(dGrantOrdTot,3)+"</font></td>");
                    out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp;</font></td>");
                    out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</td>");
                    out.println("<td align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+common.getRound(dGrantPackTot,3)+"</font></td>");
                    out.println("<td align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+common.getRound(dGrantDespTot,3)+"</font></td>");
                    out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</td>");
                    out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("<td align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("<td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("<td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp</font></td>");
                    out.println("<td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp;</font></td>");
                    out.println("<td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>&nbsp;</font></td>");
                    out.println("    </tr>");

                    dGrantOrdTot=0;
                    dGrantPackTot=0;
                    dGrantDespTot=0;
     }
     private void Head()
     {
          SPitch="M";
          String Head1="Company  :AMARJOTHI SPINNING MILL \n";
          String Head2="Document :ConeWinding RunOut Report-As onDate  "+common.parseDate(SEnDate)+"\n";
          String Head3="Unit : "+SUnit+", Process Type : "+SProcessTypeName+", Target Days : "+SRunOutTarget+"\n";
          String Head4="Page :"+iPage;

          String Str1  = "|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n";
          String Str2  = "|      |           |            |       |                           |          | Mixing Quantity  |           |           |   Excess/Shotage |  DueDate       | Dyeing | LMix  | RMix  | Total  | DaysFor | Bobin | DaysFor |  CWG   |                   |                    |\n";
          String Str3  = "| Sl.No| OrderNo   |  OrderDate | Count |    Name Of the Buyer      |   Order  |------------------|  Packed   | Despatch  |------------------|----------------| Delay  | Delay | Delay | Process| Simplex | Rack  | Spining | RunOut |      Remarks      |   Process Remarks  |\n";
          String Str4  = "|      |           |            |       |                           |   Weight | As Per |  Actual |  Weight   | Weight    |  Weight  |Percen | As Per |As Per | Days   | Days  | Days  | Days   | RunOut  | Exhaus| RunOut  | Delay  |                   |                    |\n";
          String Str5  = "|      |           |            |       |                           |    KGS   | Table  |  Mixing |   KGS     |   KGS     |    Kgs   |       |  Depo  | Mill  |        |       |       |        |         | Days  |         | Days   |                   |                    |\n";
          String Str6  = "|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------";

               SNote ="\nFoot Note : Due Date Delay Days= Report Date - Due Date , Last Mixing Date Days= Last Mix Dt - Order Date , Remixing Delay Days=Remixing Mix Dt-Ord Dt ,Prcess Days = Report Date - Last MixingDate,";
               SNote1="            Dyes For Simplex RunOut = Simplex RunOut - Last Mix Dt, Bobbin Rack Exhaust Days = Bobbin Rack Exhaust - Simplex RunOut Date, Days For Spinning Runout = Spinning RunOut - Bobbin Rack Exhaust Date";
               SNote2="            Report Runout Delay Days = CMG Date - Spinning RunOut Date";
               SNote3="Target Days : For Simplex RunOut ----> 7 Days      For Spinning RunOut ---->  6 Days    For ConeWinding RunOut  ---->  1 Day ";

             SParty  = "|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------";


                 End   = "|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|";
                 End1  = "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|";

          prnWriter(Head1+Head2+Head3+Head4+SPitch);
          prnWriter(Str1+Str2+Str3+Str4+Str5+Str6);

     }
     
     private void Body()
     {
          try
          {
               String SPriorityStatus="";
               int SlNo  = 1;

               double dGrantOrdTot=0;
               double dGrantPackTot=0;
               double dGrantDespTot=0;
          
               double dOrdWt=0;
               double dPackWt=0;
               double dDespWt=0;
          

               for(int i=0; i < VOrderNo.size();i++)
               {
                    SlNo++;

                   if(!common.parseNull((String)VPartyPriority.elementAt(i)).equals(SPriorityStatus))
                   {
                     if(i!=0)
                     {
//                         prnWriter("|"+common.Cad("Total ==>",67)+"|"+common.Rad(common.getRound(dOrdWt,3),10)+"|"+common.Pad("",18)+"|"+common.Rad(common.getRound(dPackWt,3),11)+"|"+common.Rad(common.getRound(dDespWt,3),11)+"|"+common.Pad("",10));

                   prnWriter(SParty);
                    prnWriter("|"+common.Space(6)+
                                      "|"+common.Space(11)+
                                      "|"+common.Space(12)+
                                      "|"+common.Space(7)+
                                      "|"+common.Cad("TOTAL",27)+
                                      "|"+common.Rad(common.getRound(dOrdWt,3),10)+
                                      "|"+common.Space(8)+  
                                      "|"+common.Space(9)+
                                      "|"+common.Rad(common.getRound(dPackWt,3),11)+
                                      "|"+common.Rad(common.getRound(dDespWt,3),11)+
                                      "|"+common.Space(10)+
                                      "|"+common.Space(7)+
                                      "|"+common.Space(8)+  
                                      "|"+common.Space(7)+
                                      "|"+common.Space(8)+
                                      "|"+common.Space(7)+
                                      "|"+common.Space(7)+
                                      "|"+common.Space(8)+
                                      "|"+common.Space(9)+
                                      "|"+common.Space(7)+
                                      "|"+common.Space(9)+
                                      "|"+common.Space(8)+
                                      "|"+common.Space(19)+
                                      "|"+common.Space(20)+"|");

                         dOrdWt=0;
                         dPackWt=0;
                         dDespWt=0;
                     }


                    String SPartyStatus ="|"+common.Space(6)+
                                      "|"+common.Space(11)+
                                      "|"+common.Space(12)+
                                      "|"+common.Space(7)+
                                      "|E"+common.Cad(common.parseNull((String)VPartyPriority.elementAt(i)),27)+
                                    "F|"+common.Space(10)+
                                      "|"+common.Space(8)+
                                      "|"+common.Space(9)+  
                                      "|"+common.Space(11)+
                                      "|"+common.Space(11)+
                                      "|"+common.Space(10)+
                                      "|"+common.Space(7)+
                                      "|"+common.Space(8)+  
                                      "|"+common.Space(7)+
                                      "|"+common.Space(8)+
                                      "|"+common.Space(7)+
                                      "|"+common.Space(7)+
                                      "|"+common.Space(8)+
                                      "|"+common.Space(9)+
                                      "|"+common.Space(7)+
                                      "|"+common.Space(9)+
                                      "|"+common.Space(8)+
                                      "|"+common.Space(19)+
                                      "|"+common.Space(20)+"|";



            String S1  = "|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------";

               prnWriter(S1);
               prnWriter(SPartyStatus);
               prnWriter(S1);

                }
                    String OrdNos = (String)VOrderNo.elementAt(i);

                    double dOrderWt= common.toDouble((String)VOrderWt.elementAt(i));
                    double dRealisation =common.toDouble((String)VRealisation.elementAt(i));
                    double dAsPerTable  =dOrderWt*100/dRealisation;

                    dOrdWt  =dOrdWt+ common.toDouble((String)VOrderWt.elementAt(i));
                    dPackWt =dPackWt+getPackingWt(OrdNos);
                    dDespWt =dDespWt+getDespatchWt(OrdNos);


                      dGrantOrdTot  += common.toDouble((String)VOrderWt.elementAt(i));
                      dGrantPackTot += getPackingWt(OrdNos);
                      dGrantDespTot += getDespatchWt(OrdNos);

                    String SNo     ="|"+common.Cad(String.valueOf(i+1),6)+"|";
                    String OrdNo   =common.Pad((String)VOrderNo.elementAt(i),11)+"|";
                    String OrdDate =common.Pad(common.parseDate((String)VOrderDate.elementAt(i)),12)+"|";

                    String Count   =common.Cad((String)VCount.elementAt(i),7)+"|";
                    String Party   =common.Pad((String)VParty.elementAt(i),27)+"|";
                    String OrdWt   =common.Rad(common.getRound(common.toDouble((String)VOrderWt.elementAt(i)),3),10)+"|";
                    String Emp     =common.Pad(common.getRound(dAsPerTable,3),8)+"|";
                    String MixWt   =common.Rad(common.getRound(common.toDouble((String)VMixingWt.elementAt(i)),3),9)+"|";
                    String Pack    =common.Rad(common.getRound(getPackingWt(OrdNos),3),11)+"|";
                    String Desp    =common.Rad(common.getRound(getDespatchWt(OrdNos),3),11)+"|";
                    String ExWt    =common.Rad((String)VExWt.elementAt(i),10)+"|";
                    String ExPer   =common.Rad((String)VExPer.elementAt(i)+"%",7)+"|";
                    String DueDepo =common.Cad((String)VDueDepo.elementAt(i),8)+"|";
                    String DueMill =common.Cad((String)VDueMill.elementAt(i),7)+"|";
                    String DDye    =common.Cad((String)VDDye.elementAt(i),8)+"|";
                    String DLMix   =common.Cad((String)VDLMix.elementAt(i),7)+"|";
                    String DRmix   =common.Cad((String)VDRMix.elementAt(i),7)+"|";
                    String PTotal  =common.Cad((String)VPTotal.elementAt(i),8)+"|";
                    String RSim    =common.Cad((String)VRSimple.elementAt(i),9)+"|";
                    String RBob    =common.Cad((String)VRBobbin.elementAt(i),7)+"|";
                    String RSpin   =common.Cad((String)VRSpining.elementAt(i),9)+"|";
                    String RCone   =common.Cad((String)VRCone.elementAt(i),8)+"|";
                    String Rem     =common.Pad((String)VRemarks.elementAt(i),19)+"|";
                    String ProcRem =common.Pad(common.parseNull((String)VProcessRemarks.elementAt(i)),20)+"|";
                    


                                   String SStrSpace ="|"+common.Space(6)+
                                                     "|"+common.Space(11)+
                                                     "|"+common.Space(12)+
                                                     "|"+common.Space(7)+
                                                     "|"+common.Space(27)+
                                                     "|"+common.Space(10)+
                                                     "|"+common.Space(8)+
                                                     "|"+common.Space(9)+
                                                     "|"+common.Space(11)+
                                                     "|"+common.Space(11)+
                                                     "|"+common.Space(10)+
                                                     "|"+common.Space(7)+
                                                     "|"+common.Space(8)+
                                                     "|"+common.Space(7)+
                                                     "|"+common.Space(8)+
                                                     "|"+common.Space(7)+
                                                     "|"+common.Space(7)+
                                                     "|"+common.Space(8)+
                                                     "|"+common.Space(9)+
                                                     "|"+common.Space(7)+
                                                     "|"+common.Space(9)+
                                                     "|"+common.Space(8)+
                                                     "|"+common.Space(19)+
                                                     "|"+common.Space(20)+"|";
               

                   //System.out.println(SNo+OrdNo+OrdDate+Count+Party+OrdWt+Emp+MixWt+Pack+Desp+ExWt+ExPer+DueDepo+DueMill+DDye+DLMix+DRmix+PTotal+RSim+RBob+RSpin+RCone+Rem);
                   prnWriter(SNo+OrdNo+OrdDate+Count+Party+OrdWt+Emp+MixWt+Pack+Desp+ExWt+ExPer+DueDepo+DueMill+DDye+DLMix+DRmix+PTotal+RSim+RBob+RSpin+RCone+Rem+ProcRem);
                   prnWriter(SStrSpace);

                         SPriorityStatus = common.parseNull((String)VPartyPriority.elementAt(i));

               }
//               prnWriter(End);

               prnWriter(SParty);

                    prnWriter("|"+common.Space(6)+
                                      "|"+common.Space(11)+
                                      "|"+common.Space(12)+
                                      "|"+common.Space(7)+
                                      "|"+common.Cad("TOTAL",27)+
                                      "|"+common.Rad(common.getRound(dOrdWt,3),10)+
                                      "|"+common.Space(8)+  
                                      "|"+common.Space(9)+
                                      "|"+common.Rad(common.getRound(dPackWt,3),11)+
                                      "|"+common.Rad(common.getRound(dDespWt,3),11)+
                                      "|"+common.Space(10)+
                                      "|"+common.Space(7)+
                                      "|"+common.Space(8)+  
                                      "|"+common.Space(7)+
                                      "|"+common.Space(8)+
                                      "|"+common.Space(7)+
                                      "|"+common.Space(7)+
                                      "|"+common.Space(8)+
                                      "|"+common.Space(9)+
                                      "|"+common.Space(7)+
                                      "|"+common.Space(9)+
                                      "|"+common.Space(8)+
                                      "|"+common.Space(19)+
                                      "|"+common.Space(20)+"|");

               prnWriter(SParty);


                    prnWriter("|"+common.Space(6)+
                                      "|"+common.Space(11)+
                                      "|"+common.Space(12)+
                                      "|"+common.Space(7)+
                                      "|"+common.Cad("GRAND TOTAL",27)+
                                      "|"+common.Rad(common.getRound(dGrantOrdTot,3),10)+
                                      "|"+common.Space(8)+  
                                      "|"+common.Space(9)+
                                      "|"+common.Rad(common.getRound(dGrantPackTot,3),11)+
                                      "|"+common.Rad(common.getRound(dGrantDespTot,3),11)+
                                      "|"+common.Space(10)+
                                      "|"+common.Space(7)+
                                      "|"+common.Space(8)+  
                                      "|"+common.Space(7)+
                                      "|"+common.Space(8)+
                                      "|"+common.Space(7)+
                                      "|"+common.Space(7)+
                                      "|"+common.Space(8)+
                                      "|"+common.Space(9)+
                                      "|"+common.Space(7)+
                                      "|"+common.Space(9)+
                                      "|"+common.Space(8)+
                                      "|"+common.Space(19)+
                                      "|"+common.Space(20)+"|");

//               prnWriter("|"+common.Cad("Total ==>",67)+"|"+common.Rad(common.getRound(dOrdWt,3),10)+"|"+common.Pad("",18)+"|"+common.Rad(common.getRound(dPackWt,3),11)+"|"+common.Rad(common.getRound(dDespWt,3),11)+"|"+common.Pad("",10));
//               prnWriter("|"+common.Cad("GrandTotal ==>",67)+"|"+common.Rad(common.getRound(dGrantOrdTot,3),10)+"|"+common.Pad("",18)+"|"+common.Rad(common.getRound(dGrantPackTot,3),11)+"|"+common.Rad(common.getRound(dGrantDespTot,3),11)+"|"+common.Pad("",10));
               prnWriter(End);

               dOrdWt=0;
               dPackWt=0;
               dDespWt=0;

               dGrantOrdTot=0;
               dGrantPackTot=0;
               dGrantDespTot=0;

               prnWriter(SNote);
               prnWriter(SNote1);
               prnWriter(SNote2);
               prnWriter("");
               prnWriter("");
               prnWriter(SNote3);

          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }


     public void MixingDetails(int OnDate)
     {
            VOrderNo        = new Vector();
            VOrderDate      = new Vector();
            VDeliveryDate   = new Vector();
            VCount          = new Vector();
            VParty          = new Vector();
            VOrderWt        = new Vector();
            VMixingWt       = new Vector();
            VMixingDate     = new Vector();
            VCSimplex       = new Vector();
            VCSpinning      = new Vector();
            VSConeWinding   = new Vector();
            VCorrectionMix  = new Vector();
            VRMixDate       = new Vector();
            VRealisation    = new Vector();
            VRemarks        = new Vector();
            VPartyPriority  = new Vector();
            VProcessRemarks = new Vector();


            try
            {
		    StringBuffer sb     =new StringBuffer(); 
					if(SUnit.trim().equals("A_Unit")){
						sb . append(getAUnitQs());
					}
					if(SUnit.trim().equals("B_Unit")){
						sb . append(getBUnitQs());
					}
					if(SUnit.trim().equals("C_Unit") || SUnit.trim().equals("A_Unit_A")) {
						sb . append(getCUnitQs());
					}

				Connection     conn = null;
				if(SUnit.trim().equals("A_Unit")){
               		JDBCProcessaConnection jdbc = JDBCProcessaConnection.getJDBCProcessaConnection();
                               	    conn = jdbc.getConnection();
				}
				if(SUnit.trim().equals("B_Unit") || SUnit.trim().equals("C_Unit") || SUnit.trim().equals("A_Unit_A")){
               		JDBCProcessConnection jdbc = JDBCProcessConnection.getJDBCProcessConnection();
                               	    conn = jdbc.getConnection();
				}
				               
               Statement stat      = conn.createStatement();
               ResultSet res       = stat.executeQuery(sb.toString());

                      while(res.next())
                      {
                         String SFibreForm   = ","+common.parseNull(res.getString(16));

                         if(SFibreForm.indexOf("NONE") != -1)
                         {
                              SFibreForm     = "";
                         }

                         VOrderNo.addElement(res.getString(1));
                         VOrderDate.addElement(res.getString(2));
                         VDeliveryDate.addElement(res.getString(3));
                         VCount.addElement(res.getString(4));
                         VParty.addElement(res.getString(5));
                         VOrderWt.addElement(res.getString(6));
                         VMixingWt.addElement(res.getString(7));
                         VMixingDate.addElement(res.getString(8));
                         VCSimplex.addElement(res.getString(9));
                         VCSpinning.addElement(res.getString(10));
                         VCorrectionMix.addElement(res.getString(11));
                         VRealisation.addElement(res.getString(12));
                         VSConeWinding.addElement(res.getString(13));
                         VRMixDate.addElement(String.valueOf(0));
                         VRemarks.addElement(res.getString(14)+SFibreForm);
                         VPartyPriority.addElement(res.getString(15));
                         VProcessRemarks.addElement(common.parseNull(res.getString(18)));
                      }
                      res.close();
                      stat.close();
                      
            }
            catch(Exception e)
            {
                System.out.println(e);
                e.printStackTrace();
            }
        
       }

	
	private String getAUnitQs(){
		StringBuffer sb = new StringBuffer();
		
             sb.append("  Select distinct RegularOrder.RorderNo, RegularOrder.OrderDate, DeliveryDate, YarnCount.CountName, PartyMaster.PartyName, RegularOrder.Weight, Rmixir.MixingWeight,");
		    sb.append("   Rmixir.MixingDate, to_char(processingofindent.simpcomp_date,'yyyymmdd') as SimplexStatusCompDate, to_char(processingofindent.spincomp_date,'yyyymmdd') as SpinningStatusCompDate, Rmixir.CorrectionMixing, SampleToRegularAuth.RealisationPer, ");
              sb.append("  min(pro_winding.entrydate) as ConeWindingStatusStartDate, 'NIL' as RegStatus_RemarksRemarks, deCode(RegularOrder.Shortage, 1, 'Shortage', PartyPriority.Name) as PartyPriority, FibreForm.FormName, PartyPriority.Code, processingofindent.remarks as ProcessRemarks  From SCM.RegularOrder");
		    sb.append("  Inner Join processa.processingofindent On processingofindent.rorderno = RegularOrder.ROrderNo and processingofindent.entrydate>=20150101 ");
		    sb.append("  Inner join ");
		    sb.append("  (");
		    sb.append("  select distinct rorderno from ( "); 
		    sb.append("  select distinct processingofindent.rorderno from processa.processingofindent   "); 
		    sb.append("  inner join SCM.ProcessingType on ProcessingType.ProcessCode = processingofindent.ProcessCode  						and ProcessingType.oestatus = 0   "); 
		    sb.append("  inner join processa.pro_drawing_Can_Details on pro_drawing_Can_Details.rorderno = processingofindent.rorderno 		and pro_drawing_Can_Details.orderfollowstatus = 1   "); 
		    sb.append("  inner join processa.pro_simplex_can_details on pro_simplex_can_details.rorderno = processingofindent.rorderno 		and pro_simplex_can_details.orderfollowstatus = 1   "); 
		    sb.append("  left join processa.pro_spinning_pumb_details on  pro_spinning_pumb_details.rorderno = processingofindent.rorderno 	and pro_spinning_pumb_details.orderfollowstatus = 0  "); 
		    sb.append("  where spincomp = 0 "); 
		    sb.append("  and processingofindent.rorderno not in ("); 
		    sb.append("  select rorderno from ("); 
		    sb.append("  select count(*),rorderno,orderfollowstatus from processa.pro_simplex_can_details"); 
		    sb.append("  having count(*) >= 1 and orderfollowstatus = 0 "); 
		    sb.append("  group by rorderno,orderfollowstatus"); 
		    sb.append("  )"); 
		    sb.append("  )"); 
		    sb.append("  and processingofindent.rorderno not in ("); 
		    sb.append("  select rorderno from ("); 
		    sb.append("  select count(*),rorderno,orderfollowstatus from processa.pro_drawing_Can_Details"); 
		    sb.append("  having count(*) >= 1 and orderfollowstatus = 0 "); 
		    sb.append("  group by rorderno,orderfollowstatus"); 
		    sb.append("  )"); 
		    sb.append("  )"); 
		    sb.append("  union all"); 
		    sb.append("  select distinct processingofindent.rorderno from processa.processingofindent   "); 
		    sb.append("  inner join scm.ProcessingType on ProcessingType.ProcessCode = processingofindent.ProcessCode  						and ProcessingType.oestatus = 1   "); 
		    sb.append("  inner join processa.pro_drawing_Can_Details on pro_drawing_Can_Details.rorderno = processingofindent.rorderno 		and pro_drawing_Can_Details.orderfollowstatus = 1   "); 
		    sb.append("  left join processa.pro_oe_cheese_details on  pro_oe_cheese_details.rorderno = processingofindent.rorderno 			and pro_oe_cheese_details.orderfollowstatus = 0   "); 
		    sb.append("  where oecomp = 0   and processingofindent.rorderno not in ("); 
		    sb.append("  select rorderno from ("); 
		    sb.append("  select count(*),rorderno,orderfollowstatus from processa.pro_drawing_Can_Details"); 
		    sb.append("  having count(*) >= 1 and orderfollowstatus = 0 "); 
		    sb.append("  group by rorderno,orderfollowstatus"); 
		    sb.append("  )"); 
		    sb.append("  )"); 
                   
                    /*
//		    sb.append("  select distinct processingofindent.rorderno from processa.processingofindent ");
		    sb.append("  select processingofindent.rorderno from processa.processingofindent ");
		    sb.append("  inner join SCM.ProcessingType on ProcessingType.ProcessCode = processingofindent.ProcessCode  and ProcessingType.oestatus = 0 ");
		    sb.append("  inner join processa.pro_drawing_Can_Details on pro_drawing_Can_Details.rorderno = processingofindent.rorderno and pro_drawing_Can_Details.orderfollowstatus = 1 ");
		    sb.append("  inner join processa.pro_simplex_can_details on pro_simplex_can_details.rorderno = processingofindent.rorderno and pro_simplex_can_details.orderfollowstatus = 1 ");
		    sb.append("  left join processa.pro_spinning_pumb_details on  pro_spinning_pumb_details.rorderno = processingofindent.rorderno and pro_spinning_pumb_details.orderfollowstatus = 0 ");
		    sb.append("  where spincomp = 0 and ");
			sb.append("      (select count(*) from processa.pro_simplex_can_details where pro_simplex_can_details.rorderno = processingofindent.rorderno  and orderfollowstatus = 0) = 0 ");
			sb.append("  and (select count(*) from processa.pro_drawing_Can_Details where pro_drawing_Can_Details.rorderno = processingofindent.rorderno  and orderfollowstatus = 0) = 0 ");
		    sb.append("  union all ");
//		    sb.append("  select distinct processingofindent.rorderno from processa.processingofindent ");
		    sb.append("  select processingofindent.rorderno from processa.processingofindent ");
		    sb.append("  inner join scm.ProcessingType on ProcessingType.ProcessCode = processingofindent.ProcessCode  and ProcessingType.oestatus = 1 ");
		    sb.append("  inner join processa.pro_drawing_Can_Details on pro_drawing_Can_Details.rorderno = processingofindent.rorderno and pro_drawing_Can_Details.orderfollowstatus = 1 ");
		    sb.append("  left join processa.pro_oe_cheese_details on  pro_oe_cheese_details.rorderno = processingofindent.rorderno and pro_oe_cheese_details.orderfollowstatus = 0 ");
		    sb.append("  where oecomp = 0 ");
		    //sb.append("  where windcomp = 0 ");
			sb.append("  and (select count(*) from processa.pro_drawing_Can_Details where pro_drawing_Can_Details.rorderno = processingofindent.rorderno  and orderfollowstatus = 0) = 0 ");
                        
                        */
 		    sb.append("  )");
		    sb.append("  ) t on t.rorderno = RegularOrder.rorderno ");
		    
		    sb.append("  Inner Join scm.YarnCount On  YarnCount.CountCode = RegularOrder.CountCode");
		    sb.append("  Inner Join scm.PartyMaster On  Partymaster.partyCode = RegularOrder.partyCode");
		    sb.append("  Inner Join scm.PartyPriority On PartyPriority.Code = nvl(PartyMaster.PriorityStatus,0) ");
		    sb.append("  Inner Join scm.RMixir On  Rmixir.OrdNo = RegularOrder.RorderNo");
		    sb.append("  Inner Join scm.Unit On Unit.UnitCode = RegularOrder.UnitCode");
		    sb.append("  left join scm.ordersplitdetails on ordersplitdetails.splitorderno = processingofindent.rorderno ");
		    sb.append("  Left Join processa.pro_winding On pro_winding.rorderno = processingofindent.rorderno ");
		    sb.append("  Left Join scm.SampletoRegularAuth On SampletoRegularAuth.RegularOrderNo = RegularOrder.ROrderNo");
		    sb.append("  Inner Join scm.FibreForm on FibreForm.FormCode = nvl(RegularOrder.FibreFormCode, 0) ");
		    sb.append("  inner join scm.ProcessingType on ProcessingType.ProcessCode = regularorder.ProcessTypeCode ");
		    sb.append("  Where  RegularOrder.UnitCode = 1 and  Nvl(SampletoRegularAuth.CorrectionMixing,0) = 0 ");
		    sb.append(" and  ( ");
		    sb.append("    ( ordersplitdetails.deptcode = 6 and  ordersplitdetails.baseorderno is not null and processingofindent.windcomp = 0 ) ");
		    sb.append("  or ");
		    sb.append("    ( ordersplitdetails.deptcode = 7 and  ordersplitdetails.baseorderno is not null and processingofindent.packcomp = 0 )");
		    sb.append("  or ");
		    sb.append("    (ProcessingType.oestatus = 1 and ordersplitdetails.baseorderno is null and oecomp = 0 ) ");
		    sb.append("  or ");
		    sb.append("    (ProcessingType.oestatus = 0 and ordersplitdetails.baseorderno is null and windcomp = 0 ) ");
		    sb.append("  ) ");
			
			

         if(!SUnit.equals("All")) {
		    sb.append("   and Unit.UnitName='"+SUnit+"' ");
         }
		 if(SSelectType.equalsIgnoreCase("Depo")){
		    sb.append("   and regularorder.rorderno like 'DM%'");
         }
		 if(SSelectType.equalsIgnoreCase("Party")){
		    sb.append("   and regularorder.rorderno not like 'DM%'");
         }
         /*
         if(!SOrderNo.equals("All")){
		    sb.append("  and regularorder.rorderno='"+SOrderNo+"' ");
         }
         */
		if(!SProcessTypeCode.equals("All")){
		    if(SProcessTypeCode.equals("BR-C Orders")){
			    sb.append("  and ProcessingType.OEStatus = 1 ");
		    }else if(SProcessTypeCode.equals("Other than BR-C Orders")){
			    sb.append("  and ProcessingType.OEStatus = 0 ");
		    }else{
			    sb.append("  and RegularOrder.ProcessTypeCode = "+SProcessTypeCode);
			}
		}
                sb.append("  Group by  RegularOrder.RorderNo, RegularOrder.OrderDate, DeliveryDate, YarnCount.CountName, PartyMaster.PartyName, RegularOrder.Weight, Rmixir.MixingWeight,");
                sb.append("   Rmixir.MixingDate, to_char(processingofindent.simpcomp_date,'yyyymmdd') , to_char(processingofindent.spincomp_date,'yyyymmdd') , Rmixir.CorrectionMixing, SampleToRegularAuth.RealisationPer, ");
                sb.append("  PartyPriority.Name, FibreForm.FormName, PartyPriority.Code, processingofindent.remarks, RegularOrder.Shortage ");
                sb.append("  Order By 15 desc, RegularOrder.OrderDate , RegularOrder.RorderNo ");
		
		System.out.println(sb.toString());
		return sb.toString();
	}
	
	
    private String getBUnitQs(){
        StringBuffer sb = new StringBuffer();
		
        sb.append(" Select distinct RegularOrder.RorderNo, RegularOrder.OrderDate, DeliveryDate, YarnCount.CountName, PartyMaster.PartyName, RegularOrder.Weight, Rmixir.MixingWeight, ");
        sb.append(" Rmixir.MixingDate, to_char(processingofindent.simpcomp_date,'yyyymmdd') as SimplexStatusCompDate, to_char(processingofindent.spincomp_date,'yyyymmdd') as SpinningStatusCompDate, Rmixir.CorrectionMixing, SampleToRegularAuth.RealisationPer,  ");
        sb.append("  min(pro_winding.entrydate) as ConeWindingStatusStartDate, 'NIL' as RegStatus_RemarksRemarks, deCode(RegularOrder.Shortage, 1, 'Shortage', PartyPriority.Name) as PartyPriority, FibreForm.FormName, PartyPriority.Code, '' as ProcessRemarks From SCM.RegularOrder ");
        sb.append(" Inner Join process.processingofindent On processingofindent.rorderno = RegularOrder.ROrderNo and processingofindent.entrydate>=20150101  ");
        sb.append(" Inner join  ");
        sb.append(" ( ");
        sb.append(" select distinct rorderno from (   ");
        sb.append(" select distinct processingofindent.rorderno from process.processingofindent "); 
        sb.append(" inner join process.pro_drawing_Can_Details on pro_drawing_Can_Details.rorderno = processingofindent.rorderno and pro_drawing_Can_Details.orderfollowstatus = 1  ");
        sb.append(" inner join process.pro_simplex_can_details on pro_simplex_can_details.rorderno = processingofindent.rorderno and pro_simplex_can_details.orderfollowstatus = 1  ");
        sb.append(" left join process.pro_spinning_pumb_details on  pro_spinning_pumb_details.rorderno = processingofindent.rorderno and pro_spinning_pumb_details.orderfollowstatus = 0  ");
        sb.append(" where spincomp = 0 "); 
        sb.append(" and  ");
        sb.append(" (select count(*) from process.pro_simplex_can_details where pro_simplex_can_details.rorderno = processingofindent.rorderno  and orderfollowstatus = 0) = 0  ");
        sb.append(" and 	 ");
        sb.append(" (select count(*) from process.pro_drawing_Can_Details where pro_drawing_Can_Details.rorderno = processingofindent.rorderno  and  orderfollowstatus = 0) = 0  ");
        sb.append(" ) ");
        sb.append(" ) t on t.rorderno = RegularOrder.rorderno "); 
        sb.append(" Inner Join scm.YarnCount On  YarnCount.CountCode = RegularOrder.CountCode ");
        sb.append(" Inner Join scm.PartyMaster On  Partymaster.partyCode = RegularOrder.partyCode ");
        sb.append(" Inner Join scm.PartyPriority On PartyPriority.Code = nvl(PartyMaster.PriorityStatus,0)  ");
        sb.append(" Inner Join scm.RMixir On  Rmixir.OrdNo = RegularOrder.RorderNo ");
        sb.append(" Inner Join scm.Unit On Unit.UnitCode = RegularOrder.UnitCode ");
        sb.append(" left join scm.ordersplitdetails on ordersplitdetails.splitorderno = processingofindent.rorderno  ");
        sb.append(" Left Join process.pro_winding On pro_winding.rorderno = processingofindent.rorderno  ");
        sb.append(" Left Join scm.SampletoRegularAuth On SampletoRegularAuth.RegularOrderNo = RegularOrder.ROrderNo ");
        sb.append(" Inner Join scm.FibreForm on FibreForm.FormCode = nvl(RegularOrder.FibreFormCode, 0)  ");
        sb.append(" inner join scm.ProcessingType on ProcessingType.ProcessCode = regularorder.ProcessTypeCode  ");
        sb.append(" Where  RegularOrder.UnitCode = 2 and  Nvl(SampletoRegularAuth.CorrectionMixing,0) = 0  ");
        sb.append(" and   ");
        sb.append(" (  ");
        sb.append(" ( ordersplitdetails.deptcode = 6 and  ordersplitdetails.baseorderno is not null and processingofindent.windcomp = 0 ) "); 
        sb.append(" or  ");
        sb.append(" ( ordersplitdetails.deptcode = 7 and  ordersplitdetails.baseorderno is not null and processingofindent.packcomp = 0 ) ");
        sb.append(" or  ");
        sb.append(" ( ordersplitdetails.baseorderno is null and windcomp = 0 )  ");
        sb.append("   ) ");

         if(!SUnit.equals("All")) {
		    sb.append("   and Unit.UnitName='"+SUnit+"' ");
         }
		 if(SSelectType.equalsIgnoreCase("Depo")){
		    sb.append("   and regularorder.rorderno like 'DM%'");
         }
		 if(SSelectType.equalsIgnoreCase("Party")){
		    sb.append("   and regularorder.rorderno not like 'DM%'");
         }
         /*
         if(!SOrderNo.equals("All")){
		    sb.append("  and regularorder.rorderno='"+SOrderNo+"' ");
         }
         */
		 if(!SProcessTypeCode.equals("All")){
		    if(SProcessTypeCode.equals("BR-C Orders")){
			    sb.append("  and ProcessingType.OEStatus = 1 ");
		    }else if(SProcessTypeCode.equals("Other than BR-C Orders")){
			    sb.append("  and ProcessingType.OEStatus = 0 ");
		    }else{
			    sb.append("  and RegularOrder.ProcessTypeCode = "+SProcessTypeCode);
			}
		}
                sb.append("  Group by  RegularOrder.RorderNo, RegularOrder.OrderDate, DeliveryDate, YarnCount.CountName, PartyMaster.PartyName, RegularOrder.Weight, Rmixir.MixingWeight,");
		sb.append("   Rmixir.MixingDate, to_char(processingofindent.simpcomp_date,'yyyymmdd') , to_char(processingofindent.spincomp_date,'yyyymmdd') , Rmixir.CorrectionMixing, SampleToRegularAuth.RealisationPer, ");
                sb.append("  PartyPriority.Name, FibreForm.FormName, PartyPriority.Code, RegularOrder.Shortage ");
                sb.append("  Order By 15 desc, RegularOrder.OrderDate , RegularOrder.RorderNo ");
		
		System.out.println(sb.toString());		
		return sb.toString();
	}	
	       


	private String getCUnitQs(){
		StringBuffer sb = new StringBuffer();
		
             sb.append("  Select distinct RegularOrder.RorderNo, RegularOrder.OrderDate, DeliveryDate, YarnCount.CountName, PartyMaster.PartyName, RegularOrder.Weight, Rmixir.MixingWeight,");
		    sb.append("   Rmixir.MixingDate, to_char(cunit_processgofindent.oecomp_date,'yyyymmdd') as OeStatusCompDate, '' as SpinningStatusCompDate, Rmixir.CorrectionMixing, SampleToRegularAuth.RealisationPer, ");
            sb.append("  min(cunit_pro_winding.entrydate) as ConeWindingStatusStartDate, 'NIL' as RegStatus_RemarksRemarks, deCode(RegularOrder.Shortage, 1, 'Shortage', PartyPriority.Name) as PartyPriority, FibreForm.FormName, PartyPriority.Code, '' as ProcessRemarks  From SCM.RegularOrder ");
		    sb.append("  Inner Join process.cunit_processgofindent On cunit_processgofindent.rorderno = RegularOrder.ROrderNo and cunit_processgofindent.entrydate>=20150101 ");
		    sb.append("  Inner join ");
		    sb.append("  (");
		    sb.append("  select distinct rorderno from ( "); 
		    sb.append("  select distinct cunit_processgofindent.rorderno from process.cunit_processgofindent ");
		    sb.append("  inner join scm.ProcessingType on ProcessingType.ProcessCode = cunit_processgofindent.ProcessCode  and ProcessingType.oestatus = 1 ");
		    sb.append("  inner join process.cunit_pro_drawing_Can_Details on cunit_pro_drawing_Can_Details.rorderno = cunit_processgofindent.rorderno and cunit_pro_drawing_Can_Details.orderfollowstatus = 1 ");
		    sb.append("  left join process.cunit_pro_oe_cheese_details on  cunit_pro_oe_cheese_details.rorderno = cunit_processgofindent.rorderno and cunit_pro_oe_cheese_details.orderfollowstatus = 0 ");
		    sb.append("  where oecomp = 0 ");
			sb.append("  and (select count(*) from process.cunit_pro_drawing_Can_Details where cunit_pro_drawing_Can_Details.rorderno = cunit_processgofindent.rorderno  and orderfollowstatus = 0) = 0 ");
 		    sb.append("  )");
		    sb.append("  ) t on t.rorderno = RegularOrder.rorderno ");
		    
		    sb.append("  Inner Join scm.YarnCount On  YarnCount.CountCode = RegularOrder.CountCode");
		    sb.append("  Inner Join scm.PartyMaster On  Partymaster.partyCode = RegularOrder.partyCode");
		    sb.append("  Inner Join scm.PartyPriority On PartyPriority.Code = nvl(PartyMaster.PriorityStatus,0) ");
		    sb.append("  Inner Join scm.RMixir On  Rmixir.OrdNo = RegularOrder.RorderNo");
		    sb.append("  Inner Join scm.Unit On Unit.UnitCode = RegularOrder.UnitCode");
		    sb.append("  left join scm.ordersplitdetails on ordersplitdetails.splitorderno = cunit_processgofindent.rorderno ");
		    sb.append("  Left Join process.cunit_pro_winding On cunit_pro_winding.rorderno = cunit_processgofindent.rorderno ");
		    sb.append("  Left Join scm.SampletoRegularAuth On SampletoRegularAuth.RegularOrderNo = RegularOrder.ROrderNo");
		    sb.append("  Inner Join scm.FibreForm on FibreForm.FormCode = nvl(RegularOrder.FibreFormCode, 0) ");
		    sb.append("  inner join scm.ProcessingType on ProcessingType.ProcessCode = regularorder.ProcessTypeCode ");
		    sb.append("  Where  RegularOrder.UnitCode in (10,12) and  Nvl(SampletoRegularAuth.CorrectionMixing,0) = 0 ");
		    sb.append(" and  ( ");
		    sb.append("    ( ordersplitdetails.deptcode = 6 and  ordersplitdetails.baseorderno is not null and cunit_processgofindent.windcomp = 0 ) ");
		    sb.append("  or ");
		    sb.append("    ( ordersplitdetails.deptcode = 7 and  ordersplitdetails.baseorderno is not null and cunit_processgofindent.packcomp = 0 )");
		    sb.append("  or ");
		    sb.append("    (ProcessingType.oestatus = 1 and ordersplitdetails.baseorderno is null and oecomp = 0 ) ");
		    sb.append("  ) ");
			
			

         if(!SUnit.equals("All")) {
		    sb.append("   and Unit.UnitName='"+SUnit+"' ");
         }
		 if(SSelectType.equalsIgnoreCase("Depo")){
		    sb.append("   and regularorder.rorderno like 'DM%'");
         }
		 if(SSelectType.equalsIgnoreCase("Party")){
		    sb.append("   and regularorder.rorderno not like 'DM%'");
         }
         /*
         if(!SOrderNo.equals("All")){
		    sb.append("  and regularorder.rorderno='"+SOrderNo+"' ");
         }
         */
		 if(!SProcessTypeCode.equals("All")){
		    if(SProcessTypeCode.equals("BR-C Orders")){
			    sb.append("  and ProcessingType.OEStatus = 1 ");
		    }else if(SProcessTypeCode.equals("Other than BR-C Orders")){
			    sb.append("  and ProcessingType.OEStatus = 0 ");
		    }else{
			    sb.append("  and RegularOrder.ProcessTypeCode = "+SProcessTypeCode);
			}
		}
		
		
             sb.append("  Group by  RegularOrder.RorderNo, RegularOrder.OrderDate, DeliveryDate, YarnCount.CountName, PartyMaster.PartyName, RegularOrder.Weight, Rmixir.MixingWeight,");
		    sb.append("   Rmixir.MixingDate, to_char(cunit_processgofindent.oecomp_date,'yyyymmdd') , Rmixir.CorrectionMixing, SampleToRegularAuth.RealisationPer, ");
            sb.append("  PartyPriority.Name, FibreForm.FormName, PartyPriority.Code, RegularOrder.Shortage ");
		
		
              sb.append("  Order By 15 desc, RegularOrder.OrderDate , RegularOrder.RorderNo ");
		
            System.out.println(sb.toString());
		return sb.toString();
	}



     public void DespatchDetails(int OnDate)
     {

            VDOrderNo       = new Vector();
            VPacking        = new Vector();
            VDespatch       = new Vector();

  try{
           String QS=" Select ROrderNo, Sum(Packing),Sum(Despatch) From"+
                     " ("+
                     " Select RegularOrder.ROrderNo,Sum(Balepacking.NetWeight) as Packing , 0 As Despatch "+
                     " From scm.RegularOrder";
                     
					if(SUnit.trim().equals("A_Unit")){
                     QS = QS + " Inner Join processa.processingofindent On processingofindent.rorderno = RegularOrder.ROrderNo and processingofindent.packcomp = 0 ";
					}
					if(SUnit.trim().equals("B_Unit")){
                     QS = QS + " Inner Join process.processingofindent On processingofindent.rorderno = RegularOrder.ROrderNo and processingofindent.packcomp = 0 ";
					}
                     
           			 //" Inner Join processa.processingofindent On processingofindent.rorderno = RegularOrder.ROrderNo and processingofindent.packcomp = 0 "+
           QS = QS + " Inner Join dispatch.Balepacking On Balepacking.Orderid = RegularOrder.Orderid "+  //and  "+
                     " Inner Join scm.Unit On Unit.UnitCode = RegularOrder.UnitCode "+
                     " Where Balepacking.Flag<>1 ";
                     
                     if(SUnit.trim().equals("A_Unit")) {
           QS = QS + " and RegularOrder.UnitCode = 1 ";
					 }                     
                     if(SUnit.trim().equals("B_Unit")) {
           QS = QS + " and RegularOrder.UnitCode = 2 ";
					 }                     
					 
                     if(!SUnit.equals("All"))
                     {
                         QS = QS + " and Unit.UnitName='"+SUnit+"'";
                     }
					 if(SSelectType.equalsIgnoreCase("Depo"))
                    {
                         QS=QS+" and regularorder.rorderno like 'DM%'";
                    }
                    if(SSelectType.equalsIgnoreCase("Party"))
                    {
                        QS=QS+" and regularorder.rorderno not like 'DM%'";
                    }

                     QS = QS + " group by RegularOrder.ROrderNo "+
                     " Union All"+
                     " select RegularOrder.ROrderNo, 0 as Packing, Sum(Balepacking.NetWeight)  As Despatch from scm.RegularOrder ";
					if(SUnit.trim().equals("A_Unit")){
                  QS = QS + " Inner Join processa.processingofindent On processingofindent.rorderno = RegularOrder.ROrderNo and processingofindent.packcomp = 0 ";
                    }
					if(SUnit.trim().equals("B_Unit")){
                  QS = QS + " Inner Join process.processingofindent On processingofindent.rorderno = RegularOrder.ROrderNo and processingofindent.packcomp = 0 ";
                    }
           QS = QS + " Inner Join dispatch.Balepacking On Balepacking.Orderid = RegularOrder.Orderid "+
                     " Inner Join scm.Unit On Unit.UnitCode = RegularOrder.UnitCode "+
                     " Where BalePacking.DespStatus=1 ";
                     
                     if(SUnit.trim().equals("A_Unit")) {
           QS = QS + " and RegularOrder.UnitCode = 1 ";
					 }
                     if(SUnit.trim().equals("B_Unit")) {
           QS = QS + " and RegularOrder.UnitCode = 2 ";
					 }
                     
                     if(!SUnit.equals("All"))
                     {
                         QS = QS + " and Unit.UnitName='"+SUnit+"'";
                     }
					 if(SSelectType.equalsIgnoreCase("Depo"))
                    {
                        QS=QS+" and regularorder.rorderno like 'DM%'";
                    }
                    if(SSelectType.equalsIgnoreCase("Party"))
                    {
                        QS=QS+" and regularorder.rorderno not like 'DM%'";
                    }
                     QS = QS + " group by RegularOrder.ROrderNo "+
                     " )"+
                     " Group By ROrderNo"+
                     " Order By ROrderNo";
                     
                System.out.println("QS-->"+QS);
               JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
               Connection     conn = jdbc.getConnection();

               Statement stat      = conn.createStatement();
               ResultSet res       = stat.executeQuery(QS);

                      while(res.next())
                      {
                         VDOrderNo.addElement(res.getString(1));
                         VPacking.addElement(res.getString(2));
                         VDespatch.addElement(res.getString(3));
                      }
			res.close();
			stat.close();
            }
            catch(Exception e)
            {
                System.out.println(e);
            }
       }

      private void getCorrectionMix()
      {
           int iCorrectionMix =0;
           double dCMixWt     =0;
           String CDate       ="";
           String COrderNo    ="";
                        
           for(int i=0; i<VOrderNo.size();i++)
           {
               iCorrectionMix = common.toInt((String)VCorrectionMix.elementAt(i));
               if(iCorrectionMix==1)
               {
                    COrderNo =(String)VOrderNo.elementAt(i);
                    dCMixWt  =common.toDouble((String)VMixingWt.elementAt(i));
                    CDate    =(String)VMixingDate.elementAt(i);
                    for(int j=0;j<VOrderNo.size();j++)
                    {
                        SOrderNo =(String)VOrderNo.elementAt(j);
                        int iStatus = common.toInt((String)VCorrectionMix.elementAt(j));
                        if(SOrderNo.equals(COrderNo) && iStatus!=1)
                        {

                           double  dWt =common.toDouble((String)VMixingWt.elementAt(j))+dCMixWt;
                           VRMixDate.set(j,CDate);
                           VMixingWt.set(j,common.getRound(dWt,3));
                        }

                    }

                VOrderNo.remove(i);
                VOrderDate.remove(i);
                VDeliveryDate.remove(i);
                VCount.remove(i);
                VParty.remove(i);
                VOrderWt.remove(i);
                VMixingWt.remove(i);
                VMixingDate.remove(i);
                VCSimplex.remove(i);
                VCSpinning.remove(i);
                VCorrectionMix.remove(i);
                VRMixDate.remove(i);
                VRealisation.remove(i);
                VSConeWinding.remove(i);
                VRemarks.remove(i);
                VProcessRemarks.remove(i);

               }

           }
      }
        
     private void setDataVectors()
     {
        VExWt     = new Vector();
        VExPer    = new Vector();
        VDueDepo  = new Vector();
        VDueMill  = new Vector();
        VDDye     = new Vector();
        VDLMix    = new Vector();
        VDRMix    = new Vector();
        VPTotal   = new Vector();
        VRSimple  = new Vector();
        VRBobbin  = new Vector();
        VRSpining = new Vector();
        VRCone    = new Vector();

        for(int i=0;i<VOrderNo.size();i++)
        {
            try
            {
                double TOrderWt=0;
                double TPack=0;
                double TDesp=0;

                String OrdNo    =(String)VOrderNo.elementAt(i);
                double dOrderWt =common.toDouble((String)VOrderWt.elementAt(i));
                double dPackWt  =getPackingWt(OrdNo);


                int iOrderDate  =common.toInt((String)VOrderDate.elementAt(i));
                int iDueDate    =common.toInt((String)VDeliveryDate.elementAt(i));
                int iMixDate    =common.toInt((String)VMixingDate.elementAt(i));
                int iCSimplex   =common.toInt((String)VCSimplex.elementAt(i));
                int iCSpinning  =common.toInt((String)VCSpinning.elementAt(i));
                int iSConeWinding =common.toInt((String)VSConeWinding.elementAt(i));
    
                TOrderWt=TOrderWt+dOrderWt;

                int iRepDate    =common.toInt(common.getServerDate());
    
                double dExWt    =dOrderWt-dPackWt;
                double dExPer   =(dExWt*100)/dOrderWt;
                int iDueDep     =common.getDateDiff(iRepDate,iDueDate);
                int iDueMill    =common.getDateDiff(iRepDate,iOrderDate);

                int iDyeDt      =0;
                int iDLMix      =common.getDateDiff(iMixDate,iOrderDate);
                int iProDate    =common.getDateDiff(iRepDate,iMixDate);
                int iCSimplex1  =common.getDateDiff(iCSimplex,iMixDate);
                int iCSpinning1 =common.getDateDiff(iCSpinning,iMixDate);
                int iSRunout    =common.getDateDiff(iCSpinning,iCSimplex);
                int iCRunout    =common.getDateDiff(iSConeWinding,iCSpinning);

                
                VExWt.addElement(common.getRound(dExWt,3));
                VExPer.addElement(common.getRound(dExPer,2));
                VDueDepo.addElement(Integer.toString(iDueDep));
                VDueMill.addElement(Integer.toString(iDueMill));
                VDDye.addElement(Integer.toString(iDyeDt));
                VDLMix.addElement(Integer.toString(iDLMix));
                VDRMix.addElement(Integer.toString(iDyeDt));
                VPTotal.addElement(Integer.toString(iProDate));
                VRSimple.addElement(Integer.toString(iCSimplex1));
                VRBobbin.addElement(Integer.toString(iDyeDt));
                VRSpining.addElement(Integer.toString(iSRunout));
                VRCone.addElement(Integer.toString(iCRunout));
            }
            catch(Exception e)
            {
              System.out.println(e);
            }
        }
     }
     private void FileWriter()
     {
            try
            {
                  FW = new FileWriter("/software/MixPrint/my.prn");  // 
                  //FW = new FileWriter("d://MixPrint/my.prn");  // 

                Head();
                Body();
                FW.close();
             }
             catch(Exception e)
             {
                System.out.println(e);
                try
                {
                     FW.close();
                }catch(Exception ex){}
             }
     }
     private void prnWriter(String Str)
     {
          try
          {
            TLine=TLine+1;
            if(TLine<53)
            {
               FW.write(Str+"\n");
            }
            else
            {
               
               iPage= iPage+1;
               FW.write(Str);
               FW.write(End);
               FW.write(SNote);
               FW.write(SNote1);
               FW.write(SNote2);
               FW.write("");
               FW.write("");
               FW.write(SNote3);
               FW.write(""+"\n");
               TLine =0;
               Head();

            }
          }
          catch(Exception e)
          {
            System.out.println(e);
          }
     }
  private double getPackingWt(String OrdNo)
  {
       for(int i=0;i<VDOrderNo.size();i++)
       {
           String SOrdNo =(String)VDOrderNo.elementAt(i);
           if(SOrdNo.equals(OrdNo))
           {
              
             return common.toDouble((String)VPacking.elementAt(i));
           }
       }
       return 0;       
  }
  private double getDespatchWt(String OrdNo)
  {
       for(int i=0;i<VDOrderNo.size();i++)
       {
           String SOrdNo =(String)VDOrderNo.elementAt(i);
           if(SOrdNo.equals(OrdNo))
           {
              
             return common.toDouble((String)VDespatch.elementAt(i));
           }
       }
       return 0;         
  }
}
