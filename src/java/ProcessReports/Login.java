/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProcessReports;
/*
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
*/
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;


/**
 *
 * @author Administrator
 */
public class Login extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    HttpSession    session;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        try {
            PrintWriter out = response.getWriter();
            
          session          = request.getSession(true);
          out.println("<html>");
          out.println("<head><title>AMARJOTHI SPINNING MILLS LIMITED</title>");
          out.println("</head>");

          out.println("<body bgcolor='#9AA8D6'>");
          out.println("<form method='POST' action='Login'>");
          out.println("<font color='#FFFFFF' size='5'><b>Process Reports</b><br><br></font>");
          out.println("<font color='#FFFFFF' size='4'><b>Amarjothi Spinning Mills Ltd</b><br></font></td>");
          out.println("<font color='#FFFFFF' size='3'>Pudusuripalayam<br></font></td>");
          out.println("<font color='#FFFFFF' size='3'>Nambiyur<br><br><br></font></td>");
          out.println("<center>");
          out.println("<h1><font color='#800000' face='Arial'><input type='submit' value='Submit' name='B1' style='font-family: Arial'></font><input type='reset' value='Reset' name='B2'></h1>");
          out.println("</center>");
          out.println("</form>");
          out.println("<hr color='000099' size='1'>");
          out.println("<p>");
          out.println("<p>");
          out.println("<center>");
          out.println("<font color='#FFFFFF' size='3'><b>Developed by The Department of Information Technology-AJSM Limited.<br></b></font>");
          out.println("<font color='#FFFFCC' size='3'>Dedicated to the people of Java and Oracle community, who invented this technology and makes us to pay<br></font>");
          out.println("<font color='#FFFFCC' size='3'>more concentration on the Business Logic.<br></font>");
          out.println("</center>");
          out.println("<hr color='000099' size='1'>");
          out.println("</body>");
          out.println("</html>");
          out.close();
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        response.sendRedirect("InvMainFrame");
    }
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
