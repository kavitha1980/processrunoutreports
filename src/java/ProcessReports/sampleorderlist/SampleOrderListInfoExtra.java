/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProcessReports.sampleorderlist;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

import ProcessReports.jdbc.*;
import ProcessReports.util.*;
import ProcessReports.rndi.*;

/**
 *
 * @author Administrator
 */
public class SampleOrderListInfoExtra extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    HttpSession         session;
    JDBCConnection1     theConnect;
    Connection          connect;
    Common              common = new Common();
    Connection          theConnection=null;
    String              SSDate,SEDate;
    String              SSearchShade, SFibreType;

    int                 iEnDate=0,LC=52,k=1;
    Vector              VOrderNo,VRefOrderNo,VOrderDate,VDueDate,VCount,VShade,VBuyerName,VOrderWt,VOrdertype,VForOrder,VMixerName;
    Vector              VLight,VBleech,VInstruction,VColor,VEnduse,VCotper,VVisper,VPolyper,VOthper,VProcessType;

    String              SLight,SBleech,SInstruction,SColor,SEnduse,SCotper,SVisper,SPolyper,SOthper;
    String              SOrderNo,SForOrder,SOrderDate,SDueDate,SCount,SShade,SBuyerName,SOrderWt,SOrdertype,SMixername,SProcessType;
    String              SStDate,SEnDate="",Empty="",Sparty="",SOrderType,SFormCode;

    boolean[]           bFibreType;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    
        try {
          response.setContentType("text/html");
          PrintWriter out  = response.getWriter();
          session          = request.getSession(false);
          String SServer   = (String)session.getValue("Server");
          session.putValue("Server",SServer);

          SStDate          =(request.getParameter("TStDate"));
          SEnDate          =(request.getParameter("TEnDate"));
          Sparty           =(request.getParameter("party"));
          SOrderType       =(request.getParameter("ordertype"));
          SSearchShade     =((request.getParameter("TShade")).trim()).toUpperCase();
          SFibreType       =  (request.getParameter("FibreType"));
          SFormCode       =  (request.getParameter("FibreForm"));

          setDataintoVector();
          
          iEnDate=common.toInt(SEnDate);
          SSDate=common.parseDate(SStDate);
          SEDate=common.parseDate(SEnDate);

          setDataintoVector();

          out.println("<html>");
          out.println("<script language='JavaScript' fptype='dynamicanimation'>");
          out.println("<!--");
          out.println("function dynAnimation() {}");
          out.println("function clickSwapImg() {}");
          out.println("//-->");
          out.println("</script>");
          out.println("</head>");
          out.println("<body bgcolor='#9AA8D6' onload='dynAnimation()' language='Javascript1.2'>");
          out.println("<base target='topmain'>");

          out.println("  <font color='#FFFFFF' size='5'><h3>Sample Order List From "+SSDate+" To "+SEDate+"</h3></font>");

          out.println(" <p>Fibre Type : "+SFibreType+"</p>");
        out.println(" <p>Fibre Form : "+SFormCode+"</p>");
        out.println(" <p>Fibre Form : "+SOrderType+"</p>");

          out.println(" <div align='right'>");
          out.println("<table border=1 width=187 height=95 bgproperties=fixed>");

          out.println("  <TR>");
          out.println("    <TD align=middle width=39 height=79 rowSpan=2>S.No</TD>");
          out.println("    <TD align=middle width=39 height=79 rowSpan=2>OrderNo</TD>");
          out.println("    <TD align=middle width=39 height=79 rowSpan=2>RefOrderNo</TD>");
          out.println("    <TD align=middle width=39 height=79 rowSpan=2>OrderDate</TD>");
          out.println("    <TD align=middle width=39 height=79 rowSpan=2>DueDate</TD>");
          out.println("    <TD align=middle width=39 height=79 rowSpan=2>Count</TD>");
          out.println("    <TD align=middle width=39 height=79 rowSpan=2>Shade</TD>");
          out.println("    <TD align=middle width=39 height=79 rowSpan=2>Name Of the Party</TD>");
          out.println("    <TD align=middle width=39 height=79 rowSpan=2>ProcessType</TD>");
          out.println("    <TD align=middle width=39 height=79 rowSpan=2>OrderWeight</TD>");
          out.println("    <TD align=middle width=39 height=79 rowSpan=2>ForOrder</TD>");
          out.println("    <TD align=middle width=38 height=79 rowSpan=2>Lighting</TD>");
          out.println("    <TD align=middle width=38 height=79 rowSpan=2>Bleaching</TD>");
          out.println("    <TD align=middle width=38 height=79 rowSpan=2>Color</TD>");
          out.println("    <TD align=middle width=38 height=79 rowSpan=2>ProcessInstruction</TD>");
          out.println("    <TD align=middle width=38 height=79 rowSpan=2>EndUse K/W/S/R</TD>");
          out.println("    <TD align=middle width=38 height=79 rowSpan=2>Order Type</TD>");
          out.println("    <TD align=middle width=50 colSpan=4 height=38>");
          out.println("      <P ");
          out.println("      align=right>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ");
          out.println("      Blend</P></TD>");
          out.println("    <TD align=middle width=126 height=79 rowSpan=2>Mixer Name</TD></TR>");
          out.println("  <TR>");
          out.println("    <TD align=middle width=13 height=41>Cot%</TD>");
          out.println("    <TD align=middle width=13 height=41>CC%</TD>");
          out.println("    <TD align=middle width=12 height=41>Poly%</TD>");
          out.println("    <TD align=middle width=12 ");
          out.println("height=41>Oth%</TD></TR>");

          bFibreType                    = null;
          bFibreType                    = new boolean[VOrderNo.size()];

          int iSlNo                     = 1;

          for(int i=0;i<VOrderNo.size();i++)
          {
               String SSampleOrderNo    = (String)VOrderNo.elementAt(i);

               bFibreType[i]            = isNepsCheck(SSampleOrderNo);

               if(!bFibreType[i])
               {
                    continue;
               }

               int j=iSlNo++;

               out.println("  <tr>");
               out.println("<td>"+j+"</td>");
               out.println("  <td>"+VOrderNo.elementAt(i)+"</td>");
               out.println("  <td>"+VRefOrderNo.elementAt(i)+"</td>");
               out.println("  <td>"+common.parseDate((String)VOrderDate.elementAt(i))+"</td>");
               out.println("  <td>"+common.parseDate((String)VDueDate.elementAt(i))+"</td>");
               out.println("  <td>"+VCount.elementAt(i)+"</td>");
               out.println("  <td>"+VShade.elementAt(i)+"</td>");
               out.println("  <td>"+VBuyerName.elementAt(i)+"</td>");
               out.println("  <td>"+VProcessType.elementAt(i)+"</td>");
               out.println("  <td>"+VOrderWt.elementAt(i)+"</td>");
               out.println("  <td>"+VForOrder.elementAt(i)+"</td>");

               out.println("  <td>"+VLight.elementAt(i)+"</td>");
               out.println("  <td>"+VBleech.elementAt(i)+"</td>");
               out.println("  <td>"+VColor.elementAt(i)+"</td>");
               out.println("  <td>"+VInstruction.elementAt(i)+"</td>");
               out.println("  <td>"+VEnduse.elementAt(i)+"</td>");
               out.println("  <td>"+VOrdertype.elementAt(i)+"</td>");
               out.println("  <td>"+VCotper.elementAt(i)+"</td>");
               out.println("  <td>"+VPolyper.elementAt(i)+"</td>");
               out.println("  <td>"+VVisper.elementAt(i)+"</td>");
               out.println("  <td>"+VOthper.elementAt(i)+"</td>");
               out.println("  <td>"+VMixerName.elementAt(i)+"</td>");

               out.println("  </tr>");
          }

          out.println("</table>");
          out.println(" </div>");
          out.println("  <center>");
          out.println("  </center>");
          out.println("</form>");
          out.println("</body>");
          out.println("</html>");
          out.close();

          printFile();

        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

       private void printFile()
       {
           int t2=0;
           String stDate1=common.parseDate(SStDate);
           String enDate1=common.parseDate(SEnDate);
           String empty="";
          try
            {
            FileWriter FW;
//                 FW = new FileWriter("//"+SCRIPTHOST+"/d/"+SCRIPTFOLDER+"/src/Temp/Sampleorderlist.prn");

            FW = new FileWriter("/software/MixPrint/Sampleorderlist.prn");

            String Page    = "Page No    :";
            String SPitch="M\n";
            FW.write(SPitch+Page+k+"\n");
            //FW.write(Page+k+"\n");
            FW.write("Company     : AMARJOTHI SPINNING MILL \n");
            FW.write("Document    : Sample Order List Report-From "+stDate1+" To "+enDate1+"\n");
            FW.write("Fibre Type  : "+SFibreType+" \n\n");

              /*
               String s1  = "|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n";
               String s2  = "|     |          |          |          |       |          |                                |       |         |          |         |       |           |        |     |                   |             |\n";
               String s3  = "|     |          |          |          |       |          |                                |       |         |          |         |       |           | Enduse |Order|        Blend      |             |\n";
               String s4  = "|S.No | Order No |Order Date| Due Date | Count |  Shade   |       Name of the Buyer        |OrderWt| ForOrder| Lighting |Bleeching| Color | Process   |k/W/S/R |Type |-------------------|  Mixer Name |\n"; 
               String s5  = "|     |          |          |          |       |          |                                |       |         |          |         |       |Instruction|        |     |Cot%|CC% |Pol%|Oth%|             |\n";
               String s6  = "|     |          |          |          |       |          |                                |       |         |          |         |       |           |        |     |    |    |    |    |             |\n";
               String s7  = "|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n";

               String sl  = "-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n";

               String line= "|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n";
              */

               String s1  = "|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n";
               String s2  = "|     |          |          |          |       |          |                                |               |       |         |          |         |       |           |        |     |                   |             |\n";
               String s3  = "|     |          |          |          |       |          |                                |               |       |         |          |         |       |           | Enduse |Order|        Blend      |             |\n";
               String s4  = "|S.No | Order No |Order Date| Due Date | Count |  Shade   |       Name of the Buyer        |Processing Type|OrderWt| ForOrder| Lighting |Bleeching| Color | Process   |k/W/S/R |Type |-------------------|  Mixer Name |\n"; 
               String s5  = "|     |          |          |          |       |          |                                |               |       |         |          |         |       |Instruction|        |     |Cot%|CC% |Pol%|Oth%|             |\n";
               String s6  = "|     |          |          |          |       |          |                                |               |       |         |          |         |       |           |        |     |    |    |    |    |             |\n";
               String s7  = "|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n";

               String sl  = "-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n";

               String line= "|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n";


               FW.write(s1);
               FW.write(s2);
               FW.write(s3);
               FW.write(s4);
               FW.write(s5);
               FW.write(s6);
               FW.write(s7);


               double dTotalOrderWt=0,GrantTotOrderWt=0;
               double dTotalDespWt=0;
               double TotBalance1=0;
               String  sbGT,sbV,sno="";
               int flag1=0;
               int linecount=7;
               int iSlNo = 1;

               for(int i=0;i<VOrderNo.size();i++)
               {
                    if(!bFibreType[i])
                    {
                         continue;
                    }

                         int j     = iSlNo++;
                         sno       = String.valueOf(j);

                      t2=1;


                      SOrderNo       = (String)VOrderNo.elementAt(i);
                      SOrderDate     = (String)VOrderDate.elementAt(i);
                      SDueDate       = (String)VDueDate.elementAt(i);
                      SBuyerName     = (String)VBuyerName.elementAt(i);
                      SProcessType   = (String)VProcessType.elementAt(i);

                      SCount         = (String)VCount.elementAt(i);
                      SShade         = (String)VShade.elementAt(i);
                      SOrderWt       = (String)VOrderWt.elementAt(i);
                      SForOrder      = (String)VForOrder.elementAt(i);
                      SOrdertype     = (String)VOrdertype.elementAt(i);

                      SLight         = (String)VLight.elementAt(i);
                      SBleech        = (String)VBleech.elementAt(i);
                      SInstruction   = (String)VInstruction.elementAt(i);
                      SColor         = (String)VColor.elementAt(i);
                      SEnduse        = (String)VEnduse.elementAt(i);
                      SCotper        = (String)VCotper.elementAt(i);
                      SVisper        = (String)VVisper.elementAt(i);
                      SPolyper       = (String)VPolyper.elementAt(i);
                      SOthper        = (String)VOthper.elementAt(i);
                      SMixername     = (String)VMixerName.elementAt(i);



                      sbV =   "|"+common.Rad(sno,5)+
                              "|"+common.Pad(SOrderNo,10)+
                              "|"+common.Pad(common.parseDate(SOrderDate),10)+
                              "|"+common.Pad(common.parseDate(SDueDate),10)+
                              "|"+common.Rad(SCount,7)+
                              "|"+common.Pad(SShade,10)+
                              "|"+common.Pad(SBuyerName,32)+
                              "|"+common.Pad(SProcessType,15)+
                              "|"+common.Rad(SOrderWt,7)+
                              "|"+common.Pad(SForOrder,9)+
                              "|"+common.Pad(SLight,10)+
                              "|"+common.Pad(SBleech,9)+
                              "|"+common.Pad(SColor,7)+
                              "|"+common.Pad(SInstruction,11)+
                              "|"+common.Pad(SEnduse,8)+
                              "|"+common.Pad(SOrdertype,5)+
                              "|"+common.Pad(SCotper,4)+
                              "|"+common.Pad(SVisper,4)+
                              "|"+common.Pad(SPolyper,4)+
                              "|"+common.Pad(SOthper,4)+
                              "|"+common.Pad(SMixername,13)+"|"+"\n";


               //FW.write(sbV);


                    if(iSlNo<LC)
                    {
                         FW.write(sbV);
                         int lc=iSlNo;
                    }

                    else if(iSlNo==LC)
                    {
                         k++;
                         FW.write(sl);
                         FW.write("\n");
                         FW.write(SPitch+Page+k+"\n");
                         FW.write("Company  :AMARJOTHI SPINNING MILL \n");
                         FW.write("Document :Sample Order List Report-From "+stDate1+" To "+enDate1+"\n");
                         FW.write("Fibre Type  : "+SFibreType+" \n\n");

                         FW.write(s1);
                         FW.write(s2);
                         FW.write(s3);
                         FW.write(s4);
                         FW.write(s5);
                         FW.write(s6);
                         FW.write(s7);
                         FW.write(sbV);
                         //FW.write(sl);
                         if(i==VOrderNo.size()-1)
                         {

                         }
                         LC=LC+52;
                         }

               if(flag1==1)
               linecount=linecount+3;
               else
               linecount=linecount+1;

               flag1=0;
               }
     
                         FW.write(sl);
                         LC=52;
                         k=1;
     
               FW.close();
     
            } 
            catch(Exception e)
            {
            e.printStackTrace();
            }

       }

    public void setDataintoVector()
    {

         VOrderNo      =new Vector();
         VRefOrderNo   =new Vector();
         VOrderDate    =new Vector();
         VDueDate      =new Vector();
         VBuyerName    =new Vector();
         VCount        =new Vector();
         VShade        =new Vector();
         VOrderWt      =new Vector();
         VForOrder     =new Vector();
         VOrdertype    =new Vector();
         VMixerName    =new Vector();

         VLight        =new Vector();
         VBleech       =new Vector();
         VInstruction  =new Vector();
         VColor        =new Vector();
         VEnduse       =new Vector();
         VCotper       =new Vector();
         VVisper       =new Vector();
         VPolyper      =new Vector();
         VOthper       =new Vector();
         VProcessType  =new Vector();



         String empty="";
         //String QS="";

          /*
          String  QS =  " select distinct sorderno,orderdate,deliverydate,yarncount.countname,ordershade.SHADENAME,partymaster.partyname,weight,fororder,ordertype.name,Reforderno,rawuser.username"+
                        " from sampleorder"+
                        " left  join partymaster on partymaster.partycode=sampleorder.partycode"+
                        " left  join yarncount on yarncount.countcode=sampleorder.countcode"+
                        " left  join ordershade on ordershade.id=sampleorder.shadeid"+
                        " left join blend on blend.blendcode=sampleorder.blendcode"+
                        " left join ordertype on ordertype.code=sampleorder.ordertype  "+
                        " left join smixir on smixir.ordno=sampleorder.sorderno "+
                        " left join rawuser on rawuser.usercode=smixir.Entryusercode "+
                        " where  sampleorder.orderdate>=20060301 and sampleorder.orderdate>="+common.pureDate(SStDate)+" and sampleorder.orderdate<="+common.pureDate(SEnDate)+" "+
                        " and sampleorder.orderdate>0 and sampleorder.DELIVERYDATE>0 ";
          */


          String  QS =  " select distinct sorderno,orderdate,deliverydate,yarncount.countname,ordershade.SHADENAME,partymaster.partyname,weight,fororder,ordertype.name,Reforderno,rawuser.username,"+
                        " lightsource.sourcename as lighting,nvl(bleachingtype.name,'') as Bleeching,nvl(color.colorname,'') as colorname,Instruction.instructionname,enduse.prefixname,"+
                        " cotper as CotPer, "+
                        " polyper as PolPer, "+
                        " visper as VisPer, "+
                        " otherper as otherPer, "+
                        " processingtype.processtype "+
                        " from  sampleorder"+
                        " left  join partymaster on partymaster.partycode=sampleorder.partycode"+
                        " left  join yarncount on yarncount.countcode=sampleorder.countcode"+
                        " left  join ordershade on ordershade.id=sampleorder.shadeid"+
                        " left join blend on blend.blendcode=sampleorder.blendcode"+
                        " left join ordertype on ordertype.code=sampleorder.ordertype  "+
                        " left join smixir on smixir.ordno=sampleorder.sorderno "+
                        " left join rawuser on rawuser.usercode=smixir.Entryusercode "+
                        " left join enduse on enduse.endusecode=sampleorder.endusecode "+
                        " left join lightsource on lightsource.sourcecode=sampleorder.lightcode "+
                        " left join Bleachingtype on Bleachingtype.code=sampleorder.Bleachingtypecode "+
                        " left join color on color.colorcode=sampleorder.colorcode "+
                        " left join instruction on instruction.instructioncode=sampleorder.instructioncode"+
                        " left join processingtype on processingtype.processcode=sampleorder.processtypecode "+
                        " left join FibreForm on Fibreform.FormCode=SampleOrder.FibreFormCode"+
                        " where  sampleorder.orderdate>=20060301 and sampleorder.orderdate>="+common.pureDate(SStDate)+" and sampleorder.orderdate<="+common.pureDate(SEnDate)+" "+
                        " and sampleorder.orderdate>0 and sampleorder.DELIVERYDATE>0 ";



                        if(!SOrderType.equals("ALL"))
                        QS=QS+" and  ordertype="+SOrderType;
                      
                        if(!SFormCode.equals("All"))
                        QS=QS+" and  SampleOrder.FibreFormCode="+SFormCode;


                        if(!Sparty.equalsIgnoreCase("ALL"))
                        QS=QS+" and  partymaster.partyName='"+Sparty+"' ";

                        if(SSearchShade.length()>0)
                              QS=QS+" and  (Instr(SampleOrder.RefOrderNo,'"+SSearchShade+"')>0 or Instr(SampleOrder.Instruction,'"+SSearchShade+"')>0) ";

                        QS=QS+" Order by 2,1,1";
        
                      System.out.println("Query Mixing Report "+QS);

          

          try
          {
               theConnect=JDBCConnection1.getJDBCConnection1();
               connect      =theConnect.getConnection();
               Statement st             =connect.createStatement();

               ResultSet rs             =st.executeQuery(QS);
               while(rs.next())
               {

                       VOrderNo      .addElement(rs.getString(1));
                       VOrderDate    .addElement(rs.getString(2));
                       VDueDate      .addElement(rs.getString(3));
                       VCount        .addElement(rs.getString(4));
                       VShade        .addElement(rs.getString(5));
                       VBuyerName    .addElement(rs.getString(6));
                       VOrderWt      .addElement(rs.getString(7));
                       VForOrder     .addElement(rs.getString(8));
                       VOrdertype    .addElement(rs.getString(9));
                       VRefOrderNo   .addElement(rs.getString(10));
                       VMixerName    .addElement(rs.getString(11));
                       VLight        .addElement(rs.getString(12));
                       VBleech       .addElement(rs.getString(13));
                       VColor        .addElement(rs.getString(14));
                       VInstruction  .addElement(rs.getString(15));
                       VEnduse       .addElement(rs.getString(16));
                       VCotper       .addElement(rs.getString(17));
                       VPolyper      .addElement(rs.getString(18));
                       VVisper       .addElement(rs.getString(19));
                       VOthper       .addElement(rs.getString(20));
                       VProcessType  .addElement(rs.getString(21));      



               
               }
               rs.close();
               st.close();
           }
           catch(Exception ex)
           {
               ex.printStackTrace();
               System.out.println(ex);
           }
     }

     // Get Mixing Details...

     private boolean isNepsCheck(String SSampleOrderNo)
     {
          if(SFibreType.equals("All"))
          {
               return true;
          }

          try
          {                        
               ArrayList AFibreName= null;
               AFibreName          = new ArrayList();
               AFibreName          . clear();

               // Get Fibre Names..

               PreparedStatement thePre = null;
               ResultSet rs             = null;

               thePre              = connect.prepareCall(getMixQS(SSampleOrderNo));
               rs                  = thePre.executeQuery();

               while(rs.next())
               {
                    AFibreName     . add(common.parseNull(rs.getString(1)));
               }

               rs                  . close();
               thePre              . close();

               // Check is it Neps Order..

               boolean bFlag       = false;

               for(int i=0; i<AFibreName.size(); i++)
               {
                    String SFibre  = ((String)AFibreName.get(i)).toUpperCase();

                    int iIndex     = SFibre.indexOf(SFibreType);

                    if(iIndex != -1)
                    {
                         bFlag     = true;
                    }
               }

               if(AFibreName.size() == 0)
               {
                    bFlag          = false;
               }

               AFibreName          . clear();

               // set Null..

               AFibreName          = null;
               thePre              = null;
               rs                  = null;

               return bFlag;
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
               return false;
          }
     }

     private String getMixQS(String SSampleOrderNo)
     {
          StringBuffer SB          = null;
          SB                       = new StringBuffer();
          
          SB.append(" Select FibreName from  ");
          SB.append(" (   ");
          SB.append(" select t1.FibreCode,Fibre.BaseCode,     ");
          SB.append(" concat(concat(concat(concat(Fibre.FibreName,decode(Fibre.Denier,' ',' ',','||Fibre.Denier)),  ");
          SB.append(" decode(Fibre.FibreLength,' ', ' ','*'||Fibre.FibreLength)),     ");
          SB.append(" decode(t1.MixLotNo,'NONE','',t1.MixLotNo)),' '||FibreForm.FormName)  as FibreName,     ");
          SB.append(" decode(t1.MixLotName,'NONE','',t1.MixLotName) as MixLotName,     ");
          SB.append(" substr(Supplier.Name,0,5) as SupplierName,    MRAt,Fibre.ColorCode,FibreIndex,Fibre.Denier,Fibre.FibreLength from  ");
          SB.append(" ( ");
          SB.append(" select distinct Receipt.AcCode as AcCode,t.FibreCode as FibreCode,Receipt.MixLotNo   ");
          SB.append(" as MixLotNo,MixLot.MixLotName as MixLotName,t.FibreIndex as FibreIndex,t.Denier, ");
          SB.append(" t.FibreLength from  ");
          SB.append(" ( ");
          SB.append(" Select  max(Id) as Id,Fibre.FibreCode,FibreType.FibreIndex,denier,FibreLength from Fibre     ");
          SB.append(" Left join Receipt on Receipt.FibreCode=Fibre.FibreCode     ");
          SB.append(" Left join FibreType on FibreType.FibreTypeCode= Fibre.FibreTypeCode     ");
          SB.append(" group by Fibre.FibreCode,FibreType.FibreIndex,Fibre.denier,Fibre.FibreLength ");
          SB.append(" )t     ");
          SB.append(" Left join Receipt on t.Id= Receipt.Id     ");
          SB.append(" Left join MixLot on MixLot.MixLotNo=Receipt.MixLotNo     ");
          SB.append(" and t.FibreCode=Receipt.FibreCode     ");
          SB.append(" )t1      ");
          SB.append(" Left join Fibre on Fibre.FibreCode=t1.FibreCode      ");
          SB.append(" Left join SBlenr on SBlenr.FibCd=t1.fibreCode      ");
          SB.append(" Left join FibreForm on FibreForm.FormCode=SBlenr.FibreFormCode ");
          SB.append(" Left join Supplier on t1.AcCode=Supplier.AcCode      ");
          SB.append(" where SBLENR.MIXNO = (Select MixNo from SMIXIR where OrdNo='"+SSampleOrderNo+"')  ");
          SB.append(" and  ");
          SB.append(" (t1.FibreCode like 'J%' or t1.FibreCode like 'A%'  ");
          SB.append(" or t1.FibreCode like 'S%' or t1.FibreCode like 'M%')      ");
          SB.append(" union All      ");
          SB.append(" select MixLotMaster.FibreCode,' '  as BaseCode,concat(concat(concat(concat(MixLotMaster.MixLotNo,','||MixLot.MixLotName), ");
          SB.append(" ','||WebType.WebTypeName),','||Noils||'%'),' '||FibreForm.FormName) as Fibrename,  ");
          SB.append(" ' ' as MixLotName,' ' as SupplierName,     MRat,0 as ColorCode,7 as Fibreindex, ");
          SB.append(" '' as denier,'' as FibreLength  from  ");
          SB.append(" (((MixLotMaster   ");
          SB.append(" Inner join MixLot on MixLot.MixLotNo=MixLotMaster.MixLotNo)      ");
          SB.append(" Inner join WebType on WebType.WebTypeCode=MixLotMaster.WebType)      ");
          SB.append(" inner join SBlenr on Sblenr.FibCd=MixLotMaster.FibreCode)      ");
          SB.append(" Left join FibreForm on FibreForm.FormCode=SBlenr.FibreFormCode      ");
          SB.append(" where SBLENR.MIXNO =(Select MixNo from SMIXIR where OrdNo='"+SSampleOrderNo+"')      ");
          SB.append(" union All      ");
          SB.append(" select WasteCodeALlocation.FibreCode as FibreCode,' '  as BaseCode, ");
          SB.append(" concat(concat(WasteVariety.VarietyName,','||WasteCodeAllocation.LotNo),'  ");
          SB.append(" '||FibreForm.FormName) as FibreName,      ");
          SB.append(" ' ' as MixLotName,' ' as SupplierName,     MRat,0 as ColorCode,5 as Fibreindex, ");
          SB.append(" '' as denier,'' as FibreLength     from WasteCodeAllocation      ");
          SB.append(" inner join WasteVariety on WasteVariety.VarietyCode=WasteCodeAllocation.VarietyCode      ");
          SB.append(" Inner join SBlenr on  Sblenr.FibCd=WasteCodeAllocation.FibreCode      ");
          SB.append(" Left join FibreForm on FibreForm.FormCode=SBlenr.FibreFormCode      ");
          SB.append(" where SBLENR.MIXNO =(Select MixNo from SMIXIR where OrdNo='"+SSampleOrderNo+"')      ");
          SB.append(" Union All      ");
          SB.append(" select Fibre.FibreCode,' '  as BaseCode,concat(Fibre.FibreName,' '||FibreForm.FormName),      ");
          SB.append(" concat(concat(Fibre.MixLotNo,','||MixLot.MixLotName),','||Fibre.Noils),' ' as SupplierName,   ");
          SB.append("  MRAt ,Fibre.ColorCode,FibreType.FibreIndex,Fibre.Denier,Fibre.FibreLength     from Fibre   ");
          SB.append(" Inner join SBlenr on SBlenr.FibCd=Fibre.fibreCode      ");
          SB.append(" Left join FibreForm on FibreForm.FormCode=SBlenr.FibreFormCode      ");
          SB.append(" Left join MixLot on MixLot.MixLotNo=Fibre.MixLotNo      ");
          SB.append(" Left join FibreType on FibreType.FibreTypeCode= Fibre.FibreTypeCode      ");
          SB.append(" where SBLENR.MIXNO =(Select MixNo from SMIXIR where OrdNo='"+SSampleOrderNo+"')  ");
          SB.append(" and fibre.FibreCode not like 'J%' ");
          SB.append(" and fibre.FibreCode not like 'A%' ");
          SB.append(" and fibre.FibreCode not like 'S%' ");
          SB.append(" and fibre.FibreCode not like 'M%' ");
          SB.append(" ) ");
          SB.append(" order by 1 ");

          return SB.toString();
     }
}
