/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProcessReports.sampleorderlist;

import ProcessReports.util.*;
import java.io.*;
import java.sql.*;           
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import ProcessReports.jdbc.*;
import ProcessReports.rndi.*;


/**
 *
 * @author Administrator
 */
public class SampleOrderListCrit extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    HttpSession     session;
    Vector          VOrderType;    
    Vector          VOrderTypeCode,VPartyName,VPartyCode,VFormCode,VFormName;

    JDBCConnection1 theConnect;
    Connection      connect;
    Common          common= new Common();

    ArrayList      ANepsType, ANepsDetail;
     
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
          response.setContentType("text/html");
          PrintWriter out = response.getWriter();
          setDataintoVector();
          out.println("<html>");
          out.println("<head>");

          out.println("<SCRIPT LANGUAGE='JavaScript'>");
          out.println("function func1(xyz,a1)");
          out.println("{");
          out.println(" typedstring = a1.value;");
          out.println(" for(ctr=0;ctr<xyz.options.length;ctr++)");
          out.println(" {");
          out.println(" str=xyz.options[ctr].value;");
          out.println(" if(typedstring.toUpperCase()==str.substring(0,typedstring.length))");
          out.println(" break;");
          out.println(" }");
          out.println(" if (ctr < xyz.options.length)");
          out.println(" xyz.options[ctr].selected=true;");
          out.println(" return;");
          out.println("}");
          out.println("</Script>");    
          out.println("</head>");

          /*
               out.println("<script language='JavaScript' fptype='dynamicanimation'>");
               out.println("<!--");
               out.println("function dynAnimation() {}");
               out.println("function clickSwapImg() {}");
               out.println("//-->");
               out.println("</script>");
               //out.println("<script language='JavaScript1.2' fptype='dynamicanimation' src='file://///"+SCRIPTHOST+"/D/Program%20Files/Microsoft%20Office/Office/fpclass/animate.js'>");
               out.println("</script>");
          */


          out.println("<body bgcolor='#9AA8D6'>");
          out.println("");
          out.println("<base target='topmain'>");
          out.println("<form method='GET' action='SampleOrderListInfoExtra'>");
       try
       {
          out.println("<table>");

          out.println("<tr>");
          out.println("     <td><b>From </b></td>");
          out.println("     <td><b>To  </b></td>");
          out.println("     <td></td>");
          out.println("    <td></td>");
          out.println("<tr>");


          out.println("<tr>");
          out.println("     <td><input type='text' name='TStDate' size='15'></td>");
          out.println("     <td><input type='text' name='TEnDate' size='15'></td>");
          out.println("<tr>");

          out.println("<tr>");
          out.println("      <td><font color='#FFFF66'><b>OrderType: </b></font></td>");
          out.println(" <td>     <select size=1 name='ordertype'>");
          out.println("      <option value='ALL'>ALL</option>");
          for(int i=0;i<VOrderType.size();i++) 
          {
               out.println("      <option value='"+VOrderTypeCode.elementAt(i)+"'>"+VOrderType.elementAt(i)+"</option>");
          }
          out.println("  </td>    </select>");
          out.println("</tr>");


          out.println("<tr>");
          //out.println("<td><input type='text' name='TOrderNo' size='15' onKeyUp='func1(this.form.orderno,this.form.TOrderNo)'></td>");
          out.println("      <td><font color='#FFFF66'><b>PartyName:  </b></font> <input type='text' name='TParty' size='15' onKeyUp='func1(this.form.party,this.form.TParty)'> </td>");
          out.println(" <td>     <select size=1 name='party'>");
          out.println("      <option value='ALL'>ALL</option>");
          for(int i=0;i<VPartyCode.size();i++) 
          {
               out.println("      <option value='"+VPartyName.elementAt(i)+"'>"+VPartyName.elementAt(i)+"</option>");
          }
          out.println("  </select></td>    ");
          out.println("</tr>");

          out.println("<tr>");
          out.println("     <td><font color='#FFFF66'><b>Shade for Search : </b></font></td>");
          out.println("     <td><input type='text' name='TShade' size='15'></td>");
          out.println("<tr>");

          out.println("<tr>");
          out.println("     <td><font color='#FFFF66'><b>Fibre Type : </b></font></td>");
          out.println("     <td>");
          out.println("       <select size=1 Name='FibreType'> ");
          out.println("            <option value='All'>All</option>");

          for(int i=0; i<ANepsType.size(); i++)
          {
               out.println("  <option value='"+ANepsType.get(i)+"'>"+ANepsDetail.get(i)+"</option>");
          }

          out.println("       </select>");
          out.println("     </td>");
          out.println("<tr>");
       
          out.println("<tr>");
          out.println("     <td><font color='#FFFF66'><b>FibreForm : </b></font></td>");
          out.println("     <td>");
          out.println("       <select size=1 Name='FibreForm'> ");
          out.println("            <option value='All'>All</option>");

          for(int i=0; i<VFormCode.size(); i++)
          {
               out.println("  <option value='"+VFormCode.elementAt(i)+"'>"+VFormName.elementAt(i)+"</option>");
          }

          out.println("       </select>");
          out.println("     </td>");
          out.println("<tr>");

          out.println("<tr>");
          out.println("     <td><input type='submit' value='Submit' name='B1' style='color: #003300; font-size: 10pt; font-weight: bold'></td>");
          out.println("     <td><input type='reset' value='Reset' name='B2' style='color: #800000; font-size: 10pt; font-weight: bold'></td>");
          out.println("</tr>");

          out.println("</table>");
          out.println("</form>");
          out.println("</body>");
          out.println("</html>");
          }
            catch(Exception e)
            {
            e.printStackTrace();
            }
          out.close();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    public void setDataintoVector()
    {
        VOrderType      = new Vector();
        VOrderTypeCode  = new Vector();
        VPartyName      = new Vector();
        VPartyCode      = new Vector();
        VFormCode       = new Vector();
        VFormName       = new Vector();
        ANepsType       = null;
        ANepsDetail     = null;
        ANepsType       = new ArrayList();
        ANepsDetail     = new ArrayList();
        ANepsType       . clear();
        ANepsDetail     . clear();

        try {
                        theConnect  = JDBCConnection1.getJDBCConnection1();
                        connect     = theConnect.getConnection();
            Statement   st          = connect.createStatement();
            ResultSet   rs;
                        rs          = st.executeQuery("select distinct name,code from ordertype order by 2");
            while(rs.next()) {
                VOrderType      . addElement(rs.getString(1));
                VOrderTypeCode  . addElement(rs.getString(2));
            }
            rs  . close();
            rs  = st.executeQuery("select distinct partyname,partycode from partymaster order by 1");

            while(rs.next()) {
                VPartyName  . addElement(rs.getString(1));
                VPartyCode  . addElement(rs.getString(2));
            }
            rs  . close();
            rs  = st.executeQuery("select distinct FormCode,FormName from FibreForm order by 1");

            while(rs.next())
            {
                VFormCode   . addElement(rs.getString(1));
                VFormName   . addElement(rs.getString(2));
            }
            rs  . close();
            st  . close();
            PreparedStatement   thePre  = connect.prepareCall("Select NepsType, NepsDetail from NepsOrderType Order by OrderBy");
                                rs      = thePre.executeQuery();
            while(rs.next())    {
                ANepsType   . add(rs.getString(1));
                ANepsDetail . add(rs.getString(2));
            }
            rs              . close();
            thePre          . close();
        }catch(Exception ex)    {
            System.out.println(ex);
            ex.printStackTrace();
        }
    }
}