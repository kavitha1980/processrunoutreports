/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProcessReports;

import java.io.*;
import java.util.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import ProcessReports.util.*;
import ProcessReports.jdbc.*;
import ProcessReports.rndi.*;
import ProcessReports.regularorder.*;
import java.util.Collections;
import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfBorderDictionary;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.RadioCheckField;
import com.itextpdf.text.pdf.PushbuttonField;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.draw.LineSeparator;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.*;
import javax.swing.*;
import javax.swing.border.TitledBorder;

import java.util.Vector;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Administrator
 */
public class YarnTypeWise_CountQty extends HttpServlet {

    java.sql.Connection theConnection = null; 
    Common              common;
    JDBCConnection      theConnect;
    Connection          connect;
    HttpSession         session;
    int                 iEnDate=0;
    int                 iStDate=0;
    String             SUnit ="";
    String             bgColor,bgHead,bgUom,bgBody,fgHead,fgUom,fgBody;
    String             SStDate,SEnDate;
    Vector             VOrderDate,VOrderNo,VPartyName,VOrderWt,VCount,VRMOrder,VShade,VCotPer,VVisPer,VPolyPer,VDepth,VBasicOrderNo,VStatus,VOrderType,VEntryDate,VLightSource,VRemarks,VBlendChanged,VOrderProcessType,VPartyCode;
    Vector             VSampleOrderNo,VYarnType;
    java.util.List     ShortageList = new java.util.ArrayList(); 
    Vector             VCountVariation, VSpirality, VPerfectShadeMatching, VAvoidNeps, VDyeingMethod;
    Vector             VFastnessStandard, VDischargePrintOrder, VOrderRequirementLevel, VShortageQtyAccepted, VMatchingOrderNo, VBinName;

    Vector              VGroupCode,VFormaName,VFibreFormGroupCode,VFibreFormGroupName,VOrderCount;

    int   iPDLine=0;
    int   iDDLine=0;
    String DotLine="";
    int    iNineDays=0;
    int    iThreeDays=0;

    double dPartyWtL=0;
    double dPartyWtH=0;
    double dDepoWtL=0;
    double dDepoWtH=0;
    double dMixingWt=0;

    Vector  VFibreCode,VWeight,VDyeOrderNo,VDeliveryDate,VFibreRate,VFRDate,VFibreStatus;
    Vector  VLastDate;
    Vector  VTDate,VFDDate,VSDate,VFIDate;
    Vector   VRDOrderNo,VRDepth;

    FileWriter FW,FText;
    String SLine ="";
    String SLine1 ="";
    String SServer;
    String Empty="";
    String SEnd="";

    String SHead="";
    int iTLine=0;
    int iPage=1,iCount=0,iCount1=0,iRefNo=0;
    Vector VDSOrderNo,VDDepth;
    String Head1,Head2,Head3,Head4,Head5;
    boolean  bDepo;
    boolean  bParty;
    Vector VUnitCode,VUnitName;
    int iUnitCode=0;
    Vector VSampleNo,VSampleDepth;
    Vector VRegOrderNo ,VRegRMOrderNo,VOrderStatus;
    Vector VDepthWt,VDepthOrd,VDepthShadeOrd;

    Vector    theGIVector,theReceiptVector;

    int       iDyeingStatus = 0;

    Vector vNewBlendPer,vNewBlendName;

    Connection theSCMConnection;

    String SProcessTypeCode, SProcessTypeName;
    String SShortageTypeCode, SShortageTypeName;

    String    SFDDelayDays, SSampleDays;
    String    SOrderType,SFibredyeing;
    String    Sordertype;
    String    Sfrmweight = "";
    String    Stoweight  = "";
    String    SRepCode   = "";
    String    SToday ="";

    int iColumnCount        = 0;
    int[]                   iColWidth   = {25,15,25,40,15,25,30,25,15,15,20,20,15,15};
    int[]                   iColWidth1  ;
    //int[]                   iColWidth1  = {15,15,15,40,15,25,30,25,15,15,20,20,15,15};
    PdfPCell                c1;
    private static Font     bigbold     = FontFactory.getFont("TIMES_ROMAN", 12, Font.BOLD);
    private static Font     bigboldH    = FontFactory.getFont("TIMES_ROMAN", 6, Font.BOLD);
    private static Font     mediumbold  = FontFactory.getFont("TIMES_ROMAN", 6, Font.NORMAL);
    private static Font     mediumbold1 = FontFactory.getFont("TIMES_ROMAN", 6, Font.BOLD);
    private static Font     smallnormal = FontFactory.getFont("TIMES_ROMAN", 9, Font.NORMAL);
//    private static Font     smallnormal1 = FontFactory.getFont("TIMES_ROMAN", 6, Font.NORMAL);
    private static Font     smallbold   = FontFactory.getFont("TIMES_ROMAN", 9, Font.NORMAL);
    private static Font     smallboldH1 = FontFactory.getFont("TIMES_ROMAN", 8, Font.BOLD);
    private static Font     smallboldR  = FontFactory.getFont("TIMES_ROMAN", 10, Font.BOLD);
    private static Font     smallbold1  = FontFactory.getFont("TIMES_ROMAN", 10, Font.BOLD);
//    int             iPage       =  1;
    
    private static final    String  FONT_LOCATION   = "./fonts/arial.ttf";
    public static           Font    fontArial       = FontFactory.getFont(FONT_LOCATION, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                
    Document                document;
    PdfPTable               table,table1;

    String                  SFileName="",  SFileOpenPath="",  SFileWritePath="";
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        common = new Common();
        bgColor = common.bgColor;
        bgHead  = common.bgHead;
        bgUom   = common.bgUom;
        bgBody  = common.bgBody;
        fgHead  = common.fgHead;
        fgUom   = common.fgUom;
        fgBody  = common.fgBody;

          response.setContentType("text/html");
          session             = request.getSession(false);
          SServer     	      = (String)session.getValue("Server");
          session             . putValue("Server",SServer);

          iEnDate  	      =  common.toInt(common.pureDate((String)request.getParameter("TEnDate")));
	  SToday	      =  request.getParameter("TEnDate");	

          SUnit               = request.getParameter("Unit");
          SOrderType          = request.getParameter("orderType");
          SFibredyeing        = request.getParameter("Fibredyeing");
          Sordertype          = (request.getParameter("ordertype"));
          SRepCode            = common.parseNull(request.getParameter("repcode"));
          Sfrmweight          = common.parseNull(request.getParameter("frmweight"));
          Stoweight           = common.parseNull(request.getParameter("toweight"));

            String  SFileName       = "pdfreports/YarnTypewiseCountQty.pdf";
	    String  SFileOpenPath   = "http://"+request.getServerName()+":"+request.getServerPort()+"/examples/"+SFileName;
	    String  SFileWritePath  =  request.getRealPath("/")+SFileName;
		
	//out.println("FileWritePath==>"+SFileWritePath);

          SProcessTypeCode    = null;
          SProcessTypeName    = null;

          try
          {
               StringTokenizer ST  = new StringTokenizer(request.getParameter("processType"), "#");
               while(ST.hasMoreTokens())
               {
                    SProcessTypeCode    = ST.nextToken();
                    SProcessTypeName    = ST.nextToken();
               }
          }
          catch(Exception ex)
          {
               SProcessTypeCode    = "All";
               SProcessTypeName    = "All";
          }

          try
          {
               StringTokenizer ST  = null;
               ST                  = new StringTokenizer(common.parseNull(request.getParameter("shortageType")), "#");
               while(ST.hasMoreTokens())
               {
                    SShortageTypeCode   = common.parseNull(ST.nextToken());
                    SShortageTypeName   = common.parseNull(ST.nextToken());

               }
          }
          catch(Exception ex)
          {
               SShortageTypeCode   = "All";
               SShortageTypeName   = "All";
          }

          if(common.parseNull(SShortageTypeCode).trim().length() == 0)
          {
               SShortageTypeCode   = "All";
               SShortageTypeName   = "All";
          }

          try
          {
               FText     = new FileWriter("/software/MixPrint/RegularOrderSampleStatusText.txt");
//              FText     = new FileWriter("d:\\RegularOrderSampleStatusText.txt");
          }
          catch(Exception ex){}

          initValue();
          getReceiptDetails();
          getGIDetails();

          try
          {
               setDepthVect();
               setVectorData();
//	       getOrderCountSize();
	       setYarnTypeGroup();
          }
          catch(Exception ex)
          {ex.printStackTrace();
          }
          setRegDepth();
          setSampleDepth();
          PremDepth();
          setRMOrderNos();
          FibreWeightData();
          DateAlign();
          getShortageQS();

          try
          { 
	      getOrderCountSize();
 	      setFibreFormAbstract(out,SFileWritePath,SFileOpenPath);
              out.close();

               
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
 
            FW.close();

          try
          {
               FText.close();
          }
          catch(Exception ex){}    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

     private void initValue()
     {
          iTLine=0;
          iPage=1;
          dPartyWtL=0;
          dPartyWtH=0;
          dDepoWtL=0;
          dDepoWtH=0;
          bDepo=false;
          bParty=false;
          iNineDays=0;
          iThreeDays=0;
     }

     private void setHtml(PrintWriter out) throws Exception
     {
          SStDate             = common.parseDate(String.valueOf(iStDate));
          SEnDate             = common.parseDate(String.valueOf(iEnDate));

          out.println("<html>");
          out.println("<head>");
          out.println("<title>InvPro</title>");
          out.println("</head>");
          out.println("<body bgcolor='"+bgColor+"' text = '#0000FF'>");
          out.println("<p align='center'><font size='5'><b><u>Regular Order Sample Status ReportNew</u> ");
          out.println(" From Date: "+SStDate+"  To Date:"+SEnDate+"</b></font></p>");
	  out.println("RepName-->"+SRepCode);

          out.println("<p><b>Unit : "+SUnit+", Process Type : "+SProcessTypeName+", Order Type : "+SShortageTypeName+", Fibre Dyeing : "+SFibredyeing+" </b></p>");

               out.println("<div align='center'>");
               out.println("  <table border='0' bordercolor='"+bgColor+"'>");
               out.println("    <tr>");
               out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>SL.No</b></font></td>");
               out.println("      <td  rowspan='2'align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>OrderDate</b></font></td>");
               out.println("      <td  rowspan='2'align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>OrderNo</b></font></td>");
               out.println("      <td  rowspan='2'align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Bin</b></font></td>");
               out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>PartyName</b></font></td>");
               out.println("      <td  colspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>OrderWeight</b></font></td>");
               out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Count</b></font></td>");
               out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>RMOrderNo</b></font></td>");
               out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Status</b></font></td>");
               out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>FD-LD Date</b></font></td>");
               out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Total Days</b></font></td>");
               out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>FD Days</b></font></td>");
               out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Sample Days</b></font></td>");
               for(int i= 0;i<=3;i++)
               {
                    out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Dyeing Code/Kgs</b></font></td>");
               }
               out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Shade</b></font></td>");
               out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Light Source</b></font></td>");
               out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Blend</b></font></td>");
               out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Depth</b></font></td>");
               out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Bleached Cotton Depth</b></font></td>");
               out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Basic OrderNo</b></font></td>");
               out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Remarks</b></font></td>");
               out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Yarn Type</b></font></td>");

               out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Count Variation</b></font></td>");
               out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Spirality</b></font></td>");
               out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Perfect Shade Matching?</b></font></td>");
               out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Avoid/ Include Neps?</b></font></td>");
               out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Dyeing Method</b></font></td>");
               out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Fastness Standard</b></font></td>");
               out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Discharge Print Order?</b></font></td>");
               out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Order Requirement Level</b></font></td>");
               out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Shortage Qty Accepted?</b></font></td>");

               out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Matching Order No.</b></font></td>");

               out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Cur. Process Type</b></font></td>");
               out.println("      <td  rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Prev. Process Type</b></font></td>");
               out.println("    </tr>");

               out.println("    <tr>");
               out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Below 1500 (kgs)</b></font></td>");
               out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Above 1500 (kgs)</b></font></td>");
               out.println("    </tr>");
               out.println("    <tr>");
               out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>PartyOrders</b></font></td>");
               out.println("    </tr>");

               int iSNo =0;
               for(int i=0;i<VOrderNo.size();i++)
               {
                              String SEntryDt= common.parseNull((String)VEntryDate.elementAt(i));

                              try
                              {
                                   SEntryDt  = SEntryDt.substring(11,SEntryDt.length());
                              }
                              catch(Exception ex)
                              {
                                   SEntryDt  = "";
                              }

                         String SOrderNo=(String)VOrderNo.elementAt(i);
                         String SParty  =(String)VPartyName.elementAt(i);
                         String SShade  =(String)VShade.elementAt(i);
                         String SLightSource = (String)VLightSource.elementAt(i);
                         int iOrderType =common.toInt((String)VOrderType.elementAt(i));
                        
                         try
                         {

                              if(connect == null)
                              {
                                   theConnect          = JDBCConnection.getJDBCConnection();
                                   connect             = theConnect.getConnection();
                              }

                              PreparedStatement st= connect.prepareStatement(" Select count(*) from RegularOrder where ROrderNo='"+common.parseNull((String)VBasicOrderNo.elementAt(i))+"'  ");
                              ResultSet rs        = st.executeQuery();

               			while(rs.next())
               			{
                     			iCount  = rs.getInt(1);
               			}
               			rs.close();
                              st.close();

                              st        = connect.prepareStatement(" Select count(*) from ShadeCardNo where OrderNo='"+common.parseNull((String)VBasicOrderNo.elementAt(i))+"'  ");
                                rs        = st.executeQuery();

               			while(rs.next())
               			{
                     			iCount1  = rs.getInt(1);
                                       // iRefNo   = rs.getInt(2);
               			}
               			rs.close();
                        	st.close();
                        }
                        catch(Exception ex)
                        {
                          	ex.printStackTrace();
                         	System.out.println(ex);
                        }

                         if(SOrderNo.equals("LMO00196"))
                         {
                              System.out.println("Problemetic Length---"+((String)VRemarks.elementAt(i)).length()+", New Length---"+common.parseNull((String)VRemarks.elementAt(i)).length());
                         }

                         if(!SOrderNo.startsWith("DM") && !SParty.startsWith("AMARJOTHI") )
                         {
                           String SStatus = common.parseNull((String)VStatus.elementAt(i));
                           getFibreRate(SOrderNo,SStatus);


                          if(SFibredyeing.equals("All") || SFibredyeing.equals("Fibre Dyeing"))
                          {

                              if(SFibredyeing.equals("Fibre Dyeing"))
                             { 

                            if(((String)VFibreRate.elementAt(0)).equals("") && ((String)VFibreRate.elementAt(1)).equals("") && ((String)VFibreRate.elementAt(1)).equals("") && ((String)VFibreRate.elementAt(2)).equals("") )
                                {
                                  continue;
                                }

                              }
                              iSNo =iSNo+1;

                              bParty =true;
                              String SVisPer =(String)VVisPer.elementAt(i);
                              String SCotPer =(String)VCotPer.elementAt(i);
                              String SPolyPer=(String)VPolyPer.elementAt(i);
                              double dOrderWt=common.toDouble((String)VOrderWt.elementAt(i));
                              String SBasic  =common.parseNull((String)VBasicOrderNo.elementAt(i));
                              SStatus = common.parseNull((String)VStatus.elementAt(i));
          
                              out.println("<tr>");
                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+String.valueOf(iSNo)+"</font></td>");
                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.parseDate((String)VOrderDate.elementAt(i))+"  "+SEntryDt+ "</font></td>");
                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+SOrderNo+"</td>");
                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VBinName.elementAt(i)+"</td>");
                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+SParty+"</font></td>");
                                   if(dOrderWt <= 1500)
                                   {
                                        dPartyWtL=dPartyWtL+dOrderWt;
                                        out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.getRound(dOrderWt,3)+"</font></td>");
                                        out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'></font></td>");
                                   }
                                   else
                                   {
                                        dPartyWtH=dPartyWtH+dOrderWt;
                                        out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'></font></td>");
                                        out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+dOrderWt+"</font></td>");
                                   }
                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VCount.elementAt(i)+"</font></td>");

                              String SRMOrderNo   = getRMOrderNo(i,SOrderNo);

                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+SRMOrderNo+"</font></td>");

                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+SStatus+"</font></td>");
                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+getFibreDyeingDate(SOrderNo,SStatus)+"</font></td>");

                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VTDate.elementAt(i)+"</font></td>");

                              getFDDelayDays(SRMOrderNo, common.parseNull((String)VOrderDate.elementAt(i)), SOrderNo);

//                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VFDDate.elementAt(i)+"</font></td>");
                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+SFDDelayDays+"</font></td>");

//                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VSDate.elementAt(i)+"</font></td>");
                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+SSampleDays+"</font></td>");

                            //  getFibreRate(SOrderNo,SStatus);
                                   for(int j=0; j<=3;j++)
                                   {
                                        out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.parseNull((String)VFibreRate.elementAt(j))+"</font></td>");
                                   }

                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+SShade+"</font></td>");
                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+SLightSource+"</font></td>");

                 int iBlendChanged = common.toInt((String)VBlendChanged.elementAt(i));

                 if(iBlendChanged==1)         {

                         setBlendData(SOrderNo,iBlendChanged);
                 }
                 if(iBlendChanged==0)         {

                    if(SBasic.equals("DIRECT")) {

                         setBlendData(SOrderNo,1);
                    }else {

                         setBlendData(SBasic,iBlendChanged);
                    }
                 }

                 String sAllotedBlend = "";

                 for(int j=0;j<vNewBlendName.size();j++) {

                      sAllotedBlend += common.parseNull((String)vNewBlendName.elementAt(j))+"-"+common.parseNull((String)vNewBlendPer.elementAt(j))+",";
                 }

                 if(sAllotedBlend.length()>1 ) {

                      sAllotedBlend = sAllotedBlend.substring(0,sAllotedBlend.length()-1);
                 }


//                            out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>Cot%:"+SCotPer+"Vis%:"+SVisPer+"Poly%:"+SPolyPer+"</font></td>");
                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+sAllotedBlend+"</font></td>");

                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+getDepth(SBasic)+"</font></td>");
                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+getDepthVal(SBasic)+"</font></td>");
                              if(iCount1==1)
                             {
 	                        /* if(iRefNo==1)
                                 {
                                      out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'><a href='"+SServer+"Mixing.sordermixing3.RegularOrderMixing1?OrderNo="+SBasic+" '>"+SBasic+"</a></font></td>");
                                 }
                                 else
                                 {
                                     out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'><a href='"+SServer+"Mixing.sordermixing3.SampleOrderMixing1?OrderNo="+SBasic+" '>"+SBasic+"</a></font></td>");
                                 }*/
                                 out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'><a href='"+SServer+"Mixing.sordermixing3.ShadeCodeInsCrit?OrderNo="+SBasic+" '>"+SBasic+"</a></font></td>");
                             }
                             else
                             {
                            	 if(iCount==1)
                             	{
                              	out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'><a href='"+SServer+"Mixing.RegularOrderMixing?OrderNo="+SBasic+" '>"+SBasic+"</a></font></td>");
                             	}
                             	else 
                             	{
                             	out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'><a href='"+SServer+"Mixing.sordermixing3.SampleOrderMixingList?OrderNo="+SBasic+" '>"+SBasic+"</a></font></td>");
                            	}
                            }
                             
                           
                             

                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.parseNull((String)VRemarks.elementAt(i))+"</font></td>");
                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.parseNull((String)VYarnType.elementAt(i))+"</font></td>");

                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.parseNull((String)VCountVariation.elementAt(i))+"</font></td>");
                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.parseNull((String)VSpirality.elementAt(i))+"</font></td>");
                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.parseNull((String)VPerfectShadeMatching.elementAt(i))+"</font></td>");
                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.parseNull((String)VAvoidNeps.elementAt(i))+"</font></td>");
                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.parseNull((String)VDyeingMethod.elementAt(i))+"</font></td>");
                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.parseNull((String)VFastnessStandard.elementAt(i))+"</font></td>");
                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.parseNull((String)VDischargePrintOrder.elementAt(i))+"</font></td>");
                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.parseNull((String)VOrderRequirementLevel.elementAt(i))+"</font></td>");
                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.parseNull((String)VShortageQtyAccepted.elementAt(i))+"</font></td>");
                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.parseNull((String)VMatchingOrderNo.elementAt(i))+"</font></td>");
                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.parseNull((String)VOrderProcessType.elementAt(i))+"</font></td>");

                              String SPartyCode        = common.parseNull((String)VPartyCode.elementAt(i));

                              String SLastProcessType  = getLastProcessType(SPartyCode, SBasic, SOrderNo);

                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+SLastProcessType+"</font></td>");

                              out.println("</tr>");
                         }

                     }

               }

                    out.println("<tr>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>Total</b></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>"+common.getRound(dPartyWtL,3)+"</b></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>"+common.getRound(dPartyWtH,3)+"</b></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
                    out.println("</tr>");

                    out.println("<tr>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>Grand Total</b></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>"+common.getRound(dPartyWtL+dPartyWtH,3)+"</b></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b></b></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
                    out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
                    out.println("</tr>");


          setDepoHtml(out);
          out.println("  </table>");
	  out.println(" <br>");
   	  out.println(" <br>");
//	  setFibreFormAbstract(out);
          out.println("  </center>");
          out.println("</div>");
          out.println("");
          out.println("</body>");
          out.println("");
          out.println("</html>");
         
     }

     private void setDepoHtml(PrintWriter out)
     {
               int iSNo =0;

               out.println("    <tr>");
               out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>DepoOrders</b></font></td>");
               out.println("    </tr>");

               for(int i=0;i<VOrderNo.size();i++)
               {
                  
                         String SOrderNo=(String)VOrderNo.elementAt(i);
                         String SParty  =(String)VPartyName.elementAt(i);
                         String SShade  =(String)VShade.elementAt(i);
                         String SLightSource  = (String)VLightSource.elementAt(i);
                         int iOrderType =common.toInt((String)VOrderType.elementAt(i));

                              String SEntryDt= common.parseNull((String)VEntryDate.elementAt(i));
                              try
                              {
                                   SEntryDt  = SEntryDt.substring(11,SEntryDt.length());
                              }
                              catch(Exception ex)
                              {
                                   SEntryDt  = "";
                              }

                         if(SOrderNo.startsWith("DM") || SParty.startsWith("AMARJOTHI") && SShade.startsWith("LGREY"))
                         {
                              bDepo =true;
                              iSNo=iSNo+1;
                              String SBasic="";
                              String SVisPer =(String)VVisPer.elementAt(i);
                              String SCotPer =(String)VCotPer.elementAt(i);
                              String SPolyPer=(String)VPolyPer.elementAt(i);
                              double dOrderWt=common.toDouble((String)VOrderWt.elementAt(i));
                              SBasic  =common.parseNull((String)VBasicOrderNo.elementAt(i));
                              String SStatus =common.parseNull((String)VStatus.elementAt(i));
                              out.println("<tr>");
                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+String.valueOf(iSNo)+"</font></td>");
                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.parseDate((String)VOrderDate.elementAt(i))+"  "+SEntryDt+"</font></td>");
                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+SOrderNo+"</td>");
                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VBinName.elementAt(i)+"</td>");
                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VPartyName.elementAt(i)+"</font></td>");
                                   if(dOrderWt <= 1500)
                                   {
                                        dDepoWtL=dDepoWtL+dOrderWt;
                                        out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.getRound(dOrderWt,3)+"</font></td>");
                                        out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'></font></td>");
                                   }
                                   else
                                   {
                                        dDepoWtH=dDepoWtH+dOrderWt;
                                        out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'></font></td>");
                                        out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VOrderWt.elementAt(i)+"</font></td>");
                                   }
                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VCount.elementAt(i)+"</font></td>");

                              String SRMOrderNo   = getRMOrderNo(i,SOrderNo);

                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+SRMOrderNo+"</font></td>");

                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+SStatus+"</font></td>");
                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+getFibreDyeingDate(SOrderNo,SStatus)+"</font></td>");

                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VTDate.elementAt(i)+"</font></td>");

                              getFDDelayDays(SRMOrderNo, common.parseNull((String)VOrderDate.elementAt(i)), SOrderNo);

//                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VFDDate.elementAt(i)+"</font></td>");
                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+SFDDelayDays+"</font></td>");

//                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VSDate.elementAt(i)+"</font></td>");
                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+SSampleDays+"</font></td>");

                                   getFibreRate(SOrderNo,SStatus);
                                   for(int j=0; j<=3;j++)
                                   {
                                        out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.parseNull((String)VFibreRate.elementAt(j))+"</font></td>");
                                   }
                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VShade.elementAt(i)+"</font></td>");
                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)VLightSource.elementAt(i)+"</font></td>");


                                int iBlendChanged = common.toInt((String)VBlendChanged.elementAt(i));
               
                                if(iBlendChanged==1)         {
               
                                   setBlendData(SOrderNo,iBlendChanged);
                                }
                                if(iBlendChanged==0)         {
               
                                   if(SBasic.equals("DIRECT")) {
               
                                        setBlendData(SOrderNo,1);
                                   }else {
               
                                        setBlendData(SBasic,iBlendChanged);
                                   }
                                }
               

                                String sAllotedBlend = "";
               
                                for(int j=0;j<vNewBlendName.size();j++) {
               
                                     sAllotedBlend += common.parseNull((String)vNewBlendName.elementAt(j))+"-"+common.parseNull((String)vNewBlendPer.elementAt(j))+",";
                                }
               
                                if(sAllotedBlend.length()>1 ) {
               
                                     sAllotedBlend = sAllotedBlend.substring(0,sAllotedBlend.length()-1);
                                }

//                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>Cot%:"+SCotPer+"Vis%:"+SVisPer+"Poly%:"+SPolyPer+"</font></td>");

                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+sAllotedBlend+"</font></td>");




                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+getDepth(SBasic)+"</font></td>");
                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+SBasic+"</font></td>");
                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+getDepthVal(SBasic)+"</font></td>");
                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.parseNull((String)VRemarks.elementAt(i))+"</font></td>");
                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.parseNull((String)VYarnType.elementAt(i))+"</font></td>");

                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.parseNull((String)VCountVariation.elementAt(i))+"</font></td>");
                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.parseNull((String)VSpirality.elementAt(i))+"</font></td>");
                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.parseNull((String)VPerfectShadeMatching.elementAt(i))+"</font></td>");
                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.parseNull((String)VAvoidNeps.elementAt(i))+"</font></td>");
                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.parseNull((String)VDyeingMethod.elementAt(i))+"</font></td>");
                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.parseNull((String)VFastnessStandard.elementAt(i))+"</font></td>");
                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.parseNull((String)VDischargePrintOrder.elementAt(i))+"</font></td>");
                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.parseNull((String)VOrderRequirementLevel.elementAt(i))+"</font></td>");
                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.parseNull((String)VShortageQtyAccepted.elementAt(i))+"</font></td>");
                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.parseNull((String)VMatchingOrderNo.elementAt(i))+"</font></td>");
                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.parseNull((String)VOrderProcessType.elementAt(i))+"</font></td>");

                              String SPartyCode        = common.parseNull((String)VPartyCode.elementAt(i));

                              String SLastProcessType  = getLastProcessType(SPartyCode, SBasic, SOrderNo);

                              out.println("      <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+SLastProcessType+"</font></td>");
                              out.println("</tr>");

                         }
               }
               out.println("<tr>");
               out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
               out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
               out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
               out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
               out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>Total</b></font></td>");
               out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>"+common.getRound(dDepoWtL,3)+"</b></font></td>");
               out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>"+common.getRound(dDepoWtH,3)+"</b></font></td>");
               out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
               out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
               out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
               out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
               out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
               out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
               out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
               out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
               out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
               out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
               out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
               out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
               out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
               out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
               out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
               out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
               out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
               out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
               out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
               out.println("</tr>");

               out.println("<tr>");
               out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
               out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
               out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
               out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
               out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>Grand Total</b></font></td>");
               out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b>"+common.getRound(dDepoWtL+dDepoWtH,3)+"</b></font></td>");
               out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'><b></b></font></td>");
               out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
               out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
               out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
               out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
               out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
               out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
               out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
               out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
               out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
               out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
               out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
               out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
               out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
               out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
               out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
               out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
               out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
               out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
               out.println("      <td   align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
               out.println("</tr>");

               dPartyWtL = 0;
               dPartyWtH = 0;
               dDepoWtL  = 0;
               dDepoWtH  = 0;
        

     }
// Noof Orders Total - 13.09.2017  - Kavitha
  /*   private void setFibreFormAbstract(PrintWriter out)
     {
	// createPDF();

	int iCountwiseTotal=0 , iFormwiseTotal=0, iGrandTotal=0;
        out.println("<p><b> FibreForm wise Order Abstract </b></p>");

	out.println(" <br>");
	out.println(" <table border =1  align ='left'> ");
	out.println(" <tr> ");
        out.println(" <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>FIBREFORM NAME</b></font></td>");
	for(int a=0;a<VOrderCount.size();a++){
	out.println(" <td   align='center' bgcolor='"+bgHead+"'><b><font color='"+fgHead+"'>"+common.parseNull((String)VOrderCount.elementAt(a))+"</font></b></td>");
	}
	out.println(" <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b> FORMWISE TOTAL </b></font></td>");
	out.println(" </tr> ");
	for(int i=0;i<VFibreFormGroupName.size();i++) {
	 String 	sGroupCode 	= (String)VFibreFormGroupCode.get(i);
	out.println(" <tr> ");
	out.println(" <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.parseNull((String)VFibreFormGroupName.elementAt(i))+"</font></td>");
	for(int j=0;j<VOrderCount.size();j++){
	 	String	  SCount	= 	 (String)VOrderCount.get(j);
		int iNoofOrders		= 	 getNoofOrders(SCount,sGroupCode);			 
		 
	   	out.println(" <td align='center' bgcolor='"+bgBody+"'><font color='"+fgUom+"'>"+iNoofOrders+"</font></td>");
		iFormwiseTotal		= 	 iFormwiseTotal + iNoofOrders;
		iNoofOrders		= 	 0;
	 }
	out.println(" <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+ iFormwiseTotal +"</font></td>");
	iGrandTotal			= 	iGrandTotal+iFormwiseTotal;
	out.println(" </tr> ");
	iFormwiseTotal	= 0;
	}
	out.println(" <tr>" );
	out.println(" <td   align='center' bgcolor='pink'><font color='"+fgBody+"'>COUNTWISE TOTAL</font></td>");
	for(int j=0;j<VOrderCount.size();j++){
 	String	  SCount		= 	 (String)VOrderCount.get(j);
	int iCountwiseNoofOrders	= 	 getNoofOrders(SCount);	
   	out.println(" <td align='center' bgcolor='pink'><font color='"+fgUom+"'>"+iCountwiseTotal+"</font></td>");	
	}
	out.println(" <td   align='center' bgcolor='pink'><font color='"+fgBody+"'>"+ iGrandTotal +"</font></td>");
	out.println(" </tr> ");
	iCountwiseTotal = 0;
	out.println(" </table>" );
	
     }*/


	private void setFibreFormAbstract(PrintWriter out, String SFileWritePath,String SFileOpenPath)
    {
    	out.println("<p>");
        out.println(" <a href='"+SFileOpenPath+"' target='_blank'><b>View this Report in Pdf</b></a>");
        out.println("</p>");  

	 	//createPDF(SFileWritePath);

		double	dCountwiseTotal=0.00 , dFormwiseTotal=0.00, dGrandTotal=0.00;
        out.println("<p align='center' bgcolor='"+bgHead+"' ><b> FibreForm wise Order Abstract </b></p>");
        out.println(" <p align ='center'> <b> As on Dated : "+SToday+"</b></p>");

		out.println(" <br>");
		out.println(" <table border =1  align ='left'> ");
		out.println(" <tr> ");
        out.println(" <td  rowspan='1' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>FIBREFORM NAME</b></font></td>");
		for(int a=0;a<VOrderCount.size();a++)
		{
			out.println(" <td   align='center' bgcolor='"+bgHead+"'><b><font color='"+fgHead+"'>"+common.parseNull((String)VOrderCount.elementAt(a))+"</font></b></td>");
		}
		out.println(" <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b> FORMWISE TOTAL </b></font></td>");
		out.println(" </tr> ");
		for(int i=0;i<VFibreFormGroupName.size();i++) 
		{
	 		String 	sGroupCode 	= (String)VFibreFormGroupCode.get(i);
			out.println(" <tr> ");
			out.println(" <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+common.parseNull((String)VFibreFormGroupName.elementAt(i))+"</font></td>");
			for(int j=0;j<VOrderCount.size();j++)
			{
	 			String	  SCount	= 	 (String)VOrderCount.get(j);
				double	  dOrderQty	= 	 getOrderQty (SCount,sGroupCode);			 
		 
			   	out.println(" <td align='center' bgcolor='"+bgBody+"'><font color='"+fgUom+"'>"+dOrderQty+"</font></td>");
				dFormwiseTotal		= 	 dFormwiseTotal + dOrderQty;
				dOrderQty		= 	 0.00;
	 		}
			out.println(" <td   align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+ dFormwiseTotal +"</font></td>");
			dGrandTotal			= 	dGrandTotal+dFormwiseTotal;
			out.println(" </tr> ");
			dFormwiseTotal	= 0.00;
		}
		out.println(" <tr>" );
		out.println(" <td   align='center' bgcolor='pink'><b><font color='"+fgBody+"'>COUNTWISE TOTAL</font></b></td>");
		for(int j=0;j<VOrderCount.size();j++)
		{
 			String	  SCount		= 	 (String)VOrderCount.get(j);
			dCountwiseTotal			= 	 getCountwiseTotal(SCount);	
   			out.println(" <td align='center' bgcolor='pink'><font color='"+fgUom+"'>"+dCountwiseTotal+"</font></td>");	
		}
		out.println(" <td   align='center' bgcolor='pink'><font color='"+fgBody+"'>"+ dGrandTotal +"</font></td>");
		out.println(" </tr> ");
		dCountwiseTotal = 0.00;
		out.println(" </table>" );
	}

     private Vector getOrderCountSize(){
	     VOrderCount	= new Vector();
	     String sTempCount	= "";	
		for(int i=0;i<VCount.size();i++){
    	          String sCount	= (String)VCount.get(i);
		   if(!VOrderCount.contains(sCount)){
			VOrderCount   . add(sCount);
		   }
		}
	     Collections.sort(VOrderCount);
	     return VOrderCount;			
	}	

    private int getNoofOrders(String sCount,String sFibreFormGroupCode){
		int iNoofOrders=0;
		for(int i=0;i<VCount.size();i++){
		 String		SCount		= (String)VCount.get(i);
		 String		sGroupCode	= (String)VGroupCode.get(i);
		if(sCount.trim().equals(SCount) && sFibreFormGroupCode.trim().equals(sGroupCode)){
		   iNoofOrders			= iNoofOrders+1;	
		 }
		}
		return iNoofOrders;
	}	

    private int getNoofOrders(String sCount){
		int iNoofOrders=0;
		for(int i=0;i<VCount.size();i++){
		 String		SCount		= (String)VCount.get(i);
		 String		sGroupCode	= (String)VGroupCode.get(i);
		if(sCount.trim().equals(SCount)){
		   iNoofOrders			= iNoofOrders+1;	
		 }
		}
		return iNoofOrders;
	}	


    private double getOrderQty(String sCount,String sFibreFormGroupCode){
		double 	dQty=0.00;
		for(int i=0;i<VCount.size();i++){
		 String		SCount		= (String)VCount.get(i);
		 String		sGroupCode	= (String)VGroupCode.get(i);
		if(sCount.trim().equals(SCount) && sFibreFormGroupCode.trim().equals(sGroupCode)){
			double dOrderWt		= common.toDouble(String.valueOf(VOrderWt.elementAt(i)));
		   	dQty			= dOrderWt+dQty;
		 }
		}
		return dQty;
	}	

    private double getCountwiseTotal(String sCount){
		double 	dQty=0.00;
		for(int i=0;i<VCount.size();i++){
		 String		SCount		= (String)VCount.get(i);
		 String		sGroupCode	= (String)VGroupCode.get(i);
		if(sCount.trim().equals(SCount)){
			double dOrderWt		= common.toDouble(String.valueOf(VOrderWt.elementAt(i)));
		   	dQty			= dOrderWt+dQty;
		 }
		}
		return dQty;
	}	

     private void setVectorData()
     {
               VOrderDate = new Vector();
               VOrderNo   = new Vector();
               VPartyName = new Vector();
               VOrderWt   = new Vector();
               VCount     = new Vector();

               VRMOrder       = new Vector();
               VShade         = new Vector();
               VLightSource   = new Vector();
               VCotPer        = new Vector();
               VVisPer        = new Vector();
               VPolyPer       = new Vector();
               VDepth         = new Vector();
               VBasicOrderNo  = new Vector();
               VStatus        = new Vector();
               VOrderType     = new Vector();
               VEntryDate     = new Vector();
               VRemarks       = new Vector();
               VSampleOrderNo = new Vector();
               VUnitCode      = new Vector();
               VBlendChanged  = new Vector();
               VYarnType      = new Vector();
               VOrderProcessType   = new Vector();
               VPartyCode          = new Vector();

               VCountVariation          = new Vector();
               VSpirality               = new Vector();
               VPerfectShadeMatching    = new Vector();
               VAvoidNeps               = new Vector();
               VDyeingMethod            = new Vector();
               VFastnessStandard        = new Vector();
               VDischargePrintOrder     = new Vector();
               VOrderRequirementLevel   = new Vector();
               VShortageQtyAccepted     = new Vector();
               VMatchingOrderNo         = new Vector();
               VBinName                 = new Vector();
	       VGroupCode		= new Vector();
	       VFormaName		= new Vector();

String QS =" Select  T.OrderDate,T.ROrderNo,PartyName,t.Weight,CountName,"+
           " CotPer,VisPer,PolyPer,T.RefOrderNo,Depth,subStr(SOrderNo,0,length(SOrderNo)-1),"+
           " Color.ColorName,Generalstatus.Status,T.OrderType,T.AuthDate,t.SourceName, "+
           " Replace(Replace(t.Instruction, chr(10), ' '), chr(9),'') as Instruction, "+
           " T.SampleOrderNo,t.unitcode,T.BlendChanged, T.YarnType, "+

           " T.CountVariation, T.Spirality, T.PerfectShadeMatching, "+
           " T.AvoidNeps, T.DyeingMethod, T.FastnessStandard, "+
           " T.DischargePrintOrder, T.OrderRequirementLevel,  "+
           " T.ShortageQtyAccepted, T.MatchingOrderNo, T.BinName, T.ProcessType, T.PartyCode ,T.GroupCode,T.FormName "+

           " From "+
           " ("+
           " Select RegularOrder.AuthDate,RegularOrder.OrderDate, RegularOrder.ROrderNo,PartyMaster.PartyName,"+
           " RegularOrder.Weight,YarnCount.CountName,"+
           " CotPer,VisPer,PolyPer,RegularOrder.RefOrderNo,RMixir.Depth,Max(RegularOrderStatus.id) as id,RegularOrder.ColorCode,max(SampleOrder.Orderid) as Orderid ,"+
           " RegularOrder.UnitCode,RegularOrder.OrderType,LightSource.SourceName,RegularOrder.Instruction,"+
           " SampleToRegularAuth.SampleOrderNo,RegularOrder.MixingChanged as BlendChanged, FibreForm.FormName as YarnType, "+

           " CountVariation.Name as CountVariation, Spirality.Name as Spirality, PerfectShadeMatching.Name as PerfectShadeMatching, "+
           " AvoidNeps.Name as AvoidNeps, DyeingMethod.Name as DyeingMethod, FastnessStandard.Name as FastnessStandard, "+
           " DischargePrintOrder.Name as DischargePrintOrder, OrderRequirementLevel.Name as OrderRequirementLevel,  "+
           " ShortageQtyAccepted.Name as ShortageQtyAccepted, RegularOrder.MatchingOrderNo, SampleBin.Name as BinName, ProcessingType.ProcessType, PartyMaster.PartyCode,FibreFormGroup.GroupCode,FormName  "+
           " from RegularORder "+
           " Inner Join Unit On Unit.UnitCode = RegularOrder.UnitCode";
        
           if(!SUnit.equals("All"))
           {
              QS = QS+" and Unit.UnitName ='"+SUnit+"'";
           }
           if(!SUnit.equals("All"))
           {
              QS = QS+" and Unit.UnitName ='"+SUnit+"'";
           }

           QS = QS+ " left join SampleToRegularAuth on SampleToRegularAuth.RegularORderNo=RegularORder.ROrderNO "+
           " Left  Join SampleOrder On SampleOrder.ForOrder = RegularOrder.ROrderNo"+
           " Inner Join PartyMaster On PartyMaster.PartyCode = RegularOrder.PartyCode"+
           " Inner Join YarnCount On YarnCount.CountCode = RegularOrder.CountCode"+
           " Inner Join Blend On Blend.BlendCode = RegularOrder.BlendCode"+
           " Left  Join RMixir On RmixIr.OrdNo = RegularOrder.RefOrderNo and Rmixir.CorrectionMixing=0"+
           " Left Join RegularOrderStatus On  RegularOrderStatus.Orderid = RegularOrder.Orderid"+
           " Left join LightSource on LightSource.SourceCode= Regularorder.LightCode "+

           " Left Join FibreForm on FibreForm.FormCode = RegularOrder.FibreFormCode "+
	   " Inner Join FibreFormGroup on FibreFormGroup.GroupCode = FibreForm.GroupCode  "+

           " Left Join CountVariation on CountVariation.Code = RegularOrder.CountVariationCode "+
           " Left Join Spirality on Spirality.Code = RegularOrder.SpiralityCode "+
           " Left Join PerfectShadeMatching on PerfectShadeMatching.Code = RegularOrder.PerfectShadeMatchingCode "+
           " Left Join AvoidNeps on AvoidNeps.Code = RegularOrder.AvoidNepsCode "+
           " Left Join DyeingMethod on DyeingMethod.Code = RegularOrder.DyeingMethodCode "+
           " Left Join FastnessStandard on FastnessStandard.Code = RegularOrder.FastnessStandardCode "+
           " Left Join DischargePrintOrder on DischargePrintOrder.Code = RegularOrder.DischargePrintOrderCode "+
           " Left Join OrderRequirementLevel on OrderRequirementLevel.Code = RegularOrder.OrderRequirementLevelCode "+
           " Left Join ShortageQtyAccepted on ShortageQtyAccepted.Code = RegularOrder.ShortageQtyAcceptedCode "+
           " Left Join SampleUnitOrderBin on RegularOrder.ROrderNo = SampleUnitOrderBin.OrderNo and SampleUnitOrderBin.Status=0 "+
           " Left Join SampleBin on SampleUnitOrderBin.BinCode = SampleBin.Code "+

          " inner join ProcessingType on ProcessingType.ProcessCode = regularorder.ProcessTypeCode ";

          if(!SProcessTypeCode.equals("All"))
          {
               if(SProcessTypeCode.equals("BR-C Orders"))
               {
                    QS += " and ProcessingType.OEStatus = 1 ";
               }
               else if(SProcessTypeCode.equals("Other than BR-C Orders"))
               {
                    QS += " and ProcessingType.OEStatus = 0 ";
               }
               else
               {
                    QS += " and RegularOrder.ProcessTypeCode = "+SProcessTypeCode;
               }
          }
// Commented on 17.07.2015 

//    QS +=  " WHere  RegularOrder.Status=1 and RegularOrder.OrderDate >= 20061001 and RegularOrder.OrderDate < = "+common.getNextDate(iEnDate)+" and RegularOrder.Shortage<>2 And RegularOrder.RorderNo not Like 'T%'and RegularOrder.RorderNo not Like 'PR%'"+
   
       QS +=  " WHere  RegularOrder.Status=1 and RegularOrder.OrderDate >= 20061001 and RegularOrder.Shortage<>2 And RegularOrder.RorderNo not Like 'T%'and RegularOrder.RorderNo not Like 'PR%'"+
   
// Commented on 21.02.2014.. for avoid OR Condition in Query..
//           " and SampleToRegularAuth.status is Null Or SampleToRegularAuth.Status = 0 ";

           " and nvl(SampleToRegularAuth.status, 0) = 0 ";

          if(!SShortageTypeCode.equals("All"))
          {
              QS = QS +  " and RegularOrder.Shortage = "+SShortageTypeCode;
          }

          if(!SOrderType.equals("All"))
          {
               if(SOrderType.equals("Depo Orders"))
               {
                    QS = QS +  " and RegularOrder.ROrderNo like 'DM%' ";
               }
               if(SOrderType.equals("Party Orders"))
               {
                    QS = QS +  " and RegularOrder.ROrderNo Not like 'DM%' ";
               }
          }
     
            

          if(Sordertype.equals("1000"))
           {
               QS = QS+" and (Regularorder.OrderType=1 or Regularorder.OrderType=3)";
           }
          if(Sordertype.equals("1001"))
           {
              QS = QS+" and (Regularorder.OrderType=1 or Regularorder.OrderType=3  or Regularorder.OrderType=2)";
           }
          if(!Sordertype.equals("9999")&&!Sordertype.equals("1000")&&!Sordertype.equals("1001"))
           {
              QS = QS+" and Regularorder.OrderType="+Sordertype;
           }

        

     QS += " group by  RegularOrder.AuthDate, RegularOrder.OrderDate, RegularOrder.ROrderNo,PartyMaster.PartyName,"+
           " RegularOrder.Weight,YarnCount.CountName,"+
           " CotPer,VisPer,PolyPer,RegularOrder.RefOrderNo,RMixir.Depth,RegularOrder.ColorCode,RegularOrder.Unitcode,RegularOrder.OrderType,LightSource.SourceName,RegularOrder.Instruction,SampleToRegularAuth.SampleOrderNo,RegularOrder.MixingChanged, FibreForm.FormName, "+

           " CountVariation.Name, Spirality.Name, PerfectShadeMatching.Name, "+
           " AvoidNeps.Name, DyeingMethod.Name, FastnessStandard.Name, "+
           " DischargePrintOrder.Name, OrderRequirementLevel.Name,  "+
           " ShortageQtyAccepted.Name, RegularOrder.MatchingOrderNo, SampleBin.Name, ProcessingType.ProcessType, PartyMaster.PartyCode, FibreFormGroup.GroupCode,FormName   "+

           " )t"+
           " Left join  OrderGroupDetails on OrderGroupDetails.ORderno=t.ROrderNo"+
           " Left  Join SampleOrder On SampleOrder.OrderId = T.OrderId"+
           " Left Join Color On Color.ColorCode = T.ColorCode"+
           " Left Join  RegularOrderStatus On RegularOrderStatus.id = T.id"+
           " Left  Join GeneralStatus On GeneralStatus.Statuscode = RegularorderStatus.StatusCode"+
           " Where T.RorderNo not in('DM02312') and OrderGroupDetails.OrderNo is Null";

          if(!Sfrmweight.equals("") && !Stoweight.equals(""))
           {

             System.out.println("Sfrmweight-->"+Sfrmweight);
             System.out.println("Stoweight-->"+Stoweight);

             double dfrmweight = common.toDouble(Sfrmweight);
             double dtoweight  = common.toDouble(Stoweight);
              if(dtoweight >= dfrmweight)
              {
                QS = QS  + " and t.weight >= "+dfrmweight+" and t.weight<="+dtoweight+" ";
              }
            }
 
           else if(!Sfrmweight.equals("") && Stoweight.equals(""))
            {

               double dfrmweight  = common.toDouble(Sfrmweight);
               
               QS = QS  + " and t.weight >= "+dfrmweight+" ";
         
            }

           else if(Sfrmweight.equals("") && !Stoweight.equals(""))
            {

                double dtoweight  = common.toDouble(Stoweight);
               
                QS = QS  + " and t.weight <= "+dtoweight+" ";
         
            }


           QS=QS+" Group By"+
                 " T.OrderDate,T.ROrderNo,PartyName,t.Weight,CountName,"+
                 " CotPer,VisPer,PolyPer,T.RefOrderNo,Depth,T.id ,subStr(SOrderNo,0,length(SOrderNo)-1),GeneralStatus.Status,Color.ColorName,T.OrderType,T.AuthDate,t.SourceName,t.Instruction,T.SampleOrderNo,t.unitcode,T.BlendChanged,T.YarnType, "+
                 " T.CountVariation, T.Spirality, T.PerfectShadeMatching, "+
                 " T.AvoidNeps, T.DyeingMethod, T.FastnessStandard, "+
                 " T.DischargePrintOrder, T.OrderRequirementLevel,  "+
                 " T.ShortageQtyAccepted, T.MatchingOrderNo, T.BinName, T.ProcessType, T.PartyCode , T.GroupCode,T.FormName "+
                 " Order By  T.OrderDate";


     String QS1 =  " select sum(weight),unitname from ( "+
                   " select sampletoregularauth.RegularOrderNo , "+
                   " SampleToRegularAuth.SampleOrderNo,Yarnm.YShnm, "+
                   " SampleToRegularAuth.MixType,SampleToRegularAuth.OrderId,regularorder.weight,UnitName "+
                   " From SampleToRegularAuth inner join yarnm on "+
                   " yarnm.yshcd=sampletoregularauth.shadecode "+
                   " left join sampletoregular on sampletoregularauth.regularorderno=sampletoregular.regularorderno "+
                   " left join ordergroupdetails on sampletoregularauth.regularorderno=ordergroupdetails.orderno "+
                   " left join regularorder on regularorder.rorderno=sampletoregularauth.regularorderno "+
                   " inner join unit on unit.unitcode=regularorder.unitcode "+
                   " where  sampletoregular.regularorderno is null and to_number(to_Char(LastModified,'YYYYMMDD'))>20080101 "+
                   " and SampleToRegularAuth.Status=1  "+
                   " and ordergroupdetails.orderno is null  "+
                   " union all  "+
                   " select RegularOrderNo , "+
                   " SampleToRegularAuth.SampleOrderNo,Yarnm.YShnm, "+
                   " SampleToRegularAuth.MixType,SampleToRegularAuth.OrderId,regularorder.weight,UnitName "+
                   " From (SampleToRegularAuth inner join yarnm on "+
                   " yarnm.yshcd=sampletoregularauth.shadecode ) "+
                   " left join regularorder on regularorder.rorderno=sampletoregularauth.regularorderno "+
                   " inner join unit on unit.unitcode=regularorder.unitcode "+
                   " where SampleTOregularAuth.CorrectionMixing=1 "+
                   " and to_number(to_Char(LastModified,'YYYYMMDD'))>20080101 "+
                   " and SampleOrderNo not in(select SampleOrderNo from SampleToRegular "+
                   " where CorrectionMixing=1)) ";

                if(!SUnit.equals("All"))
                {
                   QS1 =QS1+" Where UnitName='"+SUnit+"' ";
                }

                   QS1 =QS1+" Group By UnitName ";

          try
          {

               FileWriter FW       = new FileWriter("/software/MixPrint/RegularOrderSampleStatusQS.txt");
//               FileWriter FW       = new FileWriter("d:\\RegularOrderSampleStatusQS.txt");
               FW                  . write(QS);
               FW                  . close();



               if(connect == null)
               {
                    theConnect          = JDBCConnection.getJDBCConnection();
                    connect             = theConnect.getConnection();
               }
                System.out.println("QS-->"+QS);
               PreparedStatement stat = connect.prepareStatement(QS);
               ResultSet res       = stat.executeQuery();

               while(res.next())
               {
                    VOrderDate               . addElement(res.getString(1));
                    VOrderNo                 . addElement(res.getString(2));
                    VPartyName               . addElement(res.getString(3));
                    VOrderWt                 . addElement(res.getString(4));
                    VCount                   . addElement(res.getString(5));
                    VCotPer                  . addElement(res.getString(6));
                    VVisPer                  . addElement(res.getString(7));
                    VPolyPer                 . addElement(res.getString(8));
                    VBasicOrderNo            . addElement(res.getString(9));
                    VDepth                   . addElement(res.getString(10));
                    VRMOrder                 . addElement(res.getString(11));
                    VShade                   . addElement(res.getString(12));
                    VStatus                  . addElement(res.getString(13));
                    VOrderType               . addElement(res.getString(14));
                    VEntryDate               . addElement(res.getString(15));
                    VLightSource             . addElement(res.getString(16));
                    VRemarks                 . addElement(common.parseNull(res.getString(17)));
                    VSampleOrderNo           . addElement(common.parseNull(res.getString(18)));
                    VUnitCode                . addElement(res.getString(19));
                    VBlendChanged            . addElement(res.getString(20));
                    VYarnType                . addElement(common.parseNull(res.getString(21)));

                    VCountVariation          . addElement(common.parseNull(res.getString(22)));
                    VSpirality               . addElement(common.parseNull(res.getString(23)));
                    VPerfectShadeMatching    . addElement(common.parseNull(res.getString(24)));
                    VAvoidNeps               . addElement(common.parseNull(res.getString(25)));
                    VDyeingMethod            . addElement(common.parseNull(res.getString(26)));
                    VFastnessStandard        . addElement(common.parseNull(res.getString(27)));
                    VDischargePrintOrder     . addElement(common.parseNull(res.getString(28)));
                    VOrderRequirementLevel   . addElement(common.parseNull(res.getString(29)));
                    VShortageQtyAccepted     . addElement(common.parseNull(res.getString(30)));
                    VMatchingOrderNo         . addElement(common.parseNull(res.getString(31)));
                    VBinName                 . addElement(common.parseNull(res.getString(32)));
                    VOrderProcessType        . addElement(common.parseNull(res.getString(33)));
                    VPartyCode               . addElement(common.parseNull(res.getString(34)));
	       	    VGroupCode		     . addElement(common.parseNull(res.getString(35)));
	            VFormaName		     . addElement(common.parseNull(res.getString(36)));
               }
               res.close();
                System.out.println("QS1-->"+QS1);
               res       = stat.executeQuery(QS1);
               while(res.next())
               {
                    dMixingWt=res.getDouble(1);

                    System.out.println("MixingPending----"+dMixingWt);
               }
               res.close();

               stat.close();
          }
          catch(Exception e)
          {            
               System.out.println(e);
               e.printStackTrace();
          }
     }


     private void setYarnTypeGroup()
     {
		VFibreFormGroupCode			= new Vector();
		VFibreFormGroupName			= new Vector();
	
		StringBuffer	sb			= new StringBuffer();

		sb.append("  Select GroupCode,GroupName from FibreFormGroup Order by 2 ");	
		
           try        
            {
               JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
               Connection     conn = jdbc.getConnection();

               PreparedStatement stat      = conn.prepareStatement(sb.toString());
               ResultSet res      	   = stat.executeQuery();

		//  out.println("YarnType Group Qry--->"+sb.toString());


                     while(res.next()){
                         VFibreFormGroupCode     . addElement(common.parseNull(res.getString(1)));
                         VFibreFormGroupName     . addElement(common.parseNull(res.getString(2)));
                     }
                     res.close();
                     stat.close();
            }
            catch(Exception ex)
            {
                  ex.printStackTrace();
            }
     }
	  

     private void FibreWeightData()
     {
          VFibreCode   = new Vector();
          VWeight      = new Vector();
          VDyeOrderNo  = new Vector();
          VDeliveryDate= new Vector();
          VFRDate      = new Vector();
          VFIDate      = new Vector();
          VOrderStatus = new Vector();


        String QS =" Select  ROrderNo ,FibreCode, weight ,max(OrderDate),max(DeliveryDate),max(gidate),OrderCompleted"+
                   " From ( Select ROrderNo,DyeingOrderDetails.FibreCode,Sum(DyeingOrderDetails.Weight)  as weight ,DyeingOrderNo,OrderCompleted"+
                   " From RegularORder "+
                   " Left join SampleToRegularAuth on SampleToRegularAuth.RegularORderNo=RegularORder.ROrderNO"+
                   " Left Join DyeingOrderDetails On  DyeingOrderDetails.OrderNo = RegularOrder.ROrderNo"+
                   " WHere  OrderDate>=20061001 and REgularOrder.Status=1 "+
                   " And RegularOrder.Shortage<>2 And RegularOrder.RorderNo not Like 'T%' and RegularOrder.RorderNo not Like 'PR%'"+
                   " and SampleToRegularAuth.status is Null Or SampleToRegularAuth.Status=0"+
                   " group by ROrderNo ,DyeingOrderDetails.FibreCode,DyeingOrderNo,OrderCompleted )t"+
                   " Inner Join DyeingOrder On DyeingOrder.OrderNo = T.DyeingOrderNo"+
                   " Left Join Agndetails On AgnDetails.OrderNo = T.DyeingOrderNo"+
                   " Left Join Agn On Agn.agnNo = AgnDetails.AgnNo "+
                   " group by ROrderNo,FibreCode,OrderCompleted,weight ";


          try
          {
               if(connect == null)
               {
                    theConnect          = JDBCConnection.getJDBCConnection();
                    connect             = theConnect.getConnection();
               }

               PreparedStatement stat      = connect.prepareStatement(QS);
               ResultSet res       = stat.executeQuery();
               while(res.next())
               {
                    VDyeOrderNo.addElement(res.getString(1));
                    VFibreCode.addElement(res.getString(2));
                    VWeight   .addElement(res.getString(3));
                    VFIDate.addElement(res.getString(4));
                    VDeliveryDate.addElement(res.getString(5));
                    VFRDate  .addElement(res.getString(6));
                    VOrderStatus.addElement(res.getString(7));
               }
               res.close();
               stat.close();

             System.out.println("VDyeOrderNo-->"+VDyeOrderNo.size());
          }
          catch(Exception e)
          {
               System.out.println(e);
               e.printStackTrace();
          }

     }
     private String getFibreDyeingDate(String SOrderNo,String Status)
     {
          Status =common.parseNull(Status);
          if(Status.equals("FD"))
          {
               for(int i=0; i<VDyeOrderNo.size();i++)
               {
                     String DyeingOrderNo =(String)VDyeOrderNo.elementAt(i);
                     if(SOrderNo.equals(DyeingOrderNo))
                     {
                         return common.parseDate((String)VDeliveryDate.elementAt(i));
                     }
               }
          }
       return "";
     }
     private String getFibreReciptDate(String SOrderNo)
     {

          String SDate ="";
          for(int i=0; i<VDyeOrderNo.size();i++)
          {
                String DyeingOrderNo =(String)VDyeOrderNo.elementAt(i);
                if(SOrderNo.equals(DyeingOrderNo))
                {
                    SDate =  (String)VFRDate.elementAt(i);
                }

          }

       return SDate;
     }
     private String getFibreIssueDate(String SOrderNo)
     {

          String SDate ="";
          for(int i=0; i<VDyeOrderNo.size();i++)
          {
                String DyeingOrderNo =(String)VDyeOrderNo.elementAt(i);
                if(SOrderNo.equals(DyeingOrderNo))
                {
                    SDate =  (String)VFIDate.elementAt(i);
                }

          }

       return SDate;
     }


     private void DateAlign()
     {
          
          try
          {
               VTDate    = new Vector();
               VFDDate   = new Vector();
               VSDate    = new Vector();

               for(int i=0;i<VOrderNo.size();i++)
               {
                    int iFibreDy   = 0;
                    int iIssueDt   = 0;
                    int iReceiptDt = 0;

                    String Order   = common.parseNull((String)VOrderNo.elementAt(i));
                    int iOrderDt   = common.toInt((String)VOrderDate.elementAt(i));

                    int iRepDt     = common.toInt(common.getServerDate());

                    iIssueDt       = common.toInt(getFibreIssueDate(Order));
                    iReceiptDt     = common.toInt(getFibreReciptDate(Order));


                 /*
                      if(iReceiptDt!=0 && iIssueDt!=0)
                      {
                         iFibreDy  = common.getDateDiff(iReceiptDt,iIssueDt);
                      }
                      else if(iIssueDt!=0 && iReceiptDt==0)
                      {
                         iFibreDy  = common.getDateDiff(iRepDt,iIssueDt);
                      }
                 */

                    String SStatus = common.parseNull((String)VStatus.elementAt(i));

                    int iTotalDy   = common.getDateDiff(iRepDt,iOrderDt)+1;

                    iFibreDy       = getGIDate(Order,(String)VSampleOrderNo.elementAt(i),common.parseNull((String)VOrderDate.elementAt(i)));

                    VTDate         . addElement(String.valueOf(iTotalDy));

                    if(iFibreDy > 0)
                    {
                         VFDDate   . addElement(String.valueOf(iFibreDy));
                         VSDate    . addElement(String.valueOf(iTotalDy-iFibreDy));
                    }
                    else
                    {
                         if(SStatus.equals("FD"))
                         {
                              VFDDate   . addElement(String.valueOf(iTotalDy));
                              VSDate    . addElement("0");
                         }
                         else
                         {
                              VFDDate   . addElement("0");
                              VSDate    . addElement(String.valueOf(iTotalDy));
                         }
                    }
              }
         }
         catch(Exception e)
         {
              System.out.println(e);
         }

     }


   private void    getShortagePrn()
   {
       String Str1    = "Reference Order Shortage\n";
      prnWriter(Str1); 

    
        prnWriter("-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
        prnWriter("|        |            |              |                         |                        |               |   Unit    |            | Packed   |               |                 |");
        prnWriter("|  S.No  | Order Date |    orderno   |      PartyName          |     Order  weight      |      Count    |   Name    |  RepName   | Weight   |  ExshQty      |    ExshPer      |");
        prnWriter("|        |            |              |                         |                        |               |           |            |          |               |                 |");
        prnWriter("-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
       
 PrintBody();     

   }
 private void PrintBody()
     {
          try {
       System.out.println("Enter into this Shortage Prn Body");
               System.out.println(" Prn ShortageList.size()====>"+ ShortageList.size());
          for (int i = 0; i < ShortageList.size(); i++) {
            HashMap theMap = (HashMap) ShortageList.get(i);



            String sorderdate = common.parseDate((String) theMap.get("ORDERDATE"));
            String sorderno = (String) theMap.get("RORDERNO");
            String sPartyname = (String) theMap.get("PARTYNAME");
            String sorderweight = (String) theMap.get("ORDERWEIGHT");
            String scountname = (String) theMap.get("COUNTNAME");
            String sunitname = (String) theMap.get("UNITNAME");
            String srepname = (String) theMap.get("REPNAME");
            String spackedweight = (String) theMap.get("PACKEDWEIGHT");
            String sexsqty = (String) theMap.get("EXSHQTY");
            String sexper = (String) theMap.get("EXSHPER");

       
               String List1=Integer.toString(i+1);
                       String List2=sorderdate;
                       String List123=sorderno;
                       String List3=sPartyname;
                       String List4=sorderweight;
                       String List5=(scountname);
                       String List234=sunitname;
                       String List6=srepname;
                       String List7=spackedweight;
                       String List8=sexsqty;
                       String List9=sexper;
                                                         
                       List1     = common.Pad(List1,8)+" |";
                       List2     = common.Cad(List2,11)+" |";
                       List123   = common.Cad(common.parseNull(List123),13)+" |";
                       List3     = common.Pad(List3,24)+" |";
                       List4     = common.Cad(List4,23)+" |";
                       List5     = common.Cad(List5,14)+" |";
                       List234   = common.Pad(List234,11)+"|";
                       List6     = common.Pad(List6,11)+" |";
                       List7     = common.Rad(List7,9)+" |";
                       List8     = common.Rad(List8,13)+"  |";
                       List9     = common.Rad(List9,15)+"  |";
                                                         
                       String Strl   = List1+List2+List123+List3+List4+List5+List234+List6+List7+List8+List9;
                       prnWriter(Strl);
                      // fw.write(SLine + "\n");
                      
            
          }
                      prnWriter("-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
         ShortageList.clear();           
           } catch (Exception e) {
            System.out.println(e);
        }
         
     }
     private void getPrintWriter()
     {
           try
           {

           //    FW = new FileWriter("//"+SCRIPTHOST+"/d/"+SCRIPTFOLDER+"/src/temp/RegularOrderSampleStatus.prn");

               FW = new FileWriter("/software/MixPrint/RegularOrderSampleStatus.prn");
//               FW = new FileWriter("d://RegularOrderSampleStatus.prn");

               prnHead();
               if(bParty)
               prnBody();
               if(bDepo)
               DepoBody();
           }
           catch(Exception e)
           {
               System.out.println(e);
               e.printStackTrace();
           }
     }


     private void prnHead()
     {
          String Str1    = "Company   : AMARJOTHI SPINNING MILLS LIMITED\n";
          String Str2    = "Document   : Regular Order Sample Status  \n";
          String Str3    = "As On Date : "+common.parseDate(SEnDate)+"\n";
          String Unit    = "Unit       : "+SUnit+", Process Type : "+SProcessTypeName+", Order Type : "+SShortageTypeName+"\n";
          String Str4    = "Page No    : "+iPage+"\n g";

//                SHead  = "|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|";

//                Head1  = "|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n";
//                Head2  = "|     |           |             |                             |                    Order Weight                      |         |          |                          |      |      |      |          |          |          |          |           |              |      | Bleach|          |                              |\n";
//                Head3  = "|     |    Order  |  OrderNo    |           Party Name        |------------------------------------------------------|  Count  |RM OrderNo|        Status            | Total|  FD  |Sample| Dyeing   | Dyeing   | Dyeing   | Dyeing   |   Shade   |     Blend    | Depth| Cotton|  Basic   |         Remarks              |\n";
//                Head4  = "|S.No |    Date   |             |                             | A UNIT   | B UNIT   | B UNIT   | C UNIT   | S UNIT   |         |          |                          | Days | Days |Days  | Code/Kgs | Code/Kgs | Code/Kgs | Code/Kgs |           |              |      | Depth | OrderNo  |                              |\n";
//                Head5  = "|     |           |             |                             |          | <1500 Kgs|>1500 Kgs |          |          |         |          |                          |      |      |      |          |          |          |          |           |              |      |       |          |                              |\n";

                Head1  = "|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n";
                Head2  = "|     |           |          |     |                        |                    Order Weight                      |     |          |                     |      |      |      |          |          |          |          |           |              |      | Bleach|          |                         |  Cur.  |  Prev. |\n";
                Head3  = "|     |    Order  |  OrderNo | Bin |      Party Name        |------------------------------------------------------|Count|RM OrderNo|     Status          | Total|  FD  |Sample| Dyeing   | Dyeing   | Dyeing   | Yarn Type|   Shade   |     Blend    | Depth| Cotton|  Basic   |    Remarks              | Process|Process |\n";
                Head4  = "|S.No |    Date   |          |     |                        | A UNIT   | B UNIT   | B UNIT   | A UNIT-A | S UNIT   |     |          |                     | Days | Days |Days  | Code/Kgs | Code/Kgs | Code/Kgs |          |           |              |      | Depth | OrderNo  |                         |  Type  |  Type  |\n";
                Head5  = "|     |           |          |     |                        |          | <1500 Kgs|>1500 Kgs |          |          |     |          |                     |      |      |      |          |          |          |          |           |              |      |       |          |                         |        |        |\n";

                SLine  = "|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|";
                SLine1 = "|=====-===========-==========-=====-========================-==========-==========-==========-==========-==========|=====-==========-=====================-======-======-======-==========-==========-==========-=========--===========-==============-======-=======-==========-===========================================|";

                SEnd   = "----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|";

                 Empty      ="|"+common.Space(5)+"|"
                                       +common.Space(11)+"|"
                                       +common.Space(13)+"|"
                                       +common.Space(5)+"|"
                                       +common.Space(29)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(9)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(26)+"|"
                                       +common.Space(6)+"|"
                                       +common.Space(6)+"|"
                                       +common.Space(6)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(11)+"|"
                                       +common.Space(14)+"|"
                                       +common.Space(6)+"|"
                                       +common.Space(7)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(30)+"|";


          prnWriter(Str1+Str2+Str3+Unit+Str4);
          if(bParty)
          {
               if(SUnit.equals("Sample_Unit"))
               {
                    prnWriter("SAMPLE UNIT REGULARORDER SAMPLING STATUS ");
               }
               else
               {
                    prnWriter("PARTY ORDER DETAILS");
               }
               prnWriter(Head1+Head2+Head3+Head4+Head5+SLine);
          }

     }
     private void prnBody()
     {
             int iSNo =0;
             double dTotOrdWtA=0;
             double dTotOrdWtB1=0;
             double dTotOrdWtB2=0;
             double dTotOrdWtC=0;
             double dTotOrdWtS=0;
             double dNetOrdWt=0;

             double dFDWeight=0;

             for(int i=0;i<VOrderNo.size();i++)
             {
                              String SEntryDt= common.parseNull((String)VEntryDate.elementAt(i));
                              try
                              {
                                   SEntryDt  = SEntryDt.substring(11,SEntryDt.length());
                              }
                              catch(Exception ex)
                              {
                                   SEntryDt  = "";
                              }

                String SOrderNo=(String)VOrderNo.elementAt(i);
                String SBasic  =common.parseNull((String)VBasicOrderNo.elementAt(i));
                String Party  =(String)VPartyName.elementAt(i);
                String Shade  =(String)VShade.elementAt(i);
                String SLightSource = (String)VLightSource.elementAt(i);

                dFDWeight     = getFDWeight(common.parseNull((String)VStatus.elementAt(i)));

                        if(!SOrderNo.startsWith("DM") && !Party.startsWith("AMARJOTHI"))
                         {

                           String SStatus = common.parseNull((String)VStatus.elementAt(i));
                           getFibreRate(SOrderNo,SStatus);


                          if(SFibredyeing.equals("All") || SFibredyeing.equals("Fibre Dyeing"))
                           {

                              if(SFibredyeing.equals("Fibre Dyeing"))
                             { 

                            if(((String)VFibreRate.elementAt(0)).equals("") && ((String)VFibreRate.elementAt(1)).equals("") && ((String)VFibreRate.elementAt(1)).equals("") && ((String)VFibreRate.elementAt(2)).equals("") )
                                {
                                  continue;
                                }

                              } 

          
                              String SVisPer =(String)VVisPer.elementAt(i);
                              String SCotPer =(String)VCotPer.elementAt(i);
                              String SPolyPer=(String)VPolyPer.elementAt(i);
                              String SBlend  =SCotPer+"%C,"+SVisPer+"%V,"+SPolyPer+"%P";
                                iSNo =iSNo+1;
                                String SOrdWtA="";
                                String SOrdWtB1="";
                                String SOrdWtB2="";
                                String SOrdWtC="";
                                String SOrdWtS="";

                                int iUnitCode = common.toInt((String)VUnitCode.elementAt(i));
                                double dOrdWt =common.toDouble((String)VOrderWt.elementAt(i));

                                if(dOrdWt <=1500 && iUnitCode == 2)
                                   {
                                         SOrdWtB1=common.getRound(dOrdWt,3);
                                         dTotOrdWtB1=dTotOrdWtB1+dOrdWt;
                                    }
                                else if (dOrdWt >=1500 && iUnitCode == 2)
                                    {
                                        SOrdWtB2=common.getRound(dOrdWt,3);
                                        dTotOrdWtB2=dTotOrdWtB2+dOrdWt;
                                    }   
                                else if (iUnitCode == 1) 
                                    {
                                        SOrdWtA=common.getRound(dOrdWt,3);
                                        dTotOrdWtA=dTotOrdWtA+dOrdWt;
                                    }   
                                else if (iUnitCode == 3) 
                                    {
                                        SOrdWtS=common.getRound(dOrdWt,3);
                                        dTotOrdWtS=dTotOrdWtS+dOrdWt;
                                    }   
//                                else if (iUnitCode == 10) 
                                else if (iUnitCode == 12) 
                                    {
                                        SOrdWtC=common.getRound(dOrdWt,3);
                                        dTotOrdWtC=dTotOrdWtC+dOrdWt;
                                    }   

                                dNetOrdWt = dNetOrdWt + dOrdWt;

                                String SNo        ="|"+common.Cad(String.valueOf(iSNo),5)+"|";
                                String SOrderNo1  =common.Pad(SOrderNo,10)+"|";
                                String SBinName   =common.Cad((String)VBinName.elementAt(i),5)+"|";
                                String SOrderDate =common.Cad(common.parseDate((String)VOrderDate.elementAt(i))+"  ",11)+"|";
                                String SParty     =common.Pad((String)VPartyName.elementAt(i),24)+"|";
                                String SCount     =common.Cad((String)VCount.elementAt(i),5)+"|";

                                String SRMOrderNo = getRMOrderNo(i,SOrderNo);
                                String SRMOrder   =common.Pad(SRMOrderNo,10)+"|";

                                SStatus    =common.Pad((String)VStatus.elementAt(i),21)+"|";
                                String STDate     =common.Cad((String)VTDate.elementAt(i),6)+"|";

                                getFDDelayDays(SRMOrderNo, common.parseNull((String)VOrderDate.elementAt(i)), SOrderNo);

//                                String SFDDate    =common.Cad((String)VFDDate.elementAt(i),6)+"|";
                                String SFDDate    =common.Cad(SFDDelayDays,6)+"|";

//                                String SDate      =common.Cad((String)VSDate.elementAt(i),6)+"|";
                                String SDate      =common.Cad(SSampleDays,6)+"|";

                                String SShade     =common.Pad(common.parseNull((String)VShade.elementAt(i)),11)+"|";
                                String SRemarks   = common.Pad(common.parseNull((String)VRemarks.elementAt(i)),25)+"|";

                                String SYarnType  = common.Pad(common.parseNull((String)VYarnType.elementAt(i)),10)+"|";

                                String SBDepth    = common.Rad(getDepthVal(SBasic),7)+"|";

                                int iBlendChanged = common.toInt((String)VBlendChanged.elementAt(i));
               
                                if(iBlendChanged==1)         {
               
                                   setBlendData(SOrderNo,iBlendChanged);
                                }
                                if(iBlendChanged==0)         {
               
                                   if(SBasic.equals("DIRECT")) {
               
                                        setBlendData(SOrderNo,1);
                                   }else {
               
                                        setBlendData(SBasic,iBlendChanged);
                                   }
                                }
               

                                String sAllotedBlend = "";
               
                                for(int j=0;j<vNewBlendName.size();j++) {
               
                                     sAllotedBlend += common.parseNull((String)vNewBlendName.elementAt(j))+"-"+common.parseNull((String)vNewBlendPer.elementAt(j))+",";
                                }
               
                                if(sAllotedBlend.length()>1 ) {
               
                                     sAllotedBlend = sAllotedBlend.substring(0,sAllotedBlend.length()-1);
                                }
               
               

//                                       SBlend     =common.Pad(SBlend,14)+"|";
                                       SBlend     =common.Pad(sAllotedBlend,14)+"|";
                                String SDepth     =common.Cad(getDepth(SBasic),6)+"|";
                                       SBasic     =common.Pad(SBasic,10)+"|";
                               getFibreRate(SOrderNo,common.parseNull((String)VStatus.elementAt(i)));
                               String SFibreRW1 =common.Rad((String)VFibreRate.elementAt(0),10)+"|";
                               String SFibreRW2 =common.Rad((String)VFibreRate.elementAt(1),10)+"|";
                               String SFibreRW3 =common.Rad((String)VFibreRate.elementAt(2),10)+"|";
                               String SFibreRW4 =common.Rad((String)VFibreRate.elementAt(3),10)+"|";


                                SOrdWtA =common.Rad(SOrdWtA,10)+"|";
                                SOrdWtB1 =common.Rad(SOrdWtB1,10)+"|";
                                SOrdWtB2 =common.Rad(SOrdWtB2,10)+"|"; 
                                SOrdWtC =common.Rad(SOrdWtC,10)+"|";
                                SOrdWtS =common.Rad(SOrdWtS,10)+"|";


                              String SCurProcessType   = common.Pad(common.parseNull((String)VOrderProcessType.elementAt(i)), 8)+"|";

                              String SPartyCode        = common.parseNull((String)VPartyCode.elementAt(i));
                              String SROrderNo         = (String)VOrderNo.elementAt(i);
                              String SRefOrderNo       = common.parseNull((String)VBasicOrderNo.elementAt(i));

                              String SLastProcessType  = common.Pad(getLastProcessType(SPartyCode, SRefOrderNo, SROrderNo), 8)+"|";

                                getDaysLine((String)VTDate.elementAt(i));
//                              prnWriter(SNo+SOrderDate+SOrderNo1+SParty+SOrdWtA+SOrdWtB1+SOrdWtB2+SOrdWtC+SOrdWtS+SCount+SRMOrder+SStatus+STDate+SFDDate+SDate+SFibreRW1+SFibreRW2+SFibreRW3+SFibreRW4+SShade+SBlend+SDepth+SBDepth+SBasic+SRemarks);
                                prnWriter(SNo+SOrderDate+SOrderNo1+SBinName+SParty+SOrdWtA+SOrdWtB1+SOrdWtB2+SOrdWtC+SOrdWtS+SCount+SRMOrder+SStatus+STDate+SFDDate+SDate+SFibreRW1+SFibreRW2+SFibreRW3+SYarnType+SShade+SBlend+SDepth+SBDepth+SBasic+SRemarks+SCurProcessType+SLastProcessType);
                                prnWriter(SLine);

                         }

                 }


             }
                 String STotal      ="|"+common.Space(5)+"|"
                                       +common.Space(11)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(5)+"|"
                                       +common.Cad("Total",24)+"|"
                                       +common.Rad(common.getRound(dTotOrdWtA,3),10)+"|"
                                       +common.Rad(common.getRound(dTotOrdWtB1,3),10)+"|"
                                       +common.Rad(common.getRound(dTotOrdWtB2,3),10)+"|"
                                       +common.Rad(common.getRound(dTotOrdWtC,3),10)+"|"
                                       +common.Rad(common.getRound(dTotOrdWtS,3),10)+"|"
                                       +common.Space(5)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(21)+"|"
                                       +common.Space(6)+"|"
                                       +common.Space(6)+"|"
                                       +common.Space(6)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(11)+"|"
                                       +common.Space(14)+"|"
                                       +common.Space(6)+"|"
                                       +common.Space(7)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(25)+"|"
                                       +common.Space(8)+"|"
                                       +common.Space(8)+"|";

                 String SGTotal      ="|"+common.Space(5)+"|"
                                       +common.Space(11)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(5)+"|"
                                       +common.Cad("Grand Total",24)+"|"
                                       +common.Rad(common.getRound(dNetOrdWt,3),10)+"|"
                                       +common.Rad("",10)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(5)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(21)+"|"
                                       +common.Space(6)+"|"
                                       +common.Space(6)+"|"
                                       +common.Space(6)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(11)+"|"
                                       +common.Space(14)+"|"
                                       +common.Space(6)+"|"
                                       +common.Space(7)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(25)+"|"
                                       +common.Space(8)+"|"
                                       +common.Space(8)+"|";

                 try
                 {
                      prnWriter(SLine);
                      prnWriter(STotal);
                      prnWriter(SGTotal);
                      //prnWriter(SEnd+"");
                      prnWriter(SEnd);

                      /* Start New Details Get From HO Report.. 18.09.2014.. */

                   double dNetWt    = dPartyWtL+dPartyWtH;

                   double dBalTotal = (dNetWt+dMixingWt)-dFDWeight;

                   String SNetWt=common.getRound(dNetWt,3);
                   String SFDWt =common.getRound(dFDWeight,3);
                   String SBalWt=common.getRound(dBalTotal,3);
                   int iProcessDays = common.toInt(common.getRound((dBalTotal/7500),0));
                   int iFibreDays   = common.toInt(common.getRound((dFDWeight/7500),0));

                      prnWriter("GrandTotal    = "+SNetWt);
                      prnWriter("MixingPending = "+dMixingWt);
                      prnWriter("FibreDyeing   = "+SFDWt);
                      prnWriter("UnderMatching = "+SBalWt);
                      prnWriter("Processing Available Days = "+iProcessDays);
                      prnWriter("FibreDyeing Days          = "+iFibreDays);
                      prnWriter("");
                      prnWriter("");

                      /* End New Details Get From HO Report.. 18.09.2014.. */

                    // if(SShortageTypeName.equals("SHORTAGE"))
                     //{   
                        prnWriter(" Report Taken As On  Date "+common.parseDate(common.getServerDate())+" Time : " + common.getServerTime()+"");
                   // }else{
                    //  prnWriter(" Report Taken As On  Date "+common.parseDate(common.getServerDate())+" Time : " + common.getServerTime()+"");
                   // } 
                 }catch(Exception e)
                 {
                    System.out.println(e);
                 }

     }

     private double getFDWeight(String SFibreDye)
     {
          double dWeight=0;
          for(int i=0; i<VOrderNo.size();i++)
          {
                String SFibreStatus =common.parseNull((String)VStatus.elementAt(i));
                if(SFibreStatus.equals("FD"))
                {
                    dWeight+=common.toDouble((String)VOrderWt.elementAt(i));;
                }
          }
       return dWeight;
     }

     private void DepoBody()
     {
             double dTotOrdWtA=0;
             double dTotOrdWtB1=0;
             double dTotOrdWtB2=0;
             double dTotOrdWtC=0;
             double dTotOrdWtS=0;
             double dNetOrdWt=0;


             iTLine=0;
             prnWriter("DEPO ORDER DETAILS");
             prnWriter(Head1+Head2+Head3+Head4+Head5+SLine);
             int iSNo =0;
             for(int i=0;i<VOrderNo.size();i++)
             {
                         String SOrderNo=(String)VOrderNo.elementAt(i);
                         String SBasic  =common.parseNull((String)VBasicOrderNo.elementAt(i));
                         String Party  =(String)VPartyName.elementAt(i);
                         String Shade  =(String)VShade.elementAt(i);
                         String SLightSource = (String)VLightSource.elementAt(i);

                         String SEntryDt= common.parseNull((String)VEntryDate.elementAt(i));

                         try
                         {
                              SEntryDt  = SEntryDt.substring(11,SEntryDt.length());
                         }
                         catch(Exception ex)
                         {
                              SEntryDt  = "";
                         }


                         if(SOrderNo.startsWith("DM") || Party.startsWith("AMARJOTHI") && Shade.startsWith("LGREY"))
                         {
                              String SVisPer =(String)VVisPer.elementAt(i);
                              String SCotPer =(String)VCotPer.elementAt(i);
                              String SPolyPer=(String)VPolyPer.elementAt(i);
                              String SBlend  =SCotPer+"%C,"+SVisPer+"%V,"+SPolyPer+"%P";

                                iSNo = iSNo+1;

                                String SOrdWtA="";
                                String SOrdWtB1="";
                                String SOrdWtB2="";
                                String SOrdWtC="";
                                String SOrdWtS="";

                                int iUnitCode = common.toInt((String)VUnitCode.elementAt(i));
                                double dOrdWt =common.toDouble((String)VOrderWt.elementAt(i));

                                System.out.println("iUnitCode"+iSNo+"-->"+iUnitCode);
                                System.out.println("dOrdWt-->"+dOrdWt);

                                if(dOrdWt <=1500 && iUnitCode == 2)
                                   {
                                         SOrdWtB1=common.getRound(dOrdWt,3);
                                         dTotOrdWtB1=dTotOrdWtB1+dOrdWt;
                                    }
                                else if (dOrdWt >=1500 && iUnitCode == 2)
                                    {
                                        SOrdWtB2=common.getRound(dOrdWt,3);
                                        dTotOrdWtB2=dTotOrdWtB2+dOrdWt;
                                    }   
                                else if (iUnitCode == 1) 
                                    {
                                        SOrdWtA=common.getRound(dOrdWt,3);
                                        dTotOrdWtA=dTotOrdWtA+dOrdWt;
                                    }   
                                else if (iUnitCode == 3) 
                                    {
                                        SOrdWtS=common.getRound(dOrdWt,3);
                                        dTotOrdWtS=dTotOrdWtS+dOrdWt;
                                    }   
//                                else if (iUnitCode == 10) 
                                else if (iUnitCode == 12) 
                                    {
                                        SOrdWtC=common.getRound(dOrdWt,3);
                                        dTotOrdWtC=dTotOrdWtC+dOrdWt;
                                    }   

                                dNetOrdWt = dNetOrdWt + dOrdWt;

               
                                String SNo        ="|"+common.Cad(String.valueOf(iSNo),5)+"|";
                                String SOrderNo1  =common.Pad(SOrderNo,10)+"|";
                                String SBinName   =common.Cad((String)VBinName.elementAt(i),5)+"|";
                                String SOrderDate =common.Cad(common.parseDate((String)VOrderDate.elementAt(i))+"  ",11)+"|";
                                String SParty     =common.Pad((String)VPartyName.elementAt(i),24)+"|";
                                String SCount     =common.Cad((String)VCount.elementAt(i),5)+"|";

                                String SRMOrderNo =getRMOrderNo(i,SOrderNo);
                                String SRMOrder   =common.Pad(SRMOrderNo,10)+"|";

                                String SStatus    =common.Pad((String)VStatus.elementAt(i),21)+"|";
                                String SFibreDt   =common.Pad(getFibreDyeingDate(SOrderNo,(String)VStatus.elementAt(i)),11)+"|";
                                String STDate     =common.Cad((String)VTDate.elementAt(i),6)+"|";

                                getFDDelayDays(SRMOrderNo, common.parseNull((String)VOrderDate.elementAt(i)), SOrderNo);

//                                String SFDDate    =common.Cad((String)VFDDate.elementAt(i),6)+"|";
                                String SFDDate    =common.Cad(SFDDelayDays,6)+"|";

//                                String SDate      =common.Cad((String)VSDate.elementAt(i),6)+"|";
                                String SDate      =common.Cad(SSampleDays,6)+"|";

                                String SCode      =common.Cad("",10)+"|";
                                String SShade     =common.Pad(common.parseNull((String)VShade.elementAt(i)),11)+"|";

                                int iBlendChanged = common.toInt((String)VBlendChanged.elementAt(i));
               
                                if(iBlendChanged==1)         {
               
                                   setBlendData(SOrderNo,iBlendChanged);
                                }
                                if(iBlendChanged==0)         {
               
                                   if(SBasic.equals("DIRECT")) {
               
                                        setBlendData(SOrderNo,1);
                                   }else {
               
                                        setBlendData(SBasic,iBlendChanged);
                                   }
                                }
               
                                String sAllotedBlend = "";
               
                                for(int j=0;j<vNewBlendName.size();j++) {
               
                                     sAllotedBlend += common.parseNull((String)vNewBlendName.elementAt(j))+"-"+common.parseNull((String)vNewBlendPer.elementAt(j))+",";
                                }
               
                                if(sAllotedBlend.length()>1 ) {
               
                                     sAllotedBlend = sAllotedBlend.substring(0,sAllotedBlend.length()-1);
                                }

//                                       SBlend     =common.Pad(SBlend,14)+"|";
                                       SBlend     =common.Pad(sAllotedBlend,14)+"|";



                                String SDepth     =common.Cad(getDepth(SBasic),6)+"|";
                                       SBasic     =common.Pad(SBasic,10)+"|";
                              String SRemarks     =common.Pad(common.parseNull((String)VRemarks.elementAt(i)),25)+"|";

                                String SYarnType  = common.Pad(common.parseNull((String)VYarnType.elementAt(i)),10)+"|";

                                String SBDepth    = common.Rad(getDepthVal(SBasic),7)+"|";   


                               getFibreRate(SOrderNo,common.parseNull((String)VStatus.elementAt(i)));
                               String SFibreRW1 =common.Rad((String)VFibreRate.elementAt(0),10)+"|";
                               String SFibreRW2 =common.Rad((String)VFibreRate.elementAt(1),10)+"|";
                               String SFibreRW3 =common.Rad((String)VFibreRate.elementAt(2),10)+"|";
                               String SFibreRW4 =common.Rad((String)VFibreRate.elementAt(3),10)+"|";


                                SOrdWtA =common.Rad(SOrdWtA,10)+"|";
                                SOrdWtB1 =common.Rad(SOrdWtB1,10)+"|";
                                SOrdWtB2 =common.Rad(SOrdWtB2,10)+"|"; 
                                SOrdWtC =common.Rad(SOrdWtC,10)+"|";
                                SOrdWtS =common.Rad(SOrdWtS,10)+"|";

                                System.out.println("SOrdWtB1-->"+SOrdWtB1);

                                getDaysLine((String)VTDate.elementAt(i));

                              String SCurProcessType   = common.Pad(common.parseNull((String)VOrderProcessType.elementAt(i)), 8)+"|";

                              String SPartyCode        = common.parseNull((String)VPartyCode.elementAt(i));
                              String SROrderNo         = (String)VOrderNo.elementAt(i);
                              String SRefOrderNo       = common.parseNull((String)VBasicOrderNo.elementAt(i));

                              String SLastProcessType  = common.Pad(getLastProcessType(SPartyCode, SRefOrderNo, SROrderNo), 8)+"|";

//                                prnWriter(SNo+SOrderDate+SOrderNo1+SParty+SOrdWtA+SOrdWtB1+SOrdWtB2+SOrdWtC+SOrdWtS+SCount+SRMOrder+SStatus+STDate+SFDDate+SDate+SFibreRW1+SFibreRW2+SFibreRW3+SFibreRW4+SShade+SBlend+SDepth+SBDepth+SBasic+SRemarks+SCurProcessType+SLastProcessType);
                                prnWriter(SNo+SOrderDate+SOrderNo1+SBinName+SParty+SOrdWtA+SOrdWtB1+SOrdWtB2+SOrdWtC+SOrdWtS+SCount+SRMOrder+SStatus+STDate+SFDDate+SDate+SFibreRW1+SFibreRW2+SFibreRW3+SYarnType+SShade+SBlend+SDepth+SBDepth+SBasic+SRemarks+SCurProcessType+SLastProcessType);
                                prnWriter(SLine);
                         }


             }
                 String STotal      ="|"+common.Space(5)+"|"
                                       +common.Space(11)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(5)+"|"
                                       +common.Cad("Total",24)+"|"
                                       +common.Rad(common.getRound(dTotOrdWtA,3),10)+"|"
                                       +common.Rad(common.getRound(dTotOrdWtB1,3),10)+"|"
                                       +common.Rad(common.getRound(dTotOrdWtB2,3),10)+"|"
                                       +common.Rad(common.getRound(dTotOrdWtC,3),10)+"|"
                                       +common.Rad(common.getRound(dTotOrdWtS,3),10)+"|"
                                       +common.Space(5)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(21)+"|"
                                       +common.Space(6)+"|"
                                       +common.Space(6)+"|"
                                       +common.Space(6)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(11)+"|"
                                       +common.Space(14)+"|"
                                       +common.Space(6)+"|"
                                       +common.Space(7)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(25)+"|"
                                       +common.Space(8)+"|"
                                       +common.Space(8)+"|";

                 String SGTotal      ="|"+common.Space(5)+"|"
                                       +common.Space(11)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(5)+"|"
                                       +common.Cad("Grand Total",24)+"|"
                                       +common.Rad(common.getRound(dNetOrdWt,3),10)+"|"
                                       +common.Rad("",10)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(5)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(21)+"|"
                                       +common.Space(6)+"|"
                                       +common.Space(6)+"|"
                                       +common.Space(6)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(11)+"|"
                                       +common.Space(14)+"|"
                                       +common.Space(6)+"|"
                                       +common.Space(7)+"|"
                                       +common.Space(10)+"|"
                                       +common.Space(25)+"|"
                                       +common.Space(8)+"|"
                                       +common.Space(8)+"|";

             try
             {
                  prnWriter(SLine);
                  prnWriter(STotal);
                  prnWriter(SGTotal);
                  prnWriter(SEnd);
                  prnWriter(" Report Taken As On  Date "+common.parseDate(common.getServerDate())+" Time : " + common.getServerTime()+"");
            }catch(Exception e)
            {
               System.out.println(e);
            }
     }

     private void prnWriter(String Str)
     {
          try
          {
               iTLine = iTLine+1;
               if(iTLine<56)
               {
                    FW.write(Str+"\n");
               }
               else
               {
                  iPage=iPage+1;
                  iTLine=0;
                  FW.write(SEnd+"\n");
                  prnWriter(" Report Taken As On  Date "+common.parseDate(common.getServerDate())+" Time : " + common.getServerTime()+"");
                  prnHead();
                  prnWriter(Str);

               }
          }
          catch(Exception e)
          {
            System.out.println(e);
          }

     }
     private void getFibreRate(String SOrderNo,String Status)
     {

          VFibreRate = new Vector();
          VFibreStatus = new Vector();
               if(Status.equals("FD"))
               {
                    for(int i=0;i<VFibreCode.size();i++)
                    {
                            String SFibre =(String)VFibreCode.elementAt(i);
                            String SWt    =(String)VWeight.elementAt(i);      
                            String SOrdNo =(String)VDyeOrderNo.elementAt(i);
                              

                            if(SOrderNo.equals(SOrdNo))
                            {
                           System.out.println("SOrderNo-->"+SOrderNo);
                           System.out.println("Status-->"+SOrderNo);
                           System.out.println("SFibre -->"+SFibre);
                           System.out.println("Wt -->"+SWt);

                               System.out.println("SOrdNo-->"+SOrdNo);

                                //  if(getFDStatus(SOrderNo)==0)
                                //  {
                                        System.out.println("Line1");
                                        VFibreRate.addElement(SFibre+"/"+SWt);
                                //  } 
                            }
                 
          
                    }
               }
          int iSize =VFibreRate.size();

          for(int j=iSize;j<=3;j++)
          {
            String Str="";
            VFibreRate.add(j,"");
          }
     }
     private String getDepth(String SOrderNo)
     {

              String SDepth ="";
              if(SOrderNo.equals("DIRECT"))
              return "";
              SDepth =getRegOrdDepth(SOrderNo);
              if(!SDepth.equals(""))
              {
                return common.parseNull(SDepth) ;
              }
              else
              {
                 SDepth = getPremDepth(SOrderNo);
                 if(common.parseNull(SDepth).equals(""))
                 SDepth = getSampleDepth(SOrderNo);
              } 

     return SDepth;
     }
     private String getRegOrdDepth(String SOrderNo)
     {
        String SDepth="";
        for(int i=0; i < VRDOrderNo.size();i++)
        {
               String SampleNo =common.parseNull((String)VRDOrderNo.elementAt(i));
               if(SampleNo.equals(SOrderNo))
               {
                  SDepth =common.parseNull((String)VRDepth.elementAt(i));  
                  return SDepth;
               }

        }

        return SDepth;
          
     }

     private String getSampleDepth(String SOrderNo)
     {
        String SDepth="";
        for(int i=0; i < VSampleNo.size();i++)
        {
               String SampleNo = common.parseNull((String)VSampleNo.elementAt(i));
               if(SampleNo.equals(SOrderNo))
               {
                  SDepth =common.parseNull((String)VSampleDepth.elementAt(i));  
                  return SDepth;
               }

        }

        return SDepth;
          
     }
     private void setSampleDepth()
     {

         VSampleNo = new Vector();
         VSampleDepth    = new Vector();

         String QS =" Select  ROrderNo ,OrdNo,SMixir.Depth from RegularOrder"+
                    " Inner Join Smixir  On Smixir.OrdNo  = RegularOrder.RefOrderNo"+
                    " Left Join  SampleToRegularAuth On SampleToRegularAuth.RegularOrderNo=RegularOrder.ROrderNo"+
                    " Where  RegularOrder.OrderDate > = 20061001 and RegularOrder.Status=1  "+
                    " and SampleToRegularAuth.status is Null Or SampleToRegularAuth.Status=0"+
                    " and RegularOrder.Orderdate < ="+iEnDate ; // Commented on 17.07.2015

System.out.println("QS----->"+QS);
          try
          {
               if(connect == null)
               {
                    theConnect          = JDBCConnection.getJDBCConnection();
                    connect             = theConnect.getConnection();
               }

               PreparedStatement stat      = connect.prepareStatement(QS);
               ResultSet res       = stat.executeQuery();
               while(res.next())
               {
                   VSampleNo.addElement(res.getString(2));
                   VSampleDepth.addElement(res.getString(3));
               }
               res.close();
               stat.close();
          }
          catch(Exception e)
          {
               System.out.println(e);
          }


     }
    private void setRegDepth()
    {

        VRDOrderNo = new Vector();
        VRDepth    = new Vector();

        String QS =" Select RefOrderNo ,MRate from ( Select   ROrderNo,RefOrderNo,RMixir.Depth as MRate from RegularOrder"+
                   " Inner Join Rmixir On RMixir.OrdNo = RegularOrder.RefOrderNo"+
                   " Where OrderDate >20061101  and REgularOrder.Status=1 "+
                   " ) ";
        
        System.out.println("QS------>"+QS);
          try
          {
               if(connect == null)
               {
                    theConnect          = JDBCConnection.getJDBCConnection();
                    connect             = theConnect.getConnection();
               }

               PreparedStatement stat      = connect.prepareStatement(QS);
               ResultSet res       = stat.executeQuery();
               while(res.next())
               {
                   VRDOrderNo.addElement(res.getString(1));
                   VRDepth.addElement(res.getString(2));
               }
               res.close();
               stat.close();
          }
          catch(Exception e)
          {
               System.out.println(e);
          }
       
 
    }  
    private void PremDepth()
     {

         VDSOrderNo = new Vector();
         VDDepth    = new Vector();

         String QS=" Select  RegularOrder.ROrderNo,RegularOrder.RefOrderNo ,RMixir.Depth from RegularOrder"+
                   " Inner Join ShadeCardNo On ShadeCardNo.ORderNo = RegularOrder.RefOrderNo"+
                   " Inner Join RmixIr On RmixIr.OrdNo = ShadeCardNo.RefOrderNo"+
                   " Left Join  SampleToRegularAuth On SampleToRegularAuth.RegularOrderNo=RegularOrder.ROrderNo"+
                   " Where RegularOrder.OrderDate > = 20061001 and REgularOrder.Status=1 and RegularOrder.Orderdate < ="+iEnDate+" "+  // 17.07.2015
				   //" Where RegularOrder.OrderDate > = 20061001 and REgularOrder.Status=1 "+  //28-06-2021
                   "  and SampleToRegularAuth.status is Null Or SampleToRegularAuth.Status=0"+
                   " union All"+
                   " Select  RegularOrder.ROrderNo,RegularOrder.RefOrderNo ,SMixir.Depth from RegularOrder"+
                   " Inner Join ShadeCardNo On ShadeCardNo.ORderNo = RegularOrder.RefOrderNo"+
                   " Inner Join SmixIr On SmixIr.OrdNo = ShadeCardNo.RefOrderNo"+
                   " Left Join  SampleToRegularAuth On SampleToRegularAuth.RegularOrderNo=RegularOrder.ROrderNo"+
                   " Where RegularOrder.OrderDate > = 20061001 and REgularOrder.Status=1 and RegularOrder.Orderdate < ="+iEnDate+" "+ // 17.07.2015
 				   //" Where RegularOrder.OrderDate > = 20061001 and REgularOrder.Status=1 "+ // 28-06.2021
                   "  and SampleToRegularAuth.status is Null Or SampleToRegularAuth.Status=0";

                   System.out.println("QS------>"+QS);
          try
          {
               if(connect == null)
               {
                    theConnect          = JDBCConnection.getJDBCConnection();
                    connect             = theConnect.getConnection();
               }

               PreparedStatement stat      = connect.prepareStatement(QS);
               ResultSet res       = stat.executeQuery();
               while(res.next())
               {
                   VDSOrderNo.addElement(res.getString(2));
                   VDDepth.addElement(res.getString(3));
               }
               res.close();
               stat.close();
          }
          catch(Exception e)
          {
               System.out.println(e);
          }

     }
     private  void setRMOrderNos()
     {

         VRegOrderNo      = new Vector();
         VRegRMOrderNo    = new Vector();

         String QS ="Select ROrderNo ,SOrderNo from RegOrderSampleNo Order by ROrderNo";
          try
          {
               if(connect == null)
               {
                    theConnect          = JDBCConnection.getJDBCConnection();
                    connect             = theConnect.getConnection();
               }

               PreparedStatement stat      = connect.prepareStatement(QS);
               ResultSet res       = stat.executeQuery();
               while(res.next())
               {
                        VRegOrderNo .addElement(res.getString(1));
                        VRegRMOrderNo.addElement(res.getString(2));
               }
               res.close();
               stat.close();
          }
          catch(Exception e)
          {
               System.out.println(e);
          }


     }
     private String getPremDepth(String SOrderNo)
     {
        String SDepth="";
        for(int i=0; i < VDSOrderNo.size();i++)
        {
               String DSOrderNo =common.parseNull((String)VDSOrderNo.elementAt(i));
               if(DSOrderNo.equals(SOrderNo))
               {
                  SDepth =common.parseNull((String)VDDepth.elementAt(i));  
                  return SDepth;
               }
        }

        return SDepth;
     }
     private String getRMOrderNo(int iRow,String OrdNo)
     {
         String SRMOrder =common.parseNull((String)VRMOrder.elementAt(iRow));
         if(SRMOrder.equals(""))
         {
            for(int i=0 ;i< VRegOrderNo.size();i++)
            {
                 String SRegNo =common.parseNull((String)VRegOrderNo.elementAt(i));

                 if(SRegNo.equals(OrdNo))
                 {
//                   return SRMOrder =common.parseNull((String)VRegRMOrderNo.elementAt(i));

                   SRMOrder =common.parseNull((String)VRegRMOrderNo.elementAt(i));
                   break;
                 }

            }
         }
         return SRMOrder;
     }
     private int getFDStatus(String SOrderNo)
     {                                            
          int iStatus = 1;
          for(int i=0; i<VDyeOrderNo.size();i++)
          {
                String DyeingOrderNo = (String)VDyeOrderNo.elementAt(i);
                if(SOrderNo.equals(DyeingOrderNo))
                {
//                    return Integer.parseInt((String)VOrderStatus.elementAt(i));

                     if(common.toInt((String)VOrderStatus.elementAt(i)) == 0)
                    {
                         iStatus   = 0;
                         break;
                    }
                }
          }

       return iStatus;
     }
     private void getDaysLine(String STotalDt)
     {
          int iTotalDt = common.toInt(STotalDt);
          if(iTotalDt <=9 && iNineDays ==0)
          {
             prnWriter(SLine1);
             prnWriter(SLine1);
             iNineDays=1;
          }
          if(iTotalDt <=3 && iThreeDays ==0)
          {
             prnWriter(SLine1);
             iThreeDays=1;
          }

     }

     private void getGIDetails()
     {
          theGIVector                   = new Vector();

          try
          {
               if(connect == null)
               {
                    theConnect          = JDBCConnection.getJDBCConnection();
                    connect             = theConnect.getConnection();
               }

               PreparedStatement theStatement   = connect.prepareStatement(getGIDetailsQS());
               ResultSet rs             = theStatement.executeQuery();

               while(rs.next())
               {
                    RMSMDetailsReportClass theClass = null;

                    theClass            = new RMSMDetailsReportClass();

                    theClass            . setGIDate(common.parseNull(rs.getString(1)));
                    theClass            . setOrderNo(common.parseNull(rs.getString(2)));

                    theGIVector         . addElement(theClass);
               }

               rs                       . close();
               theStatement             . close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     private String getGIDetailsQS()
     {
          StringBuffer SB = new StringBuffer();

/*
          SB.append(" Select Max(Agn.GIDate) as GIDate,DyeingOrderDetails.OrderNo from DyeingOrderDetails ");
          SB.append(" left Join  AgnDetails on DyeingOrderDetails.DyeingOrderNo = AgnDetails.Orderno ");
          SB.append(" Left Join Agn  on AgnDetails.AgnNo = Agn.AgnNo ");
          SB.append(" Group by DyeingOrderDetails.OrderNo ");
          SB.append(" Order by 1 Desc ");
*/
          SB.append(" Select Max(Agn.GIDate) as GIDate,t.OrderNo,t.DyeingOrderNo from ");
          SB.append(" ( ");
          SB.append(" Select Max(DyeingOrderNo) as DyeingOrderNo,OrderNo from DyeingOrderDetails ");
          SB.append(" Group by OrderNo ");
          SB.append(" )t ");
          SB.append(" Inner Join  AgnDetails on t.DyeingOrderNo = AgnDetails.Orderno ");
          SB.append(" Inner Join Agn  on AgnDetails.AgnNo = Agn.AgnNo ");
          SB.append(" Group by t.OrderNo,t.DyeingOrderNo ");
          SB.append(" Order by 1 Desc ");

          System.out.println("SB-->"+SB);

          return SB.toString();
     }

     public void getReceiptDetails()
     {
          theReceiptVector              = new Vector();

          try
          {
               if(connect == null)
               {
                    theConnect          = JDBCConnection.getJDBCConnection();
                    connect             = theConnect.getConnection();
               }

               PreparedStatement theStatement   = connect.prepareStatement(getReceiptDetailsQS());
               ResultSet rs             = theStatement.executeQuery();

               while(rs.next())
               {
                    RMSMDetailsReportClass theClass = null;

                    theClass            = new RMSMDetailsReportClass();

                    theClass            . setGIDate(common.parseNull(rs.getString(1)));
                    theClass            . setOrderNo(common.parseNull(rs.getString(2)));

                    theReceiptVector    . addElement(theClass);
               }

               rs                       . close();
               theStatement             . close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     private String getReceiptDetailsQS()
     {
          System.out.println("En Date-----"+iEnDate);

          StringBuffer SB = new StringBuffer();

          SB.append(" Select Max(GIDate) as GIDate, OrdNo from ");
          SB.append(" ( ");
          SB.append(" Select Max(GIDate) as GIDate,GINO,AgnDetails.AgnNo,AgnDetails.BaseCode,DyerWeight,SMixIr.OrdNo from AgnDetails ");
          SB.append(" Inner join Agn on Agn.AgnNo=AgnDetails.AgnNo ");
          SB.append(" Inner Join Fibre on Fibre.FibreCode = AgnDetails.BaseCode ");
          SB.append(" Inner Join SBlenr on SBlenr.FibCd = Fibre.FibreCode ");
          SB.append(" Inner Join SMixir on SMixir.MixNo = SBlenr.MixNo ");
          SB.append(" where substr(SMixIr.MixingDate,0,6) >= "+common.getPreviousMonth(common.toInt(String.valueOf(iEnDate).substring(0,6))));  // 17.07.2015		  
          SB.append(" Group by GINo,AgnDetails.AgnNo,AgnDetails.BaseCode,DyerWeight,SMixIr.OrdNo ");
          SB.append(" Union All ");
          SB.append(" Select Max(AgnDate) as GIDate,GINO,PurchaseAgn.AgnNo,Fibre.FibreCode,0 as DyerWeight,SMixIr.OrdNo From PurchaseAgn ");
          SB.append(" Inner join Fibre on Fibre.FibreCode=PurchaseAgn.FibreCode ");
          SB.append(" Inner Join SBlenr on SBlenr.FibCd = Fibre.FibreCode ");
          SB.append(" Inner Join SMixir on SMixir.MixNo = SBlenr.MixNo ");
          SB.append(" where substr(SMixIr.MixingDate,0,6) >= "+common.getPreviousMonth(common.toInt(String.valueOf(iEnDate).substring(0,6))));  // 17.07.2015
          SB.append(" Group by AgnDate,GINO,PurchaseAgn.AgnNo,Fibre.FibreCode,SMixIr.OrdNo ");
          SB.append(" ) ");
          SB.append(" Group by OrdNo ");
          SB.append(" order by 1 Desc ");

          try
          {
               FileWriter FW = new FileWriter("/software/hoprint/AGNDetailsQS.txt");
               FW.write(SB.toString());
               FW.close();
          }
          catch(Exception ex){}

          return SB.toString();
     }

     private int getGIDate(String SOrderNo,String SSampleOrderNo,String SOrderDate)
     {
          for(int i=0;i<theGIVector.size();i++)
          {
               RMSMDetailsReportClass theClass = (RMSMDetailsReportClass)theGIVector.elementAt(i);

               if(SOrderNo.equals(theClass.getOrderNo()))
               {
                    int iFDDays    = common.getDateDiff(common.toInt(theClass.getGIDate()),common.toInt(SOrderDate));

                    System.out.println("Gate Inward----"+common.toInt(theClass.getGIDate()));

                    return iFDDays;
               }
          }

          for(int i=0;i<theReceiptVector.size();i++)
          {
               RMSMDetailsReportClass theClass = (RMSMDetailsReportClass)theReceiptVector.elementAt(i);

               if(SSampleOrderNo.equals(theClass.getOrderNo()))
               {
                    int iFDDays    = common.getDateDiff(common.toInt(theClass.getGIDate()),common.toInt(SOrderDate));

                    System.out.println("Receipt ----"+common.toInt(theClass.getGIDate()));

                    return iFDDays;
               }
          }

          return 0;
     }

     private void setDepthVect()
     {
          VDepthWt       =    new Vector();
          VDepthOrd      =    new Vector();
          VDepthShadeOrd =    new Vector();

          try
          {

               String QS =    " Select BleachedCotton,OrdNo,ShadeCardNo from "+
                              " ( "+
                              " select sum(mrat) as BleachedCotton,SMixir.OrdNo,shadecardNo.OrderNo as ShadeCardNo from sblenr "+
                              " Inner Join fibre on fibre.fibrecode = sblenr.fibcd "+
                              " Inner Join SMixir on SMixir.MixNo = SBlenr.MixNo "+
                              " Left  join ShadeCardNo on ShadeCardNo.RefOrderNo=SMixir.ORDNo"+
                              " where fibre.colorcode=561 "+
                              " Group by SMixir.OrdNo,ShadeCardNo.OrderNo "+
                              " Union All "+
                              " select sum(mrat) as BleachedCotton,RMixir.OrdNo,ShadeCardNo.OrderNo as ShadeCardNo from Rblenr "+
                              " Inner Join RMixir on RMixir.MixNo = RBlenr.MixNo "+
                              " Inner Join fibre on fibre.fibrecode = Rblenr.fibcd "+
                              " Left  join ShadeCardNo on ShadeCardNo.RefOrderNo=RMixir.ORDNo"+
                              " where fibre.colorcode=561 "+
                              " Group by RMixir.OrdNo,ShadeCardNo.OrderNo "+
                              " ) ";
                   
System.out.println("QS->"+QS);
                    if(connect == null)
                    {
                         theConnect          = JDBCConnection.getJDBCConnection();
                         connect             = theConnect.getConnection();
                    }

                    PreparedStatement stat      = connect.prepareStatement(QS);
                    ResultSet res       = stat.executeQuery();
                    while(res.next())
                    {
                        VDepthWt        .addElement(res.getString(1));
                        VDepthOrd       .addElement(res.getString(2));
                        VDepthShadeOrd  .addElement(common.parseNull(res.getString(3)));
                    }
                    res.close();
                    stat.close();
          }
          catch(Exception e)
          {
               System.out.println(e);
          }
     }

     private String getDepthVal(String SBasic)
     {
          for(int i=0;i<VDepthWt.size();i++)
          {
               if(((String)VDepthOrd.elementAt(i)).equals(SBasic) ||  ((String)VDepthShadeOrd.elementAt(i)).equals(SBasic))
               {
                    return ((String)VDepthWt.elementAt(i));
               }
          }
          return "";
     }

     private void setBlendData(String SROrderNo,int iBlendCode){

          vNewBlendPer   = new Vector();
          vNewBlendName  = new Vector();

          try {
               String SShadeCardNo = getShadeCardNo(SROrderNo);

               if(SShadeCardNo.length() > 0)
               {
                    SROrderNo = SShadeCardNo;
               }

                String sQry = "";

                if(iBlendCode==0) {

                          sQry =" select Sum(MRat),TypeName from ( "+
                                " select RBlenr.MRat,Fibre.ColorGroupCode,MaterialType.MatName,RawMaterialType.VeryShortName as TypeName,RawMaterialType.Typecode "+
                                " from RMixir "+
                                " Inner join RBlenr on Rblenr.MixNo=Rmixir.MixNo   and Rmixir.OrdNo='"+SROrderNo+"' and Rmixir.CorrectionMixing=0 "+
                                " Inner join Fibre on Fibre.Fibrecode=RBlenr.FibCd "+
                                " Inner join MaterialType on MaterialType.Matcode=Fibre.ColorGroupCode "+
                                " Inner join RaWMaterialType on RawMaterialType.TypeCode=MaterialType.MATERIALTYPECODEFORORDER "+
                                " Union All "+
                                " select RBlenr.MRat,MixLotMaster.ColorGroupCode,MaterialType.MatName, "+
                                " RawMaterialType.VeryShortName as TypeName,RawMaterialType.Typecode "+
                                " from Rmixir "+
                                " Inner join RBlenr on Rblenr.MixNo=Rmixir.MixNo  and  Rmixir.OrdNo='"+SROrderNo+"' and Rmixir.CorrectionMixing=0 "+
                                " Inner join MixLotMaster on MixLotMaster.Fibrecode=RBlenr.FibCd "+
                                " Inner join MaterialType on MaterialType.Matcode=MixLotMaster.ColorGroupCode "+
                                " Inner join RaWMaterialType on RawMaterialType.TypeCode=MaterialType.MATERIALTYPECODEFORORDER "+
                                " Union all "+
                                " select RBlenr.MRat,WasteCodeallocation.ColorGroupCode,MaterialType.MatName, "+
                                " RawMaterialType.VeryShortName as TypeName,RawMaterialType.Typecode "+
                                " from Rmixir "+
                                " Inner join RBlenr on Rblenr.MixNo=Rmixir.MixNo  and   Rmixir.OrdNo='"+SROrderNo+"' and Rmixir.CorrectionMixing=0 "+
                                " Inner join WasteCodeAllocation on WasteCodeAllocation.Fibrecode=RBlenr.FibCd "+
                                " Inner join MaterialType on MaterialType.Matcode=WasteCodeAllocation.ColorGroupCode "+
                                " Inner join RaWMaterialType on RawMaterialType.TypeCode=MaterialType.MATERIALTYPECODEFORORDER "+
                                " )Group by TypeName  "+
                                " Union All "+   
                                " select Sum(MRat),TypeName from ( "+
                                " select SBlenr.MRat,Fibre.ColorGroupCode,MaterialType.MatName,RawMaterialType.VeryShortName as TypeName,RawMaterialType.Typecode "+
                                " from SMixir "+
                                " Inner join SBlenr on SBlenr.MixNo=SMixir.MixNo   and SMixir.OrdNo='"+SROrderNo+"'  "+
                                " Inner join Fibre on Fibre.Fibrecode=SBlenr.FibCd "+
                                " Inner join MaterialType on MaterialType.Matcode=Fibre.ColorGroupCode "+
                                " Inner join RaWMaterialType on RawMaterialType.TypeCode=MaterialType.MATERIALTYPECODEFORORDER "+
                                " Union All "+
                                " select SBlenr.MRat,MixLotMaster.ColorGroupCode,MaterialType.MatName, "+
                                " RawMaterialType.VeryShortName as TypeName,RawMaterialType.Typecode "+
                                " from SMixir "+
                                " Inner join SBlenr on SBlenr.MixNo=SMixir.MixNo  and  SMixir.OrdNo='"+SROrderNo+"'  "+
                                " Inner join MixLotMaster on MixLotMaster.Fibrecode=SBlenr.FibCd "+
                                " Inner join MaterialType on MaterialType.Matcode=MixLotMaster.ColorGroupCode "+
                                " Inner join RaWMaterialType on RawMaterialType.TypeCode=MaterialType.MATERIALTYPECODEFORORDER "+
                                " Union all "+
                                " select SBlenr.MRat,WasteCodeallocation.ColorGroupCode,MaterialType.MatName, "+
                                " RawMaterialType.VeryShortName as TypeName,RawMaterialType.Typecode "+
                                " from SMixir "+
                                " Inner join SBlenr on SBlenr.MixNo=SMixir.MixNo  and   SMixir.OrdNo='"+SROrderNo+"'  "+
                                " Inner join WasteCodeAllocation on WasteCodeAllocation.Fibrecode=SBlenr.FibCd "+
                                " Inner join MaterialType on MaterialType.Matcode=WasteCodeAllocation.ColorGroupCode "+
                                " Inner join RaWMaterialType on RawMaterialType.TypeCode=MaterialType.MATERIALTYPECODEFORORDER "+
                                " )Group by TypeName  Order by 2 ";
                }

                if(iBlendCode==1) {

                    sQry = " select Regularorderdetails.per,RawMaterialType.VeryShortName from regularorder "+
                           " Inner join Regularorderdetails on regularorderdetails.orderid = regularorder.OrdeRId "+
                           " Inner join RawMaterialType on RawMaterialType.TypeCode=regularorderdetails.typecode "+
                           " where RegularORder.rorderno='"+SROrderNo+"' ";
                }
System.out.println("sQry-------->"+sQry);
               if(connect == null)
               {
                    theConnect          = JDBCConnection.getJDBCConnection();
                    connect             = theConnect.getConnection();
               }

               PreparedStatement theStatement   = connect.prepareStatement(sQry);
               ResultSet theResult      = theStatement.executeQuery();

               while(theResult.next()) {

                    vNewBlendPer   . addElement(common.parseNull(theResult.getString(1)));
                    vNewBlendName  . addElement(common.parseNull(theResult.getString(2)));
               }
               theResult      . close();
               theStatement   . close();

          } catch(Exception ex) {

               ex.printStackTrace();
          }
     }

     private String getShadeCardNo(String SROrderNo)
     {
          try
          {
               String SShadeCardNo      = "";

               if(connect == null)
               {
                    theConnect          = JDBCConnection.getJDBCConnection();
                    connect             = theConnect.getConnection();
               }

               PreparedStatement thePS  = connect.prepareStatement(" Select RefOrderNo from ShadeCardNo where OrderNo = '"+SROrderNo+"' ");
               ResultSet rs             = thePS.executeQuery();

               if(rs.next())
               {
                    SShadeCardNo        = common.parseNull(rs.getString(1));
               }

               rs                       . close();
               thePS                    . close();

               return SShadeCardNo;
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
               return "";
          }
     }

     private void getFDDelayDays(String SRMOrderNo, String SOrderDate, String SRegOrderNo)
     {
          SFDDelayDays             = "0";
          SSampleDays              = "0";

          int iFDDays              = 0;
          int iSampleDays          = 0;

          if(SRMOrderNo.length() == 0)
          {
               SFDDelayDays        = "0";
               SSampleDays         = "0";
               return;
          }

          PreparedStatement thePS  = null;
          ResultSet rs             = null;

          try
          {
               if(theSCMConnection == null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theSCMConnection    = jdbc.getConnection();
               }

               // get Max Mix No..

               int iMaxMixNo            = 0;

               String QS = " Select Max(MixNo) from SMIXIR where OrdNo like '"+SRMOrderNo+"%' ";

               thePS                    = theSCMConnection.prepareStatement(QS);
               rs                       = thePS.executeQuery();

               if(rs.next())
               {
                    iMaxMixNo           = rs.getInt(1);
               }

               rs                       . close();
               thePS                    . close();

               if(iMaxMixNo > 0)
               {
                    // get Last Fibre Date..

                    String SMaxReceiptDate   = "";

                    ArrayList ARecFibreCode  = null, ARecReceiptDate = null;
                    ArrayList AOrdFibreCode  = null;

                    ARecFibreCode            = new ArrayList();
                    ARecReceiptDate          = new ArrayList();
                    AOrdFibreCode            = new ArrayList();

                    ARecFibreCode            . clear();
                    ARecReceiptDate          . clear();
                    AOrdFibreCode            . clear();
     
                    QS = null;

//                    QS = "Select FibreCode, Max(GodownDate) as ReceiptDate from Receipt where FibreCode in (Select FibCd from SBlenr where MixNo = "+iMaxMixNo+") Group by FibreCode Order by 2 desc ";

                    QS = " Select Fibre.BaseCode, Max(Receipt.GodownDate) as ReceiptDate "+
                         " from Receipt "+
                         " Inner Join Fibre on Fibre.FibreCode = Receipt.FibreCode "+
                         " where Receipt.FibreCode in (Select FibCd from SBlenr where MixNo = "+iMaxMixNo+") "+
                         " and Receipt.GodownDate>="+SOrderDate+

                         " and Fibre.AutoEntryStatus = 1 "+

                         " Group by Fibre.BaseCode "+
                         " Order by 2 desc ";
System.out.println("QS----------->"+QS);
                    thePS     = theSCMConnection.prepareStatement(QS);
                    rs        = thePS.executeQuery();

                    while(rs.next())
                    {
                         if(SMaxReceiptDate.length() == 0)
                         {
                              SMaxReceiptDate= common.parseNull(rs.getString(2));
                         }

                         ARecFibreCode       . add(common.parseNull(rs.getString(1)));
                         ARecReceiptDate     . add(common.parseNull(rs.getString(2)));
                    }

                    rs        . close();
                    thePS     . close();

                    // get Order FibreCode..

                    for(int i=0; i<VFibreCode.size(); i++)
                    {
                         if(SRegOrderNo.equals(common.parseNull((String)VDyeOrderNo.elementAt(i))))
                         {
                              AOrdFibreCode  . add(common.parseNull((String)VFibreCode.elementAt(i)));
                         }
                    }

                    // check..

                    boolean bFlag       = true;

//                    boolean bFlag       = false;
                    int iStatus         = 0;

                    for(int i=0; i<AOrdFibreCode.size(); i++)
                    {
                         int iIndex     = ARecFibreCode.indexOf((String)AOrdFibreCode.get(i));

                         if(iIndex == -1)
                         {
                              bFlag     = false;
                         }

                         iStatus        = 1;

                         FText.write("index. "+SRegOrderNo+" : "+iIndex+" : "+AOrdFibreCode.get(i)+" : \n");
                    }


                    // get Difference..

                    if(bFlag && iStatus == 1)
                    {
                         iFDDays        = common.getDateDiff(common.toInt(SMaxReceiptDate), common.toInt(SOrderDate));
                         iSampleDays    = common.getDateDiff(iEnDate, common.toInt(SMaxReceiptDate));

                         FText.write("1. "+SRMOrderNo+" : "+SRegOrderNo+" : "+bFlag+" : "+iStatus+" : "+AOrdFibreCode.size()+","+iEnDate+","+SMaxReceiptDate+" : "+iSampleDays+"\n");
                    }

                    if((iFDDays <= 0 && AOrdFibreCode.size() == 0) || (getFDStatus(SRegOrderNo) == 1))
                    {                   
                         if(AOrdFibreCode.size()==0)
                         {
                              iFDDays        = 0;
                              iSampleDays    = common.getDateDiff(iEnDate, common.toInt(SOrderDate));
                         }
                         else
                         {
                              iSampleDays    = common.getDateDiff(iEnDate, common.toInt(SMaxReceiptDate));
                         }


                         FText.write("2. "+SRMOrderNo+" : "+SRegOrderNo+" : "+bFlag+" : "+iStatus+" : "+AOrdFibreCode.size()+","+iEnDate+","+SMaxReceiptDate+" : "+iSampleDays+"\n");
                    }

                    if(iFDDays <= 0 && AOrdFibreCode.size() > 0)
                    {
                         iFDDays        = common.getDateDiff(iEnDate, common.toInt(SOrderDate));
                    }

                    if(iSampleDays <= 0)
                    {
                         iSampleDays    = 0;
                    }

                    if(iFDDays > 0)
                    {
                         iFDDays       += 1; 
                    }
                    if(iSampleDays > 0 && iFDDays == 0)
                    {
                        iSampleDays   += 1; 
                    }

                    SFDDelayDays        = String.valueOf(iFDDays);
                    SSampleDays         = String.valueOf(iSampleDays);
               }

//               FText.write(SRMOrderNo+" : "+SOrderDate+" : "+SRegOrderNo+" : "+SFDDelayDays+" : "+SSampleDays+"\n");
          }
          catch(Exception ex)
          {
               try
               {
                    rs.close();
                    thePS.close();
               }catch(Exception e){}

               ex.printStackTrace();

               SFDDelayDays             = "ERROR";
               SSampleDays              = "ERROR";
          }
     }

     private String getLastProcessType(String SPartyCode, String SRefOrderNo, String SOrderNo)
     {
          if(SRefOrderNo.equals("DIRECT"))
          {
               return "";
          }

          StringBuffer SB = null;
          SB = new StringBuffer();

          SB.append(" Select ProcessType from ProcessingType ");
          SB.append(" where ProcessCode =  ");
          SB.append(" ( ");
          SB.append(" Select ProcessTypeCode from RegularOrder where OrderId =  ");
          SB.append(" ( ");
          SB.append(" Select Max(OrderId) from ");
          SB.append(" ( ");
          SB.append(" Select Max(OrderId) as OrderId from RegularOrder ");
          SB.append(" where PartyCode = '"+SPartyCode+"' ");
          SB.append(" and RefOrderNo = '"+SRefOrderNo+"' ");
          SB.append(" and ROrderNo <> '"+SOrderNo+"' ");
          SB.append(" Union All ");
          SB.append(" Select Max(OrderId) as OrderId from RegularOrder ");
          SB.append(" where PartyCode = '"+SPartyCode+"' ");
          SB.append(" and ROrderNo = '"+SRefOrderNo+"' ");
          SB.append(" and ROrderNo <> '"+SOrderNo+"' ");
          SB.append(" Order by 1 desc ");
          SB.append(" ) ");
          SB.append(" ) ");
          SB.append(" ) ");

          String SProcessType           = "";
          System.out.println("SB---->"+SB.toString());
          try
          {
               if(theSCMConnection == null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theSCMConnection    = jdbc.getConnection();
               }

               PreparedStatement thePS  = theSCMConnection.prepareStatement(SB.toString());
               ResultSet rs             = thePS.executeQuery();

               if(rs.next())
               {
                    SProcessType        = common.parseNull(rs.getString(1));
               }

               rs                       . close();
               thePS                    . close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
               SProcessType = "Error";
          }

          return SProcessType;
     }

  private void setShortageHtml(PrintWriter out) throws Exception
     {
		System.out.println("Enter into this ");
		out.println("<html>");
		out.println("<head>");
		out.println("<title>InvPro</title>");
		out.println("</head>");
		out.println("<body bgcolor='"+bgColor+"' text = '#0000FF'>");
                 out.println("<p align='center'><font size='5'><b><u>Reference order Shortage</u></p> ");
		out.println("<div align='center'>");
		 out.println("<table border='1' width='100%' borderColor='lightgreen'>");
                // out.println("<tr style='font-weight: bold;'>");
		out.println("    <tr>");
		out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>SL.No</b></font></td>");
		out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>OrderDate</b></font></td>");
		out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>OrderNo</b></font></td>");
		out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>PartyName</b></font></td>");
		out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>OrderWeight</b></font></td>");
		out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Count</b></font></td>");
		out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Unitname</b></font></td>");
		out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>Rep Name</b></font></td>");
		out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>PACKEDWEIGHT</b></font></td>");
		out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>EXSHQTY</b></font></td>");
		out.println("      <td   align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><b>EXSHPER</b></font></td>");

		out.println("    </tr>");
              // out.println("    </tr>");
              setShortageBody(out);

           }

private void  setShortageBody(PrintWriter out) throws Exception

{
         for (int i = 0; i < ShortageList.size(); i++) 
            {

                java.util.HashMap theMap = (java.util.HashMap) ShortageList.get(i);

                out.println("    <tr>");
                out.println("    <td   align='center'><font color='#000099' ><b>" + String.valueOf((i+1)) + "</b></font></td>");
                out.println("    <td   align='right'><font color='#000099' ><b>" + common.parseDate((String) theMap.get("ORDERDATE")) + "</b></font></td>");
                out.println("    <td   align='left'><font color='#000099' ><b>" + common.parseNull((String) theMap.get("RORDERNO"))+ "</b></font></td>");
                out.println("    <td   align='right'><font color='#000099' ><b>" +common.parseNull((String) theMap.get("PARTYNAME")) + "</b></font></td>");
                out.println("    <td   align='right'><font color='#000099' ><b>" +common.parseNull((String) theMap.get("ORDERWEIGHT"))+ "</b></font></td>");
                out.println("    <td   align='center'><font color='#000099' ><b>" + common.parseNull((String) theMap.get("COUNTNAME")) + "</b></font></td>");
                out.println("    <td   align='right'><font color='#000099' ><b>" + common.parseNull((String) theMap.get("UNITNAME")) + "</b></font></td>");
                out.println("    <td   align='left'><font color='#000099' ><b>" + common.parseNull((String) theMap.get("REPNAME"))+ "</b></font></td>");
                out.println("    <td   align='right'><font color='#000099' ><b>" +common.parseNull((String) theMap.get("PACKEDWEIGHT")) + "</b></font></td>");
                out.println("    <td   align='right'><font color='#000099' ><b>" +common.parseNull((String) theMap.get("EXSHQTY"))+ "</b></font></td>");
                out.println("    <td   align='right'><font color='#000099' ><b>" +common.parseNull((String) theMap.get("EXSHPER"))+ "</b></font></td>");
              

            }//ShortageList.clear();
 out.println("    </tr>");
		out.println("  </table>");
		out.println("</div>");
		out.println("");
		out.println("</body>");
		out.println("");
		out.println("</html>");  
 }

public void getShortageQS()
     {

      PreparedStatement pst = null;
        ResultSet rst = null;
      
          StringBuffer SB = new StringBuffer();

          SB.append(" Select ROrderNo, OrderDate, PartyName, CountName,  ");
   SB.append(" OrderWeight, UnitName, RepName, PackedWeight,    ");
   SB.append(" (PackedWeight - OrderWeight) as ExShQty,   ");
   SB.append(" Round(((PackedWeight - OrderWeight) / OrderWeight) * 100, 2) as ExShPer from   ");
   SB.append(" (   ");
   SB.append(" Select RegularOrder.ROrderNo, RegularOrder.OrderDate, PartyMaster.PartyName,   ");
   SB.append(" YarnCount.CountName, RegularOrder.Weight as OrderWeight,   ");
   SB.append(" Sum(BalePacking.NetWeight) as PackedWeight, Representative.Name as RepName,    ");
   SB.append(" nvl(Representative.UserId, 0) as UserCode, Unit.UnitName   ");
   SB.append(" from PackingStatus   ");
   SB.append(" Inner Join RegularOrder On RegularOrder.ROrderNo = PackingStatus.OrderNO    ");
   SB.append(" Inner Join BalePacking On BalePacking.OrderId = RegularORder.OrderId    ");
   SB.append(" and BalePacking.Flag <> 1    ");
   SB.append(" Inner Join PartyMaster On PartyMaster.PartyCode = RegularOrder.PartyCode    ");
   SB.append(" Inner Join YarnCount On YarnCount.CountCode = RegularORder.CountCode    ");
   SB.append(" Inner Join Unit on Unit.UnitCode = RegularOrder.UnitCode    ");
   SB.append(" Left Join Representative on Representative.Code = RegularOrder.RepresentativeCode    ");
   SB.append(" where RegularOrder.ROrderNo in    ");
   SB.append(" (    ");
   SB.append(" Select RegularOrder.RefOrderNo from RegularOrder    ");
   SB.append(" Left Join SampleToRegularAuth on SampleToRegularAuth.RegularOrderNo = RegularOrder.ROrderNo   ");
   SB.append(" where RegularOrder.Status = 1    ");
   SB.append(" and RegularOrder.OrderDate >= 20061001   ");
   SB.append(" and RegularOrder.Shortage <> 2   ");
   SB.append(" and RegularOrder.ROrderNo not like 'T%'   ");
   SB.append(" and RegularOrder.ROrderNo not like 'PR%'    ");
   SB.append(" and nvl(SampleToRegularAuth.Status, 0) = 0    ");
   SB.append(" and RegularOrder.Shortage = 1    ");
   SB.append(" )    ");
   SB.append(" Group by    ");
   SB.append(" RegularOrder.ROrderNo, RegularOrder.OrderDate, PartyMaster.PartyName,    ");
   SB.append(" YarnCount.CountName, RegularOrder.Weight, Representative.Name, nvl(Representative.UserId, 0), Unit.UnitName    ");
   SB.append(" )    ");
   SB.append(" Order by 1    ");
            System.out.println("SB->"+SB.toString());
          try {

            /*
             * if (theConnection == null) { JDBCConnection jdbc =
             * JDBCConnection.getJDBCConnection(); theConnection =
             * jdbc.getConnection(); }
             */
            Class.forName("oracle.jdbc.OracleDriver");
            theConnection = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun", "scm", "rawmat");
            pst = theConnection.prepareStatement(SB.toString());
            rst = pst.executeQuery();
            ResultSetMetaData rsmd = rst.getMetaData();
            while (rst.next()) {
                HashMap themap = new HashMap();
                for (int i = 0; i < rsmd.getColumnCount(); i++) {
                    themap.put(rsmd.getColumnName(i + 1), rst.getString(i + 1));
                }
                ShortageList.add(themap);
                System.out.println("ShortageList===>" + ShortageList.size());
            }
            rst.close();

            pst.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
     }


private void createPDF(String SFileWritePath){
try{
	    double	dCountwiseTotal=0.00 , dFormwiseTotal=0.00, dGrandTotal=0.00;

            document                = new Document();
            PdfWriter               . getInstance(document, new FileOutputStream(SFileWritePath));
            document                . setPageSize(PageSize.A4.rotate());
            document                . open();

            int iTotalColumns       = VOrderCount.size()+2;
	     iColWidth 		    = new int[iTotalColumns];
             iColWidth[0] 	    = 35;
	    for (int j = 0; j < VOrderCount.size(); j++) {
               iColWidth[j+1] 	    = 15;
             }
             int iNewColSize        = 1+VOrderCount.size();

             iColWidth[iNewColSize+0] = 20;
             
             table                        =  new PdfPTable(iTotalColumns);
             table                        . setWidths(iColWidth);
             table                        . setWidthPercentage(100f);

	      AddCellIntoTable("FIBREFORM WISE YARN COUNT ABSTRACT DATED "+SToday, table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, iTotalColumns , 0, 0, 0, 0, bigbold,30f);

	      AddCellIntoTable("FIBREFORM NAME", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallbold1,30f);
	     for(int a=0;a<VOrderCount.size();a++){
	      AddCellIntoTable(common.parseNull((String)VOrderCount.elementAt(a)), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallbold1,30f);
	    }
	      AddCellIntoTable("FORMWISE TOTAL", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallbold1,30f);

	for(int i=0;i<VFibreFormGroupName.size();i++) {
	    String 	sGroupCode 	= (String)VFibreFormGroupCode.get(i);
	    AddCellIntoTable(common.parseNull((String)VFibreFormGroupName.elementAt(i)), table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallbold1,30f);
	for(int j=0;j<VOrderCount.size();j++){
	 	String	  SCount	= 	 (String)VOrderCount.get(j);
		double	  dOrderQty	= 	 getOrderQty (SCount,sGroupCode);			 
	    AddCellIntoTable(common.parseNull((String.valueOf(dOrderQty))), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal,30f);		 
		dFormwiseTotal		= 	 dFormwiseTotal + dOrderQty;
		dOrderQty		= 	 0.00;
	 }
	    AddCellIntoTable(common.parseNull((String.valueOf(dFormwiseTotal))), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallnormal,30f);		 
	dGrandTotal			= 	dGrandTotal+dFormwiseTotal;
	dFormwiseTotal			= 	0.00;
}
	   AddCellIntoTable("COUNTWISE TOTAL", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallbold1,30f);		 

	for(int j=0;j<VOrderCount.size();j++){
 	String	  SCount		= 	 (String)VOrderCount.get(j);
	dCountwiseTotal			= 	 getCountwiseTotal(SCount);	
	    AddCellIntoTable(common.parseNull((String.valueOf(dCountwiseTotal))), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallbold1,30f);		 
	}
	    AddCellIntoTable(common.parseNull((String.valueOf(dGrandTotal))), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1 , 4, 1, 8, 2, smallbold1,30f);		 
	dCountwiseTotal = 0.00;


	      document.add(table);
              document.close();

	
  }
 catch(Exception ex){
	ex.printStackTrace();
 }

}

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont,float fHeight) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }


    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont,float fHeight,float fBorderRight) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorderWidthRight(fBorderRight);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(Paragraph p1, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(p1);
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddColumnIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, float fHeight, int iColWidth, int iLeftBorder, int iTopBorder, int iRightBorder, Font DocFont, int iBottomBorder) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColWidth);
        // c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }
}
